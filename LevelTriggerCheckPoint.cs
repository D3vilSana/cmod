﻿using System;
using UnityEngine;

public class LevelTriggerCheckPoint : MonoBehaviour
{
    public void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            if (other.gameObject.GetComponent<HERO>().photonView.isMine)
            {
                Levels.Attributes.Checkpoint r = FengGameManagerMKII.level.GetAttribute<Levels.Attributes.Checkpoint>();
                if (r != null)
                {
                    r.respawn = base.gameObject.transform.position;
                    FengGameManagerMKII.instance.chat.addLINE("<color=#00ff00>Checkpoint ! <b>(*•̀ᴗ•́*)و ̑̑ </b></color>");
                }
            }
        }
    }
    /*
    private void OnTriggerStay(Collider other)
    {
       
        if (other.gameObject.tag == "Player")
        {
            if (other.gameObject.GetComponent<HERO>().photonView.isMine)
            {
                Levels.Attributes.Respawn r = FengGameManagerMKII.level.GetAttribute<Levels.Attributes.Respawn>();
                if (r!=null)
                {
                    r.respawn = base.gameObject.transform.position;
                }
            }
        }
    }
*/

    private void Start()
    {
    }
}

