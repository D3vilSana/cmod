﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Parameters;

public abstract class ChatCommand : IEquatable<ChatCommand>
{
    public string name { get; internal set; }
    public virtual bool enabled { get; internal set; }
    public bool Equals(ChatCommand other)
    {
        return other.name == name;
    }
    public abstract bool canbeme(string param);
    public abstract bool isme(string param);
    public abstract List<string> getpossibleparameters(string command);
    public abstract string show();
    public abstract void exec(string command);

}

public class Command : ChatCommand
{
    protected Parameter[] parameters;
    protected Action<string[]> act;
    public override bool enabled { get { return true; } internal set { } }
    public Command(string name , Action<string[]> action,params Parameter[] p)
    {
        parameters = p;
        act = action;
        this.name = name;
    } 

    public override bool canbeme(string param)
    {
        string[] p = param.Split(new char[1]{' '},parameters.Length);
        for (int i = 0; i < p.Length-1; i++)
        {
            if (!parameters[i].isme(p[i]))
            {
                return false;
            }
        }
        if (!parameters[p.Length-1].canbeme(p[p.Length-1]))
        {
            return false;
        }
        return true;
    }
    public override bool isme(string param)
    {
        string[] p = param.Split(new char[1] { ' ' }, parameters.Length);
        for (int i = 0; i < p.Length; i++)
        {
            if (!parameters[i].isme(p[i]))
            {
                return false;
            }
        }
        return true;
    }

    public override List<string> getpossibleparameters(string command)
    {
        if (!canbeme(command))
        {
            return new List<string>();
        }
        string[] p = command.Split(new char[1] { ' ' }, parameters.Length);
        List<string> t = parameters[p.Length - 1].getpossibleparameters(p[p.Length - 1]);
        if (t.Count==0)
        {
            t.Add(parameters[p.Length - 1].sringshow());
        }
        return t;
    }

    public override string show()
    {
        string a = name;
        foreach (Parameter p in parameters)
        {
            a += " " + p.sringshow();
        }
        return a;
    }

    public override void exec(string command)
    {
        act.Invoke(command.Split(' '));
    }

    public bool Equals(Command other)
    {
        return name ==other.name;
    }
}

public class Command<T> : Command
{
    Parameter<T> p;
    Action<T> a;
    public Command(string name,Action<T> action, Parameter<T> p):base(name,delegate(string[] a) { },p)
    {
        this.p = p;
        this.a = action;
    }

    public override void exec(string command)
    {
        T p1 = p.Parse(command);
        a.Invoke(p1);
    }
}
public class Command<T,U> : Command
{
    Parameter<T> a;
    Parameter<U> b;
    Action<T, U> ac;
    public Command(string name,Action<T,U> action,Parameter<T> p1, Parameter<U> p2) : base(name,delegate(string[] a) { },p1,p2)
    {
        this.a = p1;
        this.b = p2;
        ac = action;
    }
    public override void exec(string command)
    {
        string[] s = command.Split(new char[] { ' ' }, 2);
        T p1 = a.Parse(s[0]);
        U p2 = b.Parse(s[1]);
        ac.Invoke(p1, p2);
    }
}

public class Command<T,U,V> : Command
{
    Parameter<T> a;
    Parameter<U> b;
    Parameter<V> c;
    Action<T, U,V> ac;
    public Command(string name,Action<T,U,V> action,Parameter<T> p1, Parameter<U> p2,Parameter<V> p3) : base(name,delegate(string[] a) { },p1,p2, p3)
    {
        this.a = p1;
        this.b = p2;
        this.c = p3;
        ac = action;
    }
    public override void exec(string command)
    {
        string[] s = command.Split(new char[] { ' ' }, 3);
        T p1 = a.Parse(s[0]);
        U p2 = b.Parse(s[1]);
        V p3 = c.Parse(s[2]);
        ac.Invoke(p1, p2,p3);
    }
}
public class MasterCommand : Command
{
    public MasterCommand(string name, Action<string[]> action, params Parameter[] p) : base(name, action, p)
    {

    }
    public override bool enabled
    { get { return PhotonNetwork.isMasterClient; } internal set { } }
    public override bool canbeme(string param)
    {
        return PhotonNetwork.isMasterClient && base.canbeme(param);
    }
    public override bool isme(string param)
    {
        return PhotonNetwork.isMasterClient && base.isme(param);
    }
}
public class MasterCommand<T> : MasterCommand
{
    private Action<T> a;
    private Parameter<T> p;
    public MasterCommand(string name,Action<T> a,Parameter<T> p1) : base(name,delegate(string[] g) { }, p1)
    {
        this.a = a;
        this.p = p1;
    }
    public override void exec(string command)
    {
        T p1 = p.Parse(command);
        a.Invoke(p1);
    }
}

public class MasterCommand<T,U> : MasterCommand
{
    private Action<T,U> a;
    private Parameter<T> p;
    private Parameter<U> u;
    public MasterCommand(string name,Action<T,U> a,Parameter<T> p1,Parameter<U> p2) : base(name,delegate(string[] g){ }, p1,p2)
    {
        this.a = a;
        this.p = p1;
        this.u = p2;
    }
    public override void exec(string command)
    {
        string[] s = command.Split(new char[]{' '}, 2);
        T p1 = p.Parse(s[0]);
        U p2 = u.Parse(s[1]);
        a.Invoke(p1,p2);
    }
}
public class MasterCommand<T,U,V> : MasterCommand
{
    private Action<T, U, V> a;
    private Parameter<T> t;
    private Parameter<U> u;
    private Parameter<V> v;

    public MasterCommand(string name,Action<T,U,V> a,Parameter<T> t,Parameter<U> u,Parameter<V> v) : base(name,delegate(string[] g) { },t,u, v)
    {
        this.t = t;
        this.u = u;
        this.v = v;
        this.a = a;
    }
}
   


