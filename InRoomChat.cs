﻿using Photon;
using System;
using System.Collections.Generic;
using UnityEngine;

public class InRoomChat : Photon.MonoBehaviour
{
    private bool AlignBottom = true;
    public static readonly string ChatRPC = "Chat";

    public Dictionary<string, string> Remplaces = new Dictionary<string, string>();

    public static Rect TextRect = new Rect(0f, 30f, 300f, 470f);
    public static Rect TextInputRect = new Rect(30f, 575f, 300f, 25f);
    public static Rect LargeTextInput = new Rect(30, 575, 800, 25);
    public static Rect HelpInput = new Rect();
    public static Rect ColorHelp = new Rect();

    private string inputLine = string.Empty;
    public bool IsVisible = true;
    //public static List<string> messages = new List<string>();
    private Vector2 scrollPos = Vector2.zero;
    public string ChatName;
    public FengGameManagerMKII GameManager;
    protected bool gotoend = false;

    private Color _textcolor = Color.white;
    private Texture2D Colortext = new Texture2D(1, 1, TextureFormat.ARGB32,false);
    public Color textcolor { get { return _textcolor; }set
        {
            Colortext.SetPixel(0, 0, _textcolor);
            Colortext.Apply();
            _textcolor = value; } }

    public ChatHandler ch = new ChatHandler(Screen.height - 355);

    public static FileManager.Datadirectory chatdir = new FileManager.Datadirectory("chat");


    public void addLINE(string newLine)
    {
        //Testing the new chat system
        ch.add_msg(newLine);
        //messages.Add(newLine);
    }
    public void clear()
    {
        //messages.Clear();
        ch.clear();
    }
    public void OnGUI()
    {
        if (this.IsVisible && (PhotonNetwork.connectionStateDetailed == PeerState.Joined))
        {
            if ((Event.current.type == EventType.KeyDown) && ((Event.current.keyCode == KeyCode.KeypadEnter) || (Event.current.keyCode == KeyCode.Return)))
            {
                if (!string.IsNullOrEmpty(this.inputLine))
                {
                    if (this.inputLine == "\t")
                    {
                        this.inputLine = string.Empty;
                        GUI.FocusControl(string.Empty);
                        return;
                    }
                    #region dictionary remplace
                    foreach (KeyValuePair<string, string> item in Remplaces)
                    {
                        inputLine = System.Text.RegularExpressions.Regex.Replace(inputLine, item.Key, item.Value);
                    }
                    #endregion
                    
                    if (!CommandManager.parse(inputLine))
                    {
                        if (CModUtilities.validcolor(inputLine))
                        {
                            inputLine = "<color=" + textcolor.tohex() + ">" + inputLine.Substring(7) + "</color>";
                        }
                        FengGameManagerMKII.instance.photonView.RPC("Chat", PhotonTargets.All, inputLine,((string)PhotonNetwork.player.customProperties[PhotonPlayerProperty.name]).hexColor());
                    }

                    this.inputLine = string.Empty;
                    GUI.FocusControl(string.Empty);
                    return;
                }
                this.inputLine = "\t";
                GUI.FocusControl("ChatInput");
            }
            if (Event.current.type==EventType.keyDown && Event.current.keyCode == KeyCode.Tab&&inputLine.StartsWith("/"))
            {
                //Autocompletion
                string input = inputLine;
                if (inputLine.Contains(" "))
                {

                    int strpos = inputLine.LastIndexOf(' ');
                    inputLine = inputLine.Substring(0, strpos);

                    inputLine += " " + CommandManager.getcompletion(input)+" ";
                }
                else
                {
                    inputLine = "/" + CommandManager.getcompletion(input)+" ";
                }
                gotoend = true;
            }

            CommandManager.autocompupdate(inputLine);

            ch.GUIUpdate();


            #region %ID remplace
            //Remplace %ID by ID's name !
            foreach (var P in PhotonNetwork.playerList)
            {
                if (!KickBanManager.isignored(P.ID))
                {
                    if (inputLine.Contains("%" + P.ID + " "))
                    {
                        inputLine = inputLine.Replace("%" + P.ID + " ", RCextensions.returnStringFromObject(P.customProperties[PhotonPlayerProperty.name]).hexColor()+" ");
                        gotoend = true;
                    }

                }
            }
            #endregion
            //Remplaces dictionarry 

            GUI.DrawTexture(ColorHelp, Colortext);

            #region #color remplace
            if (inputLine.StartsWith("#") && inputLine.Length >= 7 && CModUtilities.validcolor(inputLine))
            {
                   textcolor = CModUtilities.colorfromhex(inputLine);
            }
            else
            {
                textcolor = new Color(255, 255, 255);//Color.white don't work here
            }

            #endregion
            GUI.Label(HelpInput, CommandManager.hints);

            

            if (inputLine!="")
            {
                GUILayout.BeginArea(LargeTextInput);
            }
            else
            {
                GUILayout.BeginArea(TextInputRect);
            }
            
            GUILayout.BeginHorizontal(new GUILayoutOption[0]);
            
            GUI.SetNextControlName("ChatInput");
            this.inputLine = GUILayout.TextField(this.inputLine);

            if (gotoend)
            {

                TextEditor editor = (TextEditor)GUIUtility.GetStateObject(typeof(TextEditor), GUIUtility.keyboardControl);
                editor.MoveTextEnd();
                gotoend = false;
            }

            GUILayout.EndHorizontal();
            GUILayout.EndArea();
        }
    }

    public void setPosition()
    {
        if (this.AlignBottom)
        {
            //GuiRect = new Rect(0f, (float) (Screen.height - 500), 300f, 470f);
            TextRect = new Rect(0f, 30f, ChatHandler.xsize, (Screen.height - 355) + 0x113);
            TextInputRect = new Rect(30f, (float) ((Screen.height - 300) + 0x113), 300f, 25f);
            LargeTextInput = new Rect(30f, (float)((Screen.height - 300) + 0x113), 800, 25f);
            HelpInput = new Rect(30, ((Screen.height - 330) + 0x113), Screen.width-30, 25);
            ColorHelp = new Rect(0, (float)((Screen.height - 300) + 0x113), 25, 20);
        }
        ch.down = (Screen.height - 355) + 0x113;
    }

    public void Start()
    {
        FengGameManagerMKII.instance.chat = this;
        this.setPosition();
        //TODO : already exists
        FileManager.Datadirectory d = new FileManager.Datadirectory("chat", "conf");
        FileManager.DataFile  f = d.getfile("aliases");
        var j = f.json;

        foreach (SimpleJSON.JSONNode n in j.Childs)
        {
            Remplaces.Add(n["from"].Value, n["to"].Value);
        }

        Colortext.Apply();
        textcolor = Color.white;
    }
}

