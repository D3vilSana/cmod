using System;
using UnityEngine;

public class TITAN_CONTROLLER : MonoBehaviour
{
    public Camera currentCamera;
    public bool isAttackDown;
    public bool isAttackIIDown;
    public bool isJumpDown;
    public bool isSuicide;
    public bool isWALKDown;
    public float targetDirection;

    private void Start()
    {
        this.currentCamera = GameObject.Find("MainCamera").GetComponent<Camera>();
    }

    private void Update()
    {
        int num;
        int num2;
        if (Managers.InputManager.key(InputCode.Keys.up).keypressed)
        {
            num = 1;
        }
        else if (Managers.InputManager.key(InputCode.Keys.down).keypressed)
        {
            num = -1;
        }
        else
        {
            num = 0;
        }
        if (Managers.InputManager.key(InputCode.Keys.left).keypressed)
        {
            num2 = -1;
        }
        else if (Managers.InputManager.key(InputCode.Keys.right).keypressed)
        {
            num2 = 1;
        }
        else
        {
            num2 = 0;
        }
        if ((num2 != 0) || (num != 0))
        {
            float y = this.currentCamera.transform.rotation.eulerAngles.y;
            float num5 = Mathf.Atan2((float) num, (float) num2) * 57.29578f;
            num5 = -num5 + 90f;
            float num3 = y + num5;
            this.targetDirection = num3;
        }
        else
        {
            this.targetDirection = -874f;
        }
        this.isAttackDown = false;
        this.isJumpDown = false;
        this.isAttackIIDown = false;
        this.isSuicide = false;
        if (Managers.InputManager.key(InputCode.Keys.attack0).keydown)
        {
            this.isAttackDown = true;
        }
        if (Managers.InputManager.key(InputCode.Keys.attack1).keydown)
        {
            this.isAttackIIDown = true;
        }
        if (Managers.InputManager.key(InputCode.Keys.bothRope).keydown)
        {
            this.isJumpDown = true;
        }
        if (Managers.InputManager.key(InputCode.Keys.restart).keydown)
        {
            this.isSuicide = true;
        }
        this.isWALKDown = Managers.InputManager.key(InputCode.Keys.jump).keypressed;
    }
}

