using System;

public class InputCode
{
    public enum Keys : int { up=0,down=1, left = 2 ,right=3,jump=4,dodge=5,leftRope=6,rightRope=7,bothRope=8,focus=9,attack0=10,
        attack1 =11,salute=12,camera=13,restart=14,pause=15,hidecursor=16,fullscreen=17,reload=18,flare1=19,flare2=20,flare3=21,reel_in=22,reel_out=23,
        rottocam=24 }

    //for compatibility
    public static int attack0 = 10;
    public static int attack1 = 11;
    public static int bothRope = 8;
    public static int camera = 13;
    public static int dodge = 5;
    public static int down = 1;
    public static int flare1 = 0x13;
    public static int flare2 = 20;
    public static int flare3 = 0x15;
    public static int focus = 9;
    public static int fullscreen = 0x11;
    public static int hideCursor = 0x10;
    public static int jump = 4;
    public static int left = 2;
    public static int leftRope = 6;
    public static int pause = 15;
    public static int reload = 0x12;
    public static int restart = 14;
    public static int right = 3;
    public static int rightRope = 7;
    public static int salute = 12;
    public static int up = 0;

}

