﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

class LevelSkinConfig : ClassicConfig
{
    public enum P :int
    {
        House1,
        House2,
        House3,
        House4,
        House5,
        House6,
        House7,
        House8,
        CityGround,
        CityWAll,
        CityH,
        tree1,
        tree2,
        tree3,
        tree4,
        tree5,
        tree6,
        tree7,
        tree8,
        leaf1,
        leaf2,
        leaf3,
        leaf4,
        leaf5,
        leaf6,
        leaf7,
        leaf8,
        ForestGround,

        forestskyfront,
        forestskyback,
        forestskyleft,
        forestskyright,
        forestskyup,
        forestskydown,

        cityskyfront,
        cityskyback,
        cityskyleft,
        cityskyright,
        cityskyup,
        cityskydown,
        customskyfront,
        customskyback,
        customskyleft,
        customskyright,
        customskyup,
        customskydown,
    }


    public override Type enum_config
    {
        get
        {
            return typeof(P);
        }
    }

    public override string name
    {
        get
        {
            return "levelSkin";
        }
    }

    public override object defaultval(int val)
    {
        return "";
    }

    public override void init()
    {
        
    }

    public override void Onvalchanged(int val)
    {
        FengGameManagerMKII.instance.loadconfig();
    }

    protected override Valtypes typeofkey(int key)
    {
        return Valtypes.String;
    }

    public static P[] Houses= { P.House1,P.House2,P.House3,P.House4,P.House5,P.House6,P.House7,P.House8 };
    public static P[] Trees = { P.tree1, P.tree2, P.tree3, P.tree4, P.tree5, P.tree6, P.tree7, P.tree8 };
    public static P[] Leafs = { P.leaf1, P.leaf2, P.leaf3, P.leaf4, P.leaf5, P.leaf6, P.leaf7, P.leaf8 };

    public static P[] CitySkies = { P.cityskyfront, P.cityskyback, P.cityskyleft, P.cityskyright, P.cityskyup, P.cityskydown };
    public static P[] ForestSkies = { P.forestskyfront, P.forestskyback, P.forestskyright, P.forestskyup, P.forestskydown };
}
