﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

class GraphicConfig : ClassicConfig
{
    public enum P:int
    {
        Mipmap,
        FrameRate,
        Vsync,
        Volume,
        TextureLimit,
        GasSkin,
        SpeedEffect,
        PlayerSkins,
        MapSkins,
        RandomForestTex,
        Menusize_x,
        Menusize_y,
        CameraTilt,
        CameraDist,
        InGameSnaps,
    }
    public override Type enum_config
    {
        get
        {
            return typeof(P);
        }
    }

    public override string name
    {
        get
        {
            return "graphics";
        }
    }

    public override object defaultval(int val)
    {
        switch ((P)val)
        {
            case P.Mipmap:
                return true;
            case P.FrameRate:
                return 60;
            case P.Vsync:
                return false;
            case P.Volume:
                return 1f;
            case P.TextureLimit:
                return 0;
            case P.GasSkin:
                return true;
            case P.SpeedEffect:
                return true;
            case P.PlayerSkins:
                return true;
            case P.MapSkins:
                return true;
            case P.RandomForestTex:
                return true;
            case P.Menusize_x:
                return 1000;
            case P.Menusize_y:
                return 600;
            case P.CameraTilt:
                return true;
            case P.CameraDist:
                return 1.3f;
            case P.InGameSnaps:
                return true;
            default:
                throw new Exception(name + " Configs : default val " + ((P)val).ToString() + " not defined");
        }
    }

    public override void init()
    {
    }

    public override void Onvalchanged(int val)
    {
        switch ((P)val)
        {
            case P.Mipmap:
                break;
            case P.FrameRate:
                break;
            case P.Vsync:
                break;
            case P.Volume:
                break;
            case P.TextureLimit:
                break;
            case P.GasSkin:
                break;
            case P.SpeedEffect:
                break;
            case P.PlayerSkins:
                break;
            case P.MapSkins:
                break;
            case P.RandomForestTex:
                break;
            case P.Menusize_x:
                IngameMenu.menusizex = ints[val];
                break;
            case P.Menusize_y:
                IngameMenu.menusizey = ints[val];
                break;
            case P.CameraTilt:
                IN_GAME_MAIN_CAMERA.cameraTilt = bools[val]?1:0;
                break;
            case P.CameraDist:
                IN_GAME_MAIN_CAMERA.cameraDistance = floats[val];
                break;
            case P.InGameSnaps:
                break;
            default:
                break;
        }
    }

    protected override Valtypes typeofkey(int key)
    {
        switch ((P)key)
        {
            case P.Mipmap:
                return Valtypes.Bool;
            case P.FrameRate:
                return Valtypes.Int;
            case P.Vsync:
                return Valtypes.Bool;
            case P.Volume:
                return Valtypes.Float;
            case P.TextureLimit:
                return Valtypes.Int;
            case P.GasSkin:
                return Valtypes.Bool;
            case P.SpeedEffect:
                return Valtypes.Bool;
            case P.PlayerSkins:
                return Valtypes.Bool;
            case P.MapSkins:
                return Valtypes.Bool;
            case P.RandomForestTex:
                return Valtypes.Bool;
            case P.Menusize_x:
                return Valtypes.Int;
            case P.Menusize_y:
                return Valtypes.Int;
            case P.CameraTilt:
                return Valtypes.Bool;
            case P.CameraDist:
                return Valtypes.Float;
            case P.InGameSnaps:
                return Valtypes.Bool;
            default:
                throw new Exception(name + " type of key : " + key + " not set");
        }
    }
}