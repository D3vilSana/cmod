﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

class SoloGameGUI : GUIElem
{
    private IEnumerable<Levels.Level> lvls;

    private Vector2 levelsize = new Vector2(200, 50);

    private DayLight time;

    private Levels.Level selected;

    private Vector2 scrollpos = Vector2.zero;

    public override Vector2 getsize
    {
        get
        {
            return new Vector2(500, 200);
        }

        internal set
        {
            throw new NotImplementedException();
        }
    }

    public override Position pos
    {
        get
        {
            return Position.MidMid;
        }

        internal set
        {
            
        }
    }

    public override bool show
    {
        get { return doshow; }

        internal set { doshow = value; }
    }

    public static bool doshow
    {
        get;
        set;
    }

    public override void Init()
    {
        lvls = Levels.LevelManager.getLevels().Where(l=>l.type!=GAMEMODE.PVP_AHSS);
        time = DayLight.Day;
        selected = lvls.First();
    }

    public override void OnGui()
    {

        GUI.Box(getsize.fromCoord(Vector2.zero), "Solo Game");

        if (GUI.Button(new Rect(getsize.x - 100, getsize.y - 20, 50, 20), "GO !"))
        {
            PhotonNetwork.offlineMode = true;
            FengGameManagerMKII.level = selected;
            PhotonNetwork.CreateRoom(string.Concat(new object[] { "CMod-solo", "`", selected.name, "`", IN_GAME_MAIN_CAMERA.difficulty, "`", 999999999, "`", time.name(), "`", "", "`", UnityEngine.Random.Range(0, 0xc350) }), true, true, 0);
            show = false;
        }
        if (GUI.Button(new Rect(getsize.x - 50, getsize.y - 20, 50, 20), "Quit"))
        {
            NGUITools.SetActive(GameObject.Find("UIRefer").GetComponent<UIMainReferences>().panelMain, true);
            GameObject.Find("InputManagerController").GetComponent<FengCustomInputs>().menuOn = false;
            show = false;
        }
        int i = 0;
        scrollpos = GUI.BeginScrollView(new Vector2(levelsize.x + 20, 200).fromCoord(Vector2.zero), scrollpos, new Rect(0, 0, levelsize.x, levelsize.y * lvls.Count()));
        foreach (Levels.Level l in lvls)
        {
            GUI.BeginGroup(levelsize.fromCoord(Vector2.up * levelsize.y * i));
            showlevel(l);
            GUI.EndGroup();
            i++;
        }
        GUI.EndScrollView(true);

        time = (DayLight)GUI.SelectionGrid(new Rect(getsize.x-200,0,200,25), (int)time, new string[] { "Day", "Dawn", "Night" }, 3);
    }

    private void showlevel(Levels.Level l)
    {
        GUI.Box(levelsize.fromCoord(Vector2.zero),"");
        ServerList.DrawIcon(l.mapName, time.name());

        if (GUI.Toggle(new Rect(50,15,100,20),selected==l,l.name))
        {
            selected = l;
        }
    }

    public override void Update()
    {
       
    }
}