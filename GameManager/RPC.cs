﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public partial class FengGameManagerMKII
{
    [RPC]
    private void Chat(string content, string sender, PhotonMessageInfo info)
    {
        if (sender != string.Empty)
        {
            content = sender + ":" + content;
        }
        content = "<color=#3c3cdc>[" + Convert.ToString(info.sender.ID) + "]</color> " + content;
        chat.addLINE(content);
    }

    [RPC]
    private void ChatPM(string sender, string content, PhotonMessageInfo info)
    {
        content = sender + ":" + content;
        content = "<color=#62fc9f> <-[" + Convert.ToString(info.sender.ID) + "]</color> " + content;
        chat.addLINE(content);
    }

    [RPC]
    private void getRacingResult(string player, float time,PhotonMessageInfo info)
    {//TODO : anticheat
        float t = this.roundTime - 20f;
        if (time<t&&!t.AlmostEquals(time,5f))//5s
        {
            KickBanManager.ban(info.sender, "Cheating result (" + time + " instead of " + t + ")");
        }

        RacingResult result = new RacingResult
        {
            name = player,
            time = time
        };
        this.racingResult.Add(result);
        this.refreshRacingResult();
    }

    [RPC]
    private void netGameLose(int score, PhotonMessageInfo info)
    {
        if (!info.sender.isMasterClient)
        {
            
            if (level.type == GAMEMODE.RACING)
            {
                chat.addLINE("Game restart asked by #" + info.sender.ID);
            }
            else
            {
                    chat.addLINE("Game restarted by #" + info.sender.ID);
                return;
            }
        }
        gameEndCD.timeout = gameEndTotalCDtime;
        gameEndCD.Start();
        this.titanScore = score;
    }

    [RPC]
    private void netGameWin(int score, PhotonMessageInfo info)
    {
        if (!info.sender.isMasterClient)
        {
            chat.addLINE("Game restart asked by #" + info.sender.ID);
        }
        this.humanScore = score;
        if (FengGameManagerMKII.level.type == GAMEMODE.PVP_AHSS)
        {
            this.teamWinner = score;
            this.teamScores[this.teamWinner - 1]++;
            gameEndCD.timeout = gameEndTotalCDtime;
        }
        else if (FengGameManagerMKII.level.type == GAMEMODE.RACING)
        {
            gameEndCD.timeout = 20;
        }
        else
        {
            gameEndCD.timeout = gameEndTotalCDtime;
        }
        gameEndCD.Start();
    }

    [RPC]
    private void netRefreshRacingResult(string tmp,PhotonMessageInfo info)
    {
        if (!info.sender.isMasterClient)
        {
            chat.addLINE("racing result set by " + info.sender.ID);
        }
        
        this.localRacingResult = tmp;
    }

    [RPC]
    public void netShowDamage(int speed, PhotonMessageInfo info)
    {
        if (!(info.sender.isMasterClient||info.sender.isLocal))
        {
            chat.addLINE("warning damages set by " + info.sender.ID);
        }
        GameObject.Find("Stylish").GetComponent<StylishComponent>().Style(speed);
        GameObject target = GameObject.Find("LabelScore");
        if (target != null)
        {
            target.GetComponent<UILabel>().text = speed.ToString();
            target.transform.localScale = Vector3.zero;
            speed = (int)(speed * 0.1f);
            speed = Mathf.Max(40, speed);
            speed = Mathf.Min(150, speed);
            iTween.Stop(target);
            object[] args = new object[] { "x", speed, "y", speed, "z", speed, "easetype", iTween.EaseType.easeOutElastic, "time", 1f };
            iTween.ScaleTo(target, iTween.Hash(args));
            object[] objArray2 = new object[] { "x", 0, "y", 0, "z", 0, "easetype", iTween.EaseType.easeInBounce, "time", 0.5f, "delay", 2f };
            iTween.ScaleTo(target, iTween.Hash(objArray2));
        }
    }
    [RPC]
    public void oneTitanDown(string name1, bool onPlayerLeave = false)
    {
        if ( PhotonNetwork.isMasterClient) {
            if (FengGameManagerMKII.level.type == GAMEMODE.PVP_CAPTURE)
            {
                if (name1 != string.Empty)
                {
                    if (name1 == "Titan")
                    {
                        this.PVPhumanScore++;
                    }
                    else if (name1 == "Aberrant")
                    {
                        this.PVPhumanScore += 2;
                    }
                    else if (name1 == "Jumper")
                    {
                        this.PVPhumanScore += 3;
                    }
                    else if (name1 == "Crawler")
                    {
                        this.PVPhumanScore += 4;
                    }
                    else if (name1 == "Female Titan")
                    {
                        this.PVPhumanScore += 10;
                    }
                    else
                    {
                        this.PVPhumanScore += 3;
                    }
                }
                this.checkPVPpts();
                base.photonView.RPC("refreshPVPStatus", PhotonTargets.Others, PVPhumanScore,PVPtitanScore);
            }
            else if (FengGameManagerMKII.level.type != GAMEMODE.CAGE_FIGHT)
            {
                if (FengGameManagerMKII.level.type == GAMEMODE.KILL_TITAN)
                {
                    if (this.checkIsTitanAllDie())
                    {
                        this.gameWin();
                    }
                }
                else if (FengGameManagerMKII.level.type == GAMEMODE.SURVIVE_MODE)
                {
                    if (this.checkIsTitanAllDie())
                    {
                        this.wave++;
                        if (FengGameManagerMKII.level.respawnMode == RespawnMode.NEWROUND) 
                        {
                            base.photonView.RPC("respawnHeroInNewRound", PhotonTargets.All);
                        }
                            this.sendChatContentInfo("<color=#A8FF24>Wave : " + this.wave + "</color>");
                        if (this.wave > this.highestwave)
                        {
                            this.highestwave = this.wave;
                        }
                        if (PhotonNetwork.isMasterClient)
                        {
                            this.RequireStatus();
                        }
                        if (this.wave > 20)
                        {
                            this.gameWin();
                        }
                        else
                        {
                            int rate = 90;
                            if (this.difficulty == 1)
                            {
                                rate = 70;
                            }
                            if (!FengGameManagerMKII.level.punk)
                            {
                                this.randomSpawnTitan("titanRespawn", rate, this.wave + 2, false);
                            }
                            else if (this.wave == 5)
                            {
                                this.randomSpawnTitan("titanRespawn", rate, 1, true);
                            }
                            else if (this.wave == 10)
                            {
                                this.randomSpawnTitan("titanRespawn", rate, 2, true);
                            }
                            else if (this.wave == 15)
                            {
                                this.randomSpawnTitan("titanRespawn", rate, 3, true);
                            }
                            else if (this.wave == 20)
                            {
                                this.randomSpawnTitan("titanRespawn", rate, 4, true);
                            }
                            else
                            {
                                this.randomSpawnTitan("titanRespawn", rate, this.wave + 2, false);
                            }
                        }
                    }
                }
                else if (FengGameManagerMKII.level.type == GAMEMODE.ENDLESS_TITAN)
                {
                    if (!onPlayerLeave)
                    {
                        this.humanScore++;
                        int num2 = 90;
                        if (this.difficulty == 1)
                        {
                            num2 = 70;
                        }
                        this.randomSpawnTitan("titanRespawn", num2, 1, false);
                    }
                }
            }
        }
    }

    [RPC]
    private void refreshPVPStatus(int score1, int score2, PhotonMessageInfo info)
    {
        this.PVPhumanScore = score1;
        this.PVPtitanScore = score2;
    }

    [RPC]
    private void refreshPVPStatus_AHSS(int[] score1)
    {
        UnityEngine.MonoBehaviour.print(score1);
        this.teamScores = score1;
    }

    [RPC]
    private void refreshStatus(int score1, int score2, int wav, int highestWav, float time1, float time2, bool startRacin, bool endRacin, PhotonMessageInfo info)
    {
        if (!info.sender.isMasterClient)
        {
            if (PhotonNetwork.isMasterClient)
            {
                KickBanManager.ban(info.sender, "unauthorised refreshStatus");
            }
            else
            {
                return;
            }
        }
        this.humanScore = score1;
        this.titanScore = score2;
        this.wave = wav;
        this.highestwave = highestWav;
        this.roundTime = time1;
        this.timeTotalServer = time2;
        this.startRacing = startRacin;
        this.endRacing = endRacin;
        if (this.startRacing && (GameObject.Find("door") != null))
        {
            GameObject.Find("door").SetActive(false);
        }
    }

    [RPC]
    public void RequireStatus()
    {
        base.photonView.RPC("refreshStatus", PhotonTargets.Others, humanScore,titanScore,wave,highestwave,roundTime,timeTotalServer,startRacing,endRacing);
        base.photonView.RPC("refreshPVPStatus", PhotonTargets.Others, PVPhumanScore,PVPtitanScore);
        base.photonView.RPC("refreshPVPStatus_AHSS", PhotonTargets.Others, teamScores);
    }

    [RPC]
    private void respawnHeroInNewRound(PhotonMessageInfo info)
    {
        if (!(info.sender.isMasterClient || info.sender.isLocal))
        {
            chat.addLINE("respawned by #" + info.sender.ID);
        }
        if (BTN.BTN_choose_side.needChooseSide)
        {
            return;
        }
        
        if (info.sender.isMasterClient&& CustomGameMode.getint("RespawnTime") > 0 && FengGameManagerMKII.instance.respawn.is_running)//TODO : find better fix!!
        {
            return;
        }
        if (myhero==null)
        {
            level.Spawn(myLastHero);
        }
    }

    [RPC]
    private void restartGameByClient()
    {
        //TODO : ANTICHEAT
    }

    [RPC]
    private void RPCLoadLevel(PhotonMessageInfo info)
    {
        if (!info.sender.isMasterClient)
        {
            if (PhotonNetwork.isMasterClient)
            {
                KickBanManager.kick(info.sender, "Requests restart");
            }
            else
            {
                chat.addLINE("#" + info.sender.ID + " asked restart");
            }
        }
        level.map.Load();
    }

    [RPC]
    private void showChatContent(string content, PhotonMessageInfo info)
    {
        this.chatContent.Add("["+info.sender.ID+"] : "+content);
        if (this.chatContent.Count > 10)
        {
            this.chatContent.RemoveAt(0);
        }
        GameObject.Find("LabelChatContent").GetComponent<UILabel>().text = string.Empty;
        for (int i = 0; i < this.chatContent.Count; i++)
        {
            UILabel component = GameObject.Find("LabelChatContent").GetComponent<UILabel>();
            component.text = component.text + this.chatContent[i];
        }
    }

    [RPC]
    private void showResult(string text0, string text1, string text2, string text3, string text4, string text6, PhotonMessageInfo t)
    {
        if (!t.sender.isMasterClient)
        {
            if (PhotonNetwork.isMasterClient)
            {
                KickBanManager.kick(t.sender, "Tried to kick the Master");
            }
            else
            {
                chat.addLINE("#" + t.sender.ID+" tried to kick you ! (showresult)");

            }
            return;
        }
        if (!this.gameTimesUp)
        {
            this.gameTimesUp = true;
            GameObject obj2 = GameObject.Find("UI_IN_GAME");
            NGUITools.SetActive(obj2.GetComponent<UIReferArray>().panels[0], false);
            NGUITools.SetActive(obj2.GetComponent<UIReferArray>().panels[1], false);
            NGUITools.SetActive(obj2.GetComponent<UIReferArray>().panels[2], true);
            NGUITools.SetActive(obj2.GetComponent<UIReferArray>().panels[3], false);
            GameObject.Find("LabelName").GetComponent<UILabel>().text = text0;
            GameObject.Find("LabelKill").GetComponent<UILabel>().text = text1;
            GameObject.Find("LabelDead").GetComponent<UILabel>().text = text2;
            GameObject.Find("LabelMaxDmg").GetComponent<UILabel>().text = text3;
            GameObject.Find("LabelTotalDmg").GetComponent<UILabel>().text = text4;
            GameObject.Find("LabelResultTitle").GetComponent<UILabel>().text = text6;
            Screen.lockCursor = false;
            Screen.showCursor = true;
            this.gameStart = false;
        }
    }

    [RPC]
    public void someOneIsDead(int id = -1)
    {
        level.Someoneisdead();
    }
    [RPC]
    public void titanGetKill(PhotonPlayer player, int Damage, string name,PhotonMessageInfo info)
    {
        if (!info.sender.isLocal)
        {
            chat.addLINE("WARNING : strange 'titanGetKill' from #" + info.sender.ID);
        }
        titanGetKill(player, Damage, name);
    }

    [RPC]
    private void updateKillInfo(bool t1, string killer, bool t2, string victim, int dmg, PhotonMessageInfo info)
    {
        //TODO : anticheat
        GameObject obj4;
        GameObject obj2 = GameObject.Find("UI_IN_GAME");
        GameObject obj3 = (GameObject)UnityEngine.Object.Instantiate(Resources.Load("UI/KillInfo"));
        for (int i = 0; i < this.killInfoGO.Count; i++)
        {
            obj4 = (GameObject)this.killInfoGO[i];
            if (obj4 != null)
            {
                obj4.GetComponent<KillInfoComponent>().moveOn();
            }
        }
        if (this.killInfoGO.Count > 4)
        {
            obj4 = (GameObject)this.killInfoGO[0];
            if (obj4 != null)
            {
                obj4.GetComponent<KillInfoComponent>().destory();
            }
            this.killInfoGO.RemoveAt(0);
        }
        obj3.transform.parent = obj2.GetComponent<UIReferArray>().panels[0].transform;
        obj3.GetComponent<KillInfoComponent>().show(t1, killer, t2, victim, dmg);
        this.killInfoGO.Add(obj3);
    }
    [RPC]
    private void settingRPC(ExitGames.Client.Photon.Hashtable hash, PhotonMessageInfo info)
    {
        if (info.sender.isMasterClient)
        {
            CustomGameMode.UpdateModes(hash);
        }
    }

    [RPC]
    private void clearlevel(string[] link, int gametype, PhotonMessageInfo info)
    {
        Levels.LevelType.Custom.Custom l = level as Levels.LevelType.Custom.Custom;
        if (info.sender.isMasterClient&&l!=null)
        {
            GAMEMODE old = level.type;
            switch (gametype)
            {
                case 0:
                    l.type = GAMEMODE.KILL_TITAN;
                    break;
                case 1:
                    l.type = GAMEMODE.SURVIVE_MODE;
                    break;
                case 2:
                    l.type = GAMEMODE.PVP_AHSS;
                    break;
                case 3:
                    l.type = GAMEMODE.RACING;
                    break;
                case 4:
                    l.type = GAMEMODE.None;
                    break;
                default:
                    break;
            }
            if (info.sender.isMasterClient && link.Length > 6)
            {
                base.StartCoroutine(this.clearlevelE(link));
            }
        }
    }

    [RPC]
    private void loadskinRPC(string n, string url, string url2, string[] skybox, PhotonMessageInfo info)
    {
        if (Configs.getbool(GraphicConfig.P.MapSkins) && info.sender.isMasterClient)
        {
            base.StartCoroutine(this.loadskinE(n, url, url2, skybox));
        }
    }

    [RPC]
    private void ignorePlayer(int ID, PhotonMessageInfo info)
    {
        if (info.sender.isMasterClient)
        {
            PhotonNetwork.CloseConnection(PhotonPlayer.Find(ID));
            KickBanManager.ignore(ID);
            
        }
    }

    [RPC]
    private void ignorePlayerArray(int[] IDS, PhotonMessageInfo info)
    {
        if (info.sender.isMasterClient)
        {
            foreach (int i in IDS)
            {
                KickBanManager.ignore(i);
            }
        }
    }
    [RPC]
    private void customlevelRPC(string[] content, PhotonMessageInfo info)
    {
        if (info.sender.isMasterClient)
        {
            CustomMaps.CustomMapsManager.StartLoadMap(content);
        }

    }


}