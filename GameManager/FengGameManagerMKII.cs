using ExitGames.Client.Photon;
using Photon;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Diagnostics;
using System.IO;
using System.Runtime.CompilerServices;
using UnityEngine;
using System.Linq;

public partial class FengGameManagerMKII : Photon.MonoBehaviour
{
    public static readonly string applicationId = "f1f6195c-df4a-40f9-bae5-4744c32901ef";
    private ArrayList chatContent;
    public int difficulty;
    private bool endRacing;
    public Managers.TimerManager.Timer gameEndCD = new Managers.TimerManager.Timer();
    private float gameEndTotalCDtime = 9f;
    public bool gameStart;
    private bool gameTimesUp;
    private int highestwave = 1;
    private int humanScore;
    public bool justSuicide;
    private ArrayList killInfoGO = new ArrayList();
    public static bool LAN;
    public static Levels.Level level ;
    private string localRacingResult;
    public IN_GAME_MAIN_CAMERA mainCamera;
    public string myLastHero;
    private string myLastRespawnTag = "playerRespawn";
    public int PVPhumanScore;
    private int PVPhumanScoreMax = 200;
    public int PVPtitanScore;
    private int PVPtitanScoreMax = 200;
    private ArrayList racingResult;
    public float roundTime;
    private bool startRacing;
    private int[] teamScores;
    private int teamWinner;
    public int time = 600;
    private float timeElapse;
    public float timeTotalServer;
    private int titanScore;
    private GameObject ui;
    public int wave = 1;
    public List<GameObject> groundList;
    public List<PhotonPlayer> playersRPC;
    public static bool isFirstLoad = true;

    /// <summary>
    /// RCMod's settings
    /// </summary>
    public static object[] settings;

    public static FengGameManagerMKII instance;
    public HERO myhero;
    public static ExitGames.Client.Photon.Hashtable[] linkHash;
    public static AssetBundle RCassets;
    public static Material skyMaterial;
    public bool isUnloading;
    public static string[] s;
    public InRoomChat chat;

    public Managers.TimerManager.Timer respawn = new RespawnTimer()
    {
        timeout = 5,
    };

    public static Dictionary<int, long> RPClastmsg = new Dictionary<int, long>();

    public static System.Collections.Hashtable Heroes = new System.Collections.Hashtable();

    public void SendChat(string content)
    {
        photonView.RPC("Chat", PhotonTargets.All, content,string.Empty);
    }

    public string getDictionary(string line, string file)
    {
        Dictionary<string, string> dictionary = new Dictionary<string, string>();
        foreach (string str in File.ReadAllLines(file))
        {
            if (!str.StartsWith("!") && str.Contains("|"))
            {
                string[] strArray = str.Split(new char[] { '|' });
                dictionary.Add(strArray[0], strArray[1]);
            }
        }
        return dictionary[line];

    }

    public void addHero(HERO hero)
    {
        PhotonPlayer p = hero.photonView.owner;
        if (Heroes.ContainsKey(p.ID))
        {
            HERO h = (HERO)Heroes[p.ID];
            if (PhotonNetwork.isMasterClient)
            {
            PhotonNetwork.Destroy(h.photonView);
            }
            Heroes[p.ID] = hero;
        }
        else
        {
            Heroes.Add(p.ID, hero);
        }
    }

    public HERO getHero(int ID)
    {
        if (Heroes.ContainsKey(ID))
        {
            return (HERO)Heroes[ID];
        }
        else
        {
            return null;
        }
    }

    private bool checkIsTitanAllDie()
    {
        foreach (GameObject obj2 in GameObject.FindGameObjectsWithTag("titan"))
        {
            if ((obj2.GetComponent<TITAN>() != null) && !obj2.GetComponent<TITAN>().hasDie)
            {
                return false;
            }
            if (obj2.GetComponent<FEMALE_TITAN>() != null)
            {
                return false;
            }
        }
        return true;
    }

    public void checkPVPpts()
    {
        if (this.PVPtitanScore >= this.PVPtitanScoreMax)
        {
            this.PVPtitanScore = this.PVPtitanScoreMax;
            this.gameLose();
        }
        else if (this.PVPhumanScore >= this.PVPhumanScoreMax)
        {
            this.PVPhumanScore = this.PVPhumanScoreMax;
            this.gameWin();
        }
    }

    private void core2()
    {
        #region need to choose side
        if (BTN.BTN_choose_side.needChooseSide)
        {
            if (Managers.InputManager.key(InputCode.Keys.flare1).keydown)
            {
                if (NGUITools.GetActive(this.ui.GetComponent<UIReferArray>().panels[3]))
                {
                    Screen.lockCursor = true;
                    Screen.showCursor = true;
                    NGUITools.SetActive(this.ui.GetComponent<UIReferArray>().panels[0], true);
                    NGUITools.SetActive(this.ui.GetComponent<UIReferArray>().panels[1], false);
                    NGUITools.SetActive(this.ui.GetComponent<UIReferArray>().panels[2], false);
                    NGUITools.SetActive(this.ui.GetComponent<UIReferArray>().panels[3], false);
                    Camera.main.GetComponent<SpectatorMovement>().disable = false;
                    Camera.main.GetComponent<MouseLook>().disable = false;
                }
                else
                {
                    Screen.lockCursor = false;
                    Screen.showCursor = true;
                    NGUITools.SetActive(this.ui.GetComponent<UIReferArray>().panels[0], false);
                    NGUITools.SetActive(this.ui.GetComponent<UIReferArray>().panels[1], false);
                    NGUITools.SetActive(this.ui.GetComponent<UIReferArray>().panels[2], false);
                    NGUITools.SetActive(this.ui.GetComponent<UIReferArray>().panels[3], true);
                    Camera.main.GetComponent<SpectatorMovement>().disable = true;
                    Camera.main.GetComponent<MouseLook>().disable = true;
                }
            }
            if (Managers.InputManager.key(InputCode.Keys.pause).keydown && !IngameMenu.enabled)
            {
                if (!IngameMenu.enabled)
                {
                Screen.showCursor = true;
                Screen.lockCursor = false;
                Camera.main.GetComponent<SpectatorMovement>().disable = true;
                Camera.main.GetComponent<MouseLook>().disable = true;
                IngameMenu.showingame();
                }
                else
                {

                    IngameMenu.Exit();
                }

            }
        }
        #endregion
        
            level.core();

        if (Camera.main != null &&
            level.type != GAMEMODE.RACING &&
            myhero == null &&
            !BTN.BTN_choose_side.needChooseSide
            &&(
                FengGameManagerMKII.level.respawnMode == RespawnMode.DEATHMATCH ||
                CustomGameMode.getint("RespawnTime") > 0 ||
                CustomGameMode.getbool("Bomb") ||
                CustomGameMode.getint("PVPMode") > 0
                )
            )
        {
            respawn.Start();
            //TODO : pointmode
        }



        this.timeElapse += Time.deltaTime;
        this.roundTime += Time.deltaTime;

        this.timeTotalServer += Time.deltaTime;

        if (this.killInfoGO.Count > 0 && this.killInfoGO[0] == null)
        {
            this.killInfoGO.RemoveAt(0);
        }

    }

    public static GameObject InstantiateRCsset(string key)
    {
        key = key.Substring(8);
        return (GameObject)FengGameManagerMKII.RCassets.Load(key);
    }

    public void gameLose()
    {
        if (!gameEndCD.is_running)
        {
            gameEndCD.timeout = gameEndTotalCDtime;
            gameEndCD.Start();
            this.titanScore++;
            base.photonView.RPC("netGameLose", PhotonTargets.Others, titanScore);
        }
    }

    public void gameWin(int winner)
    {//TODO : clean the code
        if (!gameEndCD.is_running)
        {
            teamWinner = winner;
            humanScore++;
            if (winner != 0)
            {
                teamScores[winner - 1]++;
            }
            photonView.RPC("netGameWin", PhotonTargets.Others, winner);
            if (level is Levels.LevelType.Classic.Racing || level is Levels.LevelType.Custom.Racing)
            {
                gameEndCD.timeout = 20f;
            }
            else
            {
                gameEndCD.timeout = gameEndTotalCDtime;
            }
            gameEndCD.Start();
        }
    }

    public void gameWin()
    {
        gameWin(0);
    }
    
    public bool isPlayerAllDead()
    {
        foreach (PhotonPlayer p in PhotonNetwork.playerList)
        {
            if (RCextensions.returnIntFromObject(p.customProperties[PhotonPlayerProperty.isTitan])==1 && !(RCextensions.returnBoolFromObject(p.customProperties[PhotonPlayerProperty.dead])))
            {
                return false;
            }
        }
        return true;
    }

    public bool isTeamAllDead(int team)
    {
        foreach (PhotonPlayer p in PhotonNetwork.playerList)
        {
            if ((RCextensions.returnIntFromObject(p.customProperties[PhotonPlayerProperty.isTitan]) == 1 && (RCextensions.returnIntFromObject(p.customProperties[PhotonPlayerProperty.team])==team))&& !(RCextensions.returnBoolFromObject(p.customProperties[PhotonPlayerProperty.dead])))
            {
                return false;
            }
        }
        return true;
    }

    private void LateUpdate()
    {
        if (this.gameStart)
        {
            this.core2();
        }
        
    }

    public void multiplayerRacingFinsih()
    {
        float time = this.roundTime - 20f;
        if (PhotonNetwork.isMasterClient)
        {
            this.getRacingResult(LoginFengKAI.player.name, time,new PhotonMessageInfo());
        }
        else
        {
            base.photonView.RPC("getRacingResult", PhotonTargets.MasterClient, LoginFengKAI.player.name,time);
        }
        this.gameWin();
    }

    public void netShowDamage(int speed)
    {
        PhotonMessageInfo p = new PhotonMessageInfo();
        netShowDamage(speed, p);
    }

    public void NOTSpawnNonAITitan(string id)
    {
        this.myLastHero = id.ToUpper();
        ExitGames.Client.Photon.Hashtable hashtable2 = new ExitGames.Client.Photon.Hashtable();
        hashtable2.Add("dead", true);
        ExitGames.Client.Photon.Hashtable propertiesToSet = hashtable2;
        PhotonNetwork.player.SetCustomProperties(propertiesToSet);
        hashtable2 = new ExitGames.Client.Photon.Hashtable();
        hashtable2.Add(PhotonPlayerProperty.isTitan, 2);
        propertiesToSet = hashtable2;
        PhotonNetwork.player.SetCustomProperties(propertiesToSet);
        if (IN_GAME_MAIN_CAMERA.cameraMode == CAMERA_TYPE.TPS)
        {
            Screen.lockCursor = true;
        }
        else
        {
            Screen.lockCursor = false;
        }
        Screen.showCursor = true;
        mainCamera.enabled = true;
        mainCamera.setMainObject(null);
        mainCamera.setSpectorMode(true);
    }

    public void NOTSpawnPlayer(string id)
    {
        this.myLastHero = id.ToUpper();
        ExitGames.Client.Photon.Hashtable hashtable2 = new ExitGames.Client.Photon.Hashtable();
        hashtable2.Add("dead", true);
        ExitGames.Client.Photon.Hashtable propertiesToSet = hashtable2;
        PhotonNetwork.player.SetCustomProperties(propertiesToSet);
        hashtable2 = new ExitGames.Client.Photon.Hashtable();
        hashtable2.Add(PhotonPlayerProperty.isTitan, 1);
        propertiesToSet = hashtable2;
        PhotonNetwork.player.SetCustomProperties(propertiesToSet);
        if (IN_GAME_MAIN_CAMERA.cameraMode == CAMERA_TYPE.TPS)
        {
            Screen.lockCursor = true;
        }
        else
        {
            Screen.lockCursor = false;
        }
        Screen.showCursor = false;
        
        mainCamera.enabled = true;
        mainCamera.setMainObject(null, true, false);
        mainCamera.setSpectorMode(true);
    }

    public void playerKillInfoUpdate(PhotonPlayer player, int dmg)
    {
        ExitGames.Client.Photon.Hashtable p = new ExitGames.Client.Photon.Hashtable();
        p.Add(PhotonPlayerProperty.kills, ((int)player.customProperties[PhotonPlayerProperty.kills]) + 1);
        p.Add(PhotonPlayerProperty.max_dmg, Mathf.Max(dmg, (int)player.customProperties[PhotonPlayerProperty.max_dmg]));
        p.Add(PhotonPlayerProperty.total_dmg, ((int)player.customProperties[PhotonPlayerProperty.total_dmg]) + dmg);
        player.SetCustomProperties(p);
    }

    public GameObject randomSpawnOneTitan(string place, int rate = 0)
    {
        GameObject[] objArray = GameObject.FindGameObjectsWithTag(place);
        int index = UnityEngine.Random.Range(0, objArray.Length);
        GameObject obj2 = objArray[index];
        while (objArray[index] == null)
        {
            index = UnityEngine.Random.Range(0, objArray.Length);
            obj2 = objArray[index];
        }
        objArray[index] = null;
        return this.spawnTitan(rate, obj2.transform.position, obj2.transform.rotation, false);
    }

    public void randomSpawnTitan(string place, int rate, int num, bool punk = false)
    {
        if (num == -1)
        {
            num = 1;
        }
        if (CustomGameMode.getint("TitanNumber")>0)
        {
            num = CustomGameMode.getint("TitanNumber");
        }
        GameObject[] objArray = GameObject.FindGameObjectsWithTag(place);
        if (objArray.Length > 0)
        {
            for (int i = 0; i < num; i++)
            {
                int index = UnityEngine.Random.Range(0, objArray.Length);
                GameObject obj2 = objArray[index];
                while (objArray[index] == null)
                {
                    index = UnityEngine.Random.Range(0, objArray.Length);
                    obj2 = objArray[index];
                }
                objArray[index] = null;
                this.spawnTitan(rate, obj2.transform.position, obj2.transform.rotation, punk);
            }
        }
    }

    private void refreshRacingResult()
    {
        this.localRacingResult = "Result\n";
        IComparer comparer = new IComparerRacingResult();
        this.racingResult.Sort(comparer);
        int num = Mathf.Min(this.racingResult.Count, 6);
        for (int i = 0; i < num; i++)
        {
            this.localRacingResult = localRacingResult + "Rank " + i + 1 + " : ";
            this.localRacingResult = this.localRacingResult + (this.racingResult[i] as RacingResult).name;
            this.localRacingResult = this.localRacingResult + "   " + ((((int)((this.racingResult[i] as RacingResult).time * 100f)) * 0.01f)).ToString() + "s";
            this.localRacingResult = this.localRacingResult + "\n";
        }
        base.photonView.RPC("netRefreshRacingResult", PhotonTargets.All, localRacingResult);
    }

    public void removeHero(HERO hero)
    {
        if (hero==null)
        {
            return;
        }
        
        if (Heroes.ContainsKey(hero.photonView.ownerId))
        {
            Heroes.Remove(hero.photonView.ownerId);
        }
        
    }

    public void restartGame(bool masterclientSwitched = false)
    {
        UnityEngine.MonoBehaviour.print("reset game :" + this.gameTimesUp);
        if (!this.gameTimesUp)
        {
            this.PVPtitanScore = 0;
            this.PVPhumanScore = 0;
            this.startRacing = false;
            this.endRacing = false;
           // this.checkpoint = null;
            this.timeElapse = 0f;
            this.roundTime = 0f;
            if (gameEndCD.is_running)
            {
                gameEndCD.Abort();
            }
            this.wave = 1;

            respawn.Abort();
            this.killInfoGO = new ArrayList();
            this.racingResult = new ArrayList();
            PhotonNetwork.DestroyAll();
            base.photonView.RPC("RPCLoadLevel", PhotonTargets.All);
            if (masterclientSwitched)
            {
                this.sendChatContentInfo("<color=#A8FF24>MasterClient has switched to </color>" + PhotonNetwork.player.customProperties[PhotonPlayerProperty.name]);
            }
        }
    }

    public void sendChatContentInfo(string content)
    {
        base.photonView.RPC("Chat", PhotonTargets.All, content,"");
    }

    public void sendKillInfo(bool t1, string killer, bool t2, string victim, int dmg = 0)
    {
        base.photonView.RPC("updateKillInfo", PhotonTargets.All, t1, killer, t2,victim,dmg);
    }

    [Obsolete("Use GUIElms instead",true)]
    public void ShowHUDInfoCenter(string content)
    {
        GameObject obj2 = GameObject.Find("LabelInfoCenter");
        if (obj2 != null)
        {
            obj2.GetComponent<UILabel>().text = content;
        }
    }

    [Obsolete("Use GUIElms instead",true)]
    public void ShowHUDInfoCenterADD(string content)
    {
        GameObject obj2 = GameObject.Find("LabelInfoCenter");
        if (obj2 != null)
        {
            UILabel component = obj2.GetComponent<UILabel>();
            component.text = component.text + content;
        }
    }

    [Obsolete("Use GUIElms instead",true)]
    private void ShowHUDInfoTopCenter(string content)
    {
        GameObject obj2 = GameObject.Find("LabelInfoTopCenter");
        if (obj2 != null)
        {
            obj2.GetComponent<UILabel>().text = content;
        }
    }

    [Obsolete("Use GUIElms instead",true)]
    private void ShowHUDInfoTopCenterADD(string content)
    {
        GameObject obj2 = GameObject.Find("LabelInfoTopCenter");
        if (obj2 != null)
        {
            UILabel component = obj2.GetComponent<UILabel>();
            component.text = component.text + content;
        }
    }

    [Obsolete("Use GUIElms instead",true)]
    private void ShowHUDInfoTopLeft(string content)
    {
        GameObject obj2 = GameObject.Find("LabelInfoTopLeft");
        if (obj2 != null)
        {
            obj2.GetComponent<UILabel>().text = content;
        }
    }

    [Obsolete("Use GUIElms instead",true)]
    private void ShowHUDInfoTopRight(string content)
    {
        GameObject obj2 = GameObject.Find("LabelInfoTopRight");
        if (obj2 != null)
        {
            obj2.GetComponent<UILabel>().text = content;
        }
    }

    [Obsolete("Use GUIElms instead",true)]
    private void ShowHUDInfoTopRightMAPNAME(string content)
    {
        GameObject obj2 = GameObject.Find("LabelInfoTopRight");
        if (obj2 != null)
        {
            UILabel component = obj2.GetComponent<UILabel>();
            component.text = component.text + content;
        }
    }

    public void SpawnNonAITitan(string id, string tag = "titanRespawn")
    {
        GameObject obj3;
        GameObject[] objArray = GameObject.FindGameObjectsWithTag(tag);
        GameObject obj2 = objArray[UnityEngine.Random.Range(0, objArray.Length)];
        this.myLastHero = id.ToUpper();
        obj3 = PhotonNetwork.Instantiate("TITAN_VER3.1", FengGameManagerMKII.level.respawn, new Quaternion(), 0);
        FengGameManagerMKII.instance.mainCamera.setMainObjectASTITAN(obj3);
        obj3.GetComponent<TITAN>().nonAI = true;
        obj3.GetComponent<TITAN>().speed = 30f;
        obj3.GetComponent<TITAN_CONTROLLER>().enabled = true;
        if ((id == "RANDOM") && (UnityEngine.Random.Range(0, 100) < 7))
        {
            obj3.GetComponent<TITAN>().setAbnormalType(AbnormalType.TYPE_CRAWLER, true);
        }
        mainCamera.enabled = true;
        mainCamera.GetComponent<SpectatorMovement>().disable = true;
        mainCamera.GetComponent<MouseLook>().disable = true;
        ExitGames.Client.Photon.Hashtable h = new ExitGames.Client.Photon.Hashtable();
        h.Add("dead", false);
        h.Add(PhotonPlayerProperty.isTitan, 2);
        PhotonNetwork.player.SetCustomProperties(h);
        if (IN_GAME_MAIN_CAMERA.cameraMode == CAMERA_TYPE.TPS)
        {
            Screen.lockCursor = true;
        }
        else
        {
            Screen.lockCursor = false;
        }
        Screen.showCursor = true;
    }

    public void SpawnPlayer(string id, string tag = "playerRespawn")
    {
        SpawnPlayerAt(id, level.respawn);
    }

    public void SpawnPlayerAt(string id, Vector3 pos)
    {
        GameObject go = new GameObject();
        go.transform.position = pos;
        SpawnPlayerAt(id, go);
        UnityEngine.Object.Destroy(go);
    }

    public void SpawnPlayerAt(string id, GameObject pos)
    {
        IN_GAME_MAIN_CAMERA component = FengGameManagerMKII.instance.mainCamera;
        this.myLastHero = id.ToUpper();
        
            component.setMainObject(PhotonNetwork.Instantiate("AOTTG_HERO 1", pos.transform.position, pos.transform.rotation, 0), true, false);
            id = id.ToUpper();
            if (((id == "SET 1") || (id == "SET 2")) || (id == "SET 3"))
            {
                HeroCostume costume2 = CostumeConeveter.LocalDataToHeroCostume(id);
                costume2.checkstat();
                CostumeConeveter.HeroCostumeToLocalData(costume2, id);
                component.main_object.GetComponent<HERO>().GetComponent<HERO_SETUP>().init();
                if (costume2 != null)
                {
                    component.main_object.GetComponent<HERO>().GetComponent<HERO_SETUP>().myCostume = costume2;
                    component.main_object.GetComponent<HERO>().GetComponent<HERO_SETUP>().myCostume.stat = costume2.stat;
                }
                else
                {
                    costume2 = HeroCostume.costumeOption[3];
                    component.main_object.GetComponent<HERO>().GetComponent<HERO_SETUP>().myCostume = costume2;
                    component.main_object.GetComponent<HERO>().GetComponent<HERO_SETUP>().myCostume.stat = HeroStat.getInfo(costume2.name.ToUpper());
                }
                component.main_object.GetComponent<HERO>().GetComponent<HERO_SETUP>().setCharacterComponent();
                component.main_object.GetComponent<HERO>().setStat();
                component.main_object.GetComponent<HERO>().setSkillHUDPosition();
            }
            else
            {
                for (int j = 0; j < HeroCostume.costume.Length; j++)
                {
                    if (HeroCostume.costume[j].name.ToUpper() == id.ToUpper())
                    {
                        int num4 = HeroCostume.costume[j].id;
                        if (id.ToUpper() != "AHSS")
                        {
                            num4 += CheckBoxCostume.costumeSet - 1;
                        }
                        if (HeroCostume.costume[num4].name != HeroCostume.costume[j].name)
                        {
                            num4 = HeroCostume.costume[j].id + 1;
                        }
                        component.main_object.GetComponent<HERO>().GetComponent<HERO_SETUP>().init();
                        component.main_object.GetComponent<HERO>().GetComponent<HERO_SETUP>().myCostume = HeroCostume.costume[num4];
                        component.main_object.GetComponent<HERO>().GetComponent<HERO_SETUP>().myCostume.stat = HeroStat.getInfo(HeroCostume.costume[num4].name.ToUpper());
                        component.main_object.GetComponent<HERO>().GetComponent<HERO_SETUP>().setCharacterComponent();
                        component.main_object.GetComponent<HERO>().setStat();
                        component.main_object.GetComponent<HERO>().setSkillHUDPosition();
                        break;
                    }
                }
            }
            CostumeConeveter.HeroCostumeToPhotonData(component.main_object.GetComponent<HERO>().GetComponent<HERO_SETUP>().myCostume, PhotonNetwork.player);
            if (FengGameManagerMKII.level.type == GAMEMODE.PVP_CAPTURE)
            {
                Transform transform = component.main_object.transform;
                transform.position += new Vector3((float)UnityEngine.Random.Range(-20, 20), 2f, (float)UnityEngine.Random.Range(-20, 20));
            }
            ExitGames.Client.Photon.Hashtable hashtable2 = new ExitGames.Client.Photon.Hashtable();
            hashtable2.Add("dead", false);
            hashtable2 = new ExitGames.Client.Photon.Hashtable();
            hashtable2.Add(PhotonPlayerProperty.isTitan, 1);
            PhotonNetwork.player.SetCustomProperties(hashtable2);
        component.enabled = true;
        mainCamera.setHUDposition();
        mainCamera.GetComponent<SpectatorMovement>().disable = true;
        mainCamera.GetComponent<MouseLook>().disable = true;
        if (IN_GAME_MAIN_CAMERA.cameraMode == CAMERA_TYPE.TPS)
        {
            Screen.lockCursor = true;
        }
        else
        {
            Screen.lockCursor = false;
        }
        Screen.showCursor = false;
    }

    public void SpawnPlayerAt(string id, Transform pos)
    {
        this.myLastHero = id.ToUpper();
        mainCamera.setMainObject(PhotonNetwork.Instantiate("AOTTG_HERO 1", pos.position, pos.rotation, 0), true, false);
        id = id.ToUpper();
        if (((id == "SET 1") || (id == "SET 2")) || (id == "SET 3"))
        {
            HeroCostume costume2 = CostumeConeveter.LocalDataToHeroCostume(id);
            costume2.checkstat();
            CostumeConeveter.HeroCostumeToLocalData(costume2, id);
            HERO_SETUP setup = mainCamera.main_object.GetComponent<HERO>().GetComponent<HERO_SETUP>();
            setup.init();
            if (costume2 != null)
            {
                setup.myCostume = costume2;
                setup.myCostume.stat = costume2.stat;
            }
            else
            {
                costume2 = HeroCostume.costumeOption[3];
                setup.myCostume = costume2;
                setup.myCostume.stat = HeroStat.getInfo(costume2.name.ToUpper());
            }
            setup.setCharacterComponent();
            mainCamera.main_object.GetComponent<HERO>().setStat();
            mainCamera.main_object.GetComponent<HERO>().setSkillHUDPosition();
        }
        else
        {
            for (int j = 0; j < HeroCostume.costume.Length; j++)
            {
                if (HeroCostume.costume[j].name.ToUpper() == id.ToUpper())
                {
                    int num4 = HeroCostume.costume[j].id;
                    if (id.ToUpper() != "AHSS")
                    {
                        num4 += CheckBoxCostume.costumeSet - 1;
                    }
                    if (HeroCostume.costume[num4].name != HeroCostume.costume[j].name)
                    {
                        num4 = HeroCostume.costume[j].id + 1;
                    }
                    mainCamera.main_object.GetComponent<HERO>().GetComponent<HERO_SETUP>().init();
                    mainCamera.main_object.GetComponent<HERO>().GetComponent<HERO_SETUP>().myCostume = HeroCostume.costume[num4];
                    mainCamera.main_object.GetComponent<HERO>().GetComponent<HERO_SETUP>().myCostume.stat = HeroStat.getInfo(HeroCostume.costume[num4].name.ToUpper());
                    mainCamera.main_object.GetComponent<HERO>().GetComponent<HERO_SETUP>().setCharacterComponent();
                    mainCamera.main_object.GetComponent<HERO>().setStat();
                    mainCamera.main_object.GetComponent<HERO>().setSkillHUDPosition();
                    break;
                }
            }
        }
        CostumeConeveter.HeroCostumeToPhotonData(mainCamera.main_object.GetComponent<HERO>().GetComponent<HERO_SETUP>().myCostume, PhotonNetwork.player);
        if (FengGameManagerMKII.level.type == GAMEMODE.PVP_CAPTURE)
        {
            Transform transform = mainCamera.main_object.transform;
            transform.position += new Vector3((float)UnityEngine.Random.Range(-20, 20), 2f, (float)UnityEngine.Random.Range(-20, 20));
        }
        ExitGames.Client.Photon.Hashtable h = new ExitGames.Client.Photon.Hashtable();
        h.Add("dead", false);
        h.Add(PhotonPlayerProperty.isTitan, 1);
        PhotonNetwork.player.SetCustomProperties(h);
        mainCamera.enabled = true;
        mainCamera.setHUDposition();
        mainCamera.GetComponent<SpectatorMovement>().disable = true;
        mainCamera.GetComponent<MouseLook>().disable = true;
        if (IN_GAME_MAIN_CAMERA.cameraMode == CAMERA_TYPE.TPS)
        {
            Screen.lockCursor = true;
        }
        else
        {
            Screen.lockCursor = false;
        }
        Screen.showCursor = false;
    }

    public GameObject spawnTitan(int rate, Vector3 position, Quaternion rotation, bool punk = false)
    {
        GameObject obj3;
        GameObject obj2 = this.spawnTitanRaw(position, rotation);
        float n = CustomGameMode.getfloat("SpawnRate_N");
        float a = n + CustomGameMode.getfloat("SpawnRate_A");
        float j = a + CustomGameMode.getfloat("SpawnRate_J");
        float c = j + CustomGameMode.getfloat("SpawnRate_C");
        float p = c + CustomGameMode.getfloat("SpawnRate_P");
        int r = UnityEngine.Random.Range(0, 100);
        if (r <= n)
        {
            obj2.GetComponent<TITAN>().setAbnormalType(AbnormalType.NORMAL, false);
        }
        else if (r <= a)
        {
            obj2.GetComponent<TITAN>().setAbnormalType(AbnormalType.TYPE_I, false);
        }
        else if (r <= j)
        {
            obj2.GetComponent<TITAN>().setAbnormalType(AbnormalType.TYPE_JUMPER, false);
        }
        else if (r <= c)
        {
            obj2.GetComponent<TITAN>().setAbnormalType(AbnormalType.TYPE_CRAWLER, true);
        }
        else if (r <= p)
        {
            obj2.GetComponent<TITAN>().setAbnormalType(AbnormalType.TYPE_PUNK, false);
        }
        else
        {
            UnityEngine.Debug.LogWarning("Warning : Titan not spawned");
        }
        obj3 = PhotonNetwork.Instantiate("FX/FXtitanSpawn", obj2.transform.position, Quaternion.Euler(-90f, 0f, 0f), 0);
        obj3.transform.localScale = obj2.transform.localScale;
        return obj2;
    }

    public GameObject spawnTitanRaw(Vector3 position, Quaternion rotation)
    {
        return PhotonNetwork.Instantiate("TITAN_VER3.1", position, rotation, 0);
    }

    private void Start()
    {
        gameEndCD.On_End += delegate (Managers.TimerManager.Timer t)
        {
            if (PhotonNetwork.isMasterClient)
            {
                FengGameManagerMKII.instance.restartGame();
            }
        };
        //PhotonNetwork.networkingPeer.DebugOut = DebugLevel.ALL;
        //PhotonNetwork.logLevel = PhotonLogLevel.Full;
        base.gameObject.name = "MultiplayerManager";
        HeroCostume.init();
        CharacterMaterials.init();
        UnityEngine.Object.DontDestroyOnLoad(base.gameObject);
        //this.heroes = new ArrayList();
        Heroes = new System.Collections.Hashtable();
        instance = this;
        this.groundList = new List<GameObject>();
        playersRPC = new List<PhotonPlayer>();
        if (isFirstLoad)
        {
            isFirstLoad = false;
            Managers.Manager.InitAll();
            loadconfig();//LOAD RCMod's config TODO : remove it ! o/
        }

        List<string> list = new List<string>
        {
            "PanelLogin",
            "LOGIN"
        };
        UnityEngine.Object[] array = UnityEngine.Object.FindObjectsOfType(typeof(GameObject));
        for (int i = 0; i < array.Length; i++)
        {
            GameObject gameObject = (GameObject)array[i];
            foreach (string current in list)
            {
                if (gameObject.name.Contains(current) || gameObject.name == "Button" || (gameObject.name == "Label" && gameObject.GetComponent<UILabel>().text.Contains("Snap")))
                {
                    UnityEngine.Object.Destroy(gameObject);
                }
                else if (gameObject.name == "Checkbox")
                {
                    UnityEngine.Object.Destroy(gameObject);
                }
            }
        }

        this.groundList.Clear();
    }

    public void titanGetKill(PhotonPlayer player, int Damage, string name)
    {
        Damage = Mathf.Max(10, Damage);
        base.photonView.RPC("netShowDamage", player, Damage);
        base.photonView.RPC("oneTitanDown", PhotonTargets.MasterClient, name,false);
        this.sendKillInfo(false, (string)player.customProperties[PhotonPlayerProperty.name], true, name, Damage);
        this.playerKillInfoUpdate(player, Damage);
    }

    public void titanGetKillbyServer(int Damage, string name)
    {
        Damage = Mathf.Max(10, Damage);
        this.sendKillInfo(false, LoginFengKAI.player.name, true, name, Damage);
        this.netShowDamage(Damage);
        this.oneTitanDown(name, false);
        this.playerKillInfoUpdate(PhotonNetwork.player, Damage);
    }

    private void Update()
    {
        //CustomGlobalGUI.Update();
        //Managers.TimerManager.Update();
        Managers.Manager.UpdateAll();
    }

    public void OnGUI()
    {
        CustomGlobalGUI.OnGUI();
        ServerList.GUIUpdate();
        IngameMenu.GUIUpdate();
    }

    private IEnumerator clearlevelE(string[] skybox)
    {
        string text = skybox[6];
        bool flag = false;
        bool mipmap = Configs.getbool(GraphicConfig.P.Mipmap);
        if (skybox.All(s=>s!=""))
        {
            string key = string.Join(",", skybox);
            bool flag4 = !FengGameManagerMKII.linkHash[1].ContainsKey(key);
            if (flag4)
            {
                flag = true;
                Material material = Camera.main.GetComponent<Skybox>().material;
                string text2 = skybox[0];
                string text3 = skybox[1];
                string text4 = skybox[2];
                string text5 = skybox[3];
                string text6 = skybox[4];
                string text7 = skybox[5];
                bool flag5 = text2.EndsWith(".jpg") || text2.EndsWith(".png") || text2.EndsWith(".jpeg");
                if (flag5)
                {
                    WWW wWW = new WWW(text2);
                    yield return wWW;
                    Texture2D texture = RCextensions.loadimage(wWW, mipmap, 500000);
                    wWW.Dispose();
                    material.SetTexture("_FrontTex", texture);
                    wWW = null;
                    texture = null;
                }
                bool flag6 = text3.EndsWith(".jpg") || text3.EndsWith(".png") || text3.EndsWith(".jpeg");
                if (flag6)
                {
                    WWW wWW2 = new WWW(text3);
                    yield return wWW2;
                    Texture2D texture2 = RCextensions.loadimage(wWW2, mipmap, 500000);
                    wWW2.Dispose();
                    material.SetTexture("_BackTex", texture2);
                    wWW2 = null;
                    texture2 = null;
                }
                bool flag7 = text4.EndsWith(".jpg") || text4.EndsWith(".png") || text4.EndsWith(".jpeg");
                if (flag7)
                {
                    WWW wWW3 = new WWW(text4);
                    yield return wWW3;
                    Texture2D texture3 = RCextensions.loadimage(wWW3, mipmap, 500000);
                    wWW3.Dispose();
                    material.SetTexture("_LeftTex", texture3);
                    wWW3 = null;
                    texture3 = null;
                }
                bool flag8 = text5.EndsWith(".jpg") || text5.EndsWith(".png") || text5.EndsWith(".jpeg");
                if (flag8)
                {
                    WWW wWW4 = new WWW(text5);
                    yield return wWW4;
                    Texture2D texture4 = RCextensions.loadimage(wWW4, mipmap, 500000);
                    wWW4.Dispose();
                    material.SetTexture("_RightTex", texture4);
                    wWW4 = null;
                    texture4 = null;
                }
                bool flag9 = text6.EndsWith(".jpg") || text6.EndsWith(".png") || text6.EndsWith(".jpeg");
                if (flag9)
                {
                    WWW wWW5 = new WWW(text6);
                    yield return wWW5;
                    Texture2D texture5 = RCextensions.loadimage(wWW5, mipmap, 500000);
                    wWW5.Dispose();
                    material.SetTexture("_UpTex", texture5);
                    wWW5 = null;
                    texture5 = null;
                }
                bool flag10 = text7.EndsWith(".jpg") || text7.EndsWith(".png") || text7.EndsWith(".jpeg");
                if (flag10)
                {
                    WWW wWW6 = new WWW(text7);
                    yield return wWW6;
                    Texture2D texture6 = RCextensions.loadimage(wWW6, mipmap, 500000);
                    wWW6.Dispose();
                    material.SetTexture("_DownTex", texture6);
                    wWW6 = null;
                    texture6 = null;
                }
                Camera.main.GetComponent<Skybox>().material = material;
                FengGameManagerMKII.linkHash[1].Add(key, material);
                FengGameManagerMKII.skyMaterial = material;
                material = null;
                text2 = null;
                text3 = null;
                text4 = null;
                text5 = null;
                text6 = null;
                text7 = null;
            }
            else
            {
                Camera.main.GetComponent<Skybox>().material = (Material)FengGameManagerMKII.linkHash[1][key];
                FengGameManagerMKII.skyMaterial = (Material)FengGameManagerMKII.linkHash[1][key];
            }
            key = null;
        }
        bool flag11 = text.EndsWith(".jpg") || text.EndsWith(".png") || text.EndsWith(".jpeg");
        if (flag11)
        {
            foreach (GameObject gameObject in this.groundList)
            {
                bool flag12 = gameObject != null && gameObject.renderer != null;
                if (flag12)
                {
                    Renderer[] array = gameObject.GetComponentsInChildren<Renderer>();
                    for (int i = 0; i < array.Length; i++)
                    {
                        Renderer renderer = array[i];
                        bool flag13 = !FengGameManagerMKII.linkHash[0].ContainsKey(text);
                        if (flag13)
                        {
                            WWW wWW7 = new WWW(text);
                            yield return wWW7;
                            Texture2D mainTexture = RCextensions.loadimage(wWW7, mipmap, 200000);
                            wWW7.Dispose();
                            bool flag14 = !FengGameManagerMKII.linkHash[0].ContainsKey(text);
                            if (flag14)
                            {
                                flag = true;
                                renderer.material.mainTexture = mainTexture;
                                FengGameManagerMKII.linkHash[0].Add(text, renderer.material);
                                renderer.material = (Material)FengGameManagerMKII.linkHash[0][text];
                            }
                            else
                            {
                                renderer.material = (Material)FengGameManagerMKII.linkHash[0][text];
                            }
                            wWW7 = null;
                            mainTexture = null;
                        }
                        else
                        {
                            renderer.material = (Material)FengGameManagerMKII.linkHash[0][text];
                        }
                        renderer = null;
                    }
                    array = null;
                }
                //TODO : check that
                /*
                gameObject = null;
                */
            }
            List<GameObject>.Enumerator enumerator = default(List<GameObject>.Enumerator);
        }
        else
        {
            if (text.ToLower() == "transparent")
            {
                foreach (GameObject gameObject2 in this.groundList)
                {
                    if (gameObject2 != null && gameObject2.renderer != null)
                    {
                        Renderer[] array2 = gameObject2.GetComponentsInChildren<Renderer>();
                        for (int j = 0; j < array2.Length; j++)
                        {
                            Renderer renderer2 = array2[j];
                            renderer2.enabled = false;
                            renderer2 = null;
                        }
                        array2 = null;
                    }
                    //TODO : check also that
                    /*
                    gameObject2 = null;
                    */
                }
                List<GameObject>.Enumerator enumerator2 = default(List<GameObject>.Enumerator);
            }
        }
        bool flag17 = flag;
        if (flag17)
        {
            this.unloadAssets();
        }
        yield break;
    }

    public void loadconfig()
    {
        int targetFramerate = Configs.getint(GraphicConfig.P.FrameRate);
        if (targetFramerate<=0)
        {
            targetFramerate = -1;
        }
        Application.targetFrameRate = targetFramerate;

        QualitySettings.vSyncCount = Configs.getbool(GraphicConfig.P.Vsync) ? 1 : 0;

        AudioListener.volume = Configs.getfloat(GraphicConfig.P.Volume);
        
        QualitySettings.masterTextureLimit = PlayerPrefs.GetInt("skinQ", 0);
        FengGameManagerMKII.linkHash = new ExitGames.Client.Photon.Hashtable[]
        {
            new ExitGames.Client.Photon.Hashtable(),
            new ExitGames.Client.Photon.Hashtable(),
            new ExitGames.Client.Photon.Hashtable(),
            new ExitGames.Client.Photon.Hashtable(),
            new ExitGames.Client.Photon.Hashtable()
        };
    }

    private void loadskin()
    {
        bool flag = (int)FengGameManagerMKII.settings[64] >= 100;
        if (flag)
        {
            string[] array = new string[]
            {
                "Flare",
                "LabelInfoBottomRight",
                "LabelNetworkStatus",
                "skill_cd_bottom",
                "GasUI"
            };
            GameObject[] array2 = (GameObject[])UnityEngine.Object.FindObjectsOfType(typeof(GameObject));
            int num;
            for (int i = 0; i < array2.Length; i = num + 1)
            {
                GameObject gameObject = array2[i];
                bool flag2 = gameObject.name.Contains("TREE") || gameObject.name.Contains("aot_supply") || gameObject.name.Contains("gameobjectOutSide");
                if (flag2)
                {
                    UnityEngine.Object.Destroy(gameObject);
                }
                num = i;
            }
            GameObject.Find("Cube_001").renderer.material.mainTexture = ((Material)FengGameManagerMKII.RCassets.Load("grass")).mainTexture;
            UnityEngine.Object.Instantiate(FengGameManagerMKII.RCassets.Load("spawnPlayer"), new Vector3(-10f, 1f, -10f), new Quaternion(0f, 0f, 0f, 1f));
            for (int i = 0; i < array.Length; i = num + 1)
            {
                string name = array[i];
                GameObject gameObject2 = GameObject.Find(name);
                bool flag3 = gameObject2 != null;
                if (flag3)
                {
                    UnityEngine.Object.Destroy(gameObject2);
                }
                num = i;
            }
            Camera.main.GetComponent<SpectatorMovement>().disable = true;
            
        }
        else
        {
        
        /*
        this.racingSpawnPoint = new Vector3(0f, 0f, 0f);
              this.racingSpawnPointSet = false;*/
     /*
     this.racingDoors = new List<GameObject>();
     this.allowedToCannon = new Dictionary<int, CannonValues>();
     */
        bool flag8 = !FengGameManagerMKII.level.name.StartsWith("Custom") && Configs.getbool(GraphicConfig.P.MapSkins) && PhotonNetwork.isMasterClient;
            if (flag8)
            {
                string text = string.Empty;
                string text2 = string.Empty;
                string text3 = string.Empty;
                string[] array3 = new string[]
                {
                    string.Empty,
                    string.Empty,
                    string.Empty,
                    string.Empty,
                    string.Empty,
                    string.Empty
                };
                if (FengGameManagerMKII.level.map is Levels.Maps.City)
                {
                    int num;

                    foreach (LevelSkinConfig.P House in LevelSkinConfig.Houses)
                    {
                        text += Configs.getstring(House) + ",";
                    }

                    text.TrimEnd(new char[]
                    {
                        ','
                    });

                    for (int j = 0; j < 250; j++)
                    {
                        text3 += Convert.ToString((int)UnityEngine.Random.Range(0f, 8f));
                    }

                    text2 = string.Concat(new string[]
                    {
                        Configs.getstring(LevelSkinConfig.P.CityGround),
                        ",",
                        Configs.getstring(LevelSkinConfig.P.CityWAll),
                        ",",
                        Configs.getstring(LevelSkinConfig.P.CityH),
                    });

                    for (int i = 0; i < 6; i = num + 1)
                    {
                        array3[i] = Configs.getstring(LevelSkinConfig.CitySkies[i]);
                        num = i;
                    }
                }
                else
                {
                    if (FengGameManagerMKII.level.map is Levels.Maps.Forest)
                    {
                        int num;
                        foreach (LevelSkinConfig.P Tree in LevelSkinConfig.Trees)
                        {
                            text += Configs.getstring(Tree) + ",";
                        }

                        text.TrimEnd(',');
                        foreach (LevelSkinConfig.P Leaf in LevelSkinConfig.Leafs)
                        {
                            text2 += Configs.getstring(Leaf) + ",";
                        }
                        text2 += Configs.getstring(LevelSkinConfig.P.ForestGround);
                        for (int m = 0; m < 150; m = num + 1)
                        {
                            string str = Convert.ToString((int)UnityEngine.Random.Range(0f, 8f));
                            text3 += str;
                            if (Configs.getbool(GraphicConfig.P.RandomForestTex))
                            {
                                text3 += str;
                            }
                            else
                            {
                                text3 += Convert.ToString((int)UnityEngine.Random.Range(0f, 8f));
                            }
                            num = m;
                        }
                        for (int i = 0; i < 6; i = num + 1)
                        {
                            array3[i] = Configs.getstring(LevelSkinConfig.ForestSkies[i]);
                            num = i;
                        }
                    }
                }
                if (PhotonNetwork.isMasterClient)
                {
                    base.photonView.RPC("loadskinRPC", PhotonTargets.AllBuffered, text3, text, text2, array3);
                }
            }
            else
            {
                if (FengGameManagerMKII.level.name.StartsWith("Custom"))
                {
                    GameObject[] array4 = GameObject.FindGameObjectsWithTag("playerRespawn");
                    int num;
                    for (int i = 0; i < array4.Length; i = num + 1)
                    {
                        GameObject gameObject3 = array4[i];
                        gameObject3.transform.position = new Vector3(UnityEngine.Random.Range(-5f, 5f), 0f, UnityEngine.Random.Range(-5f, 5f));
                        num = i;
                    }
                    GameObject[] array2 = (GameObject[])UnityEngine.Object.FindObjectsOfType(typeof(GameObject));
                    for (int i = 0; i < array2.Length; i = num + 1)
                    {
                        GameObject gameObject = array2[i];
                        bool flag15 = gameObject.name.Contains("TREE") || gameObject.name.Contains("aot_supply");
                        if (flag15)
                        {
                            UnityEngine.Object.Destroy(gameObject);
                        }
                        else
                        {
                            bool flag16 = gameObject.name == "Cube_001" && gameObject.transform.parent.gameObject.tag != "player" && gameObject.renderer != null;
                            if (flag16)
                            {
                                this.groundList.Add(gameObject);
                                gameObject.renderer.material.mainTexture = ((Material)FengGameManagerMKII.RCassets.Load("grass")).mainTexture;
                            }
                        }
                        num = i;
                    }
                    if (PhotonNetwork.isMasterClient)
                    {
                        string[] array3 = new string[]
                        {
                            string.Empty,
                            string.Empty,
                            string.Empty,
                            string.Empty,
                            string.Empty,
                            string.Empty,
                            string.Empty
                        };
                        //TODO : Skins for custom maps
                        /*
                        for (int i = 0; i < 6; i = num + 1)
                        {
                            array3[i] = (string)FengGameManagerMKII.settings[i + 175];
                            num = i;
                        }
                        array3[6] = (string)FengGameManagerMKII.settings[162];
                        */

                        /*
                        int titanCap;
                        bool flag17 = int.TryParse((string)FengGameManagerMKII.settings[85], out titanCap);
                        if (flag17)
                        {
                            RCSettings.titanCap = titanCap;
                        }
                        else
                        {
                            RCSettings.titanCap = 0;
                            FengGameManagerMKII.settings[85] = "0";
                        }
                        RCSettings.titanCap = Math.Min(50, RCSettings.titanCap);*/


                        //TODO : gametypes
                        /*
                        base.photonView.RPC("clearlevel", PhotonTargets.AllBuffered, new object[]
                        {
                            array3,
                            RCSettings.gameType
                        });
                        */

                        //TODO : custom maps
                        //FengGameManagerMKII.RCRegions.Clear();

                        for (int i = 0; i < PhotonNetwork.playerList.Length; i = num + 1)
                        {
                            PhotonPlayer photonPlayer = PhotonNetwork.playerList[i];
                            bool flag29 = !photonPlayer.isMasterClient;
                            if (flag29)
                            {
                                this.playersRPC.Add(photonPlayer);
                            }
                            num = i;
                        }

                        //TODO : customLevel
                        /*
                        base.StartCoroutine(this.customlevelE(this.playersRPC));
                        base.StartCoroutine(this.customlevelcache());
                        */
                    }
                }
            }
        }
    }

    private void cache()
    {

        //TODO : recheck all this !!
        ClothFactory.ClearClothCache();
        this.playersRPC.Clear();
        ///   this.titanSpawners.Clear();
        this.groundList.Clear();
        //FengGameManagerMKII.noRestart = false;
        FengGameManagerMKII.skyMaterial = null;
        //this.isSpawning = false;

        //TODO : customlevels
        //this.retryTime = 0f;
        //FengGameManagerMKII.customLevelLoaded = true;

        this.isUnloading = false;
        Time.timeScale = 1f;
        Camera.main.farClipPlane = 1500f;

        //TODO : check also that
        /*
        this.pauseWaitTime = 0f;
        this.spectateSprites = new List<GameObject>();
        */

        //this.isRestarting = false;


        /*

        bool isMasterClient = PhotonNetwork.isMasterClient;
        if (isMasterClient)
        {
            base.StartCoroutine(this.WaitAndResetRestarts());
        }
        */

        this.roundTime = 0f;
        /*
        bool flag2 = FengGameManagerMKII.level.StartsWith("Custom");
        if (flag2)
        {
            FengGameManagerMKII.customLevelLoaded = false;
        }*/

        //TODO : CUSTOMGAMEMODE ROUND MODE
        /*
        if ((int)FengGameManagerMKII.settings[244] == 1)
        {
            this.chatRoom.addLINE("<color=#FFC000>(" + this.roundTime.ToString("F2") + ")</color> Round Start.");
        }*/
    }

    public IEnumerator reloadSky()
    {
        yield return new WaitForSeconds(0.5f);
        bool flag = FengGameManagerMKII.skyMaterial != null && Camera.main.GetComponent<Skybox>().material != FengGameManagerMKII.skyMaterial;
        if (flag)
        {
            Camera.main.GetComponent<Skybox>().material = FengGameManagerMKII.skyMaterial;
        }
        yield break;
    }

    public void unloadAssets()
    {
        bool flag = !this.isUnloading;
        if (flag)
        {
            this.isUnloading = true;
            base.StartCoroutine(this.unloadAssetsE(10f));
        }
    }

    public IEnumerator unloadAssetsE(float time)
    {
        yield return new WaitForSeconds(time);
        Resources.UnloadUnusedAssets();
        this.isUnloading = false;
        yield break;
    }

    private IEnumerator loadskinE(string n, string url, string url2, string[] skybox)
    {
        bool mipmap = Configs.getbool(GraphicConfig.P.Mipmap);
        bool flag = false;
        bool flag3 = skybox.Length > 5 && (skybox[0] != string.Empty || skybox[1] != string.Empty || skybox[2] != string.Empty || skybox[3] != string.Empty || skybox[4] != string.Empty || skybox[5] != string.Empty);
        if (flag3)
        {
            string key = string.Join(",", skybox);
            bool flag4 = !FengGameManagerMKII.linkHash[1].ContainsKey(key);
            if (flag4)
            {
                flag = true;
                Material material = Camera.main.GetComponent<Skybox>().material;
                string text = skybox[0];
                string text2 = skybox[1];
                string text3 = skybox[2];
                string text4 = skybox[3];
                string text5 = skybox[4];
                string text6 = skybox[5];
                bool flag5 = text.EndsWith(".jpg") || text.EndsWith(".png") || text.EndsWith(".jpeg");
                if (flag5)
                {
                    WWW wWW = new WWW(text);
                    yield return wWW;
                    Texture2D texture2D = RCextensions.loadimage(wWW, mipmap, 500000);
                    wWW.Dispose();
                    texture2D.wrapMode = TextureWrapMode.Clamp;
                    material.SetTexture("_FrontTex", texture2D);
                    wWW = null;
                    texture2D = null;
                }
                bool flag6 = text2.EndsWith(".jpg") || text2.EndsWith(".png") || text2.EndsWith(".jpeg");
                if (flag6)
                {
                    WWW wWW2 = new WWW(text2);
                    yield return wWW2;
                    Texture2D texture2D2 = RCextensions.loadimage(wWW2, mipmap, 500000);
                    wWW2.Dispose();
                    texture2D2.wrapMode = TextureWrapMode.Clamp;
                    material.SetTexture("_BackTex", texture2D2);
                    wWW2 = null;
                    texture2D2 = null;
                }
                bool flag7 = text3.EndsWith(".jpg") || text3.EndsWith(".png") || text3.EndsWith(".jpeg");
                if (flag7)
                {
                    WWW wWW3 = new WWW(text3);
                    yield return wWW3;
                    Texture2D texture2D3 = RCextensions.loadimage(wWW3, mipmap, 500000);
                    wWW3.Dispose();
                    texture2D3.wrapMode = TextureWrapMode.Clamp;
                    material.SetTexture("_LeftTex", texture2D3);
                    wWW3 = null;
                    texture2D3 = null;
                }
                bool flag8 = text4.EndsWith(".jpg") || text4.EndsWith(".png") || text4.EndsWith(".jpeg");
                if (flag8)
                {
                    WWW wWW4 = new WWW(text4);
                    yield return wWW4;
                    Texture2D texture2D4 = RCextensions.loadimage(wWW4, mipmap, 500000);
                    wWW4.Dispose();
                    texture2D4.wrapMode = TextureWrapMode.Clamp;
                    material.SetTexture("_RightTex", texture2D4);
                    wWW4 = null;
                    texture2D4 = null;
                }
                bool flag9 = text5.EndsWith(".jpg") || text5.EndsWith(".png") || text5.EndsWith(".jpeg");
                if (flag9)
                {
                    WWW wWW5 = new WWW(text5);
                    yield return wWW5;
                    Texture2D texture2D5 = RCextensions.loadimage(wWW5, mipmap, 500000);
                    wWW5.Dispose();
                    texture2D5.wrapMode = TextureWrapMode.Clamp;
                    material.SetTexture("_UpTex", texture2D5);
                    wWW5 = null;
                    texture2D5 = null;
                }
                bool flag10 = text6.EndsWith(".jpg") || text6.EndsWith(".png") || text6.EndsWith(".jpeg");
                if (flag10)
                {
                    WWW wWW6 = new WWW(text6);
                    yield return wWW6;
                    Texture2D texture2D6 = RCextensions.loadimage(wWW6, mipmap, 500000);
                    wWW6.Dispose();
                    texture2D6.wrapMode = TextureWrapMode.Clamp;
                    material.SetTexture("_DownTex", texture2D6);
                    wWW6 = null;
                    texture2D6 = null;
                }
                Camera.main.GetComponent<Skybox>().material = material;
                FengGameManagerMKII.skyMaterial = material;
                FengGameManagerMKII.linkHash[1].Add(key, material);
                material = null;
                text = null;
                text2 = null;
                text3 = null;
                text4 = null;
                text5 = null;
                text6 = null;
            }
            else
            {
                Camera.main.GetComponent<Skybox>().material = (Material)FengGameManagerMKII.linkHash[1][key];
                FengGameManagerMKII.skyMaterial = (Material)FengGameManagerMKII.linkHash[1][key];
            }
            key = null;
        }
        if (level.map is Levels.Maps.Forest)
        {
            string[] array = url.Split(new char[]
            {
                ','
            });
            string[] array2 = url2.Split(new char[]
            {
                ','
            });
            int num = 0;
            object[] array3 = UnityEngine.Object.FindObjectsOfType(typeof(GameObject));
            object[] array4 = array3;
            for (int i = 0; i < array4.Length; i++)
            {
                GameObject gameObject = (GameObject)array4[i];
                bool flag12 = gameObject != null;
                if (flag12)
                {
                    bool flag13 = gameObject.name.Contains("TREE") && n.Length > num + 1;
                    if (flag13)
                    {
                        string text7 = n.Substring(num, 1);
                        string text8 = n.Substring(num + 1, 1);
                        int num2;
                        int num3 = 0;
                        bool flag14 = int.TryParse(text7, out num2) && int.TryParse(text8, out num3) && num2 >= 0 && num2 < 8 && num3 >= 0 && num3 < 8 && array.Length >= 8 && array2.Length >= 8 && array[num2] != null && array2[num3] != null;
                        if (flag14)
                        {
                            string text9 = array[num2];
                            string text10 = array2[num3];
                            Renderer[] array5 = gameObject.GetComponentsInChildren<Renderer>();
                            for (int j = 0; j < array5.Length; j++)
                            {
                                Renderer renderer = array5[j];
                                bool flag15 = renderer.name.Contains(FengGameManagerMKII.s[22]);
                                if (flag15)
                                {
                                    bool flag16 = text9.EndsWith(".jpg") || text9.EndsWith(".png") || text9.EndsWith(".jpeg");
                                    if (flag16)
                                    {
                                        bool flag17 = !FengGameManagerMKII.linkHash[2].ContainsKey(text9);
                                        if (flag17)
                                        {
                                            WWW wWW7 = new WWW(text9);
                                            yield return wWW7;
                                            Texture2D mainTexture = RCextensions.loadimage(wWW7, mipmap, 1000000);
                                            wWW7.Dispose();
                                            bool flag18 = !FengGameManagerMKII.linkHash[2].ContainsKey(text9);
                                            if (flag18)
                                            {
                                                flag = true;
                                                renderer.material.mainTexture = mainTexture;
                                                FengGameManagerMKII.linkHash[2].Add(text9, renderer.material);
                                                renderer.material = (Material)FengGameManagerMKII.linkHash[2][text9];
                                            }
                                            else
                                            {
                                                renderer.material = (Material)FengGameManagerMKII.linkHash[2][text9];
                                            }
                                            wWW7 = null;
                                            mainTexture = null;
                                        }
                                        else
                                        {
                                            renderer.material = (Material)FengGameManagerMKII.linkHash[2][text9];
                                        }
                                    }
                                }
                                else
                                {
                                    bool flag19 = renderer.name.Contains(FengGameManagerMKII.s[23]);
                                    if (flag19)
                                    {
                                        bool flag20 = text10.EndsWith(".jpg") || text10.EndsWith(".png") || text10.EndsWith(".jpeg");
                                        if (flag20)
                                        {
                                            bool flag21 = !FengGameManagerMKII.linkHash[0].ContainsKey(text10);
                                            if (flag21)
                                            {
                                                WWW wWW8 = new WWW(text10);
                                                yield return wWW8;
                                                Texture2D mainTexture2 = RCextensions.loadimage(wWW8, mipmap, 200000);
                                                wWW8.Dispose();
                                                bool flag22 = !FengGameManagerMKII.linkHash[0].ContainsKey(text10);
                                                if (flag22)
                                                {
                                                    flag = true;
                                                    renderer.material.mainTexture = mainTexture2;
                                                    FengGameManagerMKII.linkHash[0].Add(text10, renderer.material);
                                                    renderer.material = (Material)FengGameManagerMKII.linkHash[0][text10];
                                                }
                                                else
                                                {
                                                    renderer.material = (Material)FengGameManagerMKII.linkHash[0][text10];
                                                }
                                                wWW8 = null;
                                                mainTexture2 = null;
                                            }
                                            else
                                            {
                                                renderer.material = (Material)FengGameManagerMKII.linkHash[0][text10];
                                            }
                                        }
                                        else
                                        {
                                            bool flag23 = text10.ToLower() == "transparent";
                                            if (flag23)
                                            {
                                                renderer.enabled = false;
                                            }
                                        }
                                    }
                                }
                                renderer = null;
                            }
                            array5 = null;
                            text9 = null;
                            text10 = null;
                        }
                        num += 2;
                        text7 = null;
                        text8 = null;
                    }
                    else
                    {
                        bool flag24 = gameObject.name.Contains("Cube_001") && gameObject.transform.parent.gameObject.tag != "Player" && array2.Length > 8 && array2[8] != null;
                        if (flag24)
                        {
                            string text11 = array2[8];
                            bool flag25 = text11.EndsWith(".jpg") || text11.EndsWith(".png") || text11.EndsWith(".jpeg");
                            if (flag25)
                            {
                                Renderer[] array6 = gameObject.GetComponentsInChildren<Renderer>();
                                for (int k = 0; k < array6.Length; k++)
                                {
                                    Renderer renderer2 = array6[k];
                                    bool flag26 = !FengGameManagerMKII.linkHash[0].ContainsKey(text11);
                                    if (flag26)
                                    {
                                        WWW wWW9 = new WWW(text11);
                                        yield return wWW9;
                                        Texture2D mainTexture3 = RCextensions.loadimage(wWW9, mipmap, 200000);
                                        wWW9.Dispose();
                                        bool flag27 = !FengGameManagerMKII.linkHash[0].ContainsKey(text11);
                                        if (flag27)
                                        {
                                            flag = true;
                                            renderer2.material.mainTexture = mainTexture3;
                                            FengGameManagerMKII.linkHash[0].Add(text11, renderer2.material);
                                            renderer2.material = (Material)FengGameManagerMKII.linkHash[0][text11];
                                        }
                                        else
                                        {
                                            renderer2.material = (Material)FengGameManagerMKII.linkHash[0][text11];
                                        }
                                        wWW9 = null;
                                        mainTexture3 = null;
                                    }
                                    else
                                    {
                                        renderer2.material = (Material)FengGameManagerMKII.linkHash[0][text11];
                                    }
                                    renderer2 = null;
                                }
                                array6 = null;
                            }
                            else
                            {
                                bool flag28 = text11.ToLower() == "transparent";
                                if (flag28)
                                {
                                    Renderer[] array7 = gameObject.GetComponentsInChildren<Renderer>();
                                    for (int l = 0; l < array7.Length; l++)
                                    {
                                        Renderer renderer3 = array7[l];
                                        renderer3.enabled = false;
                                        renderer3 = null;
                                    }
                                    array7 = null;
                                }
                            }
                            text11 = null;
                        }
                    }
                }
                gameObject = null;
            }
            array4 = null;
            array = null;
            array2 = null;
            array3 = null;
        }
        else
        {
            if (level.map is Levels.Maps.City)
            {
                string[] array8 = url.Split(new char[]
                {
                    ','
                });
                string[] array9 = url2.Split(new char[]
                {
                    ','
                });
                string text12 = array9[2];
                int num4 = 0;
                object[] array10 = UnityEngine.Object.FindObjectsOfType(typeof(GameObject));
                object[] array11 = array10;
                for (int m = 0; m < array11.Length; m++)
                {
                    GameObject gameObject2 = (GameObject)array11[m];
                    bool flag30 = gameObject2 != null && gameObject2.name.Contains("Cube_") && gameObject2.transform.parent.gameObject.tag != "Player";
                    if (flag30)
                    {
                        bool flag31 = gameObject2.name.EndsWith("001");
                        if (flag31)
                        {
                            bool flag32 = array9.Length != 0 && array9[0] != null;
                            if (flag32)
                            {
                                string text13 = array9[0];
                                bool flag33 = text13.EndsWith(".jpg") || text13.EndsWith(".png") || text13.EndsWith(".jpeg");
                                if (flag33)
                                {
                                    Renderer[] array12 = gameObject2.GetComponentsInChildren<Renderer>();
                                    for (int num5 = 0; num5 < array12.Length; num5++)
                                    {
                                        Renderer renderer4 = array12[num5];
                                        bool flag34 = !FengGameManagerMKII.linkHash[0].ContainsKey(text13);
                                        if (flag34)
                                        {
                                            WWW wWW10 = new WWW(text13);
                                            yield return wWW10;
                                            Texture2D mainTexture4 = RCextensions.loadimage(wWW10, mipmap, 200000);
                                            wWW10.Dispose();
                                            bool flag35 = !FengGameManagerMKII.linkHash[0].ContainsKey(text13);
                                            if (flag35)
                                            {
                                                flag = true;
                                                renderer4.material.mainTexture = mainTexture4;
                                                FengGameManagerMKII.linkHash[0].Add(text13, renderer4.material);
                                                renderer4.material = (Material)FengGameManagerMKII.linkHash[0][text13];
                                            }
                                            else
                                            {
                                                renderer4.material = (Material)FengGameManagerMKII.linkHash[0][text13];
                                            }
                                            wWW10 = null;
                                            mainTexture4 = null;
                                        }
                                        else
                                        {
                                            renderer4.material = (Material)FengGameManagerMKII.linkHash[0][text13];
                                        }
                                        renderer4 = null;
                                    }
                                    array12 = null;
                                }
                                else
                                {
                                    bool flag36 = text13.ToLower() == "transparent";
                                    if (flag36)
                                    {
                                        Renderer[] array13 = gameObject2.GetComponentsInChildren<Renderer>();
                                        for (int num6 = 0; num6 < array13.Length; num6++)
                                        {
                                            Renderer renderer5 = array13[num6];
                                            renderer5.enabled = false;
                                            renderer5 = null;
                                        }
                                        array13 = null;
                                    }
                                }
                                text13 = null;
                            }
                        }
                        else
                        {
                            bool flag37 = gameObject2.name.EndsWith("006") || gameObject2.name.EndsWith("007") || gameObject2.name.EndsWith("015") || gameObject2.name.EndsWith("000") || (gameObject2.name.EndsWith("002") && gameObject2.transform.position.x == 0f && gameObject2.transform.position.y == 0f && gameObject2.transform.position.z == 0f);
                            if (flag37)
                            {
                                bool flag38 = array9.Length != 0 && array9[1] != null;
                                if (flag38)
                                {
                                    string text14 = array9[1];
                                    bool flag39 = text14.EndsWith(".jpg") || text14.EndsWith(".png") || text14.EndsWith(".jpeg");
                                    if (flag39)
                                    {
                                        Renderer[] array14 = gameObject2.GetComponentsInChildren<Renderer>();
                                        for (int num7 = 0; num7 < array14.Length; num7++)
                                        {
                                            Renderer renderer6 = array14[num7];
                                            bool flag40 = !FengGameManagerMKII.linkHash[0].ContainsKey(text14);
                                            if (flag40)
                                            {
                                                WWW wWW11 = new WWW(text14);
                                                yield return wWW11;
                                                Texture2D mainTexture5 = RCextensions.loadimage(wWW11, mipmap, 200000);
                                                wWW11.Dispose();
                                                bool flag41 = !FengGameManagerMKII.linkHash[0].ContainsKey(text14);
                                                if (flag41)
                                                {
                                                    flag = true;
                                                    renderer6.material.mainTexture = mainTexture5;
                                                    FengGameManagerMKII.linkHash[0].Add(text14, renderer6.material);
                                                    renderer6.material = (Material)FengGameManagerMKII.linkHash[0][text14];
                                                }
                                                else
                                                {
                                                    renderer6.material = (Material)FengGameManagerMKII.linkHash[0][text14];
                                                }
                                                wWW11 = null;
                                                mainTexture5 = null;
                                            }
                                            else
                                            {
                                                renderer6.material = (Material)FengGameManagerMKII.linkHash[0][text14];
                                            }
                                            renderer6 = null;
                                        }
                                        array14 = null;
                                    }
                                    text14 = null;
                                }
                            }
                            else
                            {
                                bool flag42 = gameObject2.name.EndsWith("005") || gameObject2.name.EndsWith("003") || (gameObject2.name.EndsWith("002") && (gameObject2.transform.position.x != 0f || gameObject2.transform.position.y != 0f || gameObject2.transform.position.z != 0f) && n.Length > num4);
                                if (flag42)
                                {
                                    string text15 = n.Substring(num4, 1);
                                    int num8;
                                    bool flag43 = int.TryParse(text15, out num8) && num8 >= 0 && num8 < 8 && array8.Length >= 8 && array8[num8] != null;
                                    if (flag43)
                                    {
                                        string text16 = array8[num8];
                                        bool flag44 = text16.EndsWith(".jpg") || text16.EndsWith(".png") || text16.EndsWith(".jpeg");
                                        if (flag44)
                                        {
                                            Renderer[] array15 = gameObject2.GetComponentsInChildren<Renderer>();
                                            for (int num9 = 0; num9 < array15.Length; num9++)
                                            {
                                                Renderer renderer7 = array15[num9];
                                                bool flag45 = !FengGameManagerMKII.linkHash[2].ContainsKey(text16);
                                                if (flag45)
                                                {
                                                    WWW wWW12 = new WWW(text16);
                                                    yield return wWW12;
                                                    Texture2D mainTexture6 = RCextensions.loadimage(wWW12, mipmap, 1000000);
                                                    wWW12.Dispose();
                                                    bool flag46 = !FengGameManagerMKII.linkHash[2].ContainsKey(text16);
                                                    if (flag46)
                                                    {
                                                        flag = true;
                                                        renderer7.material.mainTexture = mainTexture6;
                                                        FengGameManagerMKII.linkHash[2].Add(text16, renderer7.material);
                                                        renderer7.material = (Material)FengGameManagerMKII.linkHash[2][text16];
                                                    }
                                                    else
                                                    {
                                                        renderer7.material = (Material)FengGameManagerMKII.linkHash[2][text16];
                                                    }
                                                    wWW12 = null;
                                                    mainTexture6 = null;
                                                }
                                                else
                                                {
                                                    renderer7.material = (Material)FengGameManagerMKII.linkHash[2][text16];
                                                }
                                                renderer7 = null;
                                            }
                                            array15 = null;
                                        }
                                        text16 = null;
                                    }
                                    int num10 = num4;
                                    num4 = num10 + 1;
                                    text15 = null;
                                }
                                else
                                {
                                    bool flag47 = (gameObject2.name.EndsWith("019") || gameObject2.name.EndsWith("020")) && array9.Length > 2 && array9[2] != null;
                                    if (flag47)
                                    {
                                        string text17 = array9[2];
                                        bool flag48 = text17.EndsWith(".jpg") || text17.EndsWith(".png") || text17.EndsWith(".jpeg");
                                        if (flag48)
                                        {
                                            Renderer[] array16 = gameObject2.GetComponentsInChildren<Renderer>();
                                            for (int num11 = 0; num11 < array16.Length; num11++)
                                            {
                                                Renderer renderer8 = array16[num11];
                                                bool flag49 = !FengGameManagerMKII.linkHash[2].ContainsKey(text17);
                                                if (flag49)
                                                {
                                                    WWW wWW13 = new WWW(text17);
                                                    yield return wWW13;
                                                    Texture2D mainTexture7 = RCextensions.loadimage(wWW13, mipmap, 1000000);
                                                    wWW13.Dispose();
                                                    bool flag50 = !FengGameManagerMKII.linkHash[2].ContainsKey(text17);
                                                    if (flag50)
                                                    {
                                                        flag = true;
                                                        renderer8.material.mainTexture = mainTexture7;
                                                        FengGameManagerMKII.linkHash[2].Add(text17, renderer8.material);
                                                        renderer8.material = (Material)FengGameManagerMKII.linkHash[2][text17];
                                                    }
                                                    else
                                                    {
                                                        renderer8.material = (Material)FengGameManagerMKII.linkHash[2][text17];
                                                    }
                                                    wWW13 = null;
                                                    mainTexture7 = null;
                                                }
                                                else
                                                {
                                                    renderer8.material = (Material)FengGameManagerMKII.linkHash[2][text17];
                                                }
                                                renderer8 = null;
                                            }
                                            array16 = null;
                                        }
                                        text17 = null;
                                    }
                                }
                            }
                        }
                    }
                    gameObject2 = null;
                }
                array11 = null;
                array8 = null;
                array9 = null;
                array10 = null;
            }
        }
        if (flag)
        {
            this.unloadAssets();
        }
        yield break;
    }

    public void addtime(float f)
    {
        this.timeTotalServer -= f;
    }

    public void Updatedproperties(PhotonPlayer sender,int who,ExitGames.Client.Photon.Hashtable newproperties)
    {
        if (!PhotonNetwork.isMasterClient||sender.isLocal)
        {
            return;
        }
        if (newproperties.ContainsKey("kills"))
        {
            if (!sender.isMasterClient)
            {
                if (sender.ID==who)
                {
                    //allows /resetkd
                    if ((int)newproperties["kills"]==0)
                    {
                        return;
                    }
                    else
                    {
                        KickBanManager.kick(sender, "Cheating stats");
                    }
                }
                /*
                else
                {
                    KickBanManager.kick(sender, "Cheating #" + who + " stats");
                }
                */
            }
        }
        if (newproperties.ContainsKey("max_dmg"))
        {
            if (!sender.isMasterClient)
            {
                if (sender.ID == who)
                {
                    //allows /resetkd
                    if ((int)newproperties["max_dmg"] == 0)
                    {
                        return;
                    }
                    else
                    {
                        KickBanManager.kick(sender, "Cheating stats");
                    }
                }
                /*
                else
                {
                    KickBanManager.kick(sender, "Cheating #" + who + " stats");
                }
                */
            }
        }
        if (newproperties.ContainsKey("total_dmg"))
        {
            if (!sender.isMasterClient)
            {
                if (sender.ID == who)
                {
                    //allows /resetkd
                    if ((int)newproperties["total_dmg"] == 0)
                    {
                        return;
                    }
                    else
                    {
                        KickBanManager.kick(sender, "Cheating stats");
                    }
                }
                /*
                else
                {
                    KickBanManager.kick(sender, "Cheating #" + who + " stats");
                }
                */
            }
        }
        if (newproperties.ContainsKey(PhotonPlayerProperty.deaths))
        {
            if (!sender.isMasterClient)
            {
                if (sender.ID == who)
                {
                    //allows /resetkd
                    if ((int)newproperties[PhotonPlayerProperty.deaths] == 0)
                    {
                        return;
                    }
                    else
                    {
                        if (PhotonNetwork.isMasterClient && CustomGameMode.getbool("deathban"))
                        {
                            KickBanManager.ban(sender, "Death = Ban !");
                        }
                        /*
                        if ((int)newproperties[PhotonPlayerProperty.deaths] < (int)sender.customProperties[PhotonPlayerProperty.deaths])
                        {
                            KickBanManager.kick(sender, "Cheating stats");
                        }
                        */
                        //Cant be sure that the player is cheating
                    }
                }
                else
                {
                    if (PhotonNetwork.isMasterClient && CustomGameMode.getbool("deathban"))
                    {
                        KickBanManager.ban(sender, "Death = Ban !");
                    }
                    /*
                    KickBanManager.kick(sender, "Cheating #" + who + " stats");
                    */
                }
            }
        }

        if (newproperties.ContainsKey("name"))
        {
            if ((!sender.isMasterClient) && sender.ID != who)
            {
                if (sender.ID==who&&RCextensions.returnStringFromObject(newproperties[PhotonPlayerProperty.name])!="")
                {
                    KickBanManager.checkbanusername(sender, (string)newproperties["name"]);
                    foreach (PhotonPlayer p in PhotonNetwork.playerList)
                    {
                        if (p!=sender)
                        {
                            if ((string)newproperties[PhotonPlayerProperty.name]==RCextensions.returnStringFromObject(p.customProperties[PhotonPlayerProperty.name]))
                            {
                                KickBanManager.kick(sender, "Taking #" + p.ID + " username");
                            }
                        }
                    }
                    
                }
                else
                {
                    if (PhotonNetwork.isMasterClient)
                    {
                        KickBanManager.kick(sender, "Changing #" + who + " name");
                    }
                    else
                    {
                        FengGameManagerMKII.instance.chat.addLINE("Warning : #" + sender.ID + " changed #" + who + " name");
                    }
                    
                }
            }
        }

    }
}