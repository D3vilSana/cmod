﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public partial class FengGameManagerMKII
{
    public void OnConnectedToMaster()
    {
    }
    public void OnConnectedToPhoton()
    {
    }
    public void OnConnectionFail(DisconnectCause cause)
    {
        UnityEngine.Debug.Log("Connection failed :" + cause.ToString());
        Screen.lockCursor = false;
        Screen.showCursor = true;
        this.gameStart = false;
        NGUITools.SetActive(this.ui.GetComponent<UIReferArray>().panels[0], false);
        NGUITools.SetActive(this.ui.GetComponent<UIReferArray>().panels[1], false);
        NGUITools.SetActive(this.ui.GetComponent<UIReferArray>().panels[2], false);
        NGUITools.SetActive(this.ui.GetComponent<UIReferArray>().panels[3], false);
        NGUITools.SetActive(this.ui.GetComponent<UIReferArray>().panels[4], true);
        GameObject.Find("LabelDisconnectInfo").GetComponent<UILabel>().text = "OnConnectionFail : " + cause.ToString();
    }
    public void OnCreatedRoom()
    {
        this.racingResult = new System.Collections.ArrayList();
        this.teamScores = new int[2];
        Antispammanager.Init();
    }
    public void OnCustomAuthenticationFailed()
    {
    }
    public void OnDisconnectedFromPhoton()
    {
        Screen.lockCursor = false;
        Screen.showCursor = true;
    }
    public void OnFailedToConnectToPhoton()
    {
    }
    public void OnJoinedLobby()
    {
        NGUITools.SetActive(GameObject.Find("UIRefer").GetComponent<UIMainReferences>().panelMultiStart, false);
        ServerList.visible = true;
    }
    public void OnJoinedRoom()
    {
        Antispammanager.Init();
        string[] strArray = PhotonNetwork.room.name.Split('`');
        level = Levels.LevelManager.getbyname(strArray[1]);
        UnityEngine.Debug.Log(level.GetType().ToString());
        if (level!=null)
        {
            UnityEngine.Debug.Log("OnJoinedRoom " + PhotonNetwork.room.name + "    >>>>   " + level.map.mapname);
        }
        this.gameTimesUp = false;
        //level = strArray[1];
        if (strArray[2] == "normal")
        {
            this.difficulty = 0;
        }
        else if (strArray[2] == "hard")
        {
            this.difficulty = 1;
        }
        else if (strArray[2] == "abnormal")
        {
            this.difficulty = 2;
        }
        IN_GAME_MAIN_CAMERA.difficulty = this.difficulty;
        this.time = int.Parse(strArray[3]);
        this.time *= 60;
        if (strArray[4] == "day")
        {
            IN_GAME_MAIN_CAMERA.dayLight = DayLight.Day;
        }
        else if (strArray[4] == "dawn")
        {
            IN_GAME_MAIN_CAMERA.dayLight = DayLight.Dawn;
        }
        else if (strArray[4] == "night")
        {
            IN_GAME_MAIN_CAMERA.dayLight = DayLight.Night;
        }
        if (level!= null)
        {
            level.map.Load();
        }
        ExitGames.Client.Photon.Hashtable hashtable2 = new ExitGames.Client.Photon.Hashtable();
        hashtable2.Add(PhotonPlayerProperty.name, LoginFengKAI.player.name);
        hashtable2.Add(PhotonPlayerProperty.guildName, LoginFengKAI.player.guildname);
        hashtable2.Add(PhotonPlayerProperty.kills, 0);
        hashtable2.Add(PhotonPlayerProperty.max_dmg, 0);
        hashtable2.Add(PhotonPlayerProperty.total_dmg, 0);
        hashtable2.Add(PhotonPlayerProperty.deaths, 0);
        hashtable2.Add(PhotonPlayerProperty.dead, true);
        hashtable2.Add(PhotonPlayerProperty.isTitan, 0);
        PhotonNetwork.player.SetCustomProperties(hashtable2);
        CModVersion.Version.SetMyversion(CModVersion.Version.BuildType.Beta, 0, 1);
        this.humanScore = 0;
        this.titanScore = 0;
        this.PVPtitanScore = 0;
        this.PVPhumanScore = 0;
        this.wave = 1;
        this.highestwave = 1;
        this.localRacingResult = string.Empty;
        BTN.BTN_choose_side.needChooseSide = true;
        this.chatContent = new ArrayList();
        this.killInfoGO = new ArrayList();
        if (!PhotonNetwork.isMasterClient)
        {
            base.photonView.RPC("RequireStatus", PhotonTargets.MasterClient);
        }
    }
    public void OnLeftLobby()
    {
    }
    public void OnLeftRoom()
    {
        if (Application.loadedLevel != 0)
        {
            Time.timeScale = 1f;
            if (PhotonNetwork.connected)
            {
                PhotonNetwork.Disconnect();
            }
            this.gameStart = false;
            Screen.lockCursor = false;
            Screen.showCursor = true;
            Managers.InputManager.ignoreKeys = false;
            UnityEngine.Object.Destroy(GameObject.Find("MultiplayerManager"));
            Application.LoadLevel("menu");
        }
        CustomGameMode.Reset();
    }
    private void OnLevelWasLoaded(int level)
    {
        if ((level != 0) && ((Application.loadedLevelName != "characterCreation") && (Application.loadedLevelName != "SnapShot")))
        {
            ChangeQuality.setCurrentQuality();
            //TODO : Check if this is really useful ??
            foreach (GameObject obj2 in GameObject.FindGameObjectsWithTag("titan"))
            {
                if ((obj2.GetPhotonView() == null) || !obj2.GetPhotonView().owner.isMasterClient)
                {
                    UnityEngine.Object.Destroy(obj2);
                }
            }
            if (gameEndCD.is_running)
            {
                gameEndCD.Abort();
            }
            this.gameStart = true;
            mainCamera = ((GameObject)UnityEngine.Object.Instantiate(Resources.Load("MainCamera_mono"), GameObject.Find("cameraDefaultPosition").transform.position, GameObject.Find("cameraDefaultPosition").transform.rotation)).GetComponent<IN_GAME_MAIN_CAMERA>();
            UnityEngine.Object.Destroy(GameObject.Find("cameraDefaultPosition"));
            mainCamera.gameObject.name = "MainCamera";
            Screen.lockCursor = true;
            Screen.showCursor = true;
            cache();
            this.ui = (GameObject)UnityEngine.Object.Instantiate(Resources.Load("UI_IN_GAME"));
            this.ui.name = "UI_IN_GAME";
            this.ui.SetActive(true);
            NGUITools.SetActive(this.ui.GetComponent<UIReferArray>().panels[0], true);
            NGUITools.SetActive(this.ui.GetComponent<UIReferArray>().panels[1], false);
            NGUITools.SetActive(this.ui.GetComponent<UIReferArray>().panels[2], false);
            NGUITools.SetActive(this.ui.GetComponent<UIReferArray>().panels[3], false);
            mainCamera.setHUDposition();
            mainCamera.setDayLight(IN_GAME_MAIN_CAMERA.dayLight);
           
            PVPcheckPoint.chkPts = new ArrayList();
            mainCamera.enabled = false;
            mainCamera.gameObject.GetComponent<CameraShake>().enabled = false;

            FengGameManagerMKII.level.LevelWasLoaded();


            if (!BTN.BTN_choose_side.needChooseSide)
            {
                if (IN_GAME_MAIN_CAMERA.cameraMode == CAMERA_TYPE.TPS)
                {
                    Screen.lockCursor = true;
                }
                else
                {
                    Screen.lockCursor = false;
                }
                FengGameManagerMKII.level.Spawn(myLastHero);
            }
            if (!PhotonNetwork.isMasterClient)
            {
                base.photonView.RPC("RequireStatus", PhotonTargets.MasterClient);
            }
        }
    }
    public void OnMasterClientSwitched(PhotonPlayer newMasterClient)
    {
        if (PhotonNetwork.player == newMasterClient)
        {
            this.restartGame(true);
        }
        else
        {
            CustomGameMode.Reset();
        }
    }
    public void OnPhotonCreateRoomFailed()
    {
    }
    public void OnPhotonCustomRoomPropertiesChanged()
    {
    }
    public void OnPhotonInstantiate()
    {
    }
    public void OnPhotonJoinRoomFailed()
    {
    }
    public void OnPhotonMaxCccuReached()
    {
    }
    public void OnPhotonPlayerConnected(PhotonPlayer player)
    {
        if (PhotonNetwork.isMasterClient)
        {
            photonView.RPC("setMasterRC", player);
            CustomGameMode.SendGameInfos(player);
            KickBanManager.photonplayerconnected(player);
            KickBanManager.checkban(player);
        }
    }
    public void OnPhotonPlayerDisconnected(PhotonPlayer player)
    {
        if (!this.gameTimesUp)
        {
            this.oneTitanDown(string.Empty, true);
            this.someOneIsDead(0);
        }
        KickBanManager.photonplayerdisconected(player);
        if (Heroes.Contains(player.ID))
        {
            Heroes.Remove(player.ID);
        }
    }
    public void OnPhotonPlayerPropertiesChanged(object[] playerAndUpdatedProps)
    {
        bool flag = playerAndUpdatedProps != null && playerAndUpdatedProps.Length >= 2 && (PhotonPlayer)playerAndUpdatedProps[0] == PhotonNetwork.player;
        if (flag)
        {
            ExitGames.Client.Photon.Hashtable hashtable = (ExitGames.Client.Photon.Hashtable)playerAndUpdatedProps[1];
            bool flag2 = hashtable.ContainsKey("name") && RCextensions.returnStringFromObject(PhotonNetwork.player.customProperties[PhotonPlayerProperty.name]) != RCextensions.returnStringFromObject(hashtable["name"]);
            if (flag2)
            {
                ExitGames.Client.Photon.Hashtable hashtable2 = new ExitGames.Client.Photon.Hashtable();
                hashtable2.Add(PhotonPlayerProperty.name, RCextensions.returnStringFromObject(hashtable["name"]));
                PhotonNetwork.player.SetCustomProperties(hashtable2);
            }
            bool flag3 = hashtable.ContainsKey("guildName") && RCextensions.returnStringFromObject(PhotonNetwork.player.customProperties[PhotonPlayerProperty.guildName]) != LoginFengKAI.player.guildname;
            if (flag3)
            {
                ExitGames.Client.Photon.Hashtable hashtable2 = new ExitGames.Client.Photon.Hashtable();
                hashtable2.Add(PhotonPlayerProperty.guildName, LoginFengKAI.player.guildname);
                PhotonNetwork.player.SetCustomProperties(hashtable2);
            }
        }
    }
    public void OnPhotonRandomJoinFailed()
    {
    }
    public void OnPhotonSerializeView()
    {
    }
    public void OnReceivedRoomListUpdate()
    {
    }
    public void OnUpdatedFriendList()
    {
    }
}
