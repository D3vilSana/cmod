﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

/// <summary>
/// used in order to blacklist the RPC/Instentiates
/// </summary>
static class AntiAbusiveHandler
{
    public static bool CheckRPC(PhotonPlayer p, string name)
    {
        if (p.isLocal||p.isMasterClient)
        {
            return true;
        }

        return true;
    }
    public static bool CheckInstentiate(PhotonPlayer p, ExitGames.Client.Photon.Hashtable datas)
    {
        if (p.isLocal || p.isMasterClient)
        {
            return true;
        }
        string obj = (string)datas[(byte)0];
        if (obj == "FX/rockThrow")
        {
            KickBanManager.ban(p, "instensiating Rocks");
            return false;
        }
        return true;
    }
}

