﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

[Managers.Manager]
static class IngameMenu
{
    public static bool enabled = false;
    private static bool ingame;
    private static List<Menutab> Tabs;

    public static int menusizex = 800;
    public static int menusizey = 600;
    
    public static void Init()
    {
        Tabs = new List<Menutab>();
        foreach (Menutab t in Menutab.getTabs())
        {
            Tabs.Add(t);
        }

        foreach (Menutab t in Tabs)
        {
            t.Init();
        }
        Tabs[0].show();
    }
    public static void AddTab(Menutab t)
    {
        Tabs.Add(t);
    }
    public static void showingame()
    {
        enabled = true;
        ingame = true;
        Screen.showCursor = true;
        Screen.lockCursor = false;
    }
    public static void showinmenu()
    {
        enabled = true;
        ingame = false;
        Screen.showCursor = true;
        Screen.lockCursor = false;
    }
    public static void GUIUpdate()
    {
        if (!enabled)
        {
            return;
        }
        if (!Screen.showCursor)
        {
            Screen.showCursor = true;
            Screen.lockCursor = false;
        }
        GUI.BeginGroup(new Rect((Screen.width / 2) - (menusizex/2), (Screen.height / 2) - (menusizey/2), menusizex, menusizey));
        GUI.Box(new Rect(0, 0, menusizex, menusizey), "CMod Configs :");
        if (GUI.Button(new Rect(0,menusizey-20,45,20),"Close")){Exit();}
        if (ingame)
        {
            if (GUI.Button(new Rect(45, menusizey - 20, 45, 20), "Quit")) { Quit(); }
        }
        Menutab[] r = Tabs.Where(i => i.doshow()).ToArray();
        for (int i = 0; i < r.Length; i++)
        {
            if (GUI.Button(new Rect(10+(75*i),30,70,20),r[i].name))
            {
                Array.ForEach(r, t => t.hide());
                r[i].show();
            }
        }
        
        GUI.BeginGroup(new Rect(0, 50, menusizex, menusizey-100));
        foreach (Menutab t in r)
        {
            t.OnGui(new Vector2(menusizex,menusizey-100));
        }
        GUI.EndGroup();

        GUI.EndGroup();
    }
    public static void Exit()
    {
        enabled = false;
        if (ingame)
        {
            if (IN_GAME_MAIN_CAMERA.cameraMode == CAMERA_TYPE.TPS)
            {
                Screen.showCursor = false;
                Screen.lockCursor = true;
            }
            else
            {
                Screen.showCursor = false;
                Screen.lockCursor = false;
            }
            IN_GAME_MAIN_CAMERA.isPausing = false;
        }
        else
        {
            NGUITools.SetActive(GameObject.Find("UIRefer").GetComponent<UIMainReferences>().panelMain, true);
        }

    }
    private static void Quit()
    {
        Exit();
        if (PhotonNetwork.offlineMode)
        {
            Application.LoadLevel("menu");
        }
        PhotonNetwork.Disconnect();
    }

}

