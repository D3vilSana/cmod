﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

[Managers.Manager]
static class KickBanManager
{
    private static List<string> bantable;
    private static List<int> ignorelist; //same as RCMod's one

    public static void Init()
    {
        bantable = new List<string>();
        ignorelist = new List<int>();
    }
    public static void photonplayerconnected(PhotonPlayer p)
    {

        if (ignorelist != null && ignorelist.Count > 0)
        {
            FengGameManagerMKII.instance.photonView.RPC("ignorePlayerArray", p, new object[]
            {
                        ignorelist.ToArray()
            });
        }
    }
    public static void photonplayerdisconected(PhotonPlayer p)
    {
        if (ignorelist.Contains(p.ID))
        {
            ignorelist.Remove(p.ID);
        }
    }
    public static void kick(PhotonPlayer p)
    {
        PhotonNetwork.DestroyPlayerObjects(p);
        PhotonNetwork.CloseConnection(p);
        FengGameManagerMKII.instance.photonView.RPC("ignorePlayer", PhotonTargets.Others, p.ID ); //For RCMod clients
        object[] objArray2 = new object[] { "<color=#ff0000ff><b>" +RCextensions.returnStringFromObject(p.customProperties[PhotonPlayerProperty.name]).hexColor() + "</b> was kicked</color>","KickBanMngr" };
        FengGameManagerMKII.instance.photonView.RPC("Chat", PhotonTargets.All, objArray2);

    }
    public static void kick(PhotonPlayer p,string reason)
    {
        PhotonNetwork.DestroyPlayerObjects(p);
        PhotonNetwork.CloseConnection(p);
        FengGameManagerMKII.instance.photonView.RPC("ignorePlayer", PhotonTargets.Others, p.ID ); //For RCMod clients
        object[] objArray2 = new object[] { "<color=#ff0000ff><b>" + RCextensions.returnStringFromObject(p.customProperties[PhotonPlayerProperty.name]).hexColor() + "</b> was kicked : "+reason+"</color>", "KickBanMngr" };
        FengGameManagerMKII.instance.photonView.RPC("Chat", PhotonTargets.All, objArray2);

    }
    public static void kick(int ID)
    {
        PhotonNetwork.DestroyPlayerObjects(ID);
        PhotonNetwork.CloseConnection(PhotonPlayer.Find(ID));
        FengGameManagerMKII.instance.photonView.RPC("ignorePlayer", PhotonTargets.Others,  ID ); //For RCMod clients
        PhotonPlayer pp = PhotonNetwork.playerList.First(p => p.ID == ID);
        object[] objArray2 = new object[] { "<color=#ff0000ff><b>[" + RCextensions.returnStringFromObject(pp.customProperties[PhotonPlayerProperty.name]).hexColor() + "]</b> was kicked</color>", "KickBanMngr" };
        FengGameManagerMKII.instance.photonView.RPC("Chat", PhotonTargets.All, objArray2);

    }
    public static bool isignored(int ID)
    {
        return ignorelist.Contains(ID);
    }
    
    public static void ban(PhotonPlayer p)
    {
        
        bantable.Add(RCextensions.returnStringFromObject(p.customProperties[PhotonPlayerProperty.name]));
        object[] objArray2 = new object[] { "<color=#ff0000ff><b>" + RCextensions.returnStringFromObject(p.customProperties[PhotonPlayerProperty.name]).hexColor() + "</b> was banned</color>", "KickBanManager" };
        FengGameManagerMKII.instance.photonView.RPC("Chat", PhotonTargets.All, objArray2);
        PhotonNetwork.DestroyPlayerObjects(p);
        PhotonNetwork.CloseConnection(p);
        FengGameManagerMKII.instance.photonView.RPC("ignorePlayer", PhotonTargets.Others,  p.ID); //For RCMod clients
    }
    public static void ban(PhotonPlayer p,string s)
    {

        bantable.Add(RCextensions.returnStringFromObject(p.customProperties[PhotonPlayerProperty.name]));
        object[] objArray2 = new object[] { "<color=#ff0000ff><b>" + RCextensions.returnStringFromObject(p.customProperties[PhotonPlayerProperty.name]).hexColor() + "</b> was banned : " + s + "</color>", "KickBanMngr" };
        FengGameManagerMKII.instance.photonView.RPC("Chat", PhotonTargets.All, objArray2);
        PhotonNetwork.DestroyPlayerObjects(p);
        PhotonNetwork.CloseConnection(p);
        FengGameManagerMKII.instance.photonView.RPC("ignorePlayer", PhotonTargets.Others, p.ID); //For RCMod clients
    }
    public static void ban(int ID)
    {
        PhotonPlayer pp = PhotonNetwork.playerList.First(p => p.ID == ID);
        string name = RCextensions.returnStringFromObject(pp.customProperties[PhotonPlayerProperty.name]);
        bantable.Add(name);
        object[] objArray2 = new object[] { "<color=#ff0000ff><b>" + name.hexColor() + "</b> was banned</color>", "KickBanManager" };
        FengGameManagerMKII.instance.photonView.RPC("Chat", PhotonTargets.All, objArray2);
        PhotonNetwork.DestroyPlayerObjects(pp);
        PhotonNetwork.CloseConnection(pp);
        FengGameManagerMKII.instance.photonView.RPC("ignorePlayer", PhotonTargets.Others, pp.ID ); //For RCMod clients
    }
    public static void checkban(PhotonPlayer p)
    {
        if (bantable.Contains(RCextensions.returnStringFromObject(p.customProperties[PhotonPlayerProperty.name])))
        {
            PhotonNetwork.DestroyPlayerObjects(p.ID);
            PhotonNetwork.CloseConnection(PhotonPlayer.Find(p.ID));
        }

    }
    public static void checkbanusername(PhotonPlayer p,string name)
    {
        if (bantable.Contains(name))
        {
            PhotonNetwork.DestroyPlayerObjects(p.ID);
            PhotonNetwork.CloseConnection(p);
        }
    }

    public static void ignore(int ID)
    {
        ignorelist.Add(ID);
        PhotonNetwork.DestroyPlayerObjects(ID);
        PhotonNetwork.networkingPeer.HandleEventLeave(ID);
    }

}