﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;


[Managers.Manager(typeof(TextureManager),typeof(FileManager))]
static class CustomGlobalGUI
{
    private static List<GUIElem>[] elms;
    public static void Init()
    {
        elms = new List<GUIElem>[Enum.GetNames(typeof(GUIElem.Position)).Length];
        for (int i = 0; i < elms.Length; i++)
        {
            elms[i] = new List<GUIElem>();
        }
        foreach (Type t in CModUtilities.GetTypesof<GUIElem>())
        {
            AddGUIElm((GUIElem)Activator.CreateInstance(t));
        }

    }
    public static void OnGUI()
    {
        for (int i = 0; i < elms.Length; i++)
        {
            if (elms[i].Any())
            {
                int usedpos = 0;
                switch ((GUIElem.Position)i)
                {
                    case GUIElem.Position.TopLeft:
                        foreach (GUIElem e in elms[i].Where(r=>r.show))
                        {
                            Vector2 s = e.getsize;
                            GUI.BeginGroup(new Rect(usedpos, 0, s.x, s.y));
                            e.OnGui();
                            GUI.EndGroup();
                            usedpos += (int)s.x;
                        }
                        break;
                    case GUIElem.Position.TopMid:
                        usedpos = Screen.width/2;
                        usedpos -= (elms[i].Where(r => r.show).Sum(e =>(int)e.getsize.x))/2;
                        foreach (GUIElem e in elms[i].Where(r => r.show))
                        {
                            Vector2 s = e.getsize;
                            GUI.BeginGroup(new Rect(usedpos, 0, s.x, s.y));
                            e.OnGui();
                            GUI.EndGroup();
                            usedpos += (int)s.x;
                        }
                        break;
                    case GUIElem.Position.TopRight:
                        usedpos = Screen.width;
                        usedpos -= elms[i].Where(r => r.show).Sum(e => (int)e.getsize.x);
                        foreach (GUIElem e in elms[i].Where(r => r.show))
                        {
                            Vector2 s = e.getsize;
                            GUI.BeginGroup(new Rect(usedpos, 0, s.x, s.y));
                            e.OnGui();
                            GUI.EndGroup();
                            usedpos += (int)s.x;
                        }
                        break;
                    case GUIElem.Position.MidLeft:
                        usedpos = Screen.height / 2;
                        usedpos -= (elms[i].Where(r => r.show).Sum(e => (int)e.getsize.y)) / 2;
                        foreach (GUIElem e in elms[i].Where(r => r.show))
                        {
                            Vector2 s = e.getsize;
                            GUI.BeginGroup(new Rect(0, usedpos, s.x, s.y));
                            e.OnGui();
                            GUI.EndGroup();
                            usedpos += (int)s.y;
                        }
                        break;
                    case GUIElem.Position.MidRight:
                        usedpos = Screen.height / 2;
                        usedpos -= (elms[i].Where(r => r.show).Sum(e => (int)e.getsize.y)) / 2;
                        foreach (GUIElem e in elms[i].Where(r => r.show))
                        {
                            Vector2 s = e.getsize;
                            GUI.BeginGroup(new Rect(Screen.width-s.x, usedpos, s.x, s.y));
                            e.OnGui();
                            GUI.EndGroup();
                            usedpos += (int)s.y;
                        }
                        break;
                    case GUIElem.Position.BotLeft:
                        foreach (GUIElem e in elms[i].Where(r => r.show))
                        {
                            Vector2 s = e.getsize;
                            GUI.BeginGroup(new Rect(usedpos, Screen.height-s.y, s.x, s.y));
                            e.OnGui();
                            GUI.EndGroup();
                            usedpos += (int)s.x;
                        }
                        break;
                    case GUIElem.Position.BotMid:
                        usedpos = Screen.width / 2;
                        usedpos -= (elms[i].Where(r => r.show).Sum(e => (int)e.getsize.x)) / 2;
                        foreach (GUIElem e in elms[i].Where(r => r.show))
                        {
                            Vector2 s = e.getsize;
                            GUI.BeginGroup(new Rect(usedpos, Screen.height-s.y, s.x, s.y));
                            e.OnGui();
                            GUI.EndGroup();
                            usedpos += (int)s.x;
                        }
                        break;
                    case GUIElem.Position.BotRight:
                        usedpos = Screen.width;
                        usedpos -= elms[i].Where(r => r.show).Sum(e => (int)e.getsize.x);
                        foreach (GUIElem e in elms[i].Where(r => r.show))
                        {
                            Vector2 s = e.getsize;
                            GUI.BeginGroup(new Rect(usedpos, Screen.height - s.y, s.x, s.y));
                            e.OnGui();
                            GUI.EndGroup();
                            usedpos += (int)s.x;
                        }
                        break;
                    case GUIElem.Position.Custom:
                        foreach (GUIElem e in elms[i].Where(r => r.show))
                        {
                            Vector2 s = e.getsize;
                            GUI.BeginGroup(new Rect(e.custompos.x, e.custompos.y, s.x, s.y));
                            e.OnGui();
                            GUI.EndGroup();

                        }
                        break;
                    case GUIElem.Position.MidMid:
                        usedpos = Screen.height / 2;
                        usedpos -= (elms[i].Where(r => r.show).Sum(e => (int)e.getsize.y)) / 2;
                        foreach (GUIElem e in elms[i].Where(r => r.show))
                        {
                            Vector2 s = e.getsize;
                            GUI.BeginGroup(new Rect((Screen.width - s.x)/2, usedpos, s.x, s.y));
                            e.OnGui();
                            GUI.EndGroup();
                            usedpos += (int)s.y;
                        }
                        break;
                    default:
                        break;
                }
            }
        }
    }
    public static void AddGUIElem<T>() where T : GUIElem,new()
    {
        T t = new T();
        elms[(int)t.pos].Add(t);
        t.Init();
    }
    public static void AddGUIElm(GUIElem e)
    {
        elms[(int)e.pos].Add(e);
        e.Init();
    }
    public static void Update()
    {
        foreach (List<GUIElem> l in elms)
        {
            foreach (GUIElem e in l)
            {
                e.Update();
            }
        }
    }

    public static float GetTopLeftheight()
    {
        float a = 0;
        List<GUIElem> l = elms[(int)GUIElem.Position.TopLeft];
        if (l.Count>0)
        {
            return l[0].getsize.y;
        }
        else
        {
            return 0;
        }
    }
}

public abstract class GUIElem
{
    public enum Position:int { TopLeft,TopMid,TopRight,MidLeft,MidRight,BotLeft,BotMid,BotRight,MidMid,Custom }
    public Vector2 custompos { get { return new Vector2(); } }
    public abstract Position pos { get; internal set; }
    public abstract Vector2 getsize { get; internal set; }
    public abstract bool show { get; internal set; }
    public abstract void OnGui();
    public abstract void Update();
    public abstract void Init();

}
