﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Levels
{
    [Managers.Manager]
    static class LevelManager
    {
        private static Dictionary<Type, Level> levels = new Dictionary<Type, Level>();


        public static void Init()
        {
            foreach (Type t in CModUtilities.GetTypesof<Level>())
            {
                levels.Add(t, (Level)Activator.CreateInstance(t));
            }
        }
        public static Level getbyname(string name)
        {
            Level lv = levels.Values.First(l => l.isMyLevel(name));
            return lv ?? new LevelType.Custom.None(); 
        }
        public static IEnumerable<Level> getLevels()
        {
            return levels.Values;
        }
    }
}
