﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;


[Managers.Manager(typeof(FileManager))]
static class CommandManager
{
    private static List<ChatCommand> commands;
    private static string oldtext = "";
    public static string hints = "";
    private static Dictionary<Type, Parameters.Parameter> defaults;

    public static void Init()
    {
        commands = new List<ChatCommand>();
        defaults = new Dictionary<Type, Parameters.Parameter>();
        foreach (Type t in CModUtilities.GetTypesof<Parameters.Parameter>())
        {
            Parameters.DefaultParamForType p = t.GetCustomAttributes(typeof(Parameters.DefaultParamForType), true).FirstOrDefault() as Parameters.DefaultParamForType;
            if (p!=null)
            {
                defaults.Add(p.type, (Parameters.Parameter)Activator.CreateInstance(t));

            }
        }

        LoadComands();


    }

    private static void LoadComands()
    {
        commands.Add(new Command("pm", delegate (string[] p)
         {
             int i;
             if (int.TryParse(p[0], out i))
             {
                 FengGameManagerMKII.instance.photonView.RPC("ChatPM", PhotonPlayer.Find(i), new object[]
                                                                                     {
                                                                                        RCextensions.returnStringFromObject(PhotonNetwork.player.customProperties[PhotonPlayerProperty.name]).hexColor(),
                                                                                        string.Join(" ",p,1,p.Length-1)
                                                                                     });
                 FengGameManagerMKII.instance.chat.addLINE("<color=#fdcb12> ->[" + i + "]</color> " + RCextensions.returnStringFromObject(PhotonPlayer.Find(i).customProperties[PhotonPlayerProperty.name]).hexColor() + " : " + string.Join(" ", p, 1, p.Length - 1));
             }
         }, new Parameters.PlayerID(), new Parameters.Anytext("Message")));
        commands.Add(new MasterCommand("room", delegate (string[] a)
         {
             int b;
             if (int.TryParse(a[2],out b))
             {
                     FengGameManagerMKII.instance.addtime(b);
             }
         }, new Parameters.StringParam("time"),new Parameters.StringParam("add"), new Parameters.Int("Time")));
        commands.Add(new MasterCommand("kick", delegate (string[] a)
         {
             int b;
             if (int.TryParse(a[0], out b))
             {
                 KickBanManager.kick(b);
             }

         }, new Parameters.PlayerID()));
        commands.Add(new MasterCommand("ban", delegate (string[] a)
        {
            int b;
            if (int.TryParse(a[0], out b))
            {
                KickBanManager.ban(b);
            }

        }, new Parameters.PlayerID()));
        commands.Add(new MasterCommand("spawn", delegate (string[] a)
         {
             
             int b;
             if (int.TryParse(a[0], out b))
             {
                 if (b<0)
                 {
                     return;
                 }
                 for (int i = 0; i < b; i++)
                 {
                     FengGameManagerMKII.instance.randomSpawnOneTitan("titanRespawn");
                 }
                 FengGameManagerMKII.instance.chat.addLINE(" titans spawned !");
             }
         }, new Parameters.Int("Number"), new Parameters.StringParam("titans")));
        commands.Add(new Command("clear", delegate (string[] a)
         {
             FengGameManagerMKII.instance.chat.clear();
         },new Parameters.StringParam("chat")));
        commands.Add(new MasterCommand("spawn", delegate (string[] a)
         {
             int b;
             if (int.TryParse(a[0], out b))
             {
                 int c = Array.IndexOf<string>(Parameters.TitanType.types, a[1]);
                 AbnormalType t = (AbnormalType)c;

                 UnityEngine.GameObject[] objArray = UnityEngine.GameObject.FindGameObjectsWithTag("titanRespawn");
                 int index = UnityEngine.Random.Range(0, objArray.Length);
                 UnityEngine.GameObject obj2 = objArray[index];
                 while (objArray[index] == null)
                 {
                     index = UnityEngine.Random.Range(0, objArray.Length);
                     obj2 = objArray[index];
                 }
                 objArray[index] = null;

                 for (int i = 0; i < b; i++)
                 {
                     UnityEngine.GameObject obj;
                     obj = PhotonNetwork.Instantiate("TITAN_VER3.1", obj2.transform.position, obj2.transform.rotation, 0);
                     obj.GetComponent<TITAN>().setAbnormalType(t, false);
                     UnityEngine.GameObject obj3;
                     obj3 = PhotonNetwork.Instantiate("FX/FXtitanSpawn", obj2.transform.position, UnityEngine.Quaternion.Euler(-90f, 0f, 0f), 0);
                     obj3.transform.localScale = obj.transform.localScale;
                 }
             }
         }, new Parameters.Int("Number"), new Parameters.TitanType()));

        commands.Add(new MasterCommand("spawn", delegate (string[] a)
         {
             int b;
             if (int.TryParse(a[0], out b))
             {
                 int c = Array.IndexOf<string>(Parameters.TitanType.types, a[1]);
                 AbnormalType t = (AbnormalType)c;

                 UnityEngine.GameObject[] objArray = UnityEngine.GameObject.FindGameObjectsWithTag("titanRespawn");
                 int index = UnityEngine.Random.Range(0, objArray.Length);
                 UnityEngine.GameObject obj2 = objArray[index];
                 while (objArray[index] == null)
                 {
                     index = UnityEngine.Random.Range(0, objArray.Length);
                     obj2 = objArray[index];
                 }
                 objArray[index] = null;

                 for (int i = 0; i < b; i++)
                 {
                     UnityEngine.GameObject obj;
                     obj = PhotonNetwork.Instantiate("TITAN_VER3.1", obj2.transform.position, obj2.transform.rotation, 0);
                     obj.GetComponent<TITAN>().setAbnormalType(t, false);
                     UnityEngine.GameObject obj3;
                     obj3 = PhotonNetwork.Instantiate("FX/FXtitanSpawn", obj2.transform.position, UnityEngine.Quaternion.Euler(-90f, 0f, 0f), 0);
                     obj3.transform.localScale = obj.transform.localScale;
                     obj.rigidbody.freezeRotation = false;
                     obj.GetComponent<TITAN>().gravity = 1;
                 }
             }
         }, new Parameters.Int("Number"), new Parameters.StringParam("spacetitans")));

        commands.Add(new MasterCommand("restart", delegate (string[] a)
         {
             FengGameManagerMKII.instance.restartGame(false);
             
         }));
        commands.Add(new MasterCommand("revive", delegate (string[] a)
         {
            int b;
             if (int.TryParse(a[0], out b))
             {
                 PhotonPlayer p = PhotonPlayer.Find(b);
                 if (p!=null)
                 {
                     FengGameManagerMKII.instance.photonView.RPC("respawnHeroInNewRound", p, new object[0]);
                 }
                 else
                 {
                     FengGameManagerMKII.instance.chat.addLINE("<color=#ff0000>ERROR : Player not found</color>");
                 }

             }
             else
             {
                 FengGameManagerMKII.instance.chat.addLINE("<color=#ff0000>ERROR : Bad Argumets</color>");
             }
         },new Parameters.PlayerID()));
        commands.Add(new MasterCommand("revive", delegate (string[] a)
         {
                 FengGameManagerMKII.instance.photonView.RPC("respawnHeroInNewRound", PhotonTargets.AllBuffered);
         }, new Parameters.StringParam("all")));
        commands.Add(new MasterCommand("room", delegate (string[] a)
         {
             int b;
             if (int.TryParse(a[1],out b))
             {
                 PhotonNetwork.room.maxPlayers = b;
             }
             else
             {
                 FengGameManagerMKII.instance.chat.addLINE("FAIL");
             }
         }, new Parameters.StringParam("max"), new Parameters.Int("Maximum")));
        commands.Add(new Command<string,bool>("room", delegate (string s,bool b) {
                PhotonNetwork.room.visible = b;
        }, new Parameters.StringParam("visible"), new Parameters.Boolean()));


        commands.Add(new Command<string,bool>("room", delegate (string s,bool b) {
                PhotonNetwork.room.open = b;
        }, new Parameters.StringParam("open"), new Parameters.Boolean()));

        commands.Add(new MasterCommand("r", delegate (string[] a)
         {
                 FengGameManagerMKII.instance.restartGame(false);
         }));
        addM("tp", new Parameters.StringParam("all"), new Parameters.Vector3(), delegate (string s, UnityEngine.Vector3 v)
           {
               foreach (HERO i in FengGameManagerMKII.Heroes.Values)
               {
                   i.photonView.RPC("moveToRPC", i.photonView.owner, new object[3] { v.x,v.y,v.z });
               }
           });


        commands.Add(new Command("link", s => Parameters.ChatLinkID.exec(s[0]), new Parameters.ChatLinkID(),new Parameters.StringParam("open")));
        
        FileManager.Datadirectory d = new FileManager.Datadirectory("skins","skin");

        add("skin", new Parameters.StringParam("load"), d.fileparameter, delegate (string a, FileManager.DataFile f)
           {
               SkinManager.Setskin(f);
           });
    add("quote", new Parameters.StringParam("chat"), new Parameters.ChatMessageIDs(), delegate (string a, List<ChatHandler.Chatcontent> l)
           {
             string s = "<b><i>---<color=#fd0c1f>[QUOTE]</color>---</i></b>\n";
             s+= string.Join("\n", l.Select(r => r.text).ToArray());
             s += "\n<b><i>-<color=#fd0c1f>[END QUOTE]</color>-</i></b>";
             FengGameManagerMKII.instance.photonView.RPC("Chat",PhotonTargets.All,new object[]{" ",s});
           });
        FileManager.Datadirectory dd = new FileManager.Datadirectory("quotes", "txt");
        add("quote", new Parameters.StringParam("save"), new Parameters.ChatMessageIDs(),new Parameters.Anytext("file name"), delegate (string a, List<ChatHandler.Chatcontent> l,string name)
           {
             dd.writeinnewfile(string.Join("\n", l.Select(r => r.text).ToArray()), name);
             FengGameManagerMKII.instance.chat.addLINE("Sucess !!");
           });
        add("quote", new Parameters.StringParam("load"), dd.fileparameter, delegate (string a, FileManager.DataFile l)
           {
             string s = "<b><i>---<color=#fd0c1f>[QUOTE]</color>---</i></b>\n";
             s += l.alltext;
             s += "\n<b><i>-<color=#fd0c1f>[END QUOTE]</color>-</i></b>";
             FengGameManagerMKII.instance.photonView.RPC("Chat", PhotonTargets.All," ", s );

           });

        commands.Add(new Command("clear", delegate (string[] a)
         {
             long b = GC.GetTotalMemory(false);
             long af;
             FengGameManagerMKII.instance.chat.addLINE("Memory used Before Cleaning cache :<color=#FF0000>" + b+"</color>");
             //TODO : clear all the caches
             PhotonNetwork.PrefabCache = new Dictionary<string, UnityEngine.GameObject>();
             TextureManager.clearCache();
             ClothFactory.ClearClothCache();
             UnityEngine.Resources.UnloadUnusedAssets();
             GC.Collect();
             af = GC.GetTotalMemory(true);
             FengGameManagerMKII.instance.chat.addLINE("Memory used After Cleaning cache :<color=#00FF00>" + af+"</color>");
             FengGameManagerMKII.instance.chat.addLINE("Memory Freed :<color=#0000FF>" + (b-af) + "</color>");
             FengGameManagerMKII.instance.chat.addLINE("<b>Note:</b>Game may freeze a little bit in the next seconds !");
         }, new Parameters.StringParam("cache")));



        addM("tp",new Parameters.StringParam("me"),new Parameters.Vector3(), delegate (string s,UnityEngine.Vector3 pos)
         {
             FengGameManagerMKII.instance.myhero.transform.position = pos;
         });



        addM("tp", delegate (HERO h, UnityEngine.Vector3 pos)
         {
             h.photonView.RPC("moveToRPC", h.photonView.owner, new object[3] { pos.x, pos.y, pos.z });
         });
        add("commands", delegate ()
        {
            foreach (ChatCommand c in commands)
            {
                FengGameManagerMKII.instance.chat.addLINE(" /"+c.show());
            }
            
        });
        

    }

    /// <summary>
    /// execute a command
    /// </summary>
    /// <param name="text"></param>
    /// <returns>true if text is a command</returns>
    public static bool parse(string text)
    {
        hints = "";
        oldtext = "";
        text = text.Trim(' ','\t');
        if (!text.StartsWith("/"))
        {
            return false;
        }
        string[] p = text.Split(new char[1] { ' ' }, 2);
        if (p.Length==1)
        {
            p = new string[2] { text, "" };
        }
        ChatCommand[] c = commands.Where(u => ("/" + u.name) == p[0]).Where(u => u.isme(p[1])).ToArray();
        if (c.Length != 0)
        {
            c[0].exec(p[1]);
        }
        return true;
    }

    public static void autocompupdate(string text)
    {
        bool b = text.EndsWith(" ");
        text = text.TrimStart(' ', '\t');
        if (oldtext == text)
        {
            return;
        }
        oldtext = text;
        hints = "";
        if (!text.StartsWith("/"))
        {
            return;
        }


        string[] p = text.Split(new char[1] { ' ' }, 2);
        if (p.Length == 1)
        {
            p = new string[2] { text, "" };
        }
        if (p[1]=="")
        {
            if (!b)
            {//first parameter
                string[] m = commands.Where(q => q.enabled).Select(q=>q.name).Where(q=>("/"+q).StartsWith(p[0])).Distinct().ToArray();
                hints = "<b><color=#8113b6>"+String.Join(" ", m)+"</color></b>";
            }
            else
            {//second parameter
                IEnumerable<ChatCommand> m = commands.Where(q => q.enabled).Where(y => "/"+y.name ==p[0]);
                List<string> hint = new List<string>();
                foreach (Command com in m)
                {
                    hint.AddRange(com.getpossibleparameters(""));
                }
                hints = "<b><i><color=#48b613>" + String.Join(" ", hint.Distinct().ToArray())+"</color></i></b>";
            }
        }
        else
        {
            IEnumerable<ChatCommand> m = commands.Where(q => q.enabled).Where(y =>"/"+y.name == p[0]);
            List<string> hint = new List<string>();
            foreach (ChatCommand comm in m)
            {
                hint.AddRange(comm.getpossibleparameters(p[1]));
            }
            hints = "<b><i><color=#48b613>" + String.Join(" ", hint.Distinct().ToArray()) + "</color></i></b>";
        }

    }

    public static string getcompletion(string text)
    {
        bool b = text.EndsWith(" ");
        text = text.TrimStart(' ', '\t');

        oldtext = text;
        hints = "";
        if (!text.StartsWith("/"))
        {
            return "";
        }


        string[] p = text.Split(new char[1] { ' ' }, 2);
        if (p.Length == 1)
        {
            p = new string[2] { text, "" };
        }
        List<string> hint = new List<string>();
        if (p[1] == "")
        {
            if (!b)
            {//first parameter
                hint = commands.Where(q => q.enabled).Select(q => q.name).Where(q => ("/" + q).StartsWith(p[0])).Distinct().ToList();

            }
            else
            {//second parameter
                IEnumerable<ChatCommand> m = commands.Where(q => q.enabled).Where(y => "/" + y.name == p[0]);
                foreach (ChatCommand com in m)
                {
                    hint.AddRange(com.getpossibleparameters(""));
                }

            }
        }
        else
        {
            IEnumerable<ChatCommand> m = commands.Where(q => q.enabled).Where(y => "/" + y.name == p[0]);
            
            foreach (ChatCommand comm in m)
            {
                hint.AddRange(comm.getpossibleparameters(p[1]));
            }
        }
        if (hint.Count!=0)
        {
            return hint[0];
        }
        else
        {
            return "";
        }
        
    }
    private static void add(string name,Action a)
    {
        commands.Add(new Command(name, s => a.Invoke()));
    }
    private static void add<T>(string name,Parameters.Parameter<T> p,Action<T> ac)
    {
        commands.Add(new Command<T>(name, ac, p));
    }
    private static void add<T,U>(string name,Parameters.Parameter<T> p,Parameters.Parameter<U> p1,Action<T,U> ac)
    {
        commands.Add(new Command<T,U>(name, ac, p,p1));
    }

    private static void add<T,U,V>(string name,Parameters.Parameter<T> p,Parameters.Parameter<U> p1,Parameters.Parameter<V> p2,Action<T,U,V> ac)
    {
        commands.Add(new Command<T,U,V>(name, ac, p,p1,p2));
    }

    private static Parameters.Parameter<T> toParam<T>()
    {
        if (defaults.ContainsKey(typeof(T)))
        {
            return (Parameters.Parameter<T>)defaults[typeof(T)];
        }
        if (typeof(T).IsEnum)
        {
            return new Parameters.PEnum<T>();
        }
        UnityEngine.Debug.LogError("Can't convert the parameter");
        return null;
    }

    /// <summary>
    /// add a command
    /// </summary>
    /// <typeparam name="T">type of the parameter</typeparam>
    /// <param name="name">name of the command</param>
    /// <param name="a">action to invoke</param>
    private static void add<T>(string name,Action<T> a)
    {
        commands.Add(new Command<T>(name, a, toParam<T>()));
    }
    private static void add<T,U>(string name,Action<T,U> a)
    {
        commands.Add(new Command<T, U>(name, a, toParam<T>(), toParam<U>()));
    }

    private static void addM<T>(string name,Action<T> a)
    {
        commands.Add(new MasterCommand<T>(name, a, toParam<T>()));
    }
    private static void addM<T,U>(string name,Action<T,U> a)
    {
        commands.Add(new MasterCommand<T, U>(name, a, toParam<T>(), toParam<U>()));
    }
    private static void addM<T,U,V>(string name,Action<T,U,V> a)
    {
        commands.Add(new MasterCommand<T, U, V>(name, a, toParam<T>(), toParam<U>(), toParam<V>()));
    }
    private static void addM<T>(string name, Parameters.Parameter<T> p, Action<T> ac)
    {
        commands.Add(new MasterCommand<T>(name, ac, p));
    }
    private static void addM<T, U>(string name, Parameters.Parameter<T> p, Parameters.Parameter<U> p1, Action<T, U> ac)
    {
        commands.Add(new MasterCommand<T, U>(name, ac, p, p1));
    }
}

