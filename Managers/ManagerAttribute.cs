﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Managers
{
    /// <summary>
    /// Say that the Class is a manager and specify his required managers (if any)
    /// </summary>
    [System.AttributeUsage(AttributeTargets.Class, Inherited = false, AllowMultiple = true)]
    sealed class Manager : Attribute
    {
        private static Action[] updates;

        private static readonly int Maximumloops = 500;

        public Type[] mymanagers;
        public Manager(params Type[] requirement) {
            mymanagers = requirement;
        }

        public static void InitAll()
        {
            IEnumerable<Type> u =
                  from a in AppDomain.CurrentDomain.GetAssemblies()
                  from t in a.GetTypes()
                  where t.GetCustomAttributes(typeof(Manager), false).Count() ==1
                  select t;


            // Init all the Managers
            Dictionary<Type, List<Type>> v = new Dictionary<Type, List<Type>>(u.Count());
            foreach (Type l in u)
            {
                v.Add(l, ((Manager)l.GetCustomAttributes(typeof(Manager), false)[0]).mymanagers.ToList());
            }
            for (int i = 0; i < Maximumloops&&(v.Count != 0); i++)
            {
                List<Type> removed = new List<Type>();
                foreach (var elem in v.Where(t=>t.Value.Count==0))
                {
                    try
                    {
                        elem.Key.GetMethod("Init", System.Reflection.BindingFlags.Public | System.Reflection.BindingFlags.Static).Invoke(null, null);
                        UnityEngine.Debug.Log(elem.Key.ToString() + " initialised");
                    }
                    catch (Exception e)
                    {
                        UnityEngine.Debug.LogException(e);
                    }
                    removed.Add(elem.Key);
                }
                removed.ForEach(r => v.Remove(r));
                foreach (var elem in v)
                {
                    elem.Value.RemoveAll(w => removed.Contains(w));
                }

            }
            List<Action> ups = new List<Action>();
            //Building the UpdateList
            foreach (Type l in u)
            {
                System.Reflection.MethodInfo method = l
           .GetMethod("Update",
                      System.Reflection.BindingFlags.Public
                      | System.Reflection.BindingFlags.Static);
                if (method != null)
                {
                   ups.Add((Action)Delegate.CreateDelegate(typeof(Action), method));
                }
            }
            Manager.updates = ups.Where(a=>a!=null).ToArray();
        }

        public static void UpdateAll()
        {
            foreach (Action a in updates)
            {
                a.Invoke();
            }
        }

    }
}
