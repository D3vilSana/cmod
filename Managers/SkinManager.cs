﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

class SkinManager : ClassicConfig
{
    public enum P {Horse,Hair,Eye,Glass,Face,Skin,Costume,Logo,BladeL,BladeR,Gas,Hoodie,Trail }
    public override Type enum_config
    {
        get
        {
            return typeof(P);
        }
    }

    public override string name
    {
        get
        {
            return "Skin";
        }
    }

    public override object defaultval(int val)
    {
        return "";
    }
    public override void load()
    {
        //base.load();
        startload(200,60,60);
    }
    public override void init()
    {
        
    }

    public override void Onvalchanged(int val)
    {
        
    }

    protected override Valtypes typeofkey(int key)
    {
        return Valtypes.String;
    }

    public static void Setskin(FileManager.DataFile skinfile)
    {
        foreach (P i in Enum.GetValues(typeof(P)))
        {
            Configs.set(i,skinfile.json[i.ToString()].Value);
            UnityEngine.Debug.Log(i + "<=" + skinfile.json[i.ToString()].Value);
        }
        FengGameManagerMKII.instance.myhero.loadskin();
    }
    public static void SetDefaultSkin()
    {
        foreach (P i in Enum.GetValues(typeof(P)))
        {
            Configs.set(i, "");
        }
        FengGameManagerMKII.instance.myhero.loadskin();
    }
}

