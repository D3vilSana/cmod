﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CustomMaps
{
    [Managers.Manager]
    static class CustomMapsManager
    {
        public static void Init()
        {

        }

        public static void StartLoadMap(string[] map)
        {
            if (map.Length == 1)
            {
                if (map[0] == "loadcached")
                {
                    UnityEngine.Debug.Log("Custommape : loadcache");
                    return; //TODO !!!!!
                }
                if (map[0] == "loadempty")
                {
                    UnityEngine.Debug.Log("Custommaps:loadempty");
                    return; // TODO !!!!!
                }
            }

            if (map[map.Length - 1].StartsWith("a"))
            {
                UnityEngine.Debug.LogError("CustomMap , startwith A");
                return;
            }
            else if (map[map.Length - 1].StartsWith("z"))
            {
                UnityEngine.Debug.LogError("CustomMap , startWith Z");
                return;
            }
                foreach (string line in map)
                {
                    CallType.execLine(line);
                }
                UnityEngine.Debug.Log("Custom map loaded o/");

            //FengGameManagerMKII.instance.RespawnPlayer();
            FengGameManagerMKII.level.LevelWasLoaded();
            //TODO : cache the map in customproperties
        }
    }
}
