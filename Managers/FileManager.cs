﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using SimpleJSON;


[Managers.Manager]
public static class FileManager
{
    private static string moddirectory = UnityEngine.Application.dataPath+"/../CMod_Datas";
    public static void Init()
    {
        if (!Directory.Exists(moddirectory))
        {
            Directory.CreateDirectory(moddirectory);
        }
    }
    public class Datadirectory
    {
        private string path;
        private string ext;
        public Parameters.Parameter<DataFile> fileparameter { get { return new FileSelector(path,ext,this); } }

        public Datadirectory(string path,string ext="")
        {
            this.ext = ext;
            this.path = moddirectory+"\\"+path;
            if (!Directory.Exists(this.path))
            {
                Directory.CreateDirectory(this.path);
            }
        }
        private Datadirectory(string path,string ext="",bool usemoddir = true)
        {
            this.ext = ext;
            if (usemoddir)
            {
                this.path = moddirectory + "\\" + path;
            }
            else
            {
                this.path = path;
            }

        }
        /// <summary>
        /// writes the text in a new file
        /// </summary>
        /// <param name="text"></param>
        public void writeinnewfile(string text,string additionnalname="")
        {
            string s = (path + "\\" + additionnalname.Replace(' ', '_') + "_" + DateTime.Now.ToString("dd_MMM_yyyy_HH-mm-ss") + '.' + ext);
            File.WriteAllText(s, text);
        }

        private Datadirectory getSubDirectory(string path)
        {
            return new Datadirectory(this.path + "\\" + path, ext, true);
        }
        private class FileSelector : Parameters.Parameter<DataFile>
        {
            private DirectoryInfo info;
            string ext;
            private Datadirectory d;
            public FileSelector(string path,string ext,Datadirectory d)
            {
                this.ext = ext;
                this.info = new DirectoryInfo(path);
                this.d = d;
            }
            public override bool canbeme(string parameter)
            {
                return info.GetFiles(parameter + "*"+(ext!=""?'.'+ext:"")).Any();
                //return Directory.GetFiles(path).Any(s => s.ToString().StartsWith(parameter));
            }

            public override List<string> getpossibleparameters(string start)
            {
                return info.GetFiles(start + '*' + (ext != "" ? '.' + ext : "")).Select(f => f.Name).ToList();
              //  DirectoryInfo i = new DirectoryInfo(p);
               //return Directory.GetFiles(path).Where(s => s.ToString().StartsWith(start)).ToList();
                
            }

            public override bool isme(string param)
            {
                return info.GetFiles(param).Any();
               // return File.Exists(path + "\\" + param);
            }

            public override string sringshow()
            {
                return "FILE";
            }

            public override DataFile Parse(string param)
            {
                return d.getfile(param);
            }
        }

        public DataFile getfile(string name)
        {
            return new DataFile(path + "\\" + name);
        }

    }
    public class DataFile
    {
        private string f;
        public JSONClass json;


        public string alltext {
            get {
                if (File.Exists(f))
                {
                    return File.ReadAllText(f);
                }
                else
                {
                    return "";
                }
            }
            set { File.WriteAllText(f, value); }
        }
        public string[] lines { get { return File.ReadAllLines(f); } }
       
        public string fullpath
        {
            get
            {
                return Path.GetFullPath(f);
            }
        }
         
        public void flushjson() {
            string s = json.ToString();
            s = s.Replace("\",", "\",\n");
            alltext = s;
        }


        public DataFile(string path)
        {
            this.f = path;
            JSONNode n;
            try
            {
                n = JSON.Parse(alltext);
            }
            catch (Exception)
            {

                n = null;
            }
            
            
            if (n==null)
            {
                json = new JSONClass();
            }
            else
            {
                json = n.AsObject;
            }

        }

    }
        
}
