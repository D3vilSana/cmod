﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;

[Managers.Manager]
public static class CustomGameMode
{
    public static ExitGames.Client.Photon.Hashtable mode { get; internal set; }
    public static List<Mode> modes;


    public static void Init()
    {
        modes = new List<Mode>();
        mode = new ExitGames.Client.Photon.Hashtable();
        
        modes.Add(new RestartRCMode<bool, int>("bomb", "Bomb", false));
        modes.Add(new RCMode<bool, int>("globalDisableMinimap", "DisableMinimap", false));
        modes.Add(new RCMode<int, int>("team", "TeamMode", 0));
        modes.Add(new MaxRCMode<int, int>("titanc", "TitanNumber", 0, 50));
        modes.Add(new RCMode<int, int>("damage", "Nape", 0)); // DONE ! 
        modes.Add(new RCMode<bool, int>("sizeMode", "CustomSize", false));
        modes.Add(new RCMode<float, float>("sizeLower", "MinSize", 0));
        modes.Add(new RCMode<float, float>("sizeUpper", "MaxSize", 0));
        modes.Add(new RCMode<bool, int>("spawnMode", "CustomSpawnModes",false));
        modes.Add(new RCMode<float, float>("nRate", "SpawnRate_N", 0));
        modes.Add(new RCMode<float, float>("aRate", "SpawnRate_A", 0));
        modes.Add(new RCMode<float, float>("jRate", "SpawnRate_J", 0));
        modes.Add(new RCMode<float, float>("cRate", "SpawnRate_C", 0));
        modes.Add(new RCMode<float, float>("pRate", "SpawnRate_P", 0));

        modes.Add(new RCMode<int, int>("pvp", "PVPMode",0));
        modes.Add(new RCMode<int, int>("endless", "RespawnTime", 0));
        modes.Add(new RCMode<bool, int>("horse", "Horses", false));//Implemented
        //modes.Add(new InternMode<bool>("KickGuest", true));
        modes.Add(new InternMode<bool>("deathban", false));
        modes.Add(new InternMode<bool>("nopvphook", false));
        modes.Add(new Motd(""));
            


    }
    #region getters
    public static int getint(string name)
    {
        Mode m = getMode(name);
        if (m != null)
        {
            return m.toint();
        }
        else
        {
            //MyLog.Debug("[CustomGameMode.getint(" + name + ") : Object not found !!!!!", MyLog.LogType.CRITICAL);
            return default(int);
        }
    }
    public static bool getbool(string name)
    {
        Mode m = getMode(name);
        if (m != null)
        {
            return m.tobool();
        }
        else
        {
            //MyLog.Debug("[CustomGameMode.getbool(" + name + ") : Object not found !!!!!", MyLog.LogType.CRITICAL);
            return default(bool);
        }
    }
    public static float getfloat(string name)
    {
        Mode m = getMode(name);
        if (m != null)
        {
            return m.tofloat();
        }
        else
        {
            //MyLog.Debug("[CustomGameMode.getfloat(" + name + ") : Object not found !!!!!", MyLog.LogType.CRITICAL);
            return default(float);
        }
    }
    public static string getstring(string name)
    {
        Mode m = getMode(name);
        if (m != null) {
            return m.tostring();
        }else{
            return "";
        }
    }
    public static Mode getMode(string name)
    {
        return modes.Find(m => m.pname == name);
    }
    #endregion
    public static void SendGameInfos(PhotonPlayer p)
    {
        //MyLog.Debug("Sending game infos to : " + p.ID + " Dsize = " + mode.Count, MyLog.LogType.LOG);
        FengGameManagerMKII.instance.photonView.RPC("settingRPC", p, new object[1]
{
                    mode
});
        string s = "";
        foreach (Mode m in modes)
        {
            string f = m.onplayerlogged();
            if (f!="")
            {
                s += f+"\n";
            }
            
        }
        object[] objArray2 = new object[] { "<color=#f5ae0f>" + s + " </color>", " " };
        FengGameManagerMKII.instance.photonView.RPC("Chat",p, objArray2);
    }
    public static void SendGameInfos()
    {
        FengGameManagerMKII.instance.photonView.RPC("settingRPC", PhotonTargets.Others,mode);
    }
    public static void UpdateModes(ExitGames.Client.Photon.Hashtable ht)
    {
        UnityEngine.GameObject.Find("Chatroom").GetComponent<InRoomChat>().addLINE("<color=#6e7b8a>The config is updated !! check the configs</color>");
        if (ht==null)
        {
            return;
        }
        
        foreach (Mode m in modes)
        {
            RCMode rmod;
            if ((rmod = m as RCMode)!=null)
            {
                if (ht.ContainsKey(rmod.name))
                {
                    UnityEngine.Debug.LogError("Updated " + rmod.name + " config " + rmod.tostring() + "->" + ht[rmod.name].ToString());
                    rmod.set(ht[rmod.name].ToString());
                }
            }
        }
        mode = ht;

    }

    public static void Reset()
    {
        mode = new ExitGames.Client.Photon.Hashtable();
        foreach (Mode m in modes)
        {
            m.todefault();
        }
    }

    public static void SetMotd(string motd)
    {
        mode["motd"] = motd;
    }

    public abstract class Mode
    {
        public abstract int toint();
        public abstract float tofloat();
        public abstract bool tobool();
        public abstract string tostring();
        public string pname;
        public abstract Type ptype();
        public abstract bool set(string val,bool updateRC = true);
        public abstract string onplayerlogged();
        public abstract void todefault();

    }
    public abstract class RCMode : Mode {
        protected string hname;
        public string name { get { return hname; } }
    }
    public class RCMode<T, U> : RCMode
    {
        public RCMode(string name, string pubname, T defaultval)
        {
            hname = name;
            pname = pubname;
            currentval = defaultval;
            this.defaultval = defaultval;
            sendRCinfos();

            
        }

        
        public T currentval;
        private T defaultval;
        public string publicname { get { return pname; } }
        public void sendRCinfos()
        {
            if (!PhotonNetwork.isMasterClient)
            {
                return;
            }
            if (typeof(T) == typeof(bool) && typeof(U) == typeof(int))
            {
                if ((bool)(object)currentval)
                {
                    if (!mode.ContainsKey(hname))
                    {
                        mode.Add(hname, 1);
                    }
                }
                else
                {
                    if (mode.ContainsKey(hname))
                    {
                        mode.Remove(hname);
                    }
                }
            }
            else if (typeof(T) == typeof(U))
            {
                if (mode.ContainsKey(hname))
                {
                    if (!currentval.Equals((T)mode[hname]))
                    {
                        mode[hname] = currentval;
                    }
                }
                else
                {
                    mode.Add(hname, currentval);
                }

            }
            else
            {
                UnityEngine.Debug.Log("CustomGameMode.pushmode() ,in <b>" + name + "(" + hname + ")</b> types not handled ! T=" + typeof(T) + " U=" + typeof(U));
            }
            SendGameInfos();
        }
        public override bool tobool()
        {
            if (typeof(T) == typeof(bool))
            {
                return (bool)(object)currentval;
            }
            else if (typeof(T) == typeof(int))
            {
                return (int)(object)currentval > 0;
            }
            else if (typeof(T) == typeof(float))
            {
                return (float)(object)currentval > 0;
            }
            else
            {
                return default(bool);
            }
        }
        public override int toint()
        {
            if (typeof(T) == typeof(bool))
            {
                return ((bool)(object)currentval) ? 1 : 0;
            }
            else if (typeof(T) == typeof(int))
            {
                return (int)(object)currentval;
            }
            else if (typeof(T) == typeof(float))
            {
                return (int)(float)(object)currentval;
            }
            else
            {
                return default(int);
            }
        }
        public override float tofloat()
        {
            if (typeof(T) == typeof(bool))
            {
                return ((bool)(object)currentval) ? 1 : 0;
            }
            else if (typeof(T) == typeof(int))
            {
                return (int)(object)currentval;
            }
            else if (typeof(T) == typeof(float))
            {
                return (float)(object)currentval;
            }
            else
            {
                return default(int);
            }
        }
        public override string tostring()
        {
            if (typeof(T) == typeof(bool))
            {
                return ((bool)(object)currentval).ToString();
            }
            else if (typeof(T) == typeof(int))
            {
                return ((int)(object)currentval).ToString();
            }
            else if (typeof(T) == typeof(float))
            {
                return ((float)(object)currentval).ToString();
            }
            else
            {
                return String.Empty;
            }
        }
        public override Type ptype()
        {
            return typeof(T);
        }
        public override bool set(string val,bool updaterc = true)
        {
            if (typeof(T) == typeof(bool))
            {
                currentval = (T)(object)(val == "True" || val == "1" || val == "yes" || val == "true" || val == "on");
                if (PhotonNetwork.isMasterClient&&updaterc)
                {
                    sendRCinfos();
                }
                return true;
            }
            else if (typeof(T) == typeof(int))
            {
                int i;
                if (!Int32.TryParse(val, out i))
                {
                    return false;
                }
                currentval = (T)(object)i;

                if (PhotonNetwork.isMasterClient&&updaterc)
                {
                    sendRCinfos();
                }
                return true;
            }
            else if (typeof(T) == typeof(float))
            {
                float f;
                if (!float.TryParse(val, out f))
                {
                    return false;
                }
                currentval = (T)(object)f;
                if (PhotonNetwork.isMasterClient&&updaterc)
                {
                    sendRCinfos();
                }
                return true;
            }
            else if (typeof(T) == typeof(string))
            {
                currentval = (T)(object)val;
                if (PhotonNetwork.isMasterClient&&updaterc)
                {
                    sendRCinfos();
                }
                return true;
            }
            else
            {
                UnityEngine.Debug.Log("Mode.set() type not handled : " + typeof(T).ToString());
                return false;
            }
        }
        public override string ToString()
        {
            if (typeof(T) == typeof(bool))
            {
                if (((bool)(object)currentval))
                {
                    return "<color=#008000ff>" + pname + "</color>";
                }
                else
                {
                    return "<color=#ff0000ff>" + pname + "</color>";
                }
            }
            else if (typeof(T) == typeof(int))
            {
                return "<b>" + pname + "</b> : " + currentval.ToString();
            }
            else if (typeof(T) == typeof(float))
            {
                return "<b>" + pname + "</b> : " + currentval.ToString();
            }
            else
            {
                return "<b>" + pname + "</b> : " + currentval.ToString();
            }
        }
        public override string onplayerlogged()
        {
            return String.Empty;
        }
        public override void todefault()
        {
            currentval = defaultval;
            sendRCinfos();
        }

    }
    public class RestartRCMode<T, U> : RCMode<T, U>
    {
        public RestartRCMode(string name, string pubname, T defaultval) : base(name, pubname, defaultval) { }
        public override bool set(string val, bool updaterc = true)
        {
            if (base.set(val))
            {
                if (PhotonNetwork.isMasterClient)
                {
                    FengGameManagerMKII.instance.restartGame(false);
                }
                return true;
            }
            else
            {
                return false;
            }
        }
    }
    public class ConstRCMode<T, U> : RCMode<T, U>
    {
        public ConstRCMode(string name, string pubname, T defaultval) : base(name, pubname, defaultval) { }
        public override bool set(string val,bool updaterc = true)
        {
            return false;
        }
    }
    public class MaxRCMode<T, U> : RCMode<T, U>
    {
        private T max;
        public MaxRCMode(string name, string pubname, T defaultval, T maximum) : base(name, pubname, defaultval)
        {
            max = maximum;
        }
        public override bool set(string val,bool updaterc=true)
        {
            if (typeof(T) == typeof(bool))
            {
                currentval = (T)(object)(val == "True" || val == "1" || val == "yes" || val == "true" || val == "on");
               // MyLog.Debug("CustomGameMode.MaxRCMode.set() : value cannot be boolean ", MyLog.LogType.WARNING);
                sendRCinfos();
                return true;
            }
            else if (typeof(T) == typeof(int))
            {
                int i;
                if (!(Int32.TryParse(val, out i) && i < (int)(object)max))
                {
                    return false;
                }
                currentval = (T)(object)i;
                sendRCinfos();
                return true;
            }
            else if (typeof(T) == typeof(float))
            {
                float f;
                if (!float.TryParse(val, out f) && f < (float)(object)max)
                {
                    return false;
                }
                currentval = (T)(object)f;
                sendRCinfos();
                return true;
            }
            else if (typeof(T) == typeof(string))
            {
                currentval = (T)(object)val;
                //MyLog.Debug("CustomGameMode.MaxRCMode.set() : value cannot be string ", MyLog.LogType.WARNING);
                sendRCinfos();
                return true;
            }
            else
            {
                //MyLog.Debug("CustomGameMode.MaxRCMode.set() type not handled : " + typeof(T).ToString(), MyLog.LogType.ERROR);
                return false;
            }
        }
    }
    public class InternMode<T> : RCMode<T,T>
    {
        private T defaultval;
        public InternMode(string name, T defaultval) : base(name,name,defaultval)
        {
            this.defaultval = defaultval;
        }
        public override bool set(string val, bool updaterc = true)
        {
            object[] objArray2 = new object[] { "<color=#f5ae0f> Rule "+publicname+" = "+val+" </color>", "<b>GameModeManager</b>" };
            FengGameManagerMKII.instance.photonView.RPC("Chat", PhotonTargets.All, objArray2);
            return base.set(val, updaterc);
        }
        public override string onplayerlogged()
        {
           
            if (!currentval.Equals(defaultval))
            {
                return "<color=#625766>"+publicname + "</color> = <color=#ff9966>" + currentval.ToString()+"</color>";
            }
            else
            {
                return "";
            }
        }
    }
    public class Motd : RCMode
    {
        public string motd;

        public Motd(string def)
        {
            hname = "motd";
            motd = def;
            pname = "Motd";
        }

        public override string onplayerlogged()
        {
            return motd;
        }

        public override Type ptype()
        {
            return typeof(string);
        }

        public override bool set(string val, bool updateRC = true)
        {
            FengGameManagerMKII.instance.chat.addLINE("<b>Motd : </b>" + val);
            motd = val;
            if (mode.ContainsKey("motd"))
            {
                mode["motd"] = motd;
            }
            else
            {
                mode.Add("motd", motd);
            }
            return true;
        }

        public override bool tobool()
        {
            return motd != "";
        }

        public override float tofloat()
        {
            return 0;
        }

        public override int toint()
        {
            return 0;
        }

        public override string tostring()
        {
            return motd;
        }

        public override void todefault()
        {
            
        }
    }
}

