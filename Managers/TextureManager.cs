﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
    [Managers.Manager(typeof(FileManager))]
public static class TextureManager
{

    private static Dictionary<string,Texture2D> texts = new Dictionary<string, Texture2D>();

    private static Dictionary<string, Texture2D> httpcache = new Dictionary<string, Texture2D>();

    public static void clearCache()
    {
        httpcache = new Dictionary<string, Texture2D>();
    }

    private static FileManager.Datadirectory texdir = new FileManager.Datadirectory("textures");

    private static FileManager.DataFile conf;

    private static readonly Texture2D def = new Texture2D(1, 1);

    public static void Init()
    {
        conf = texdir.getfile("conf.txt");
        conf.flushjson();
    }

    public static void loadtexture(string name, string link)
    {
        if (conf.json[name].Value == "")
        {
            conf.json[name] = link;
            conf.flushjson();
        }
        else
        {
            link = conf.json[name];
        }
        FengGameManagerMKII.instance.StartCoroutine(gettex(name,link));
    }

    private static System.Collections.IEnumerator gettex(string name,string link)
    {
        Texture2D t;

        if (httpcache.ContainsKey(link))
        {
            t = httpcache[link];
        }
        else
        {
            WWW tex = new WWW(link);
            yield return tex;
            t = tex.texture;
            httpcache.Add(link, t);
            tex.Dispose();
        }
        if (texts.ContainsKey(name))
        {
            texts[name] = t;
        }
        else
        {
            texts.Add(name, t);
        }
        yield break;
    }

    public static Texture2D gettex(string name)
    {
        if (texts.ContainsKey(name))
        {
            return texts[name];
        }
        else
        {
            return def;
        }
    }

    public class WWWTex
    {
        private string link;
        public Texture2D tex { get; private set; }

        public WWWTex(string link)
        {
            this.link = link;
        }

        public System.Collections.IEnumerator gettex()
        {
            if (httpcache.ContainsKey(link))
            {
                tex = httpcache[link];
            }
            else
            {
                WWW t = new WWW(link);
                yield return tex;
                tex = RCextensions.loadimage(t, true, 2000000);
                httpcache.Add(link, tex);
                t.Dispose();
            }
        }
    }


}

