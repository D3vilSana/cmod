﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

[Managers.Manager(typeof(FileManager))]
public abstract class Configs
{
    /// <summary>
    /// enum_config MUST be an enum
    /// </summary>
    public abstract Type enum_config { get; }
    public abstract string name { get; }

    public abstract int getintVal<T>(T key);
    public abstract float getfloatVal<T>(T key);
    public abstract string getstringVal<T>(T key);
    public abstract bool getBoolVal<T>(T key);
    public abstract void setVal<T>(T key, object val);
    public static Dictionary<Type, Configs> allconfigs;
    public abstract void init();
    public abstract void load();

    public static FileManager.Datadirectory config = new FileManager.Datadirectory("config");

    public static void Init()
    {

        allconfigs = new Dictionary<Type, Configs>();
        /*addConfig<CModConfigs>();
        addConfig<SkinManager>();*/
        foreach (Type t in CModUtilities.GetTypesof<Configs>())
        {
            Configs u = (Configs)Activator.CreateInstance(t);
            allconfigs.Add(u.enum_config, u);
        }




        foreach (KeyValuePair<Type, Configs> c in allconfigs)
        {
            try
            {
                c.Value.init();
            }
            catch (Exception e)
            {
                UnityEngine.Debug.LogException(e);
            }

        }
        foreach (KeyValuePair<Type, Configs> c in allconfigs)
        {
            try
            {
                c.Value.load();
            }
            catch (Exception e)
            {
                UnityEngine.Debug.LogException(e);
            }
        }
        UnityEngine.Debug.Log(allconfigs.Count + " Configs types succesfully added !");

    }
    private static void addConfig<T>() where T : Configs, new()
    {
        Configs c = new T();
        allconfigs.Add(c.enum_config, c);

    }
    /// <summary>
    /// WARNING : T MUST be an enum of a config class
    /// </summary>
    /// <typeparam name="T">Enum</typeparam>
    /// <param name="key">The configurationparameter requested</param>
    /// <returns></returns>
    public static int getint<T>(T key) where T : struct, IConvertible // T  must be an enum
    {
        Configs c;
        if (allconfigs.TryGetValue(typeof(T), out c))
        {
            return c.getintVal<T>(key);
        }
        else
        {
            UnityEngine.Debug.LogError("ERROR : Bad int enum type !! : got " + key.GetType().ToString());
            return default(int);
        }
    }
    /// <summary>
    /// WARNING : T MUST be an enum of a config class
    /// </summary>
    /// <typeparam name="T">Enum</typeparam>
    /// <param name="key">The configurationparameter requested</param>
    /// <returns></returns>
    public static float getfloat<T>(T key) where T : struct, IConvertible // T  must be an enum
    {
        Configs c;
        if (allconfigs.TryGetValue(typeof(T), out c))
        {
            return c.getfloatVal<T>(key);
        }
        else
        {
            UnityEngine.Debug.LogError("ERROR : Bad float enum type !! : got " + key.GetType().ToString());
            return default(float);
        }
    }
    /// <summary>
    /// WARNING : T MUST be an enum of a config class
    /// </summary>
    /// <typeparam name="T">Enum</typeparam>
    /// <param name="key">The configurationparameter requested</param>
    /// <returns></returns>
    public static string getstring<T>(T key) where T : struct, IConvertible // T  must be an enum
    {
        Configs c;
        if (allconfigs.TryGetValue(typeof(T), out c))
        {
            return c.getstringVal<T>(key);
        }
        else
        {
            UnityEngine.Debug.LogError("ERROR : Bad string enum type !! : got " + key.GetType().ToString());
            return default(string);
        }
    }
    public static bool getbool<T>(T key) where T : struct, IConvertible
    {
        Configs c;
        if (allconfigs.TryGetValue(typeof(T), out c))
        {
            return c.getBoolVal<T>(key);
        }
        else
        {
            UnityEngine.Debug.LogError("ERROR : Bad bool enum type !! : got " + key.GetType().ToString());
            return default(bool);
        }
    }

    public static void set<T>(T key, object val)
    {
        Configs c;
        if (allconfigs.TryGetValue(typeof(T), out c))
        {
            c.setVal<T>(key, val);
        }
        else
        {
            UnityEngine.Debug.LogError("ERROR : Bad setvalue enum type !! : got " + key.GetType().ToString());
        }
    }
}
/// <summary>
/// Note : enum_config must be an int enum
/// </summary>
public abstract class ClassicConfig : Configs
{
    protected enum Valtypes {String,Int,Float,Bool}
    protected Dictionary<int, int> ints;
    protected Dictionary<int, float> floats;
    protected Dictionary<int, string> strings;
    protected Dictionary<int, bool> bools;

    protected abstract Valtypes typeofkey(int key);
    public abstract void Onvalchanged(int key);
    public abstract object defaultval(int val);

    protected FileManager.DataFile file;

    public override void load() { startload(100, 60, 60); }
    public void startload(int stringsize,int intsize,int floatsize)
    {
        ints = new Dictionary<int, int>();
        floats = new Dictionary<int, float>();
        strings = new Dictionary<int, string>();
        bools = new Dictionary<int, bool>();

        file = config.getfile(name + ".conf.txt");

        

        string[] names = Enum.GetNames(enum_config);
        int[] vals = (int[])Enum.GetValues(enum_config);
        

        #region get_from_config
        for (int i = 0; i < vals.Length; i++)
        {

            string key = names[i];
            if (file.json[key].Value != "" /*UnityEngine.PlayerPrefs.HasKey(key)*/)
            {
                
                switch (typeofkey(vals[i]))
                {
                    case Valtypes.String:
                        strings.Add(vals[i], file.json[key]);
                        //strings.Add(vals[i],UnityEngine.PlayerPrefs.GetString(key));
                        break;
                    case Valtypes.Int:
                        ints.Add(vals[i], file.json[key].AsInt);
                        //ints.Add(vals[i],UnityEngine.PlayerPrefs.GetInt(key));
                        break;
                    case Valtypes.Float:
                        floats.Add(vals[i], file.json[key].AsFloat);
                        //floats.Add(vals[i],UnityEngine.PlayerPrefs.GetFloat(key));
                        break;
                    case Valtypes.Bool:
                        bools.Add(vals[i], file.json[key].AsBool);
                        break;
                }
            }
            else
            {
                
                switch (typeofkey(vals[i]))
                {
                    case Valtypes.String:
                        strings.Add(vals[i], (string)defaultval(vals[i]));
                        break;
                    case Valtypes.Int:
                        ints.Add(vals[i], (int)defaultval(vals[i]));
                        break;
                    case Valtypes.Float:
                        floats.Add(vals[i], (float)defaultval(vals[i]));
                        break;
                    case Valtypes.Bool:
                        bools.Add(vals[i], (bool)defaultval(vals[i]));
                        break;
                }
                save(vals[i]);

            }
            Onvalchanged(vals[i]);
        }
        #endregion
        //ADD A TAB
        UnityEngine.Debug.Log("ClassicConfig(" + name + ") : Config loaded");
        #region adding_a_tab
        Menu.ConfigLayoutTab t = new Menu.ConfigLayoutTab(name);
        t.Init();


        foreach (int val in ints.Keys)
        {
            int key = val;//Key of the ElemValue
            
            ConfigElem i = new Menu.LayoutElems.Int(
                delegate (int newval, Menu.LayoutElems.Int box)
                {
                    ints[key] = newval;
                    Onvalchanged(key);
                    save(key);
                },
                Enum.GetName(enum_config, key),
                ints[key].ToString(), intsize);
            t.AddElem(i);
        }

        foreach (int val in floats.Keys)
        {
            int key = val;//Key of the ElemValue
            ConfigElem i = new Menu.LayoutElems.Float(
                delegate (float newval, Menu.LayoutElems.Float box)
                {
                    floats[key] = newval;
                    Onvalchanged(key);
                    save(key);
                },
                Enum.GetName(enum_config, key),
                floats[key].ToString(), floatsize);
            t.AddElem(i);
        }
        foreach (int val in strings.Keys)
        {
            //UnityEngine.Debug.Log(Enum.GetName(enum_config, val));
            int key = val;//Key of the ElemValue
            ConfigElem i = new Menu.LayoutElems.Text(
                delegate (string newval, Menu.LayoutElems.Text box)
                {
                    strings[key] = newval;
                    Onvalchanged(key);
                    save(key);
                }
                , Enum.GetName(enum_config, key)
                , strings[key],stringsize
           );

            t.AddElem(i);
        }
        foreach (int val in bools.Keys)
        {
            int key = val;
            ConfigElem i = new Menu.LayoutElems.Toggleable(
                Enum.GetName(enum_config, key),
                delegate (bool b, Menu.LayoutElems.Toggleable v)
                {
                    bools[key] = b;
                    Onvalchanged(key);
                    save(key);
                }, bools[key]);
            t.AddElem(i);
        }
        #endregion
        IngameMenu.AddTab(t);
    }
    public void save(int key)
    {
        string k =/* "CMod." + name + "." +*/ Enum.GetName(enum_config, key);
        switch (typeofkey(key))
        {
            case Valtypes.String:
                file.json[k] = strings[key];
                //UnityEngine.PlayerPrefs.SetString("CMod."+name + "." + Enum.GetName(enum_config, key), strings[key]);
                //MyLog.Debug("Playerprefs : " + name + "." + Enum.GetName(enum_config, key) + " = " + strings[key], MyLog.LogType.LOG);
                break;
            case Valtypes.Int:
                file.json[k].AsInt = ints[key];
                //UnityEngine.PlayerPrefs.SetInt("CMod."+name + "." + Enum.GetName(enum_config, key), ints[key]);
                //MyLog.Debug("Playerprefs : " + name + "." + Enum.GetName(enum_config, key) + " = " + ints[key], MyLog.LogType.LOG);
                break;
            case Valtypes.Float:
                file.json[k].AsFloat = floats[key];
                //UnityEngine.PlayerPrefs.SetFloat("CMod."+name + "." + Enum.GetName(enum_config, key), floats[key]);
                //MyLog.Debug("Playerprefs : " + name + "." + Enum.GetName(enum_config, key) + " = " + floats[key], MyLog.LogType.LOG);
                break;
            case Valtypes.Bool:
                file.json[k].AsBool = bools[key];
                break;
        }
        file.flushjson();
    }
    public override int getintVal<T>(T key)
    {
        int v;
        if (ints.TryGetValue((int)(object)key,out v)) // We are 100% sure that it will be ok
        {
            return v;
        }
        else
        {
            //Key not found
            UnityEngine.Debug.LogError(enum_config.ToString() + " : Key <b>" + key.ToString() + "</b> is not an int !!");


                
            return default(int);
        }
    }
    public override float getfloatVal<T>(T key)
    {
        float v;
        if (floats.TryGetValue((int)(object)key, out v)) // We are 100% sure that it will be ok
        {
            return v;
        }
        else
        {
            //Key not found
            UnityEngine.Debug.LogError(enum_config.ToString() + " : Key <b>" + key.ToString() + "</b> is not a float !!");
            return default(float);
        }
    }
    public override string getstringVal<T>(T key)
    {
        string v;
        if (strings.TryGetValue((int)(object)key, out v)) // We are 100% sure that it will be ok
        {
            return v;
        }
        else
        {
            //Key not found
            UnityEngine.Debug.LogError(enum_config.ToString() + " : Key <b>" + key.ToString() + "</b> is not a string !!");
            return default(string);
        }
    }
    public override bool getBoolVal<T>(T key)
    {
        bool b;
        if (bools.TryGetValue((int)(object)key,out b))
        {
            return b;
        }
        else
        {
            UnityEngine.Debug.LogError(enum_config.ToString() + " : Key <b>" + key.ToString() + "</b> is not a bool !!");
            return default(bool);
        }
    }
    public override void setVal<T>(T key, object val)
    {
        if (val.GetType()==typeof(string))
        {
            strings[(int)(object)key] = (string)val;
        }
        else if (val.GetType()==typeof(int))
        {
            ints[(int)(object)key] = (int)val;
        }
        else if (val.GetType() == typeof(float))
        {
            floats[(int)(object)key] = (float)val;
        }
        else if (val.GetType() == typeof(bool))
        {
            bools[(int)(object)key] = (bool)val;
        }
        else
        {
            UnityEngine.Debug.LogError(enum_config.ToString() + " : Key <b>" + key.ToString() + "</b> : " + val.GetType().ToString() + " is not a valid type !!!");

        }

    }

}
