﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

[Managers.Manager]
static class CustomPlayerRotations
{
    public static Gesture[] current;
    public enum PlayerRotState : int
    {
        Camera,
        LastCable,
        Free
    }
    public struct Pos
    {
        public Quaternion handL;
        public Quaternion handR;
        public Quaternion forearmL;
        public Quaternion forearmR;
        public Quaternion upperarmL;
        public Quaternion upperarmR;
        public Quaternion playerRelativerotation;

        public static Pos Lerp(Pos start,Pos end,float lerp)
        {
            Pos p = new Pos();
            p.handL = Quaternion.Lerp(start.handL, end.handR,lerp);
            p.handR = Quaternion.Lerp(start.handR, end.handR, lerp);
            p.forearmL = Quaternion.Lerp(start.forearmL, end.forearmL, lerp);
            p.forearmR = Quaternion.Lerp(start.forearmR, end.forearmR, lerp);
            p.upperarmL = Quaternion.Lerp(start.upperarmL, end.upperarmL, lerp);
            p.upperarmR = Quaternion.Lerp(start.upperarmR, end.upperarmR, lerp);
            p.playerRelativerotation = Quaternion.Lerp(start.playerRelativerotation, end.playerRelativerotation, lerp);
            return p;
        }
        
    }
    public struct Gesture
    {
        public Pos Standing;

        public Pos StartingPos;
        public Pos MidPos;
        public Pos EndingPos;

        public PlayerRotState RotState;
    }
    
    public class OpenCharrotmenuGUI : GUIElem
    {
        private HERO_SETUP setup;
        public override Vector2 getsize
        {
            get
            {
                return new Vector2(150, 20);
            }

            internal set
            {
            }
        }

        public override Position pos
        {
            get
            {
                return Position.MidLeft;
            }

            internal set
            {

            }
        }

        public override bool show
        {
            get
            {
                return Application.loadedLevelName=="menu";
            }

            internal set
            {
                
            }
        }

        public override void Init()
        {
            
        }

        public override void OnGui()
        {
            if(GUI.Button(new Rect(0,0,150,20),"Customise rotations"))
            {
                CustomCharacterManager.showGUI = false;
                Application.LoadLevel("characterCreation");
            }
        }

        public override void Update()
        {
            
        }
    }

    public static void Init()
    {
        //Load from playerpref
        current = new Gesture[3] { new Gesture(), new Gesture(), new Gesture() };
        current[0] = LoadGesture("CMod.Gesture.0");

    }

    public static Gesture LoadGesture(string name)
    {
        Gesture g = new Gesture();
        g.EndingPos = LoadPos(name + ".EndingPos");
        g.MidPos = LoadPos(name + ".MidPos");
        g.Standing = LoadPos(name + ".Standing");
        g.StartingPos = LoadPos(name + ".StartingPos");
        g.RotState = (PlayerRotState)PlayerPrefs.GetInt(name + ".RotState");
        return g;
    }
    public static void SaveGesture(string name,Gesture g)
    {
        SavePos(name + ".EndingPos", g.EndingPos);
        SavePos(name + ".MidPos", g.MidPos);
        SavePos(name + ".Standing", g.Standing);
        SavePos(name + ".StartingPos", g.StartingPos);
        PlayerPrefs.SetInt(name + ".RotState", (int)g.RotState);
    }
    public static Pos LoadPos(string name)
    {
        Pos p = new Pos();
        p.forearmL = LoadQuat(name + ".forearmL");
        p.forearmR = LoadQuat(name + ".forearmR");
        p.handL = LoadQuat(name + ".handL");
        p.handR = LoadQuat(name + ".handR");
        p.upperarmL = LoadQuat(name + ".upperarmL");
        p.upperarmR = LoadQuat(name + ".upperarmR");
        p.playerRelativerotation = LoadQuat(name + ".rotation");



        return p;
    }
    public static void SavePos(string name,Pos p)
    {
        SaveQuaternion(name + ".forearmL", p.forearmL);
        SaveQuaternion(name + ".forearmR", p.forearmR);
        SaveQuaternion(name + ".handL", p.handL);
        SaveQuaternion(name + ".handR", p.handR);
        SaveQuaternion(name + ".upperarmL", p.upperarmL);
        SaveQuaternion(name + ".upperarmR", p.upperarmR);
        SaveQuaternion(name + ".rotation", p.playerRelativerotation);
    }

    public static Quaternion LoadQuat(string name)
    {
        return new Quaternion(
            PlayerPrefs.GetFloat(name + ".x", 0),
            PlayerPrefs.GetFloat(name + ".y", 0),
            PlayerPrefs.GetFloat(name + ".z", 0),
            PlayerPrefs.GetFloat(name + ".w", 0));
    }
    public static void SaveQuaternion(string name, Quaternion q)
    {
        PlayerPrefs.SetFloat(name + ".x", q.x);
        PlayerPrefs.SetFloat(name + ".y", q.y);
        PlayerPrefs.SetFloat(name + ".z", q.z);
        PlayerPrefs.SetFloat(name + ".w", q.w);

    }

}

