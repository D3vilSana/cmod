﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Inputs.InputTypes;

namespace Managers
{
    [Manager(typeof(FileManager),typeof(Configs))]
    public static class InputManager
    {
        public static bool ignoreKeys = false;

        private static FileManager.DataFile conf;
        private static Inputs.InputTypes.InputType[] keys;
        public static Inputs.InputTypes.InputType key(InputCode.Keys k)
        {
            return keys[(int)k];
        }
        public static void setKey(InputCode.Keys c, InputType t)
        {
            keys[(int)c] = t;
            conf.json[c.ToString()] = t.serialize();
            conf.flushjson();
        }

        public static void Init()
        {
            conf = Configs.config.getfile("keys.conf.txt");
            int keycount = Enum.GetNames(typeof(InputCode.Keys)).Length;
            keys = new Inputs.InputTypes.InputType[keycount];
            setbindings();
        }
        private static void setbindings()
        {

            var b = from a in AppDomain.CurrentDomain.GetAssemblies()
                    from t in a.GetTypes()
                    where t.IsSubclassOf(typeof(Inputs.InputTypes.InputType))
                    where !t.IsAbstract
                    where t.GetConstructor(new Type[] { typeof(SimpleJSON.JSONNode) })!=null
                    select t;



            IEnumerable<InputCode.Keys> keys = Enum.GetValues(typeof(InputCode.Keys)).Cast<InputCode.Keys>();
            foreach (InputCode.Keys k in keys)
            {
                var o = conf.json[k.ToString()];
                string type = o["Type"];
                Type t = b.FirstOrDefault(ty => ty.ToString() == type);
                UnityEngine.Debug.Log(t);
                if (t!=null)
                {
                    InputManager.keys[(int)k] = (InputType)Activator.CreateInstance(t, o);
                }
                else
                {
                    InputManager.keys[(int)k] = defaultof(k);
                }
            }
        }

        private static Inputs.InputTypes.InputType defaultof(InputCode.Keys t)
        {
            switch (t)
            {
                case InputCode.Keys.up:
                    return new Key(UnityEngine.KeyCode.W);
                case InputCode.Keys.down:
                    return new Key(UnityEngine.KeyCode.S);
                case InputCode.Keys.left:
                    return new Key(UnityEngine.KeyCode.A);
                case InputCode.Keys.right:
                    return new Key(UnityEngine.KeyCode.D);
                case InputCode.Keys.jump:
                    return new Key(UnityEngine.KeyCode.LeftShift);
                case InputCode.Keys.dodge:
                    return new Key(UnityEngine.KeyCode.LeftControl);
                case InputCode.Keys.leftRope:
                    return new Key(UnityEngine.KeyCode.Mouse0);
                case InputCode.Keys.rightRope:
                    return new Key(UnityEngine.KeyCode.Mouse1);
                case InputCode.Keys.bothRope:
                    return new Null();
                case InputCode.Keys.focus:
                    return new Null();
                case InputCode.Keys.attack0:
                    return new Key(UnityEngine.KeyCode.Space);
                case InputCode.Keys.attack1:
                    return new Key(UnityEngine.KeyCode.Z);
                case InputCode.Keys.salute:
                    return new Key(UnityEngine.KeyCode.N);
                case InputCode.Keys.camera:
                    return new Null();
                case InputCode.Keys.restart:
                    return new Key(UnityEngine.KeyCode.T);
                case InputCode.Keys.pause:
                    return new Key(UnityEngine.KeyCode.P);
                case InputCode.Keys.hidecursor:
                    return new Null();
                case InputCode.Keys.fullscreen:
                    return new Null();
                case InputCode.Keys.reload:
                    return new Key(UnityEngine.KeyCode.R);
                case InputCode.Keys.flare1:
                    return new Key(UnityEngine.KeyCode.Alpha1);
                case InputCode.Keys.flare2:
                    return new Key(UnityEngine.KeyCode.Alpha2);
                case InputCode.Keys.flare3:
                    return new Key(UnityEngine.KeyCode.Alpha3);
                case InputCode.Keys.reel_in:
                    return new Scroll("Mouse ScrollWheel");
                case InputCode.Keys.reel_out:
                    return new Scroll("Mouse ScrollWheel", true);
                case InputCode.Keys.rottocam:
                    return new Null();
                default:
                    return new Null();
            }
        }

        public static void Update()
        {
            if (ignoreKeys)
            {
                return;
            }
            foreach (InputType r in keys)
            {
                r.Update();

            }
        }

    }
}
