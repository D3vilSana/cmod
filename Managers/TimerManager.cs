﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Managers
{
    [Manager]
    public static class TimerManager
    {
        private static List<Timer> runningTimers = new List<Timer>();
        private static float next_timer = float.PositiveInfinity;
        public delegate void TimerEventHandler(Timer t);
        public static void Init()
        {

        }
        private static void RefreshNextTimer()
        {
            if (runningTimers.Count>=1)
            {
                next_timer = runningTimers.Select(t => t.end_time).Min();
            }
            else
            {
                next_timer = float.PositiveInfinity;
            }
        }
        public static void Update()
        {
            if (UnityEngine.Time.time>next_timer)
            {
                IEnumerable<Timer> tmrs = runningTimers.Where(t => t.end_time < UnityEngine.Time.time).ToArray();
                foreach (Timer t in tmrs)
                {
                    t.TriggerEnd();
                }
            }
        }

        public class Timer
        {
            public virtual void Start()
            {
                if (!is_running)
                {
                    start_time = UnityEngine.Time.time;
                    runningTimers.Add(this);
                    if (On_Start!=null)
                    {
                        On_Start(this);
                    }
                    RefreshNextTimer();
                }

            }
            public virtual void Abort()
            {
                if (is_running)
                {
                    runningTimers.Remove(this);
                    RefreshNextTimer();
                    if (On_Abort != null)
                    {
                        On_Abort(this);
                    }
                }
            }
            public virtual void TriggerEnd()
            {
                if (is_running)
                {
                    if (On_End != null)
                    {
                        On_End(this);
                    }
                    runningTimers.Remove(this);
                    RefreshNextTimer();
                }
            }
            public event TimerEventHandler On_End;
            public event TimerEventHandler On_Start;
            public event TimerEventHandler On_Abort;
            private float start_time = 0;
            private float _timeout = float.PositiveInfinity;
            public bool is_running
            {
                get
                {
                    return runningTimers.Contains(this);
                }
                set
                {
                    if (is_running!=value)
                    {
                        if (value)
                        {
                            Start();
                        }
                        else
                        {
                            Abort();
                        }
                    }
                }
            }
            public float end_time
            {
                get
                {
                    return _timeout + start_time;
                }
                set
                {
                    _timeout = value - UnityEngine.Time.time;
                    if (is_running)
                    {
                        RefreshNextTimer();
                    }
                }
            }
            public float timeout
            {
                get
                {
                    return _timeout;
                }
                set
                {
                    _timeout = value;
                    if (is_running)
                    {
                        RefreshNextTimer();
                    }
                }
            }
            public float remainingtime
            {
                get
                {
                    return end_time - UnityEngine.Time.time;
                }
                set
                {
                    end_time = UnityEngine.Time.time - value;
                }
            }
        }

    }
}
