﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

[Managers.Manager(typeof(TextureManager))]
public static class ServerList
{
    public static bool visible
    {
        get;
        set;
    }
    private const int roomysize = 50;
    private static Vector2 scrollpos = new Vector2();

    private static GUIStyle toplabelstyle = new GUIStyle();
    private static GUIStyle cantjoinreason = new GUIStyle();
    private static GUIStyle roomsize = new GUIStyle();
    private static SimpleAES eaes = new SimpleAES();

    private static Rect iconpos = new Rect(0, 0, 50, 50);

    public static void Init()
    {
        visible = false;
        toplabelstyle.alignment = TextAnchor.UpperCenter;
        toplabelstyle.normal.textColor = Color.white;
        toplabelstyle.fontSize = 25;

        cantjoinreason.alignment = TextAnchor.MiddleCenter;
        cantjoinreason.fontSize = 15;

        roomsize.alignment = TextAnchor.LowerCenter;
        roomsize.fontSize = 15;
        

        UpdateRoomList();
        TextureManager.loadtexture("City", "https://s31.postimg.org/zd3vllrq3/tumblr_inline_n81afxua501sa18wb.png");
        TextureManager.loadtexture("Day", "https://s31.postimg.org/4lgiappqz/7_Ta_FVRL.png");
        TextureManager.loadtexture("Forest", "https://s31.postimg.org/9lijwekrf/PR1_Vu_BI.png");
        TextureManager.loadtexture("Racing", "https://s31.postimg.org/elg04cqe3/weekend_racing.png");
        TextureManager.loadtexture("Night", "https://s31.postimg.org/a0ttpf6or/Yxg_W0do.png");
        TextureManager.loadtexture("Dawn", "https://s31.postimg.org/rfe1xp3tn/5_F19l_Uc.png");
        TextureManager.loadtexture("Colossal", "https://s31.postimg.org/txzqydpjv/F3_RRr2b.png");
        TextureManager.loadtexture("Trost", "https://s31.postimg.org/fg2jqdy8r/chyvz_Ow.png");
        TextureManager.loadtexture("Outside", "https://s31.postimg.org/qgxoveqhn/z_NRUe_Uw.png");
        TextureManager.loadtexture("Annie", "https://s31.postimg.org/t0tbpie1n/l_TJCPt_H.png");
    }
    public static void GUIUpdate()
    {
        if (!visible)
        {
            return;
        }
        GUI.Box(new Rect(200, 100, Screen.width - 400, Screen.height-200),"Server List");

        scrollpos =  GUI.BeginScrollView(new Rect(250, 150, Screen.width - 500, Screen.height - 300), scrollpos, new Rect(0, 0, Screen.width - 520, (roomysize+5) * RoomFilterGUI.filter(PhotonNetwork.networkingPeer.mGameList.Values).Count()));
        int y = 0;
        foreach (RoomInfo i in RoomFilterGUI.filter(PhotonNetwork.networkingPeer.mGameList.Values))
        {
            GUI.BeginGroup(new Rect(0, y*(roomysize+5), Screen.width - 520, roomysize));
            GUIRoom(i, Screen.width - 520);
            y++;
            GUI.EndGroup();
        }
        GUI.EndScrollView(true);
        if (GUI.Button(new Rect(200,Screen.height-150,50,50),"Quit"))
        {
            visible = false;
            NGUITools.SetActive(GameObject.Find("UIRefer").GetComponent<UIMainReferences>().panelMultiStart, true);
        }
        if (GUI.Button(new Rect(250, Screen.height - 150, 100, 50), "New Game"))
        {
            visible = false;
            NGUITools.SetActive(GameObject.Find("UIRefer").GetComponent<UIMainReferences>().panelMultiSet, true);
        }

    }
    public static void UpdateRoomList()
    {

    }

    private static bool GUIRoom(RoomInfo i,int xsize)
    {
        if (i.name.Length>1000)
        {
            return false;
        }
        string[] infos = i.name.Split('`');
        if (infos.Length<5)
        {
            return false;
        }
        string name = infos[0];
        string map = infos[1];
        string difficulty = infos[2];
        string time = infos[4];
        bool pass = (infos[5] != "");
        string pwd = infos[5];


        GUI.Box(new Rect(0, 0, xsize, roomysize), "");
        GUI.Label(new Rect(50, 0, xsize-50, 20), name.hexColor(), toplabelstyle);
        if (i.maxPlayers!=0)
        {
            GUI.Label(new Rect(50, 30, xsize - 50, 20), "<color=" + Color.Lerp(Color.green,Color.red,(((float)i.playerCount)/((float)i.maxPlayers))).tohex() + ">" + i.playerCount + "/" + i.maxPlayers+"</color>", roomsize);
        }
        else
        {
            GUI.Label(new Rect(50, 30, xsize - 50, 20), "<color=#ff6dba>" + i.playerCount + "/" + i.maxPlayers+"</color>", roomsize);
        }

        DrawIcon(map, time);
        if (pass)
        {
           
            if (i.canjoin)
            {
                i.currentGUIpass = GUI.TextField(new Rect(xsize - 100, 0, 100, 25), i.currentGUIpass);
                if (GUI.Button(new Rect(xsize - 100, roomysize - 25, 100, 25), "Join")&&(eaes.Decrypt(pwd)==i.currentGUIpass))
                {
                    PhotonNetwork.JoinRoom(i.name);
                    visible = false;
                }
            }
            else
            {
                GUI.Label(new Rect(xsize - 100, 0, 100, 50), "<color=#ff7f50>" + i.cantjoinreason+"</color>",cantjoinreason);
            }

        }
        else
        {
            if (i.canjoin)
            {
                if (GUI.Button(new Rect(xsize - 100, 0, 100, 50), "Join"))
                {
                    PhotonNetwork.JoinRoom(i.name);
                    visible = false;
                }
            }
            else
            {
                GUI.Label(new Rect(xsize - 100, 0, 100, 50), "<color=#ff7f50>"+i.cantjoinreason+"</color>",cantjoinreason);
            }

        }
        

        return true;
    }
    public static void DrawIcon(string mapname,string time)
    {
        Texture2D map = null;
        Texture2D ttime = null;
        switch (time)
        {
            case "day":
                ttime = TextureManager.gettex("Day");
                break;
            case "dawn":
                ttime = TextureManager.gettex("Dawn");
                break;
            case "night":
                ttime = TextureManager.gettex("Night");
                break;
        }
        // MAP
        if (mapname.StartsWith("The City"))
        {
            map = TextureManager.gettex("City");
        }
        else if (mapname.StartsWith("The Forest"))
        {
            map = TextureManager.gettex("Forest");
        }
        else if (mapname.StartsWith("Racing"))
        {
            map = TextureManager.gettex("Racing");
        }
        else if (mapname.StartsWith("Colossal"))
        {
            map = TextureManager.gettex("Colossal");
        }
        else if (mapname.StartsWith("Trost"))
        {
            map = TextureManager.gettex("Trost");
        }
        else if (mapname.StartsWith("Outside"))
        {
            map = TextureManager.gettex("Outside");
        }
        else if (mapname.StartsWith("Annie"))
        {
            map = TextureManager.gettex("Annie");
        }

        if (map==null||ttime==null)
        {
            GUI.Label(new Rect(0, 0, 200, 20), mapname);
            GUI.Label(new Rect(0, 30, 200, 20), time);
        }
        else
        {
            
            GUI.DrawTexture(iconpos, ttime);
            GUI.DrawTexture(iconpos, map);
            
        }
    }
}
