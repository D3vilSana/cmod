﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


[Managers.Manager]
static class Antispammanager
{
    private static Dictionary<string, long> dic;
    private static long lastlocalchatmsg = DateTime.Now.Ticks;
    public static bool caninstanciate(PhotonPlayer p,ExitGames.Client.Photon.Hashtable datas)
    {
        if (p==null || p.isMasterClient||p.isLocal)
        {
            return true;
        }
        string obj = (string)datas[(byte)0];
        string key = p.ID+"`ins`"+obj;
        if (obj=="hitMeat"||obj =="hook"||obj=="redCross" || obj == "redCross1"|| obj=="FX/boost_smoke")
        {
            return true;
        }
        if (dic.ContainsKey(key))
        {
            if (dic[key]>DateTime.Now.Ticks)
            {
                
                if (PhotonNetwork.isMasterClient)
                {
                    if (!KickBanManager.isignored(p.ID))
                    {
                        FengGameManagerMKII.instance.SendChat("<color=#b84848>Kicked <b>#" + p.ID + "</b> !!! (Spam instentiate :" + obj + ") </color>");
                        KickBanManager.kick(p);
                    }
                }
                else
                {
                    if (lastlocalchatmsg+ 100000000<DateTime.Now.Ticks)//10s
                    {
                        lastlocalchatmsg = DateTime.Now.Ticks;
                        FengGameManagerMKII.instance.chat.addLINE("<color=#b84848>Received Spam Instenciate from <b>#" + p.ID + "</b> !!!(" + obj + ") (blocked for 1s) </color>");
                       
                    }
                }
                
                dic[key] = DateTime.Now.Ticks + 10000000;//1s
                return false;
            }
            else
            {
                //FengGameManagerMKII.instance.chat.addLINE("<color=#b84848>Received Instenciate from <b>#" + p.ID + "</b> !!!(" + obj + ") (accepted) </color>");
                dic[key] = DateTime.Now.Ticks + 500000;//5ms
            }
        }
        else
        {
            dic.Add(key, DateTime.Now.Ticks);//0ms
        }
        return true;
    } 
    public static bool canRPC(PhotonPlayer p,string name)
    {
        if (p==null || p.isMasterClient || p.isLocal)
        {
            return true;
        }
        string obj =name;
        string key = p.ID + "`rpc`" + obj;
        if (obj=="netCrossFade"||obj=="netPauseAnimation"||obj=="tieMeTo"||obj=="setPhase"||obj=="net3DMGSMOKE"||obj=="myMasterIs"||obj=="setVelocityAndLeft"||obj=="tieMeToOBJ"||obj=="netPlayAnimation"||obj=="blowAway"||obj=="setDust"||obj=="netPlayAnimationAt" || obj == "setIfLookTarget" || obj == "refreshPVPStatus" || obj =="titanGetHit" || obj == "hitEyeRPC")
        {
            return true;
        }
        if (dic.ContainsKey(key))
        {
            if (dic[key] > DateTime.Now.Ticks)
            {
                if (PhotonNetwork.isMasterClient)
                {
                    if (!KickBanManager.isignored(p.ID))
                    {
                        FengGameManagerMKII.instance.SendChat("<color=#b84848>Kicked <b>#" + p.ID + "</b> !!! (Spam RPC :" + obj + ") </color>");
                        KickBanManager.kick(p);
                    }
                }
                else
                {
                    if (lastlocalchatmsg + 100000000 < DateTime.Now.Ticks)//10s
                    {
                        lastlocalchatmsg = DateTime.Now.Ticks;
                        FengGameManagerMKII.instance.chat.addLINE("<color=#b84848>Received Spam RPC from <b>#" + p.ID + "</b> !!!(" + obj + ") (blocked for 1s) </color>");
                    }
                }

                dic[key] = DateTime.Now.Ticks + 10000000;//1s
                return false;
            }
            else
            {
                //FengGameManagerMKII.instance.chat.addLINE("<color=#b84848>Received Instenciate from <b>#" + p.ID + "</b> !!!(" + obj + ") (accepted) </color>");
                dic[key] = DateTime.Now.Ticks + 100000;//10ms
            }
        }
        else
        {
            dic.Add(key, DateTime.Now.Ticks);//0ms
        }
        return true;
    }

    public static bool canEvent(int actor,ExitGames.Client.Photon.EventData data)
    {
        return true;
        /*
        if (data.Code==234)
        {
            return false;
        }
        string key = actor + data.ToStringFull();
        if (dic.ContainsKey(key))
        {
            if (dic[key] > DateTime.Now.Ticks)
            {

                if (PhotonNetwork.isMasterClient)
                {
                    if (!KickBanManager.isignored(actor))
                    {
                        FengGameManagerMKII.instance.SendChat("<color=#b84848>Kicked <b>#" + actor + "</b> !!! (Event  :" + data.Code + "  spamming ) </color>");
                        KickBanManager.ban(actor);
                    }
                }
                else
                {
                    if (lastlocalchatmsg + 100000000 < DateTime.Now.Ticks)//10s
                    {
                        lastlocalchatmsg = DateTime.Now.Ticks;
                        FengGameManagerMKII.instance.chat.addLINE("<color=#b84848>Received Event from <b>#" + actor + "</b> !!!(" + data.Code + ") (blocked for 1s) </color>");

                    }
                }

                dic[key] = DateTime.Now.Ticks + 10000000;//1s
                return false;
            }
            else
            {
                //FengGameManagerMKII.instance.chat.addLINE("<color=#b84848>Received Instenciate from <b>#" + p.ID + "</b> !!!(" + obj + ") (accepted) </color>");
                dic[key] = DateTime.Now.Ticks + 5000000;//50ms
            }
        }
        else
        {
            dic.Add(key, DateTime.Now.Ticks);//0ms
        }
        return true;
        */
    }

    public static bool canOpresponse(ExitGames.Client.Photon.OperationResponse rep)
    {

        return true;
    }

    public static void Init()
    {
        dic = new Dictionary<string, long>();
    } 

}
