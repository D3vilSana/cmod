﻿using Photon;
using System;
using UnityEngine;

public class BombExplode : Photon.MonoBehaviour
{
    public GameObject myExplosion;

    public void Start()
    {
        if (base.photonView != null)
        {
            PhotonPlayer owner = base.photonView.owner;
            if (CustomGameMode.getint("TeamMode") > 0)
            {
                int num = RCextensions.returnIntFromObject(owner.customProperties[PhotonPlayerProperty.RCteam]);
                if (num == 1)
                {
                    base.GetComponent<ParticleSystem>().startColor = Color.cyan;
                }
                else if (num == 2)
                {
                    base.GetComponent<ParticleSystem>().startColor = Color.magenta;
                }
                else
                {
                    float r = RCextensions.returnFloatFromObject(owner.customProperties[PhotonPlayerProperty.RCBombR]);
                    float g = RCextensions.returnFloatFromObject(owner.customProperties[PhotonPlayerProperty.RCBombG]);
                    float b = RCextensions.returnFloatFromObject(owner.customProperties[PhotonPlayerProperty.RCBombB]);
                    float num2 = RCextensions.returnFloatFromObject(owner.customProperties[PhotonPlayerProperty.RCBombA]);
                    num2 = Mathf.Max(0.5f, num2);
                    base.GetComponent<ParticleSystem>().startColor = new Color(r, g, b, num2);
                }
            }
            else
            {
                float r = RCextensions.returnFloatFromObject(owner.customProperties[PhotonPlayerProperty.RCBombR]);
                float g = RCextensions.returnFloatFromObject(owner.customProperties[PhotonPlayerProperty.RCBombG]);
                float b = RCextensions.returnFloatFromObject(owner.customProperties[PhotonPlayerProperty.RCBombB]);
                float num2 = RCextensions.returnFloatFromObject(owner.customProperties[PhotonPlayerProperty.RCBombA]);
                num2 = Mathf.Max(0.5f, num2);
                base.GetComponent<ParticleSystem>().startColor = new Color(r, g, b, num2);
            }
            float num3 = RCextensions.returnFloatFromObject(owner.customProperties[PhotonPlayerProperty.RCBombRadius]) * 2f;
            num3 = Mathf.Clamp(num3, 40f, 120f);
            base.GetComponent<ParticleSystem>().startSize = num3;
        }
    }
}
