﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;


public class ChatHandler
{
    public const int xsize = 400;
    public const int ysize = 19;

    public int top
    {
        get { return (int)CustomGlobalGUI.GetTopLeftheight(); }
    }

    public int down;
    public static List<Chatcontent> messages = new List<Chatcontent>();
    private Queue<string> incomingmessages = new Queue<string>();

    public static bool showId = false;

    public ChatHandler(int down)
    {
        this.down = down;

    }

    public void add_msg(string msg)
    {
        incomingmessages.Enqueue(msg);
    }

    public void clear()
    {
        messages = new List<Chatcontent>();
        CText.recount_link();
    }

    public void GUIUpdate()
    {
        if (incomingmessages.Count!=0)
        {

            while (incomingmessages.Count!=0)
            {

                Chatcontent c = new RawMsg(incomingmessages.Dequeue());

                if (messages.Count != 0)
                {
                    Chatcontent e = messages.Last();
                    if (e != null && e.hash == c.hash)
                    {
                        e.number++;
                        return;
                    }
                }
                messages.Add(c);
            }
        }
        int p = down;
        int i = messages.Count -1;
        for (; i >= 0; i--)
        {
            if (p<top)
            {
                break;
            }
            messages[i].drawChat(ref p);
        }
        if (i>0)
        {
            messages.RemoveRange(0, i);//TEST
        }
        
        /*
        foreach (Chatcontent c in messages.Reverse<Chatcontent>())
        {
            if (p<top)
            {
                break;
            }
            c.drawChat(ref p);
        }*/
    }
    
    public abstract class Chatcontent
    {
        private static int currentid = 0;
        public int Id { get; internal set; }
        public abstract int hash { get; }
        public abstract int number { get; set; }

        public Chatcontent() { Id = currentid++; }
        public void drawChat(ref int ypos)
        {
            print(ref ypos);
            if (showId)
            {
                GUI.Label(new Rect(lastrect.xMax, lastrect.yMin, lastrect.xMax + 20, lastrect.height), "<color=#003366>" + Id+"</color>");
            }
            

        }
        public abstract void print(ref int ypos);
        public abstract Rect lastrect { get; set; }
        public abstract void onClick(Vector2 pos);
        public abstract string text { get; }
        protected float getsize(string s)
        {

           return GUI.skin.GetStyle("Label").CalcHeight(new GUIContent(s), xsize);
          // return GUI.skin.GetStyle("Label").CalcSize(new GUIContent(s));
        }
        private void DrawBox(Rect r,Color c)
        {
            Color old = GUI.color;
            GUI.color = c;
            GUI.Box(r, "");
            GUI.color = old;
        }
    }
    protected class ChatMsg : Chatcontent
    {
        private int sid;
        private string s;
        private CText msg;
        //private string msg;
        private int size;

        public ChatMsg(int senderid,string sender,string msg):base()
        {
            sid = senderid;
            s = sender;
            this.msg = new CText(msg);
            number = 1;
            size = (int)getsize(text);
        }
        public override int hash { get { return (new object[3] { sid, s, msg.raw }).GetHashCode(); } }
        public override void print(ref int posy)
        {
            lastrect = new Rect(0, posy - size, xsize, posy);
            GUI.Label(lastrect,text);
            posy -= size;
        }
        public override string text { get { return "<color=#3c3cdc>[" + sid + "]</color> " + s + " : " + msg + (number == 1 ? "" : (" <color=#00d9f9>[" + number + "]</color>")); } }
        public override int number
        {
            get;

            set;
        }

        public override Rect lastrect
        {
            get;
            set;
        }

        public override void onClick(Vector2 pos)
        {
            throw new NotImplementedException();
        }
    }
    protected class RawMsg : Chatcontent
    {
        private CText s;
        //private string s;
        private int size;
        public RawMsg(string msg):base()
        {
            s = new CText(msg);
            number = 1;
            //size = (int)(getsize(text).x) / xsize;
            //size++;
            size = (int)getsize(text);
        }
        public override int hash
        {
            get
            {
                return s.raw.GetHashCode();
            }
        }

        public override Rect lastrect
        {
            get;
            set;
        }

        public override int number
        {
            get;
            set;
        }

        public override void onClick(Vector2 pos)
        {
            throw new NotImplementedException();
        }

        public override string text { get { return s + (number == 1 ? "" : " <color=#00d9f9>[" + number + "]</color>"); } }

        public override void print(ref int ypos)
        {
            lastrect = new Rect(0, ypos - size, xsize, size);
            GUI.Label(lastrect, text);
            ypos -= size;
        }
    }



    public class CText
    {
        public static bool dolabellink = false;
        private static Dictionary<int, string> links = new Dictionary<int, string>();
        public static IEnumerable<int> ids { get { return links.Keys; } }
        public string raw;
        private string linklabelled;
        private static int currentlink = 0;
        public int mylinklabelstart;

        private static System.Text.RegularExpressions.Regex link = new System.Text.RegularExpressions.Regex(@"(ht|f)tp(s?)\:\/\/[0-9a-zA-Z]([-.\w]*[0-9a-zA-Z])*(:(0-9)*)*(\/?)([a-zA-Z0-9\-\.\?\,\'\/\\\+&amp;%\$#_]*)?");

        public CText(string s)
        {
            raw = s;
            mylinklabelstart = currentlink;
            currentlink += updatelinklabel();
        }
        /// <summary>
        /// used when the chat is cleared
        /// </summary>
        public static void recount_link()
        {
            currentlink = 0;
            links = new Dictionary<int, string>();
        }
        public string text
        {
            get { return dolabellink?linklabelled:raw; }
        }
        /// <summary>
        /// recalculate the string with the link numbers
        /// </summary>
        /// <returns>the number of links in the string</returns>
        private int updatelinklabel()
        {
            linklabelled = raw;
            System.Text.RegularExpressions.MatchCollection m = link.Matches(linklabelled);
            
            for (int i = m.Count - 1; i >= 0; i--)
            {
                
                System.Text.RegularExpressions.Match c = m[i];
                links[currentlink + i] = c.Value;
                linklabelled = linklabelled.Insert(c.Index + c.Length, "</i></color><color=#fca530><size=10>[" + (currentlink+i) + "]</size></color>");
                linklabelled = linklabelled.Insert(c.Index,"<color=#37519b><i>");
            }
            return m.Count;
        }
        public static void Openlink(int i)
        {
            Application.OpenURL(links[i]);
        }
        public override string ToString()
        {
            return text;
        }
    }
}

