using System;
using UnityEngine;

public class MapNameChange : MonoBehaviour
{
    private void OnSelectionChange()
    {
        
        Levels.Level info = Levels.LevelManager.getbyname(base.GetComponent<UIPopupList>().selection);
        if (info != null)
        {
            GameObject.Find("LabelLevelInfo").GetComponent<UILabel>().text = info.name+" pvp:"+info.pvp.ToString()+" PT:"+info.teamTitan.ToString();
        }
    }
}

