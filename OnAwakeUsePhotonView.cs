using Photon;
using System;
using UnityEngine;

[RequireComponent(typeof(PhotonView))]
public class OnAwakeUsePhotonView : Photon.MonoBehaviour
{
    private void Awake()
    {
        if (base.photonView.isMine)
        {
            base.photonView.RPC("OnAwakeRPC", PhotonTargets.All);
        }
    }

    [RPC]
    public void OnAwakeRPC()
    {
        Debug.Log("RPC: 'OnAwakeRPC' PhotonView: " + base.photonView);
    }

    [RPC]
    public void OnAwakeRPC(byte myParameter)
    {
        Debug.Log(string.Concat(new object[] { "RPC: 'OnAwakeRPC' Parameter: ", myParameter, " PhotonView: ", base.photonView }));
    }

    private void Start()
    {
        if (base.photonView.isMine)
        {
            base.photonView.RPC("OnAwakeRPC", PhotonTargets.All, (byte)1);
        }
    }
}

