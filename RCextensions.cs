﻿using System;
using UnityEngine;

public static class RCextensions
{
    public static void Add<T>(ref T[] source, T value)
    {
        T[] array = new T[source.Length + 1];
        int num;
        for (int i = 0; i < source.Length; i = num + 1)
        {
            array[i] = source[i];
            num = i;
        }
        array[array.Length - 1] = value;
        source = array;
    }

    public static string hexColor(this string text)
    {
        bool flag = text.Contains("]");
        if (flag)
        {
            text = text.Replace("]", ">");
        }
        bool flag2 = false;
        while (text.Contains("[") && !flag2)
        {
            int num = text.IndexOf("[");
            bool flag3 = text.Length >= num + 7;
            if (flag3)
            {
                string str = text.Substring(num + 1, 6);
                text = text.Remove(num, 7).Insert(num, "<color=#" + str);
                int startIndex = text.Length;
                bool flag4 = text.Contains("[");
                if (flag4)
                {
                    startIndex = text.IndexOf("[");
                }
                text = text.Insert(startIndex, "</color>");
            }
            else
            {
                flag2 = true;
            }
        }
        bool flag5 = flag2;
        string result;
        if (flag5)
        {
            result = string.Empty;
        }
        else
        {
            result = text;
        }
        return result;
    }

   

    public static bool isLowestID(this PhotonPlayer player)
    {
        PhotonPlayer[] playerList = PhotonNetwork.playerList;
        bool result;
        for (int i = 0; i < playerList.Length; i++)
        {
            PhotonPlayer photonPlayer = playerList[i];
            bool flag = photonPlayer.ID < player.ID;
            if (flag)
            {
                result = false;
                return result;
            }
        }
        result = true;
        return result;
    }

    public static Texture2D loadimage(WWW link, bool mipmap, int size)
    {
        Texture2D texture2D = new Texture2D(4, 4, TextureFormat.DXT1, mipmap);
        bool flag = link.size >= size;
        Texture2D result;
        if (flag)
        {
            result = texture2D;
        }
        else
        {
            Texture2D texture = link.texture;
            int num = texture.width;
            int num2 = texture.height;
            int i = 0;
            bool flag2 = num < 4 || (num & num - 1) != 0;
            if (flag2)
            {
                i = 4;
                num = Math.Min(num, 1023);
                while (i < num)
                {
                    i *= 2;
                }
            }
            else
            {
                bool flag3 = num2 < 4 || (num2 & num2 - 1) != 0;
                if (flag3)
                {
                    i = 4;
                    num2 = Math.Min(num2, 1023);
                    while (i < num2)
                    {
                        i *= 2;
                    }
                }
            }
            bool flag4 = i == 0;
            if (flag4)
            {
                if (mipmap)
                {
                    try
                    {
                        link.LoadImageIntoTexture(texture2D);
                    }
                    catch
                    {
                        texture2D = new Texture2D(4, 4, TextureFormat.DXT1, false);
                        link.LoadImageIntoTexture(texture2D);
                    }
                    result = texture2D;
                }
                else
                {
                    link.LoadImageIntoTexture(texture2D);
                    result = texture2D;
                }
            }
            else
            {
                bool flag5 = i < 4;
                if (flag5)
                {
                    result = texture2D;
                }
                else
                {
                    Texture2D texture2D2 = new Texture2D(4, 4, TextureFormat.DXT1, false);
                    link.LoadImageIntoTexture(texture2D2);
                    if (mipmap)
                    {
                        try
                        {
                            texture2D2.Resize(i, i, TextureFormat.DXT1, mipmap);
                        }
                        catch
                        {
                            texture2D2.Resize(i, i, TextureFormat.DXT1, false);
                        }
                    }
                    else
                    {
                        texture2D2.Resize(i, i, TextureFormat.DXT1, mipmap);
                    }
                    texture2D2.Apply();
                    result = texture2D2;
                }
            }
        }
        return result;
    }

    public static void RemoveAt<T>(ref T[] source, int index)
    {
        bool flag = source.Length == 1;
        if (flag)
        {
            source = new T[0];
        }
        else
        {
            bool flag2 = source.Length > 1;
            if (flag2)
            {
                T[] array = new T[source.Length - 1];
                int i = 0;
                int num = 0;
                while (i < source.Length)
                {
                    bool flag3 = i != index;
                    int num2;
                    if (flag3)
                    {
                        array[num] = source[i];
                        num2 = num;
                        num = num2 + 1;
                    }
                    num2 = i;
                    i = num2 + 1;
                }
                source = array;
            }
        }
    }

    public static bool returnBoolFromObject(object obj)
    {
        return obj != null && obj is bool && (bool)obj;
    }

    public static float returnFloatFromObject(object obj)
    {
        bool flag = obj != null && obj is float;
        float result;
        if (flag)
        {
            result = (float)obj;
        }
        else
        {
            result = 0f;
        }
        return result;
    }

    public static int returnIntFromObject(object obj)
    {
        bool flag = obj != null && obj is int;
        int result;
        if (flag)
        {
            result = (int)obj;
        }
        else
        {
            result = 0;
        }
        return result;
    }

    public static string returnStringFromObject(object obj)
    {
        bool flag = obj != null;
        string result;
        if (flag)
        {
            string text = obj as string;
            bool flag2 = text != null;
            if (flag2)
            {
                result = text;
                return result;
            }
        }
        result = string.Empty;
        return result;
    }
}
