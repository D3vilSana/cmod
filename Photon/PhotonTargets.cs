using System;

public enum PhotonTargets
{
    All,
    Others,
    MasterClient,
    AllBuffered,
    OthersBuffered,
    AllViaServer,
    AllBufferedViaServer,
    CMod_Users,
    No_CMod_Users
}

