﻿using System;
using UnityEngine;

public class TitanTrigger : MonoBehaviour
{
    //detects if player is near enough to justify colliders
    public bool isCollide;
    private void OnTriggerEnter(Collider other)
    {
        GameObject obj = other.transform.root.gameObject;
        if (obj.layer == 8)
        {
            GameObject myPlayer = Camera.main.GetComponent<IN_GAME_MAIN_CAMERA>().main_object;
            if (myPlayer != null && myPlayer == obj)
            {
                this.isCollide = true;
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        GameObject obj = other.transform.root.gameObject;
        if (obj.layer == 8)
        {
            GameObject myPlayer = Camera.main.GetComponent<IN_GAME_MAIN_CAMERA>().main_object;
            if (myPlayer != null && myPlayer == obj)
            {
                this.isCollide = false;
            }
        }
    }
}