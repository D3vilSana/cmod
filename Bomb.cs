﻿using Photon;
using System;
using System.Collections;
using UnityEngine;

public class Bomb : Photon.MonoBehaviour
{
    public bool disabled;

    public GameObject myExplosion;

    private Vector3 correctPlayerPos = Vector3.zero;

    private Quaternion correctPlayerRot = Quaternion.identity;

    private Vector3 correctPlayerVelocity = Vector3.zero;

    public float SmoothingDelay = 10f;

    public void Awake()
    {
        if (base.photonView != null)
        {
            base.photonView.observed = this;
            this.correctPlayerPos = base.transform.position;
            this.correctPlayerRot = Quaternion.identity;
            PhotonPlayer owner = base.photonView.owner;
            
            if (CustomGameMode.getint("pvp") > 0)
            {
                int num = RCextensions.returnIntFromObject(owner.customProperties[PhotonPlayerProperty.RCteam]);
                if (num == 1)
                {
                    base.GetComponent<ParticleSystem>().startColor = Color.cyan;
                }
                else if (num == 2)
                {
                    base.GetComponent<ParticleSystem>().startColor = Color.magenta;
                }
                else
                {
                    float r = RCextensions.returnFloatFromObject(owner.customProperties[PhotonPlayerProperty.RCBombR]);
                    float g = RCextensions.returnFloatFromObject(owner.customProperties[PhotonPlayerProperty.RCBombG]);
                    float b = RCextensions.returnFloatFromObject(owner.customProperties[PhotonPlayerProperty.RCBombB]);
                    base.GetComponent<ParticleSystem>().startColor = new Color(r, g, b, 1f);
                }
            }
            else
            {
                float r = RCextensions.returnFloatFromObject(owner.customProperties[PhotonPlayerProperty.RCBombR]);
                float g = RCextensions.returnFloatFromObject(owner.customProperties[PhotonPlayerProperty.RCBombG]);
                float b = RCextensions.returnFloatFromObject(owner.customProperties[PhotonPlayerProperty.RCBombB]);
                base.GetComponent<ParticleSystem>().startColor = new Color(r, g, b, 1f);
            }
        }
    }

    public void Explode(float radius)
    {
        this.disabled = true;
        base.rigidbody.velocity = Vector3.zero;
        Vector3 position = base.transform.position;
        this.myExplosion = PhotonNetwork.Instantiate("RCAsset/BombExplodeMain", position, Quaternion.Euler(0f, 0f, 0f), 0);
        foreach (HERO hERO in FengGameManagerMKII.Heroes.Values)
        {
            GameObject gameObject = hERO.gameObject;
            if (Vector3.Distance(gameObject.transform.position, position) < radius && !gameObject.GetPhotonView().isMine && !hERO.bombImmune)
            {
                PhotonPlayer owner = gameObject.GetPhotonView().owner;
                if (CustomGameMode.getint("pvp") > 0 && PhotonNetwork.player.customProperties[PhotonPlayerProperty.RCteam] != null && owner.customProperties[PhotonPlayerProperty.RCteam] != null)
                {
                    int num = RCextensions.returnIntFromObject(PhotonNetwork.player.customProperties[PhotonPlayerProperty.RCteam]);
                    int num2 = RCextensions.returnIntFromObject(owner.customProperties[PhotonPlayerProperty.RCteam]);
                    if (num == 0 || num != num2)
                    {
                        gameObject.GetComponent<HERO>().markDie();
                        gameObject.GetComponent<HERO>().photonView.RPC("netDie2", PhotonTargets.All, -1, RCextensions.returnStringFromObject(PhotonNetwork.player.customProperties[PhotonPlayerProperty.name]) + " " );
                        FengGameManagerMKII.instance.playerKillInfoUpdate(PhotonNetwork.player, 0);
                    }
                }
                else
                {
                    gameObject.GetComponent<HERO>().markDie();
                    gameObject.GetComponent<HERO>().photonView.RPC("netDie2", PhotonTargets.All, -1, RCextensions.returnStringFromObject(PhotonNetwork.player.customProperties[PhotonPlayerProperty.name]) + " " );
                    FengGameManagerMKII.instance.playerKillInfoUpdate(PhotonNetwork.player, 0);
                }
            }
        }
        base.StartCoroutine(this.WaitAndFade(1.5f));
    }

    private IEnumerator WaitAndFade(float time)
    {
        yield return new WaitForSeconds(time);
        PhotonNetwork.Destroy(this.myExplosion);
        PhotonNetwork.Destroy(base.gameObject);
        yield break;
    }

    public void Update()
    {
        if (!this.disabled && !base.photonView.isMine)
        {
            base.transform.position = Vector3.Lerp(base.transform.position, this.correctPlayerPos, Time.deltaTime * this.SmoothingDelay);
            base.transform.rotation = Quaternion.Lerp(base.transform.rotation, this.correctPlayerRot, Time.deltaTime * this.SmoothingDelay);
            base.rigidbody.velocity = this.correctPlayerVelocity;
        }
    }

    public void destroyMe()
    {
        if (base.photonView.isMine)
        {
            if (this.myExplosion != null)
            {
                PhotonNetwork.Destroy(this.myExplosion);
            }
            PhotonNetwork.Destroy(base.gameObject);
        }
    }

    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        if (stream.isWriting)
        {
            stream.SendNext(base.transform.position);
            stream.SendNext(base.transform.rotation);
            stream.SendNext(base.rigidbody.velocity);
        }
        else
        {
            this.correctPlayerPos = (Vector3)stream.ReceiveNext();
            this.correctPlayerRot = (Quaternion)stream.ReceiveNext();
            this.correctPlayerVelocity = (Vector3)stream.ReceiveNext();
        }
    }
}
