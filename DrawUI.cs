﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;


public class DrawUI:MonoBehaviour
{
    private LineRenderer lr_dir;

    private LineRenderer ntitan;

    public HERO myhero;

    public void Update()
    {
        if (myhero!=null)
        {
            GameObject titan = myhero.findNearestTitan();
            if (titan!=null)
            {
                ntitan.SetPosition(0, gameObject.transform.position);
                ntitan.SetPosition(1, gameObject.transform.position + titan.transform.forward);
            }
            if (myhero.checkBoxLeft!=null)
            {
                lr_dir.SetPosition(0, gameObject.transform.position);
                Vector3 v = new Vector3(gameObject.transform.forward.x,0, gameObject.transform.forward.z);
                lr_dir.SetPosition(1, gameObject.transform.position +v.normalized);
            }

        }


    }
    public void OnDestroyed()
    {
        UnityEngine.Object.Destroy(lr_dir);
        UnityEngine.Object.Destroy(ntitan);
    }
    public void Start()
    {
        GameObject g1 = new GameObject("sword1line");
        lr_dir = g1.AddComponent<LineRenderer>();
        lr_dir.material = new Material(Shader.Find("Unlit/Texture"));
        lr_dir.SetColors(Color.red, Color.red);
        lr_dir.SetWidth(.05f, .05f);

        GameObject g2 = new GameObject("titanneckdir");
        ntitan = g2.AddComponent<LineRenderer>();
        ntitan.SetWidth(.05f, .05f);

    }
}

