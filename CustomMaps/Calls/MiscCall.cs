﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CustomMaps.Calls
{
    class MiscCall : CallType
    {
        private IEnumerable<MiscSubCalls> s = CModUtilities.GetTypesof<MiscSubCalls>().Select(t => (MiscSubCalls)Activator.CreateInstance(t));
        public override string name
        {
            get
            {
                return "misc";
            }
        }

        public override void exec(string[] param)
        {
            foreach (MiscSubCalls su in s)
            {
                if (param[1].StartsWith(su.name))
                {
                    su.exec(param);
                    return;
                }
            }
        }

        public abstract class MiscSubCalls : MapBuilderCallable
        {
            public abstract string name { get; }
            public abstract void exec(string[] param);
        }
    }
}
