﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace CustomMaps.Calls
{
    class CustomCall : CallType
    {
        public override string name
        {
            get
            {
                return "custom";
            }
        }

        public override void exec(string[] param)
        {
            Vector3 pos = Parameters.ToVector3(param, 12, 13, 14);
            Quaternion rot = Parameters.ToQuaternion(param, 15, 16, 17, 18);
            GameObject go = Parameters.ToGameObjectRC(param, 1);//Cache the object ?
            GameObject newobj = (GameObject)UnityEngine.Object.Instantiate(go, pos, rot);
            if (param[2] != "default")
            {
                Parameters.ApplyTexture(param, newobj);
            }
            Parameters.SetScale(param, 3, 4, 5, newobj);
            Parameters.SetColor(param, newobj);
        }
    }
}
