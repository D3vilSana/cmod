﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CustomMaps.Calls
{
    class TitanSpawnCall : CallType
    {
        public static List<UnityEngine.Vector3> spawns = new List<UnityEngine.Vector3>();
        public override string name
        {
            get
            {
                return "titan";
            }
        }

        public override void exec(string[] param)
        {
            spawns.Add(Parameters.ToVector3(param, 1, 2, 3));
        }
    }
}
