﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CustomMaps.Calls
{
    class SpawnPointCall : CallType
    {
        private IEnumerable<SpawnPointSubCalls> s = CModUtilities.GetTypesof<SpawnPointSubCalls>().Select(t => (SpawnPointSubCalls)Activator.CreateInstance(t));
        public override string name
        {
            get
            {
                return "spawnpoint";
            }
        }

        public override void exec(string[] param)
        {
            foreach (SpawnPointSubCalls su in s)
            {
                if (param[1].StartsWith(su.name))
                {
                    su.exec(param);
                    return;
                }
            }
        }

        public abstract class SpawnPointSubCalls : MapBuilderCallable
        {
            public abstract string name { get; }
            public abstract void exec(string[] param);
        }
    }
}
