﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace CustomMaps.Calls.Photon
{
    class Cannon : PhotonCall.PhotonSubCalls
    {
        public override string name
        {
            get
            {
                return "Cannon";
            }
        }

        public override void exec(string[] param)
        {
            UnityEngine.Debug.LogError("CustomMap : Can't spawn Cannon ");
            return; //TODO : cannon
        }
    }
}
