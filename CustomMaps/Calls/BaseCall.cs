﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace CustomMaps.Calls
{
    class BaseCall : CallType
    {
        public override string name
        {
            get
            {
                return "base";
            }
        }

        public override void exec(string[] param)
        {
            if (param.Length<15)
            {
                simpleInstentiate(param);
            }
            else
            {
                completeInstentiate(param);
            }
        }

        private void simpleInstentiate(string[] p)
        {
            GameObject go = Parameters.ToGameObjectBase(p, 1);
            Vector3 pos = Parameters.ToVector3(p, 2, 3, 4);
            Quaternion rot = Parameters.ToQuaternion(p, 5, 6, 7, 8);
            UnityEngine.Object.Instantiate(go, pos, rot);
        }

        private void completeInstentiate(string[] p)
        {
            GameObject go = Parameters.ToGameObjectBase(p, 1);
            Vector3 pos = Parameters.ToVector3(p, 12, 13, 14);
            Quaternion rot = Parameters.ToQuaternion(p, 15, 16, 17, 18);
            GameObject ngo = (GameObject)UnityEngine.Object.Instantiate(go, pos, rot);
            if (p[2] != "default")
            {
                Parameters.ApplyTexture(p, ngo);
            }
            Parameters.SetScale(p, 3, 4, 5, ngo);
            Parameters.SetColor(p, ngo);
        }
    }
}
