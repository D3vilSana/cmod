﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace CustomMaps.Calls.Racing
{
    class Checkpoint : RacingCall.RacingSubCalls
    {
        public override string name
        {
            get
            {
                return "checkpoint";
            }
        }

        public override void exec(string[] param)
        {
            GameObject go = Parameters.ToGameObjectRC(param, 1);
            Vector3 pos = Parameters.ToVector3(param, 5, 6, 7);
            Quaternion rot = Parameters.ToQuaternion(param, 8, 9, 10, 11);
            GameObject n = (GameObject)UnityEngine.Object.Instantiate(go, pos, rot);
            Parameters.SetScale(param, 2, 3, 4, n);
            n.AddComponent<SpecialElements.RacingCheckpointTrigger>();
        }
    }
}
