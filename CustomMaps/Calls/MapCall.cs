﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CustomMaps.Calls
{
    class MapCall : CallType
    {
        private IEnumerable<MapSubCalls> s = CModUtilities.GetTypesof<MapSubCalls>().Select(t=> (MapSubCalls)Activator.CreateInstance(t));
        public override string name
        {
            get
            {
                return "map";
            }
        }

        public override void exec(string[] param)
        {
            foreach (MapSubCalls su in s)
            {
                if (param[1].StartsWith(su.name))
                {
                    su.exec(param);
                    return;
                }
            }
        }

        public abstract class MapSubCalls : MapBuilderCallable
        {
            public abstract string name { get; }

            public abstract void exec(string[] param);
        }
    }
}
