﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace CustomMaps.Calls
{
    static class Parameters
    {
        public static Vector3 ToVector3(string[] p,int x,int y,int z)
        {
            return new Vector3(Convert.ToSingle(p[x]), Convert.ToSingle(p[y]), Convert.ToSingle(p[z]));
        }

        public static Quaternion ToQuaternion(string[] p,int x,int y,int z,int w)
        {
            return new Quaternion(Convert.ToSingle(p[x]), Convert.ToSingle(p[y]), Convert.ToSingle(p[z]), Convert.ToSingle(p[w]));
        }

        public static GameObject ToGameObjectRC(string[] p,int n)
        {
            string prefabName = "RCAsset/"+p[n];
            GameObject gameObject;
            if (!PhotonNetwork.UsePrefabCache || !PhotonNetwork.PrefabCache.TryGetValue(prefabName, out gameObject))
            {
                gameObject = FengGameManagerMKII.InstantiateRCsset(prefabName);
                if (PhotonNetwork.UsePrefabCache)
                {
                    PhotonNetwork.PrefabCache.Add(prefabName, gameObject);
                }
            }
            return gameObject;
        }
        public static GameObject ToGameObjectBase(string[] p, int n)
        {
            return (GameObject)Resources.Load(p[n]);
        }
        public static void ApplyTexture(string[] p, GameObject go)
        {
            string textureAsset = p[2].StartsWith("transparent") ? "transparent" : p[2];
            Renderer[] componentsInChildren = go.GetComponentsInChildren<Renderer>();

            for (int j = 0; j < componentsInChildren.Length; j++)
            {
                Renderer renderer = componentsInChildren[j];
                renderer.material = (Material)FengGameManagerMKII.RCassets.Load(textureAsset);// TODO : use the cache !
                if (Convert.ToSingle(p[10]) != 1f || Convert.ToSingle(p[11]) != 1f)
                {
                    renderer.material.mainTextureScale = new Vector2(renderer.material.mainTextureScale.x * Convert.ToSingle(p[10]), renderer.material.mainTextureScale.y * Convert.ToSingle(p[11]));
                }
            }

        }
        public static void SetScale(string[] p, int x, int y, int z, GameObject go)
        {
            float sx = go.transform.localScale.x * Convert.ToSingle(p[x]) - 0.001f;
            float sy = go.transform.localScale.y * Convert.ToSingle(p[y]);
            float sz = go.transform.localScale.z * Convert.ToSingle(p[z]);

            go.transform.localScale = new Vector3(sx, sy, sz);
        }

        public static void SetColor(string[] p, GameObject go)
        {
            float a = 1f;
            if (p[2].StartsWith("transparent"))
            {
                float.TryParse(p[2].Substring(11), out a);
            }
            if (p[6] != "0")
            {
                Color color = new Color(Convert.ToSingle(p[7]), Convert.ToSingle(p[8]), Convert.ToSingle(p[9]), a);
                MeshFilter[] componentsInChildren2 = go.GetComponentsInChildren<MeshFilter>();
                for (int j = 0; j < componentsInChildren2.Length; j++)
                {
                    MeshFilter meshFilter = componentsInChildren2[j];
                    Mesh mesh = meshFilter.mesh;
                    Color[] array2 = new Color[mesh.vertexCount];
                    for (int k = 0; k < mesh.vertexCount; k++)
                    {
                        array2[k] = color;
                    }
                    mesh.colors = array2;
                }
            }
        }
    }
}
