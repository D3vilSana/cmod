﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CustomMaps.Calls.Map
{
    class DisableBounds : MapCall.MapSubCalls
    {
        public override string name
        {
            get
            {
                return "disablebounds";
            }
        }

        public override void exec(string[] param)
        {
            UnityEngine.Object.Destroy(UnityEngine.GameObject.Find("gameobjectOutSide"));
            UnityEngine.Object.Instantiate(FengGameManagerMKII.RCassets.Load("outside"));
        }
    }
}
