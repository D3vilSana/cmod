﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CustomMaps.Calls.SpawnPoint
{
    class PlayerSpawnMagenta : SpawnPointCall.SpawnPointSubCalls
    {
        public static List<UnityEngine.Vector3> spawns = new List<UnityEngine.Vector3>();
        public override string name
        {
            get
            {
                return "playerM";
            }
        }

        public override void exec(string[] param)
        {
            spawns.Add(Parameters.ToVector3(param, 2, 3, 4));
        }
    }
}
