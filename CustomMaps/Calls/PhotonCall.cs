﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CustomMaps.Calls
{
    class PhotonCall : CallType
    {
        private IEnumerable<PhotonSubCalls> s = CModUtilities.GetTypesof<PhotonSubCalls>().Select(t => (PhotonSubCalls)Activator.CreateInstance(t));
        public override string name
        {
            get
            {
                return "photon";
            }
        }

        public override void exec(string[] param)
        {
            if (!PhotonNetwork.isMasterClient) 
            {
                return;
            }
            foreach (PhotonSubCalls su in s)
            {
                if (param[1].StartsWith(su.name))
                {
                    su.exec(param);
                    return;
                }
            }
            UnityEngine.Debug.LogError("CustomMap : Cant spawn Titanspawner");
            //TODO : if is not cannon (titanspawners)
        }

        public abstract class PhotonSubCalls : MapBuilderCallable
        {
            public abstract string name { get; }

            public abstract void exec(string[] param);
        }
    }
}
