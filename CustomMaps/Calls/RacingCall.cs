﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CustomMaps.Calls
{
    class RacingCall : CallType
    {
        private IEnumerable<RacingSubCalls> s = CModUtilities.GetTypesof<RacingSubCalls>().Select(t => (RacingSubCalls)Activator.CreateInstance(t));
        public override string name
        {
            get
            {
                return "racing";
            }
        }

        public override void exec(string[] param)
        {
            foreach (RacingSubCalls su in s)
            {
                if (param[1].StartsWith(su.name))
                {
                    su.exec(param);
                    return;
                }
            }
        }

        public abstract class RacingSubCalls : MapBuilderCallable
        {
            public abstract string name { get; }

            public abstract void exec(string[] param);
        }
    }
}
