﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace CustomMaps.Calls.Miscs
{
    class Region : MiscCall.MiscSubCalls
    {
        public override string name
        {
            get
            {
                return "region";
            }
        }

        public override void exec(string[] param)
        {
            if (!PhotonNetwork.isMasterClient)
            {
                return;
            }
            Vector3 pos = Parameters.ToVector3(param, 6, 7, 8);
            Vector3 size = Parameters.ToVector3(param, 3, 4, 5);
            SpecialElements.Region region = new SpecialElements.Region(pos, size);

            SpecialElements.Region.regions.Add(param[2], region);
            //TODO : add trigger gameobject ??
        }
    }
}
