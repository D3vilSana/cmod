﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace CustomMaps.SpecialElements
{
    public class RacingCheckpointTrigger : MonoBehaviour
    {
        private void OnTriggerEnter(Collider other)
        {
            GameObject gameObject = other.gameObject;
            if (gameObject.layer == 8)
            {
                gameObject = gameObject.transform.root.gameObject;
                if (gameObject.GetPhotonView() != null && gameObject.GetPhotonView().isMine)
                {
                    HERO component = gameObject.GetComponent<HERO>();
                    if (component != null)
                    {
                        FengGameManagerMKII.instance.chat.addLINE("<color=#00ff00>Checkpoint ! <b>(*•̀ᴗ•́*)و ̑̑ </b></color>");
                        gameObject.GetComponent<HERO>().fillGas();
                        Levels.Attributes.Checkpoint r = FengGameManagerMKII.level.GetAttribute<Levels.Attributes.Checkpoint>();
                        if (r!=null)
                        {
                            r.respawn = gameObject.transform.position;
                        }
                    }
                }
            }
        }
    }

}
