﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace CustomMaps.SpecialElements
{
    class Region
    {
        public static Dictionary<string, Region> regions = new Dictionary<string, Region>();
        private Vector3 pos;
        private Vector3 size;

        public Region(Vector3 pos,Vector3 size)
        {
            this.pos = pos;
            this.size = size;
        }

    }
}
