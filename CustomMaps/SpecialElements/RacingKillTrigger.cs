﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CustomMaps.SpecialElements
{
    using UnityEngine;

    public class RacingKillTrigger : MonoBehaviour
    {
        private void OnTriggerEnter(Collider other)
        {
            GameObject gameObject = other.gameObject;
            if (gameObject.layer == 8)
            {
                gameObject = gameObject.transform.root.gameObject;
                if (gameObject.GetPhotonView() != null && gameObject.GetPhotonView().isMine)
                {
                    HERO component = gameObject.GetComponent<HERO>();
                    if (component != null)
                    {
                        component.markDie();
                        component.photonView.RPC("netDie2", PhotonTargets.All, -1, "Server (Noscope)" );
                    }
                }
            }
        }
    }

}
