﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CustomMaps
{
    interface MapBuilderCallable
    {
        string name { get; }
        void exec(string[] param);
    }
}
