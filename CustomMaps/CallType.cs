﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CustomMaps
{
    abstract class CallType : MapBuilderCallable
    {
        private static IEnumerable<CallType> s = CModUtilities.GetTypesof<CallType>().Select(t => (CallType)Activator.CreateInstance(t));

        public abstract string name { get; }

        public abstract void exec(string[] param);

        public static void execLine(string line)
        {
            string[] p = line.Split(',');
            foreach (CallType su in s)
            {
                if (p[0].StartsWith(su.name))
                {
                    su.exec(p);
                    return;
                }
            }
        }

    }
}
