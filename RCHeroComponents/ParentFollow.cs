﻿using System;
using UnityEngine;

public class ParentFollow : MonoBehaviour
{
    private Transform parent;

    private Transform bTransform;

    public bool isActiveInScene;

    public void SetParent(Transform transform)
    {
        this.parent = transform;
        this.bTransform.rotation = transform.rotation;
    }

    public void RemoveParent()
    {
        this.parent = null;
    }

    private void Awake()
    {
        this.bTransform = base.transform;
        this.isActiveInScene = true;
    }

    private void Update()
    {
        if (this.isActiveInScene && this.parent != null)
        {
            this.bTransform.position = this.parent.position;
        }
    }
}
