﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;


static class CommandManager
{
    private static List<ChatCommand> commands;
    private static string oldtext = "";
    public static string hints = "";

    public static void Init()
    {
        commands = new List<ChatCommand>();
        LoadComands();

    }

    private static void LoadComands()
    {
        commands.Add(new Command("pm", delegate (string[] p)
         {
             int i;
             if (int.TryParse(p[0], out i))
             {
                 FengGameManagerMKII.instance.photonView.RPC("ChatPM", PhotonPlayer.Find(i), new object[]
                                                                                     {
                                                                                        RCextensions.returnStringFromObject(PhotonNetwork.player.customProperties[PhotonPlayerProperty.name]).hexColor(),
                                                                                        string.Join(" ",p,1,p.Length-1)
                                                                                     });
                 FengGameManagerMKII.instance.chat.addLINE("<color=#fdcb12> ->[" + i + "]</color> " + RCextensions.returnStringFromObject(PhotonPlayer.Find(i).customProperties[PhotonPlayerProperty.name]).hexColor() + " : " + string.Join(" ", p, 1, p.Length - 1));
             }
         }, new Command.Parameter.PlayerID(), new Command.Parameter.Anytext("Message")));
        commands.Add(new MasterCommand("room", delegate (string[] a)
         {
             int b;
             if (int.TryParse(a[2],out b))
             {
                 if (PhotonNetwork.isMasterClient)
                 {
                     FengGameManagerMKII.instance.addtime(b);
                 }
                 else
                 {
                     FengGameManagerMKII.instance.chat.addLINE("You are not Master (feelsbadman)");
                 }
                 
             }
         }, new Command.Parameter.StringParam("time"),new Command.Parameter.StringParam("add"), new Command.Parameter.Int("Time")));
        commands.Add(new MasterCommand("kick", delegate (string[] a)
         {
             int b;
             if (int.TryParse(a[0], out b))
             {
                 KickBanManager.kick(b);
             }

         }, new Command.Parameter.PlayerID()));
        commands.Add(new MasterCommand("ban", delegate (string[] a)
        {
            int b;
            if (int.TryParse(a[0], out b))
            {
                KickBanManager.ban(b);
            }

        }, new Command.Parameter.PlayerID()));
        commands.Add(new MasterCommand("spawn", delegate (string[] a)
         {
             
             if (!PhotonNetwork.isMasterClient)
             {
                 FengGameManagerMKII.instance.chat.addLINE("ERROR : You must be masterclient !");
                 return;
             }
             int b;
             if (int.TryParse(a[0], out b))
             {
                 if (b<0)
                 {
                     return;
                 }
                 for (int i = 0; i < b; i++)
                 {
                     FengGameManagerMKII.instance.randomSpawnOneTitan("titanRespawn");
                 }
                 FengGameManagerMKII.instance.chat.addLINE(" titans spawned !");
             }
         }, new Command.Parameter.Int("Number"), new Command.Parameter.StringParam("titans")));
        commands.Add(new Command("clear", delegate (string[] a)
         {
             FengGameManagerMKII.instance.chat.clear();
         }));
        commands.Add(new MasterCommand("spawn", delegate (string[] a)
         {
             if (!PhotonNetwork.isMasterClient)
             {
                 FengGameManagerMKII.instance.chat.addLINE("ERROR : You must be masterclient !");
                 return;
             }
             int b;
             if (int.TryParse(a[0], out b))
             {
                 int c = Array.IndexOf<string>(Command.Parameter.TitanType.types, a[1]);
                 AbnormalType t = (AbnormalType)c;

                 UnityEngine.GameObject[] objArray = UnityEngine.GameObject.FindGameObjectsWithTag("titanRespawn");
                 int index = UnityEngine.Random.Range(0, objArray.Length);
                 UnityEngine.GameObject obj2 = objArray[index];
                 while (objArray[index] == null)
                 {
                     index = UnityEngine.Random.Range(0, objArray.Length);
                     obj2 = objArray[index];
                 }
                 objArray[index] = null;

                 for (int i = 0; i < b; i++)
                 {
                     UnityEngine.GameObject obj;
                     if (IN_GAME_MAIN_CAMERA.gametype == GAMETYPE.SINGLE)
                     {
                         obj = (UnityEngine.GameObject)UnityEngine.Object.Instantiate(UnityEngine.Resources.Load("TITAN_VER3.1"), obj2.transform.position, obj2.transform.rotation);
                     }
                     obj = PhotonNetwork.Instantiate("TITAN_VER3.1", obj2.transform.position, obj2.transform.rotation, 0);
                     obj.GetComponent<TITAN>().setAbnormalType(t, false);
                     UnityEngine.GameObject obj3;
                     if (IN_GAME_MAIN_CAMERA.gametype == GAMETYPE.SINGLE)
                     {
                         obj3 = (UnityEngine.GameObject)UnityEngine.Object.Instantiate(UnityEngine.Resources.Load("FX/FXtitanSpawn"), obj2.transform.position, UnityEngine.Quaternion.Euler(-90f, 0f, 0f));
                     }
                     else
                     {
                         obj3 = PhotonNetwork.Instantiate("FX/FXtitanSpawn", obj2.transform.position, UnityEngine.Quaternion.Euler(-90f, 0f, 0f), 0);
                     }
                     obj3.transform.localScale = obj.transform.localScale;
                 }
             }
         }, new Command.Parameter.Int("Number"), new Command.Parameter.TitanType()));
        commands.Add(new MasterCommand("restart", delegate (string[] a)
         {
             if (PhotonNetwork.isMasterClient)
             {
                 FengGameManagerMKII.instance.restartGame(false);
             }
             
         }));
        commands.Add(new MasterCommand("revive", delegate (string[] a)
         {
            int b;
             if (int.TryParse(a[0], out b))
             {
                 PhotonPlayer p = PhotonPlayer.Find(b);
                 if (p!=null)
                 {
                     if (PhotonNetwork.isMasterClient)
                     {
                         FengGameManagerMKII.instance.photonView.RPC("respawnHeroInNewRound", p, new object[0]);
                     }
                     else
                     {
                         FengGameManagerMKII.instance.chat.addLINE("<color=#ff0000>ERROR : Not master</color>");
                     }
                 }
                 else
                 {
                     FengGameManagerMKII.instance.chat.addLINE("<color=#ff0000>ERROR : Player not found</color>");
                 }

             }
             else
             {
                 FengGameManagerMKII.instance.chat.addLINE("<color=#ff0000>ERROR : Bad Argumets</color>");
             }
         }, new Command.Parameter.PlayerID()));
        commands.Add(new MasterCommand("revive", delegate (string[] a)
         {
             if (PhotonNetwork.isMasterClient)
             {
                 FengGameManagerMKII.instance.photonView.RPC("respawnHeroInNewRound", PhotonTargets.AllBuffered, new object[0]);
             }
             else
             {
                 FengGameManagerMKII.instance.chat.addLINE("<color=#ff0000>ERROR : Not master</color>");
             }
         }, new Command.Parameter.StringParam("all")));
        commands.Add(new MasterCommand("room", delegate (string[] a)
         {
             int b;
             if (PhotonNetwork.isMasterClient&&int.TryParse(a[1],out b))
             {
                 PhotonNetwork.room.maxPlayers = b;
             }
             else
             {
                 FengGameManagerMKII.instance.chat.addLINE("FAIL");
             }
         }, new Command.Parameter.StringParam("max"), new Command.Parameter.Int("Maximum")));
        commands.Add(new MasterCommand("room", delegate (string[] a)
         {
             if (PhotonNetwork.isMasterClient)
             {
                 PhotonNetwork.room.visible = Command.Parameter.Boolean.parse(a[1]);
             }
             else
             {
                 FengGameManagerMKII.instance.chat.addLINE("FAIL");
             }
         }, new Command.Parameter.StringParam("visible"), new Command.Parameter.Boolean()));
        commands.Add(new MasterCommand("room", delegate (string[] a)
         {
             if (PhotonNetwork.isMasterClient)
             {
                 PhotonNetwork.room.open = Command.Parameter.Boolean.parse(a[1]);
             }
             else
             {
                 FengGameManagerMKII.instance.chat.addLINE("FAIL");
             }
         }, new Command.Parameter.StringParam("open"), new Command.Parameter.Boolean()));
        commands.Add(new MasterCommand("r", delegate (string[] a)
         {
             if (PhotonNetwork.isMasterClient)
             {
                 FengGameManagerMKII.instance.restartGame(false);
             }
         }));
        /*commands.Add(new Command("tp", delegate (string[] a) {

                HERO h = Command.Parameter.HEROID.parse(a[0]);

                FengGameManagerMKII.instance.myhero.transform.position = h.transform.position + UnityEngine.Vector3.up * .5f;

        }, new Command.Parameter.HEROID()));*/
        commands.Add(new MasterCommand("tp", delegate (string[] a) {
            HERO h = Command.Parameter.HEROID.parse(a[1]);
            foreach (HERO i in FengGameManagerMKII.Heroes.Values)
            {
                    i.photonView.RPC("moveToRPC",i.photonView.owner, new object[3] { h.transform.position.x, h.transform.position.y+.5f, h.transform.position.z });
            }
        }, new Command.Parameter.StringParam("all"),new Command.Parameter.HEROID()));
        commands.Add(new Command("link", s => Command.Parameter.ChatLinkID.exec(s[0]), new Command.Parameter.ChatLinkID(),new Command.Parameter.StringParam("open")));
        
        FileManager.Datadirectory d = new FileManager.Datadirectory("skins","skin");
        commands.Add(new Command("skin", delegate (string[] a)
         {
             FileManager.DataFile f = d.parseparam(a[1]);
             SkinManager.Setskin(f);
         }, new Command.Parameter.StringParam("load"), d.fileparameter));

        commands.Add(new Command("quote", delegate (string[] a)
         {
             List<ChatHandler.Chatcontent> l = Command.Parameter.ChatMessageIDs.parse(a[1]);
             string s = "<b><i>---<color=#fd0c1f>[QUOTE]</color>---</i></b>\n";
             s+= string.Join("\n", l.Select(r => r.text).ToArray());
             s += "\n<b><i>-<color=#fd0c1f>[END QUOTE]</color>-</i></b>";
             FengGameManagerMKII.instance.photonView.RPC("Chat",PhotonTargets.All,new object[]{" ",s});
         }, new Command.Parameter.StringParam("chat"), new Command.Parameter.ChatMessageIDs()));

        FileManager.Datadirectory dd = new FileManager.Datadirectory("quotes", "txt");
        commands.Add(new Command("quote", delegate (string[] a)
         {
             List<ChatHandler.Chatcontent> l = Command.Parameter.ChatMessageIDs.parse(a[1]);


             dd.writeinnewfile(string.Join("\n", l.Select(r => r.text).ToArray()), a[2]);
             FengGameManagerMKII.instance.chat.addLINE("Sucess !!");

         }, new Command.Parameter.StringParam("save"), new Command.Parameter.ChatMessageIDs(),new Command.Parameter.Anytext("file name")));
        commands.Add(new Command("quote", delegate (string[] a)
         {
             FileManager.DataFile f = dd.parseparam(a[1]);
             string s = "<b><i>---<color=#fd0c1f>[QUOTE]</color>---</i></b>\n";
             s += f.alltext;
             s += "\n<b><i>-<color=#fd0c1f>[END QUOTE]</color>-</i></b>";
             FengGameManagerMKII.instance.photonView.RPC("Chat", PhotonTargets.All, new object[] { " ", s });
         }, new Command.Parameter.StringParam("load"), dd.fileparameter));

    }

        
   



    /// <summary>
    /// execute a command
    /// </summary>
    /// <param name="text"></param>
    /// <returns>true if text is a command</returns>
    public static bool parse(string text)
    {
        hints = "";
        oldtext = "";
        text = text.Trim(' ','\t');
        if (!text.StartsWith("/"))
        {
            return false;
        }
        string[] p = text.Split(new char[1] { ' ' }, 2);
        if (p.Length==1)
        {
            p = new string[2] { text, "" };
        }
        ChatCommand[] c = commands.Where(u => ("/" + u.name) == p[0]).Where(u => u.isme(p[1])).ToArray();
        if (c.Length != 0)
        {
            c[0].exec(p[1]);
        }
        return true;
    }

    public static void autocompupdate(string text)
    {
        bool b = text.EndsWith(" ");
        text = text.TrimStart(' ', '\t');
        if (oldtext == text)
        {
            return;
        }
        oldtext = text;
        hints = "";
        if (!text.StartsWith("/"))
        {
            return;
        }


        string[] p = text.Split(new char[1] { ' ' }, 2);
        if (p.Length == 1)
        {
            p = new string[2] { text, "" };
        }
        if (p[1]=="")
        {
            if (!b)
            {//first parameter
                string[] m = commands.Where(q => q.enabled).Select(q=>q.name).Where(q=>("/"+q).StartsWith(p[0])).Distinct().ToArray();
                hints = "<b><color=#8113b6>"+String.Join(" ", m)+"</color></b>";
            }
            else
            {//second parameter
                IEnumerable<ChatCommand> m = commands.Where(q => q.enabled).Where(y => "/"+y.name ==p[0]);
                List<string> hint = new List<string>();
                foreach (Command com in m)
                {
                    hint.AddRange(com.getpossibleparameters(""));
                }
                hints = "<b><i><color=#48b613>" + String.Join(" ", hint.Distinct().ToArray())+"</color></i></b>";
            }
        }
        else
        {
            IEnumerable<ChatCommand> m = commands.Where(q => q.enabled).Where(y =>"/"+y.name == p[0]);
            List<string> hint = new List<string>();
            foreach (ChatCommand comm in m)
            {
                hint.AddRange(comm.getpossibleparameters(p[1]));
            }
            hints = "<b><i><color=#48b613>" + String.Join(" ", hint.Distinct().ToArray()) + "</color></i></b>";
        }

    }

    public static string getcompletion(string text)
    {
        bool b = text.EndsWith(" ");
        text = text.TrimStart(' ', '\t');

        oldtext = text;
        hints = "";
        if (!text.StartsWith("/"))
        {
            return "";
        }


        string[] p = text.Split(new char[1] { ' ' }, 2);
        if (p.Length == 1)
        {
            p = new string[2] { text, "" };
        }
        List<string> hint = new List<string>();
        if (p[1] == "")
        {
            if (!b)
            {//first parameter
                hint = commands.Where(q => q.enabled).Select(q => q.name).Where(q => ("/" + q).StartsWith(p[0])).Distinct().ToList();

            }
            else
            {//second parameter
                IEnumerable<ChatCommand> m = commands.Where(q => q.enabled).Where(y => "/" + y.name == p[0]);
                foreach (ChatCommand com in m)
                {
                    hint.AddRange(com.getpossibleparameters(""));
                }

            }
        }
        else
        {
            IEnumerable<ChatCommand> m = commands.Where(q => q.enabled).Where(y => "/" + y.name == p[0]);
            
            foreach (ChatCommand comm in m)
            {
                hint.AddRange(comm.getpossibleparameters(p[1]));
            }
        }
        if (hint.Count!=0)
        {
            return hint[0];
        }
        else
        {
            return "";
        }
        
    }
}

