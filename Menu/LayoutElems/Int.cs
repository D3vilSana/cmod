﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Menu.LayoutElems
{

    public class Int : ConfigElem
    {
        public string text;
        public string name;
        protected Action<int, Int> action;
        public Int(Action<int, Int> action, string name, string text = "", float size = 80)
        {
            this.text = text;
            this.action = action;
            this.size = new Vector2(size + 30, 40);
            this.name = name;
        }
        public override Vector2 size
        {
            get;
            internal set;
        }
        public override void draw(Vector2 pos)
        {
            GUI.Label(new Rect(pos.x, pos.y, size.x, 20), name + " :");
            text = GUI.TextField(new Rect(pos.x, pos.y + 20, size.x - 30, 20), text);
            if (GUI.Button(new Rect(pos.x + size.x - 30, pos.y + 20, 30, 20), "OK"))
            {
                int i;
                if (Int32.TryParse(text,out i))
                {
                    action.Invoke(i, this);
                }
                else
                {
                    text = "";
                }
            }
        }
    }
}
