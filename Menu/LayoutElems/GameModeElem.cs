﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Menu.LayoutElems
{
    public class GameModeElem : ConfigElem
    {
        protected string text;
        protected CustomGameMode.Mode mode;
        public GameModeElem(CustomGameMode.Mode m)
        {
            mode = m;
            text = mode.tostring();
            if (typeof(bool) == mode.ptype())
            {
                size = new Vector2(100, 20);
            }
            else
            {
                size = new Vector2(110, 45);
            }
        }
        public override Vector2 size
        {
            get;
            internal set;
        }

        public override void draw(Vector2 pos)
        {
            if (typeof(bool) == mode.ptype())
            {
                if (PhotonNetwork.isMasterClient)
                {
                    bool n = GUI.Toggle(new Rect(pos.x, pos.y, size.x, size.y), mode.tobool(), mode.pname);
                    if (n != mode.tobool())
                    {
                        mode.set(n?"1":"0");
                    }
                }
                else
                {
                    GUI.Toggle(new Rect(pos.x, pos.y, size.x, size.y), mode.tobool(), mode.pname);
                }
            }
            else {
                GUI.Label(new Rect(pos.x, pos.y+5, size.x, 20), mode.pname + " :");
                if (PhotonNetwork.isMasterClient)
                {
                    text = GUI.TextField(new Rect(pos.x, pos.y + 25, size.x - 30, 20), text);
                    if (GUI.Button(new Rect(pos.x + size.x - 30, pos.y + 25, 30, 20), "OK"))
                    {
                        mode.set(text);
                    }
                }
                else
                {
                    GUI.TextField(new Rect(pos.x, pos.y + 25, size.x, 20), mode.tostring());
                }
            }
        }
    }
}
