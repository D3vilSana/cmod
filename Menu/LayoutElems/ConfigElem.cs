﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public abstract class ConfigElem
{
    public abstract Vector2 size { get; internal set; }
    public abstract void draw(Vector2 pos);
}

