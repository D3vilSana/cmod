﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Menu.LayoutElems
{
    public class Button : ConfigElem
    {
        public string text;
        private Action<Button> action;
        public override Vector2 size
        {
            get;
            internal set;
        }
        public Button(string text,Action<Button> action,Vector2 size)
        {
            this.text = text;
            this.action = action;
            this.size = size;
        }
        public Button(string text, Action<Button> action)
        {
            this.text = text;
            this.action = action;
            this.size = new Vector2(75, 20);

        }

        public override void draw(Vector2 pos)
        {
            if (GUI.Button(new Rect(pos.x,pos.y,size.x,size.y),text))
            {
                action.Invoke(this);
            }
        }
    }
}
