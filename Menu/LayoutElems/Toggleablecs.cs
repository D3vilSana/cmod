﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Menu.LayoutElems
{
    public class Toggleable : ConfigElem
    {
        private string name;
        private Action<bool,Toggleable> action;
        public bool val;

        public Toggleable(string name,Action<bool,Toggleable> action,bool val=false)
        {
            this.name = name;
            this.action = action;
            size = new Vector2(100, 20);
            this.val = val;

        }
        public Toggleable(string name,Action<bool,Toggleable> action,Vector2 size,bool val=false)
        {
            this.name = name;
            this.action = action;
            this.size = size;
            this.val = val;
        }


        public override Vector2 size
        {
            get;
            internal set;
        }
        public override void draw(Vector2 pos)
        {
            bool n = GUI.Toggle(new Rect(pos.x, pos.y, size.x, size.y), val, name);
            if (n != val)
            {
                val = n;
                action.Invoke(val, this);
            }
        }
    }
}
