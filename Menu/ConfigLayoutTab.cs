﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Menu
{
    public class ConfigLayoutTab : Menu.LayoutTab
    {
        private string Tname;
        public ConfigLayoutTab(string Tabname) { Tname = Tabname; }
        public override string name
        {
            get
            {
                return Tname;
            }
        }
        public override bool doshow()
        {
            return true;
        }
    }
}
