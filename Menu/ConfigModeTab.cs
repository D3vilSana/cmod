﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Menu
{
    public class ConfigModeTAb : LayoutTab // Only for master
    {
        public override string name
        {
            get
            {
                return "ModeConfig";
            }
        }
        public override void Init()
        {
            base.Init();

            AddElem(new ModesConfigElems.Titansize(CustomGameMode.getMode("MinSize"),CustomGameMode.getMode("MaxSize"),CustomGameMode.getMode("CustomSize")));
            AddfromCustomgame("DisableMinimap");
            AddfromCustomgame("TitanNumber");
            AddfromCustomgame("Nape");
            AddfromCustomgame("RespawnTime");
            AddfromCustomgame("Horses");
            CustomGameMode.Mode[] m = { CustomGameMode.getMode("SpawnRate_N"), CustomGameMode.getMode("SpawnRate_A"), CustomGameMode.getMode("SpawnRate_J"), CustomGameMode.getMode("SpawnRate_C"), CustomGameMode.getMode("SpawnRate_P") };
            AddElem(new ModesConfigElems.TitanSpawnRate(CustomGameMode.getMode("CustomSpawnModes"),m));
            AddfromCustomgame("deathban");
            AddfromCustomgame("Motd");
            AddfromCustomgame("nopvphook");
            AddfromCustomgame("Bomb");
        }
        private void AddfromCustomgame(string a)
        {
            AddElem(new Menu.LayoutElems.GameModeElem(CustomGameMode.getMode(a)));
        }
        public override bool doshow()
        {
            return PhotonNetwork.isMasterClient;
        }
    }
}
