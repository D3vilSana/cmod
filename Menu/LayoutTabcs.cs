﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using System.Text;

namespace Menu
{

    public abstract class LayoutTab : Menutab
    {
        protected List<ConfigElem> elems;
        protected bool isopen = false;
        public override void hide()
        {
            isopen = false;
        }
        public override void show()
        {
            isopen = true;
        }
        public override void Init()
        {
            elems = new List<ConfigElem>();
        }
        public void AddElem(ConfigElem elem)
        {
            elems.Add(elem);
        }
        public void RemoveElem(ConfigElem elem)
        {
            elems.Remove(elem);
        }
        public override void OnGui(Vector2 size)
        {
            if (!isopen)
            {
                return;
            }
            float maxx = 0;
            Vector2 pos = new Vector2(0, 0);
            foreach (ConfigElem c in elems)
            {
                if (c.size.y + pos.y > size.y)
                {
                    pos.x = maxx + 5;
                    pos.y = 0;
                    //new column
                }
                c.draw(pos);
                pos.y += c.size.y;
                if (c.size.x + pos.x > maxx)
                {
                    maxx = (c.size.x + pos.x);
                }

            }
        }
        //TODO : if maxx
    }
}
