﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Menu
{
    public class ServerManagement : Menutab
    {
        private bool showtab = false;
        private static Vector2 maxclientsize;
        private Vector2 scrollpos = new Vector2();
        private GUIStyle center = new GUIStyle();
        private GUIStyle defaultstyle = new GUIStyle();
        public override string name
        {
            get
            {
                return "Server";
            }
        }

        public override void hide()
        {
            showtab = false;
        }

        public override void Init()
        {
            maxclientsize = new Vector2(750,50);
            center.alignment = TextAnchor.UpperCenter;
            center.richText = true;
            center.normal.textColor = Color.white;
            defaultstyle.richText = true;
            defaultstyle.normal.textColor = Color.white;
        }

        public override void OnGui(Vector2 size)
        {
            if (!showtab)
            {
                return;
            }
            else
            {
                PhotonPlayer[] p = PhotonNetwork.playerList.Where(b => !KickBanManager.isignored(b.ID)).ToArray();
                Rect r = new Rect(0, 0, maxclientsize.x, (maxclientsize.y+5) * p.Length);
                scrollpos = GUI.BeginScrollView(new Rect(0, 0, size.x, size.y),scrollpos,r);
                for (int i = 0; i < p.Length; i++)
                {
                    Rect rect = new Rect(0, ((int)maxclientsize.y + 5) * i, maxclientsize.x <size.x?size.x : maxclientsize.x, maxclientsize.y);
                    GUI.BeginGroup(rect);
                    Clientinfos(p[i]);
                    GUI.EndGroup();
                }
                GUI.EndScrollView(true);
            }
        }
           
        private void Clientinfos(PhotonPlayer p)
        {
            #region toplabel
            string txt = "";
            if (p.isMasterClient)
            {
                txt += "<color=#ff5722>";
            }
            else if (p.isLocal)
            {
                txt += "<color=#062e60>";
            }
            else
            {
                txt += "<color=#c6b7b7>";
            }
            GUI.Box(new Rect(0, 0, maxclientsize.x, maxclientsize.y),"");
            txt += "[" + p.ID + "]</color> ";
            /*
            if (p.customProperties.ContainsKey("RCteam"))
            {
                txt += "<color=#fc9ece>[RC]</color>";
            }
            if (p.customProperties.ContainsKey("NRC"))
            {
                txt += "<color=#53802d>[NRC]</color>";
            }
            if (p.customProperties.ContainsKey("KageNoKishi"))
            {
                txt += "<color=#d7a5ff>KnK</color>";
            }
            */
            txt += p.mod.ToString();
            if (RCextensions.returnIntFromObject(p.customProperties[PhotonPlayerProperty.isTitan]) == 2)
            {
                txt += "<color=#F5DA81>[T]</color>";
            }
            else if (RCextensions.returnIntFromObject(p.customProperties[PhotonPlayerProperty.team]) < 2)
            {
                txt += "<color=#A9D0F5>[H]</color>";
            }
            else
            {
                txt += "<color=#82FA58>[A]</color>";
            }

            txt+= RCextensions.returnStringFromObject(p.customProperties[PhotonPlayerProperty.name]).Replace("[-]","").hexColor() + " ";
            txt +=
                RCextensions.returnIntFromObject(p.customProperties[PhotonPlayerProperty.kills]) + "/" +
                RCextensions.returnIntFromObject(p.customProperties[PhotonPlayerProperty.deaths]) + "/" +
                RCextensions.returnIntFromObject(p.customProperties[PhotonPlayerProperty.max_dmg]) + "/" +
                RCextensions.returnIntFromObject(p.customProperties[PhotonPlayerProperty.total_dmg]);
            GUI.Label(new Rect(0, 0, maxclientsize.x, 20), txt, center);
            #endregion
            if (PhotonNetwork.isMasterClient)
            {
                if (GUI.Button(new Rect(5,20,40,20),"Kick"))
                {
                    KickBanManager.kick(p);
                }
                if (GUI.Button(new Rect(50,20,40,20),"Ban"))
                {
                    KickBanManager.ban(p);
                }
                if (GUI.Button(new Rect(195,20,60,20),"SetMC"))
                {
                    PhotonNetwork.networkingPeer.SetMasterClient(p.ID, true);
                }
            }
            else
            {
                if (GUI.Button(new Rect(5, 20, 40, 20), "Kick"))
                {
                    FengGameManagerMKII.instance.photonView.RPC("Chat", PhotonTargets.MasterClient, "/kick #"+p.ID,String.Empty );
                }
                if (GUI.Button(new Rect(50, 20, 60, 20), "Ignore"))
                {
                    FengGameManagerMKII.instance.photonView.RPC("Chat", PhotonTargets.All, "<color=#fa6607>This player ignored #" + p.ID+" he won't see him anymore !</color>" , "CMod");
                    KickBanManager.ignore(p.ID);
                }
            }
            if (GUI.Button(new Rect(130,20,60,20),"Dumpnfo"))
            {
                foreach (var item in p.customProperties)
                {
                    FengGameManagerMKII.instance.chat.addLINE(item.Key + " : " + item.Value);
                }
            }



        }
        public override void show()
        {
            showtab = true;
        }
        public override bool doshow()
        {
            return PhotonNetwork.connected;
        }
    }
}
