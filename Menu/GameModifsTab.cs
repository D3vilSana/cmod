﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Menu
{
    public class GameModifsTab : LayoutTab
    {
        public static bool free_rotation = false;
        public static bool real_cables = false;
        public static bool crash_walls = false;
        public static bool bouncyness = false;
        public override string name
        {
            get
            {
                return "GameModifs";
            }
        }
        public override void Init()
        {
            base.Init();
            AddElem(new Menu.LayoutElems.Toggleable("FreeRot", delegate (bool val, Menu.LayoutElems.Toggleable to)
             {
                 if (FengGameManagerMKII.instance.myhero != null)
                 {
                     FengGameManagerMKII.instance.myhero.freerot = val;
                     FengGameManagerMKII.instance.myhero.rigidbody.freezeRotation = !val;
                 }
                 free_rotation = val;
             }, false));

            AddElem(new Menu.LayoutElems.Toggleable("RealCables", delegate (bool val, Menu.LayoutElems.Toggleable to)
             {
                 if (FengGameManagerMKII.instance.myhero !=null)
                 {
                     FengGameManagerMKII.instance.myhero.realcables = val;
                     
                 }
                 real_cables = val;
             }, false));
            AddElem(new Menu.LayoutElems.Toggleable("Crash Walls", delegate (bool val, Menu.LayoutElems.Toggleable to)
            {
                crash_walls = val;
            }, false));
            AddElem(new Menu.LayoutElems.Toggleable("Bounce !!", delegate (bool val, Menu.LayoutElems.Toggleable to)
            {
                if (FengGameManagerMKII.instance.myhero != null)
                {
                    FengGameManagerMKII.instance.myhero.bouncing = val;
                }
                bouncyness = val;
            }, false));

        }
        public override bool doshow()
        {
            return true;
        }
    }
}
