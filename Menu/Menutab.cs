﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;


public abstract class Menutab
{
    public abstract void hide();
    public abstract string name { get; }
    public abstract void show();
    public abstract void OnGui(Vector2 size);
    public abstract void Init();
    public static IEnumerable<Menutab> getTabs()
    {
        return CModUtilities.GetTypesof<Menutab>().Select(b=>(Menutab)Activator.CreateInstance(b));
    }
    public abstract bool doshow();





}