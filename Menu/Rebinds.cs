﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Menu
{
    public class Rebinds : Menutab
    {
        private bool isopen;
        private InputCode.Keys? waiting = null;
        private const int keypercolumn = 19;
        private const int xsize = 200;

        public override string name
        {
            get
            {
                return "Rebinds";
            }
        }

        public override void hide()
        {
            isopen = false;
        }

        public override void Init()
        {
        }

        public override void OnGui(Vector2 size)
        {
            if (!isopen)
            {
                return;
            }
            IEnumerable<InputCode.Keys> keys = Enum.GetValues(typeof(InputCode.Keys)).Cast<InputCode.Keys>();
            foreach (InputCode.Keys k in keys)
            {

                GUI.Label(new Rect(((int)k / keypercolumn) * xsize + 5, keys.Count() * ((int)k % keypercolumn), 70, 20), k.ToString() + " :");
                if (waiting == k)
                {

                    GUI.Button(new Rect(75 + ((int)k / keypercolumn) * (xsize+5), keys.Count() * ((int)k % keypercolumn), xsize-70, 20), "Waiting...");
                    Inputs.InputTypes.InputType it = Inputs.InputTypes.InputType.FromKeypressed(Event.current);
                    if (it!=null)
                    {
                        Managers.InputManager.setKey(k, it);
                        waiting = null;
                    }
                }
                else
                {

                    if (GUI.Button(new Rect(75 + ((int)k / keypercolumn) * (xsize+5), keys.Count() * ((int)k % keypercolumn), xsize-70, 20), Managers.InputManager.key(k).displayinfo))
                    {
                        //now allows duplicates
                        waiting = k;
                    }
                }
            }
        }

        public override void show()
        {
            isopen = true;
        }
        public override bool doshow()
        {
            return true;
        }
    }
}
