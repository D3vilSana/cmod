﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public class RespawnTimer : Managers.TimerManager.Timer
{
    public RespawnTimer()
    {
        On_End += RespawnTimer_On_End;
        On_Start += On_Timer_Start;
    }

    private void On_Timer_Start(Managers.TimerManager.Timer t)
    {
        t.timeout = FengGameManagerMKII.level.respawnTime;
    }

    private void RespawnTimer_On_End(Managers.TimerManager.Timer t)
    {
        if (BTN.BTN_choose_side.needChooseSide)
        {
            waitForChooseSide();
        }
        else
        {
            FengGameManagerMKII.level.Spawn(FengGameManagerMKII.instance.myLastHero);
        }
    }

    private void waitForChooseSide()
    {
        BTN.BTN_choose_side.OnSideChoosen += spawnAndRemoveEvent;
    }

    private void spawnAndRemoveEvent()
    {
        FengGameManagerMKII.level.Spawn(FengGameManagerMKII.instance.myLastHero);
        BTN.BTN_choose_side.OnSideChoosen -= spawnAndRemoveEvent;
    }
}
