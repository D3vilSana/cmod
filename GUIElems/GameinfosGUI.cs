﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

class GameinfosGUI : GUIElem
{
    public override Vector2 getsize
    {
        get
        {
            return new Vector2(120, 60);
        }

        internal set
        {
            
        }
    }

    public override Position pos
    {
        get
        {
            return Position.TopRight;
        }

        internal set
        {
            
        }
    }

    public override bool show
    {
        get
        {
            return PhotonNetwork.inRoom;
        }

        internal set
        {
            
        }
    }

    public override void Init()
    {
        
    }

    public override void OnGui()
    {
        GUI.Box(new Rect(0, 0, 120, 60), "");
        GUI.Label(new Rect(0, 0, 120, 20), PhotonNetwork.room.name.Split('`')[0].hexColor());
        GUI.Label(new Rect(0, 20, 60, 20),PhotonNetwork.room.playerCount+"/"+PhotonNetwork.room.maxPlayers );
        GUI.Label(new Rect(80, 20, 60, 20), "<color=#8f6cf1>" + ((FengGameManagerMKII.instance.time - ((int)FengGameManagerMKII.instance.timeTotalServer))).ToString()+"</color>");
        if (PhotonNetwork.room.open)
        {
            GUI.Label(new Rect(0, 40, 60, 20), "<color=#6cf18f>Open</color>");
        }
        else
        {
            GUI.Label(new Rect(0, 40, 60, 20), "<color=#ff4004>Closed</color>");
        }
        if (PhotonNetwork.room.visible)
        {
            GUI.Label(new Rect(60, 40, 60, 20), "<color=#6cf18f>Visible</color>");
        }
        else
        {
            GUI.Label(new Rect(60, 40, 60, 20), "<color=#ff4004>Hidden</color>");
        }
    }

    public override void Update()
    {
       
    }
}

