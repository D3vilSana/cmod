﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

class SoloGameGUI : GUIElem
{
    private IEnumerable<Levels.Level> lvls;

    private Vector2 levelsize = new Vector2(200, 50);

    private DayLight time;

    private Levels.Level selected;

    private Vector2 scrollpos = Vector2.zero;

    public override Vector2 getsize
    {
        get
        {
            return new Vector2(500, 200);
        }

        internal set
        {
           
        }
    }

    public override Position pos
    {
        get
        {
            return Position.MidMid;
        }

        internal set
        {
            
        }
    }

    public override bool show
    {
        get { return doshow; }

        internal set { doshow = value; }
    }

    public static bool doshow
    {
        get;
        set;
    }

    public override void Init()
    {
        lvls = Levels.LevelManager.getLevels().Where(l=>l.type!=GAMEMODE.PVP_AHSS);
        time = DayLight.Day;
        selected = lvls.First();
    }

    public override void OnGui()
    {

        GUI.Box(getsize.fromCoord(Vector2.zero), "Solo Game");

        if (GUI.Button(new Rect(getsize.x - 100, getsize.y - 20, 50, 20), "GO !"))
        {
            PhotonNetwork.offlineMode = true;
            FengGameManagerMKII.level = selected;
            RoomOptions ro = new RoomOptions()
            {
                isOpen = false,
                isVisible = false,
                maxPlayers = 1,
            };
            PhotonNetwork.CreateRoom("CMod-solo`"+selected.name+"`"+IN_GAME_MAIN_CAMERA.difficulty+"`99999999`"+time.name()+"``", ro, new TypedLobby());
            show = false;
        }
        if (GUI.Button(new Rect(getsize.x - 50, getsize.y - 20, 50, 20), "Quit"))
        {
            NGUITools.SetActive(GameObject.Find("UIRefer").GetComponent<UIMainReferences>().panelMain, true);
            Managers.InputManager.ignoreKeys = false;
            show = false;
        }
        int i = 0;
        scrollpos = GUI.BeginScrollView(new Vector2(levelsize.x + 20, 200).fromCoord(Vector2.zero), scrollpos, new Rect(0, 0, levelsize.x, levelsize.y * lvls.Count()));
        foreach (Levels.Level l in lvls)
        {
            GUI.BeginGroup(levelsize.fromCoord(Vector2.up * levelsize.y * i));
            bool sel = selected == l;
            l.ShowLevel(time, ref sel);
            if (sel&&selected!=l)
            {
                selected = l;
            }
            GUI.EndGroup();
            i++;
        }
        GUI.EndScrollView(true);

        time = (DayLight)GUI.SelectionGrid(new Rect(getsize.x-200,0,200,25), (int)time, new string[] { "Day", "Dawn", "Night" }, 3);
    }

    public override void Update()
    {
       
    }
}