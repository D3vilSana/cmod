﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

class TitGUI : GUIElem
{
    private TITAN[] titans;
    private const string titimg = "https://s32.postimg.org/nrfevjwjp/u_Pc_Ik2z.png";
    private const int imgsize = 30;

    public override Vector2 getsize
    {
        get
        {
            
            return new Vector2(Math.Min(titans.Length, 10)*imgsize, imgsize*(1+((titans.Length-1) / 10)));
        }

        internal set
        {
        }
    }

    public override Position pos
    {
        get
        {
            return Position.TopMid;
        }

        internal set
        {
        }
    }

    public override bool show
    {
        get;

        internal set;
    }

    public override void Init()
    {
        titans = new TITAN[0];
        TextureManager.loadtexture("Titan", titimg);
    }

    public override void OnGui()
    {
        GUI.Box(new Rect(0, 0, getsize.x, getsize.y), "");
        int j = titans.Length;
        for (int i = 0; i < j; i++)
        {
            GUI.DrawTexture(new Rect(imgsize * (i%10), (i/10)*imgsize, imgsize, imgsize), TextureManager.gettex("Titan"), ScaleMode.ScaleToFit);
        }
    }

    public override void Update()
    {
        titans = GameObject.FindGameObjectsWithTag("titan").Select(g => g.GetComponent<TITAN>()).Where(u => u != null).Where(t => !t.hasDie).ToArray();
        show = titans.Length > 0;
    }
}
