﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

abstract class ConfigInGUI : GUIElem
{
    protected abstract string url { get; }
    private string texname;
    public override Vector2 getsize
    {
        get
        {
            return new Vector2(75, 75);
        }

        internal set{}
    }
    public override bool show
    {
        get;
        internal set;
    }

    public override void Init()
    {
        texname = this.GetType().Name + "_Tex";
        TextureManager.loadtexture(texname, url);
    }
    public override void OnGui()
    {
        GUI.Box(new Rect(0, 0, getsize.x, getsize.y), "");
        GUI.DrawTexture(new Rect(0, 0, getsize.x, getsize.y), TextureManager.gettex(texname), ScaleMode.ScaleToFit);
    }


    public class FreeRot : ConfigInGUI
    {
        public override Position pos
        {
            get
            {
                return Position.MidRight;
            }

            internal set
            {
            }
        }

        protected override string url
        {
            get
            {
                return "https://s31.postimg.org/issvdhcnf/l_TJCPt_H.png";
            }
        }

        public override void Update()
        {
            show = Menu.GameModifsTab.free_rotation;
        }
    }
}

