﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

class PlayerListGUI : GUIElem
{
    protected const int xsize = 300;
    protected const int ysize = 15;
    protected PhotonPlayer[] players;
    protected GUIStyle style;
    public override Vector2 getsize
    {
        get;
        internal set;
    }

    public override Position pos
    {
        get
        {
            return Position.TopLeft;
        }
        internal set
        {
            return;
        }
    }

    public override bool show
    {
        get;
        internal set;
    }

    public override void OnGui()
    {
        if (CustomGameMode.getint("TeamMode") <= 0)
        {
            for (int i = 0; i < players.Length; i++)
            {
                PhotonPlayer p = players[i];
                GUI.Box(new Rect(0, i * ysize, xsize, ysize), "");

                GUI.Label(new Rect(0, i * ysize, xsize, ysize), getplayertag(p), style);
            }
        }
        else
        {
            int i = 0;
            foreach (PhotonPlayer p in players.Where(u=>RCextensions.returnIntFromObject(u.customProperties[PhotonPlayerProperty.RCteam])==0))
            {
                GUI.Box(new Rect(0, i * ysize, xsize, ysize), "");
                GUI.Label(new Rect(0, i++ * ysize, xsize, ysize), getplayertag(p), style);
            }
            IEnumerable<PhotonPlayer> pl = players.Where(u => RCextensions.returnIntFromObject(u.customProperties[PhotonPlayerProperty.RCteam]) == 1);
            GUI.Box(new Rect(0, i * ysize, xsize, ysize*pl.Count()),"");
            GUI.Label(new Rect(0, i++ * ysize, xsize, ysize), "Team 1", style);
            GUI.Label(new Rect(0, i++ * ysize, xsize, ysize),
                pl.Select(p => RCextensions.returnIntFromObject(p.customProperties[PhotonPlayerProperty.kills])).Sum()+"/"+
                pl.Select(p => RCextensions.returnIntFromObject(p.customProperties[PhotonPlayerProperty.deaths])).Sum() + "/"+
                pl.Select(p => RCextensions.returnIntFromObject(p.customProperties[PhotonPlayerProperty.max_dmg])).Sum() + "/" +
                pl.Select(p => RCextensions.returnIntFromObject(p.customProperties[PhotonPlayerProperty.total_dmg])).Sum()
                , style);
            foreach (PhotonPlayer p in pl)
            {
                GUI.Box(new Rect(0, i * ysize, xsize, ysize), "");
                GUI.Label(new Rect(0, i++ * ysize, xsize, ysize),getplayertag(p), style);
            }


            pl = players.Where(u => RCextensions.returnIntFromObject(u.customProperties[PhotonPlayerProperty.RCteam]) == 2);
            GUI.Box(new Rect(0, i * ysize, xsize, ysize * pl.Count()), "");
            GUI.Label(new Rect(0, i++ * ysize, xsize, ysize), "Team 2", style);
            GUI.Label(new Rect(0, i++ * ysize, xsize, ysize),
                pl.Select(p => RCextensions.returnIntFromObject(p.customProperties[PhotonPlayerProperty.kills])).Sum() + "/" +
                pl.Select(p => RCextensions.returnIntFromObject(p.customProperties[PhotonPlayerProperty.deaths])).Sum() + "/" +
                pl.Select(p => RCextensions.returnIntFromObject(p.customProperties[PhotonPlayerProperty.max_dmg])).Sum() + "/" +
                pl.Select(p => RCextensions.returnIntFromObject(p.customProperties[PhotonPlayerProperty.total_dmg])).Sum()
                , style);
            foreach (PhotonPlayer p in pl)
            {
                GUI.Box(new Rect(0, i * ysize, xsize, ysize), "");
                GUI.Label(new Rect(0, i++ * ysize, xsize, ysize), getplayertag(p), style);
            }
        }
    }
    private string getplayertag(PhotonPlayer p)
    {
        string txt = "";
        if (p.isMasterClient)
        {
            txt += "<color=#ff5722>";
        }
        else if (p.isLocal)
        {
            txt += "<color=#062e60>";
        }
        else
        {
            txt += "<color=#c6b7b7>";
        }
        txt += "[" + p.ID + "]</color> ";

        if (RCextensions.returnIntFromObject(p.customProperties[PhotonPlayerProperty.isTitan]) == 2)
        {
            txt += "<color=#F5DA81>[T]</color>";
        }
        else if (RCextensions.returnIntFromObject(p.customProperties[PhotonPlayerProperty.team]) < 2)
        {
            txt += "<color=#A9D0F5>[H]</color>";
        }
        else
        {
            txt += "<color=#82FA58>[A]</color>";
        }
        if (RCextensions.returnBoolFromObject(p.customProperties[PhotonPlayerProperty.dead]))
        {
            txt += "<color=#b92f1c>☠</color>";
        }
        txt += RCextensions.returnStringFromObject(p.customProperties[PhotonPlayerProperty.name]).Replace("[-]", "").hexColor() + " ";

        txt +=
            RCextensions.returnIntFromObject(p.customProperties[PhotonPlayerProperty.kills]) + "/" +
            RCextensions.returnIntFromObject(p.customProperties[PhotonPlayerProperty.deaths]) + "/" +
            RCextensions.returnIntFromObject(p.customProperties[PhotonPlayerProperty.max_dmg]) + "/" +
            RCextensions.returnIntFromObject(p.customProperties[PhotonPlayerProperty.total_dmg]);
        return txt;
    }

    public override void Update()
    {
        players = PhotonNetwork.playerList.Where(b=>!KickBanManager.isignored(b.ID)).ToArray();
        if (CustomGameMode.getint("TeamMode")>0)
        {
            getsize = new Vector2(xsize, (players.Length + 6) * ysize);
        }
        else
        {
            getsize = new Vector2(xsize, players.Length * ysize);
        }
        

        show  = PhotonNetwork.connected;
    }
    public override void Init()
    {
        style = new GUIStyle();
        style.richText = true;
        style.alignment = TextAnchor.MiddleCenter;
        style.normal.textColor = Color.white;
    }
}
