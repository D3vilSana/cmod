﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

class RoomConfig : GUIElem
{
    private static Texture2D[] textures;
    
    private const string swordimg = "https://s32.postimg.org/41iw052np/ll1r0_HS.png";
    private const string sizeimg = "https://s32.postimg.org/nknh9i1f9/aa_Z4_G7_N.png";
    private const string horseimg = "https://s32.postimg.org/ft6raxx9x/V2rfcf_S.png";
    private static string[] exeptstr = { "damage","titanc", "sizeMode", "sizeLower", "sizeUpper", "spawnMode","nRate","aRate","jRate","cRate","pRate","horse" };
    private static string[] nocountlist = { "sizeLower", "sizeUpper","nRate", "aRate", "jRate", "cRate", "pRate" };
    public override Vector2 getsize
    {
        get;

        internal set;
    }

    public override Position pos
    {
        get
        {
            return Position.BotRight;
        }

        internal set
        {
            
        }
    }

    public override bool show
    {
        get
        {
            return CustomGameMode.mode.Count > 0;
        }

        internal set
        {
         
        }
    }

    public override void Init()
    {
        TextureManager.loadtexture("Damage", swordimg);
        TextureManager.loadtexture("Size", sizeimg);
        TextureManager.loadtexture("Horse", horseimg);
    }

    public override void OnGui()
    {
        GUI.Box(new Rect(0,0,getsize.x,getsize.y), "");
        int i = 0;

        if (CustomGameMode.mode.ContainsKey("titanc"))//titan number
        {
            addimglabel(ref i,"Titan", "<color=#cdaa7d>" + CustomGameMode.mode["titanc"] + "</color>");
        }

        if (CustomGameMode.mode.ContainsKey("damage"))//titan number
        {
            addimglabel(ref i, "Damage", "<color=#f44343>" + CustomGameMode.mode["damage"] + "</color>");
        }
        if (CustomGameMode.mode.ContainsKey("sizeMode"))//titan number
        {
            addimglabel(ref i, "Size", "<color=#cdaa7d>" + CustomGameMode.mode["sizeLower"]+ " → "+CustomGameMode.mode["sizeUpper"] + "</color>");
        }
        if (CustomGameMode.mode.ContainsKey("spawnMode"))
        {
            
            addlabel(ref i, "Spawn : N:<color=#09cff6>" + Mathf.Round((float)CustomGameMode.mode["nRate"]) + "</color> A:<color=#53af8b>" + Mathf.Round((float)CustomGameMode.mode["aRate"]) + "</color> J:<color=#ffd700>" + Mathf.Round((float)CustomGameMode.mode["jRate"]) + "</color> C:<color=#49b687>" + Mathf.Round((float)CustomGameMode.mode["cRate"]) + "</color> P:<color=#f44343>" + Mathf.Round((float)CustomGameMode.mode["pRate"]) + "</color>");
        }
        if (CustomGameMode.mode.ContainsKey("horse"))//titan number
        {
            addimglabel(ref i,"Horse", "");
        }



        foreach (KeyValuePair<object,object> item in CustomGameMode.mode.Where(r=>!exeptstr.Contains((string)r.Key)))
        {
            addlabel(ref i, "<color=#a2cd5a>" + item.Key.ToString() + "</color> : <color=#c4c4c4>" + item.Value.ToString() + "</color>");
        }

    }
    private void addlabel(ref int i,string txt)
    {
        GUI.Label(new Rect(0, i++ * 19, 200, 19),txt);
    }

    private void addimglabel(ref int i,string texturename,string text)
    {
        GUI.DrawTexture(new Rect(0, i*19, 20, 19), TextureManager.gettex(texturename));
        GUI.Label(new Rect(21, i++ * 19, 179, 19), text);
    }

    public override void Update()
    {
        getsize = new Vector2(200, CustomGameMode.mode.Count(t=>!nocountlist.Contains((string)t.Key)) * 19);
    }


}   

