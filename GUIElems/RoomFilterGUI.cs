﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

class RoomFilterGUI : GUIElem
{
    private static string namefilter = "";
    private static bool available;


    public override Vector2 getsize
    {
        get;

        internal set;
    }

    public override Position pos
    {
        get
        {
            return Position.BotLeft;
        }

        internal set
        {
        }
    }

    public override bool show
    {
        get
        {
            return ServerList.visible;
        }

        internal set
        {

        }
    }

    public override void Init()
    {
        getsize = new Vector2(200, 100);
    }

    public override void OnGui()
    {
        GUI.Box(getsize.fromCoord(Vector2.zero), "");
        GUI.Label(new Rect(0, 0, 200, 25), "Name : ");
        namefilter = GUI.TextField(new Rect(0, 25, 200, 25), namefilter).ToLower();
        available = GUI.Toggle(new Rect(0, 75, 200, 25), available, "Show only available");
    }

    public override void Update()
    {

    }
    public static IEnumerable<RoomInfo> filter(IEnumerable<RoomInfo> t)
    {
        t = t.Where(r => r.name.Split('`')[0].ToLower().Contains(namefilter));
        if (available)
        {
            t = t.Where(r => r.canjoin);
        }
        return t;
    } 
}

