﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

class RespawnGUI : GUIElem
{
    GUIStyle style;

    public override Vector2 getsize
    {
        get
        {
            return new Vector2(300, 100);
        }

        internal set
        {
        }
    }

    public override Position pos
    {
        get
        {
            return Position.MidMid;
        }

        internal set
        {
        }
    }
    public override bool show
    {
        get;

        internal set;
    }

    public override void Init()
    {
        style = new GUIStyle();
        style.alignment = TextAnchor.MiddleCenter;
    }

    public override void OnGui()
    {
        string s = "";
        s += FengGameManagerMKII.instance.respawn.is_running? "<color=#fa6607>"+FengGameManagerMKII.instance.respawn.remainingtime.ToString("0.0")+"</color> ":"";
        s = FengGameManagerMKII.instance.gameEndCD.is_running? "<color=#c3b0c3>" + FengGameManagerMKII.instance.gameEndCD.remainingtime.ToString("0.0") + "</color>" : s;
        GUI.Label(new Rect(0, 0, getsize.x, getsize.y), "<size=70>"+s+"</size>",style);
    }

    public override void Update()
    {
       show = FengGameManagerMKII.instance.respawn.is_running;
        show |= FengGameManagerMKII.instance.gameEndCD.is_running;

    }
}
