﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace GUIElems
{
    class CreditGUI : GUIElem
    {

        public static bool visible = false;
        private static List<Credit> creds = new List<Credit>();
        private Vector2 scroll = Vector2.zero;

        private Vector2 Cbox = new Vector2(300, 70);
        

        public override Vector2 getsize
        {
            get;

            internal set;
        }

        public override Position pos
        {
            get
            {
                return Position.MidLeft;
            }

            internal set
            {
            }
        }

        public override bool show
        {
            get
            {
                return visible;
            }

            internal set
            {
            }
        }

        public override void Init()
        {
            add("Feng Li", "Attack On Titan Tribute Game Creator");
            add("Jiang Li", "Additionial Animator");
            add("Aya", "Special Thanks");
            add("RC Mod Creators", "some of their Assets/scripts Are used","","","","", "http://aotrc.weebly.com/");
            add("PolyB", "CMod Main Developper", "https://www.reddit.com/user/Poly-B", "", "PolyB@protonmail.com", "https://bitbucket.org/PolyB/");
            //Add yourself Here !!



            //----------

            add("Pixel", "Special Thanks", "https://www.reddit.com/user/Pix-I");
            add("Raoh's Forum members", "Special Thanks", "", "", "", "", "http://aotraohmods.forumfree.it/");
            add("Takanat", "Special Thanks");
            //---------------------------------------------------------------------

            TextureManager.loadtexture("reddit_ico", "https://s31.postimg.org/toe59t9qz/Reddit_icon.png");
            TextureManager.loadtexture("mail_ico", "https://s32.postimg.org/wop7ilu1h/mail.png");
            TextureManager.loadtexture("skype_ico", "https://s32.postimg.org/tzqou1cud/skype_add.png");
            TextureManager.loadtexture("bitbucket_ico", "https://s31.postimg.org/6vbu8p3bf/Bitbucket.png");
            TextureManager.loadtexture("www_ico", "https://s32.postimg.org/8d0twrnud/at_2.png");
            getsize = new Vector2(Cbox.x+20, 6 * Cbox.y);
        }

        public override void OnGui()
        {
            GUI.Box(getsize.fromCoord(Vector2.zero), "");
            scroll = GUI.BeginScrollView(new Rect(0,0,Cbox.x+15,getsize.y), scroll, new Rect(0, 0, Cbox.x, Cbox.y * creds.Count));
            for (int i = 0; i < creds.Count; i++)
            {
                GUI.BeginGroup(Cbox.fromCoord(Vector2.up * Cbox.y*i));
                Show(creds[i]) ;
                GUI.EndGroup();
            }
            GUI.EndScrollView(true);
        }

        

        public override void Update()
        {
        }

        private void add(string name,string role,string reddit="",string skype="",string mail="",string bitbucket="",string www="")
        {
            Credit c = new Credit();
            c.name = name;
            c.role = role;
            c.reddit = reddit;
            c.skype = skype;
            c.Mail = mail;
            c.bitbucket = bitbucket;
            c.www = www;
            creds.Add(c);
        }
        private struct Credit
        {
            public string name;
            public string role;
            public string reddit;
            public string skype;
            public string bitbucket;
            public string Mail;
            public string www;

        }
        private void Show(Credit c)
        {
            GUI.Box(Cbox.fromCoord(Vector2.zero), "<b>"+c.name+"</b>\n<i>"+c.role+"</i>");
            int p = 0;
            if (c.reddit!="")
            {
                if(GUI.Button(new Rect(p, Cbox.y - 25, 25,25), TextureManager.gettex("reddit_ico")))
                {
                    Application.OpenURL(c.reddit);
                }
                p += 25;
            }
            if (c.Mail!="")
            {
                if (GUI.Button(new Rect(p,Cbox.y-25,25,25),TextureManager.gettex("mail_ico")))
                {
                    Application.OpenURL("mailto:" + c.Mail);
                }
                p += 25;
            }
            if (c.skype!="")
            {
                if (GUI.Button(new Rect(p, Cbox.y - 25, 25, 25), TextureManager.gettex("skype_ico")))
                {
                    Application.OpenURL("skype:" + c.skype+"?chat");
                }
                p += 25;
            }
            if (c.bitbucket!="")
            {
                if (GUI.Button(new Rect(p,Cbox.y-25,25,25),TextureManager.gettex("bitbucket_ico")))
                {
                    Application.OpenURL(c.bitbucket);
                }
                p += 25;
            }
            if (c.www!="")
            {
                if (GUI.Button(new Rect(p,Cbox.y-25,25,25),TextureManager.gettex("www_ico")))
                {
                    Application.OpenURL(c.www);
                }
                p += 25;
            }

        }
    }
}
