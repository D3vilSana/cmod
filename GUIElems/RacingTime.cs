﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace GUIElems
{
    class RacingTime : GUIElem
    {
        public override Vector2 getsize
        {
            get
            {
                return new Vector2(100, 50);
            }

            internal set
            {
            }
        }

        public override Position pos
        {
            get
            {
                return Position.TopMid;
            }

            internal set
            {

            }
        }

        public override bool show
        {
            get
            {   
                return FengGameManagerMKII.level!=null && FengGameManagerMKII.level.HasAttribute<Levels.Attributes.Racing>();
            }

            internal set
            {

            }
        }

        public override void Init()
        {

        }

        public override void OnGui()
        {
            GUI.Box(getsize.fromCoord(Vector2.zero), ((FengGameManagerMKII.instance.roundTime >= 20f) ? ((float)((int)(FengGameManagerMKII.instance.roundTime * 10f)) * 0.1f - 20f).ToString() : "<color=#00FF00><size=20>"+(20f - FengGameManagerMKII.instance.roundTime)+"</size></color>"));
        }

        public override void Update()
        {

        }
    }
}
