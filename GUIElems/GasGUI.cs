﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

class GasGUI : GUIElem
{
    private const string img = "https://s32.postimg.org/wsn8yvnmt/p_YBt_F1_U.png";
    public override Vector2 getsize
    {
        get { return new Vector2(75, 75); }

        internal set{}
    }

    public override Position pos
    {
        get { return Position.MidRight; }

        internal set { }
    }

    public override bool show
    {
        get;
        internal set;
    }

    public override void Init()
    {
        TextureManager.loadtexture("Gas", img);
    }
    public override void OnGui()
    {
       GUI.Box(new Rect(0, 0, getsize.x, getsize.y),"");
       GUI.DrawTexture(new Rect(0, 0, getsize.x, getsize.y), TextureManager.gettex("Gas"),ScaleMode.ScaleToFit);
    }

    public override void Update()
    {
        
        show  =  (FengGameManagerMKII.instance.myhero != null) && (FengGameManagerMKII.instance.myhero.currentGas <= 20);
    }
}

