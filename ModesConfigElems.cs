﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

static class ModesConfigElems
{
    public class Titansize : ConfigElem
    {
        public Titansize(CustomGameMode.Mode modemin,CustomGameMode.Mode modemax,CustomGameMode.Mode isenabled)
        {
            minm = modemin;
            maxm = modemax;
            enam = isenabled;
        }
        private CustomGameMode.Mode minm;
        private CustomGameMode.Mode maxm;
        private CustomGameMode.Mode enam;
        private string min = "";
        private string max = "";

        public override Vector2 size
        {
            get
            {
                return new Vector2(60, 60);
            }

            internal set
            {
            }
        }

        public override void draw(Vector2 pos)
        {
            GUI.Box(new Rect(pos.x, pos.y, size.x, size.y), "Size :");
            min =GUI.TextArea(new Rect(pos.x+0, pos.y+20, 20, 20), min);
            GUI.Label(new Rect(pos.x+25, pos.y+20, 15, 20), "→");
            max = GUI.TextArea(new Rect(pos.x+40, pos.y+20, 20, 20), max);
            if (GUI.Button(new Rect(pos.x+0, pos.y+40, 60,20),"OK"))
            {
                float mi;
                float ma;
                if (min==""&&max=="")
                {
                    enam.set("0");
                    return;
                }
                if (float.TryParse(min,out mi))
                {
                    if (float.TryParse(max,out ma))
                    {
                        minm.set(min);
                        maxm.set(max);
                        enam.set("1");
                        return;
                    }
                }
                min = minm.tostring();
                max = minm.tostring();
                
                //FAIL
            }
        }
    }

    public class TitanSpawnRate : ConfigElem
    {
        public TitanSpawnRate(CustomGameMode.Mode enabled ,CustomGameMode.Mode[] modes)
        {
            this.modes = modes;
            this.enabled = enabled;
        }

        float[] vals = new float[5];
        float[] newvals = new float[5];
        CustomGameMode.Mode[] modes;
        CustomGameMode.Mode enabled;

        public override Vector2 size
        {
            get
            {
                return new Vector2(130, 140);
            }

            internal set
            {
            }
        }

        public override void draw(Vector2 pos)
        {
            GUI.Box(new Rect(pos.x, pos.y, size.x, size.y),"Spawn Rate :");
            GUI.Label(new Rect(pos.x, pos.y + 15, 60, 20), "Normal");
            newvals[0] = GUI.HorizontalSlider(new Rect(pos.x + 60, pos.y + 20, 70, 15), vals[0], 0, 100);

            GUI.Label(new Rect(pos.x, pos.y + 35, 60, 20), "Aberant");
            newvals[1] = GUI.HorizontalSlider(new Rect(pos.x + 60, pos.y + 40, 70, 15), vals[1], 0, 100);

            GUI.Label(new Rect(pos.x, pos.y + 55, 60, 20), "Jumper");
            newvals[2] = GUI.HorizontalSlider(new Rect(pos.x + 60, pos.y + 60, 70, 15), vals[2], 0, 100);

            GUI.Label(new Rect(pos.x, pos.y + 75, 60, 20), "Crawler");
            newvals[3] = GUI.HorizontalSlider(new Rect(pos.x + 60, pos.y + 80, 70, 15), vals[3], 0, 100);

            GUI.Label(new Rect(pos.x, pos.y + 95, 60, 20), "Punk");
            newvals[4] = GUI.HorizontalSlider(new Rect(pos.x + 60, pos.y + 100, 70, 15), vals[4], 0, 100);

            for (int i = 0; i < 5; i++)
            {
                newvals[i] = newvals[i] < 0 ? 0 : newvals[i];
                if (newvals[i] != vals[i])
                {
                    float tot = newvals.Sum();
                    if (tot>100)
                    {
                        newvals[i] = 100 - (tot - newvals[i]);
                        newvals[i] = newvals[i] < 0 ? 0 : newvals[i];
                    }
                    vals[i] = newvals[i];
                }
            }
            if (GUI.Button(new Rect(pos.x, pos.y + 120, 130, 20), "OK !"))
            {
                if (vals.Sum()==0)
                {
                    enabled.set("0");
                }
                else
                {
                    for (int i = 0; i < 5; i++)
                    {
                        modes[i].set(vals[i].ToString());
                    }
                    enabled.set("1");
                }

                CustomGameMode.SendGameInfos();
            }

        }
    }
}

