﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Levels
{
    [Attributes.Respawn]
    class CityII : LevelType.Classic.Kill_titan
    {
        public CityII()
        {
            map = new Maps.City();
            map.checkpoint = false;
        }
        public new Levels.Maps.City map { get { return (Maps.City)base.map; } protected set { base.map = value; } }
        public override string desc
        {
            get
            {
                return "Fight the titans with your friends.(RESPAWN AFTER 10 SECONDS/SUPPLY/TEAM TITAN)";
            }
        }

        public override int enemyNumber
        {
            get
            {
                return 10;
            }
        }

        public override string name
        {
            get
            {
                return "The City II";
            }
        }

        public override bool pvp
        {
            get
            {
                return true;
            }
        }

        public override RespawnMode respawnMode
        {
            get
            {
                return RespawnMode.DEATHMATCH;
            }
        }

        public override bool teamTitan
        {
            get
            {
                return true;
            }
        }
    }
}
