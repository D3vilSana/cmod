﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Levels
{
    [Attributes.Respawn]
    class ColossalII : LevelType.Classic.BossFight
    {
        public ColossalII()
        {
            map = new Maps.Colossal();
        }
        public new Maps.Colossal map { get { return (Maps.Colossal)base.map; } private set { base.map = value; } }
        public override string desc
        {
            get
            {
                return "Defeat the Colossal Titan.\nPrevent the abnormal titan from running to the north gate.\n Nape Armor:\n Normal:5000\nHard:8000\nAbnormal:12000\n(RESPAWN AFTER 10 SECONDS)";
            }
        }

        public override int enemyNumber
        {
            get
            {
                return 2;
            }
        }

        public override string name
        {
            get
            {
                return "Colossal Titan II";
            }
        }

        public override bool pvp
        {
            get
            {
                return false;
            }
        }

        public override RespawnMode respawnMode
        {
            get
            {
                return RespawnMode.DEATHMATCH;
            }
        }

        public override bool teamTitan
        {
            get
            {
                return false;
            }
        }

        public override void LevelWasLoaded()
        {
            GameObject.Find("playerRespawnTrost").SetActive(false);
            UnityEngine.Object.Destroy(GameObject.Find("playerRespawnTrost"));
            UnityEngine.Object.Destroy(GameObject.Find("rock"));
            if (PhotonNetwork.isMasterClient)
            {
                if (!FengGameManagerMKII.instance.isPlayerAllDead())
                {
                    PhotonNetwork.Instantiate("COLOSSAL_TITAN", (Vector3)(-Vector3.up * 10000f), Quaternion.Euler(0f, 180f, 0f), 0);
                }
            }
            base.LevelWasLoaded();
        }
    }
}
