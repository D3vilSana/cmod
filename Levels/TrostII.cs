﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Levels
{
    [Attributes.Respawn]
    class TrostII : LevelType.Classic.Trost
    {
        public TrostII()
        {
            map = new Maps.Trost();
        }
        public new Maps.Trost map { get { return (Maps.Trost)base.map; } private set { base.map = value; } }
        public override string desc
        {
            get
            {
                return "Escort Titan Eren(RESPAWN AFTER 10 SECONDS)";
            }
        }

        public override int enemyNumber
        {
            get
            {
                return 2;
            }
        }

        public override string name
        {
            get
            {
                return "Trost II";
            }
        }

        public override bool pvp
        {
            get
            {
                return false;
            }
        }

        public override RespawnMode respawnMode
        {
            get
            {
                return RespawnMode.DEATHMATCH;
            }
        }

        public override bool teamTitan
        {
            get
            {
                return false;
            }
        }
    }
}
