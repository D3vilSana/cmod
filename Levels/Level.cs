﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Levels
{
    public abstract class Level
    {
        //protected List<Attributes.LevelAttributes> Myattributes;
        protected Dictionary<Type, Attributes.LevelAttributes> Myattributes;

        public bool HasAttribute<T>() where T : Attributes.LevelAttributes
        {
            return Myattributes.ContainsKey(typeof(T));
        }
        public T GetAttribute<T>() where T : Attributes.LevelAttributes
        {
            Type t = typeof(T);
            
            if (Myattributes.ContainsKey(t))
            {
                return Myattributes[t] as T;
            }
            return null;
        }
        public Level()
        {
            Myattributes = new Dictionary<Type, Attributes.LevelAttributes>();
            
            foreach (Attributes.LevelAttributes att in Attribute.GetCustomAttributes(this.GetType(), typeof(Attributes.LevelAttributes)).Cast<Attributes.LevelAttributes>())
            {
                Myattributes.Add(att.GetType(), att);
                if (att.GetType().BaseType != null && att.GetType().BaseType!=typeof(Attributes.LevelAttributes))
                {
                    Myattributes.Add(att.GetType().BaseType, att);
                }
                att.setMyLevel(this);
            }
        }

        public abstract bool isMyLevel(string LevelName);

        public abstract GAMEMODE type { get; set; }

        //public abstract string mapName { get; }

        public Maps.Map map {
            get;
            set;
        }

        public virtual void LevelWasLoaded()
        {
            foreach (Attributes.LevelAttributes l in Myattributes.Values)
            {
                l.OnLevelLoaded();

            }
            map.LevelLoaded();
        }

        public virtual void core()
        {
            foreach (Attributes.LevelAttributes l in Myattributes.Values)
            {
                l.core();
            }
        }

        protected abstract Vector3 respawnHuman
        {
            get;
        }
        protected abstract Vector3 respawnTitan
        {
            get;
        }
        public Vector3 respawn
        {
            get
            {
                Attributes.Checkpoint r = GetAttribute<Attributes.Checkpoint>();
                if (r != null && r.respawn != null)
                {
                    return r.respawn ?? Vector3.zero;
                }
                return PhotonNetwork.player.isHuman() ? respawnHuman : respawnTitan;
            }
        }
        public abstract string name
        {
            get;
        }
        public abstract void ShowLevel(DayLight time, ref bool selected);

        public abstract bool pvp { get; }

        public abstract bool teamTitan { get; }

        public abstract RespawnMode respawnMode { get; }

        public abstract bool horse { get; }

        public abstract bool punk { get; }

        public float respawnTime
        {
            get
            {
                float resptime = -1;
                if (HasAttribute<Attributes.Respawn>())
                {
                    resptime = GetAttribute<Attributes.Respawn>().respawntime;
                }
                if (CustomGameMode.getint("RespawnTime") > 0)
                {
                    resptime = CustomGameMode.getint("RespawnTime");
                }
                return resptime;
            }
        }
        public void Spawn(string id,bool Human)
        {
            FengGameManagerMKII.instance.respawn.Abort();
            if (Human)
            {
                ExitGames.Client.Photon.Hashtable hashtable2 = new ExitGames.Client.Photon.Hashtable();
                hashtable2.Add("dead", false);
                hashtable2.Add(PhotonPlayerProperty.isTitan, 1);
                PhotonNetwork.player.SetCustomProperties(hashtable2);
                IN_GAME_MAIN_CAMERA component = FengGameManagerMKII.instance.mainCamera;
                FengGameManagerMKII.instance.myLastHero = id.ToUpper();

                component.setMainObject(PhotonNetwork.Instantiate("AOTTG_HERO 1", respawn, new Quaternion(), 0), true, false);
                id = id.ToUpper();
                if (((id == "SET 1") || (id == "SET 2")) || (id == "SET 3"))
                {
                    HeroCostume costume2 = CostumeConeveter.LocalDataToHeroCostume(id);
                    costume2.checkstat();
                    CostumeConeveter.HeroCostumeToLocalData(costume2, id);
                    component.main_object.GetComponent<HERO>().GetComponent<HERO_SETUP>().init();
                    if (costume2 != null)
                    {
                        component.main_object.GetComponent<HERO>().GetComponent<HERO_SETUP>().myCostume = costume2;
                        component.main_object.GetComponent<HERO>().GetComponent<HERO_SETUP>().myCostume.stat = costume2.stat;
                    }
                    else
                    {
                        costume2 = HeroCostume.costumeOption[3];
                        component.main_object.GetComponent<HERO>().GetComponent<HERO_SETUP>().myCostume = costume2;
                        component.main_object.GetComponent<HERO>().GetComponent<HERO_SETUP>().myCostume.stat = HeroStat.getInfo(costume2.name.ToUpper());
                    }
                    component.main_object.GetComponent<HERO>().GetComponent<HERO_SETUP>().setCharacterComponent();
                    component.main_object.GetComponent<HERO>().setStat();
                    component.main_object.GetComponent<HERO>().setSkillHUDPosition();
                }
                else
                {
                    for (int j = 0; j < HeroCostume.costume.Length; j++)
                    {
                        if (HeroCostume.costume[j].name.ToUpper() == id.ToUpper())
                        {
                            int num4 = HeroCostume.costume[j].id;
                            if (id.ToUpper() != "AHSS")
                            {
                                num4 += CheckBoxCostume.costumeSet - 1;
                            }
                            if (HeroCostume.costume[num4].name != HeroCostume.costume[j].name)
                            {
                                num4 = HeroCostume.costume[j].id + 1;
                            }
                            component.main_object.GetComponent<HERO>().GetComponent<HERO_SETUP>().init();
                            component.main_object.GetComponent<HERO>().GetComponent<HERO_SETUP>().myCostume = HeroCostume.costume[num4];
                            component.main_object.GetComponent<HERO>().GetComponent<HERO_SETUP>().myCostume.stat = HeroStat.getInfo(HeroCostume.costume[num4].name.ToUpper());
                            component.main_object.GetComponent<HERO>().GetComponent<HERO_SETUP>().setCharacterComponent();
                            component.main_object.GetComponent<HERO>().setStat();
                            component.main_object.GetComponent<HERO>().setSkillHUDPosition();
                            break;
                        }
                    }
                }
                CostumeConeveter.HeroCostumeToPhotonData(component.main_object.GetComponent<HERO>().GetComponent<HERO_SETUP>().myCostume, PhotonNetwork.player);
                /*
                if (FengGameManagerMKII.level.type == GAMEMODE.PVP_CAPTURE)
                {
                    Transform transform = component.main_object.transform;
                    transform.position += new Vector3((float)UnityEngine.Random.Range(-20, 20), 2f, (float)UnityEngine.Random.Range(-20, 20));
                }
                */
                component.enabled = true;
                FengGameManagerMKII.instance.mainCamera.setHUDposition();
                GameObject.Find("MainCamera").GetComponent<SpectatorMovement>().disable = true;
                GameObject.Find("MainCamera").GetComponent<MouseLook>().disable = true;
                if (IN_GAME_MAIN_CAMERA.cameraMode == CAMERA_TYPE.TPS)
                {
                    Screen.lockCursor = true;
                }
                else
                {
                    Screen.lockCursor = false;
                }
                Screen.showCursor = false;

            }
            else
            {

                ExitGames.Client.Photon.Hashtable hashtable2 = new ExitGames.Client.Photon.Hashtable();
                hashtable2.Add("dead", false); ;
                hashtable2.Add(PhotonPlayerProperty.isTitan, 2);
                PhotonNetwork.player.SetCustomProperties(hashtable2);


                GameObject obj3;
                FengGameManagerMKII.instance.myLastHero = id.ToUpper();
                obj3 = PhotonNetwork.Instantiate("TITAN_VER3.1", FengGameManagerMKII.level.respawn, new Quaternion(), 0);
                FengGameManagerMKII.instance.mainCamera.setMainObjectASTITAN(obj3);
                obj3.GetComponent<TITAN>().nonAI = true;
                obj3.GetComponent<TITAN>().speed = 30f;
                obj3.GetComponent<TITAN_CONTROLLER>().enabled = true;
                if ((id == "RANDOM") && (UnityEngine.Random.Range(0, 100) < 7))
                {
                    obj3.GetComponent<TITAN>().setAbnormalType(AbnormalType.TYPE_CRAWLER, true);
                }
                FengGameManagerMKII.instance.mainCamera.enabled = true;
                GameObject.Find("MainCamera").GetComponent<SpectatorMovement>().disable = true;
                GameObject.Find("MainCamera").GetComponent<MouseLook>().disable = true;
                if (IN_GAME_MAIN_CAMERA.cameraMode == CAMERA_TYPE.TPS)
                {
                    Screen.lockCursor = true;
                }
                else
                {
                    Screen.lockCursor = false;
                }
                Screen.showCursor = true;
            }
        }
        public void Spawn(string id)
        {
            this.Spawn(id, PhotonNetwork.player.isHuman());
        }

        public virtual void Someoneisdead()
        {
            if (FengGameManagerMKII.instance.isPlayerAllDead())
            {
                FengGameManagerMKII.instance.gameLose();
            }
        }

        public Adv_LevelOpts config = new Adv_LevelOpts()
        {
            FT_Nape_Armor_Multiplier = 1f,
            FT_DieAnimationTime = 5f,
            TitanChaseDistance = 80f,
            AllowAHSSAirReload = true,
        };
        public struct Adv_LevelOpts
        {
            public Action<FEMALE_TITAN> On_Female_titan_die;
            public float FT_Nape_Armor_Multiplier;
            public float FT_DieAnimationTime;
            public float TitanChaseDistance;
            public bool AllowAHSSAirReload;
        }
    }
}
