﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Levels
{
    [Attributes.Respawn]
    class Outside : LevelType.Classic.PvpCapture
    {
        public Outside()
        {
            map = new Maps.Outside();
            map.checkpoint = true;
            config.TitanChaseDistance = 200f;
             
        }
        public new Maps.Outside map
        {
            get { return (Maps.Outside)base.map; }
            set { base.map = value; }
        }
        public override string desc
        {
            get
            {
               return "Capture Checkpoint mode.";
            }
        }

        public override int enemyNumber
        {
            get
            {
                return 0;
            }
        }

        public override string name
        {
            get
            {
               return "Outside The Walls";
            }
        }

        public override bool pvp
        {
            get
            {
                return false;
            }
        }

        public override RespawnMode respawnMode
        {
            get
            {
                return RespawnMode.DEATHMATCH;
            }
        }

        public override bool teamTitan
        {
            get
            {
                return true;
            }
        }

        public override void LevelWasLoaded()
        {
            if (PhotonNetwork.isMasterClient)
            {
                GameObject[] objArray5 = GameObject.FindGameObjectsWithTag("titanRespawn");
                if (objArray5.Length <= 0)
                {
                    return;
                }
                for (int i = 0; i < objArray5.Length; i++)
                {
                    FengGameManagerMKII.instance.spawnTitanRaw(objArray5[i].transform.position, objArray5[i].transform.rotation).GetComponent<TITAN>().setAbnormalType(AbnormalType.TYPE_CRAWLER, true);
                }
            }
            base.LevelWasLoaded();

        }
        public override bool horse
        {
            get
            {
                return true;
            }
        }
    }
}
