﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Levels
{

    class House : LevelType.Classic.PvpAhss
    {
        public House()
        {
            map = new Maps.House();
        }
        public new Maps.House map { get { return (Maps.House)base.map; } private set { base.map = value; } }

        public override string desc
        {
            get
            {
                return "***Spoiler Alarm!***";
            }
        }

        public override int enemyNumber
        {
            get
            {
                return -1;
            }
        }

        public override string name
        {
            get
            {
               return "House Fight";
            }
        }

        public override bool pvp
        {
            get
            {
                return true;
            }
        }

        public override RespawnMode respawnMode
        {
            get
            {
                return RespawnMode.NEVER;
            }
        }

        public override bool teamTitan
        {
            get
            {
                return true;
            }
        }

        public override void LevelWasLoaded()
        {
            base.LevelWasLoaded();
        }
    }
}
