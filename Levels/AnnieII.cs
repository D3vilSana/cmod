﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Levels.Maps;
using UnityEngine;

namespace Levels
{
    [Attributes.Annie]
    class AnnieII : LevelType.Classic.Kill_titan
    {

        public AnnieII()
        {
            map = new Maps.Forest();
        }
        public new Maps.Forest map
        {
            get { return (Maps.Forest)base.map; }
            private set { base.map = value; }
        }
        public override string desc
        {
            get
            {
                return "Nape Armor/ Ankle Armor:\nNormal:1000/50\nHard:3000/200\nAbnormal:6000/1000\n(RESPAWN AFTER 10 SECONDS)";
            }
        }

        public override int enemyNumber
        {
            get
            {
                return 15;
            }
        }

        public override string name
        {
            get
            {
                return "Annie II";
            }
        }

        public override bool pvp
        {
            get
            {
                return true;
            }
        }

        public override RespawnMode respawnMode
        {
            get
            {
                return RespawnMode.DEATHMATCH;
            }
        }

        public override bool teamTitan
        {
            get
            {
                return false;
            }
        }


    }
}
