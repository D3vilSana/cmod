﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Levels
{
    class CageFighting : LevelType.Classic.PvpAhss
    {
        public override string desc
        {
            get
            {
                return "2 players in different cages. when you kill a titan,  one or more titan will spawn to your opponent's cage.";
            }
        }

        public override int enemyNumber
        {
            get
            {
                return 1;
            }
        }

        public override string mapName
        {
            get
            {
                return "Cage Fighting";
            }
        }

        public override string name
        {
            get
            {
                return "Cage Fighting";
            }
        }

        public override bool pvp
        {
            get
            {
                return false;
            }
        }

        public override RespawnMode respawnMode
        {
            get
            {
                return RespawnMode.NEVER;
            }
        }

        public override bool teamTitan
        {
            get
            {
                return false;
            }
        }

        protected override GAMEMODE basetype
        {
            get
            {
                return GAMEMODE.CAGE_FIGHT;
            }
        }

        public new void LevelWasLoaded()
        {
            base.LevelWasLoaded();
        }
    }
}
