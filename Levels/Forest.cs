﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Levels
{
    class Forest : LevelType.Classic.Kill_titan
    {
        public Forest()
        {
            map = new Maps.Forest();
        }
        public new Maps.Forest map { get { return (Maps.Forest)base.map; } private set { base.map = value; } }
        public override string desc
        {
            get
            {
                return "The Forest Of Giant Trees.(No RESPAWN/SUPPLY/PLAY AS TITAN)";
            }
        }

        public override int enemyNumber
        {
            get
            {
                return 5;
            }
        }

        public override string name
        {
            get
            {
                return "The Forest";
            }
        }

        public override bool pvp
        {
            get
            {
                return true;
            }
        }

        public override RespawnMode respawnMode
        {
            get
            {
                return RespawnMode.NEVER;
            }
        }

        public override bool teamTitan
        {
            get
            {
                return true;
            }
        }

       
    }
}
