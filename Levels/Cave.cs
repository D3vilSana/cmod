﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Levels
{
    class Cave : LevelType.Classic.PvpAhss
    {
        public Cave()
        {
            map = new Maps.Cave();
        }
        public new Maps.Cave map { get { return (Maps.Cave)base.map; } private set { base.map = value; } }
        public override string desc
        {
            get
            {
                return "***Spoiler Alarm!***";
            }
        }

        public override int enemyNumber
        {
            get
            {
                return -1;
            }
        }

        public override string name
        {
            get
            {
                return "Cave Fight";
            }
        }

        public override bool pvp
        {
            get
            {
                return true;
            }
        }

        public override RespawnMode respawnMode
        {
            get
            {
                return RespawnMode.NEVER;
            }
        }

        public override bool teamTitan
        {
            get
            {
                return true;
            }
        }

        public override void LevelWasLoaded()
        {
            base.LevelWasLoaded();
        }
    }
}
