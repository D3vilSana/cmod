﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Levels
{
    [Attributes.Annie]
    class Annie : LevelType.Classic.Kill_titan
    {
        public Annie()
        {
            map = new Maps.Forest();
        }
        public new Maps.Forest map { get { return (Maps.Forest)base.map; } private set { base.map = value; } }
        public override string desc
        {
            get
            {
               return "Nape Armor/ Ankle Armor:\nNormal:1000/50\nHard:2500/100\nAbnormal:4000/200\nYou only have 1 life.Don't do this alone.";
            }
        }

        public override int enemyNumber
        {
            get
            {
                return 15;
            }
        }

        public override string name
        {
            get
            {
                return "Annie";
            }
        }

        public override bool pvp
        {
            get
            {
                return true;
            }
        }

        public override RespawnMode respawnMode
        {
            get
            {
                return RespawnMode.NEVER;
            }
        }

        public override bool teamTitan
        {
            get
            {
                return false;
            }
        }

        public new bool punk { get { return false; } }

    }
}
