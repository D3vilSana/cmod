﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Levels
{
    [Levels.Attributes.Lava]
    class ForestIV : LevelType.Classic.Survive
    {
        public ForestIV()
        {
            map = new Maps.Forest();
        }
        public new Maps.Forest map { get { return (Maps.Forest)base.map; } private set { base.map = value; } }
        public override string desc
        {
            get
            {
                return "Survive for 20 waves.player will respawn in every new wave.\nNO CRAWLERS\n***YOU CAN'T TOUCH THE GROUND!***";
            }
        }

        public override int enemyNumber
        {
            get
            {
                return 3;
            }
        }

        public override string name
        {
            get
            {
                return "The Forest IV  - LAVA";
            }
        }

        public override bool pvp
        {
            get
            {
                return false;
            }
        }

        public override RespawnMode respawnMode
        {
            get
            {
                return RespawnMode.NEWROUND;
            }
        }

        public override bool teamTitan
        {
            get
            {
                return false;
            }
        }

       
    }
}
