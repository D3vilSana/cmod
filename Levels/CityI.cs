﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Levels
{
    class CityI : LevelType.Classic.Kill_titan
    {
        public CityI()
        {
            map = new Maps.City();
            map.checkpoint = false;
        }
        public new Levels.Maps.City map { get { return (Maps.City)base.map; } protected set { base.map = value; } }
        public override string desc
        {
            get
            {
                return "kill all the titans with your friends.(No RESPAWN/SUPPLY/PLAY AS TITAN)";
            }
        }

        public override int enemyNumber
        {
            get
            {
                return 10;
            }
        }

        public override string name
        {
            get
            {
                return "The City";
            }
        }

        public override bool pvp
        {
            get
            {
                return true;
            }
        }

        public override RespawnMode respawnMode
        {
            get
            {
                return RespawnMode.NEVER;
            }
        }

        public override bool teamTitan
        {
            get
            {
                return true;
            }
        }
        
    }
}
