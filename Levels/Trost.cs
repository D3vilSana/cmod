﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Levels
{
    class Trost : LevelType.Classic.Trost
    {
        public Trost()
        {
            map = new Maps.Trost();
        }
        public new Maps.Trost map { get { return (Maps.Trost)base.map; } private set { base.map = value; } }
        public override string desc
        {
            get
            {
                return "Escort Titan Eren";
            }
        }

        public override int enemyNumber
        {
            get
            {
                return 2;
            }
        }

        public override string name
        {
            get
            {
                return "Trost";
            }
        }

        public override bool pvp
        {
            get
            {
                return false;
            }
        }

        public override RespawnMode respawnMode
        {
            get
            {
                return RespawnMode.NEVER;
            }
        }

        public override bool teamTitan
        {
            get
            {
                return false;
            }
        }
    }
}
