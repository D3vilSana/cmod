﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Levels
{
    class Colossal : LevelType.Classic.BossFight
    {
        public Colossal()
        {
            map = new Maps.Colossal();
        }
        public new Maps.Colossal map { get { return (Maps.Colossal)base.map; } private set { base.map = value; } }
        public override string desc
        {
            get
            {
                return "Defeat the Colossal Titan.\nPrevent the abnormal titan from running to the north gate.\n Nape Armor:\n Normal:2000\nHard:3500\nAbnormal:5000\n";
            }
        }

        public override int enemyNumber
        {
            get
            {
                return 2;
            }
        }

        public override string name
        {
            get
            {
                return "Colossal Titan";
            }
        }

        public override bool pvp
        {
            get
            {
                return false;
            }
        }

        public override RespawnMode respawnMode
        {
            get
            {
                return RespawnMode.NEVER;
            }
        }

        public override bool teamTitan
        {
            get
            {
                return false;
            }
        }

        public override void LevelWasLoaded()
        {
            base.LevelWasLoaded();
        }

    }
}
