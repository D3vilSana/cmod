﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Levels
{

    class ForestII : LevelType.Classic.Survive
    {
        public ForestII()
        {
            map = new Maps.Forest();
        }
        public new Maps.Forest map { get { return (Maps.Forest)base.map; } private set { base.map = value; } }
        public override string desc
        {
            get
            {
                return "Survive for 20 waves.";
            }
        }

        public override int enemyNumber
        {
            get
            {
                return 3;
            }
        }

        public override string name
        {
            get
            {
                return "The Forest II";
            }
        }

        public override bool pvp
        {
            get
            {
                return false;
            }
        }

        public override RespawnMode respawnMode
        {
            get
            {
                return RespawnMode.NEVER;
            }
        }

        public override bool teamTitan
        {
            get
            {
                return false;
            }
        }


    }
}
