﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Levels
{
    [Attributes.Respawn]
    class CityIII : LevelType.Classic.PvpCapture
    {
        public CityIII()
        {
            map = new Maps.City();
            map.checkpoint = true;
            config.TitanChaseDistance = 120f;
        }

        public new Levels.Maps.City map { get { return (Maps.City)base.map; } private set { base.map = value; } }
        public override string desc
        {
            get
            {
                return "Capture Checkpoint mode.";
            }
        }

        public override int enemyNumber
        {
            get
            {
                return 0;
            }
        }

        public override string name
        {
            get
            {
               return "The City III";
            }
        }

        public override bool pvp
        {
            get
            {
                return false;
            }
        }

        public override RespawnMode respawnMode
        {
            get
            {
                return RespawnMode.DEATHMATCH;
            }
        }

        public override bool teamTitan
        {
            get
            {
                return true;
            }
        }

        public override void LevelWasLoaded()
        {
            if (PhotonNetwork.isMasterClient)
            {
                int num5 = 90;
                if (FengGameManagerMKII.instance.difficulty == 1)
                {
                    num5 = 70;
                }
                FengGameManagerMKII.instance.randomSpawnTitan("titanRespawn", num5, enemyNumber, false);
            }
            base.LevelWasLoaded();
        }
    }
}
