﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Levels.LevelType.Custom
{
    class Kill_Titan : Custom
    {
        public override GAMEMODE customtype
        {
            get
            {
                return GAMEMODE.KILL_TITAN;
            }
        }

        public override void LevelWasLoaded()
        {
            if (PhotonNetwork.isMasterClient)
            {
                int num5 = 90;
                if (FengGameManagerMKII.instance.difficulty == 1)
                {
                    num5 = 70;
                }
                FengGameManagerMKII.instance.randomSpawnTitan("titanRespawn", num5, 1, false);
            }
            base.LevelWasLoaded();
        }
    }
}
