﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Levels.LevelType.Custom
{
    [Attributes.Racing("CustomDoor")]
    class Racing : Custom
    {
        public override GAMEMODE customtype
        {
            get
            {
                return GAMEMODE.RACING;
            }
        }
        /*
        protected override Vector3 respawnHuman
        {
            get
            {
                return respawn ?? base.respawnHuman;
            }
        }*/
    }
}
