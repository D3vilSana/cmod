﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Levels.LevelType.Custom
{
    class PvpAhss : Custom
    {
        public override GAMEMODE customtype
        {
            get
            {
                return GAMEMODE.PVP_AHSS;
            }
        }
    }
}
