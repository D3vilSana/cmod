﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Levels.LevelType.Custom
{
    class None : Custom
    {
        public override GAMEMODE customtype
        {
            get
            {
                return GAMEMODE.None;
            }
        }

        public override bool isMyLevel(string LevelName)
        {
            return LevelName.StartsWith("Custom");
        }
    }
}
