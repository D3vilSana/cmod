﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Levels.Attributes;
using UnityEngine;

namespace Levels.LevelType.Custom
{

    abstract class Custom : Level
    {
        public Custom()
        {
            map = new Maps.Flat();
        }
        public new Maps.Flat map { get { return (Maps.Flat)base.map; } private set { base.map = value; } }

        public abstract GAMEMODE customtype { get; }

        public override bool isMyLevel(string LevelName)
        {
            return false;
        }

        public override void core()
        {
            base.core();
        }

        public override void ShowLevel(DayLight time, ref bool selected)
        {
            GUI.Box(new Rect(0, 0, 200, 50), "");
            ServerList.DrawIcon(this.name, time.name());

            selected = GUI.Toggle(new Rect(50, 15, 100, 20), selected, this.name);
        }
        

        public override GAMEMODE type
        {
            get
            {
                return customtype;
            }

            set
            {
                FengGameManagerMKII.level = LevelManager.getLevels().Where(l => l is Custom).First(l => l.type == value);
                FengGameManagerMKII.level.LevelWasLoaded();
            }
        }

        protected override Vector3 respawnHuman
        {
            get
            {
                GameObject go = new GameObject();
                
                switch (RCextensions.returnIntFromObject(PhotonNetwork.player.customProperties[PhotonPlayerProperty.RCteam]))
                {
                    case 1://Cyan
                        return  CustomMaps.Calls.SpawnPoint.PlayerSpawnCyan.spawns.getRandom();
                    case 2://Magenta
                        return CustomMaps.Calls.SpawnPoint.PlayerSpawnMagenta.spawns.getRandom();
                    default:
                        List<Vector3> l = new List<Vector3>();
                        foreach (Vector3 p in CustomMaps.Calls.SpawnPoint.PlayerSpawnMagenta.spawns)
                        {
                            l.Add(p);
                        }
                        foreach (Vector3 p in CustomMaps.Calls.SpawnPoint.PlayerSpawnCyan.spawns)
                        {
                            l.Add(p);
                        }
                        if (l.Count > 0)
                        {
                            return l.getRandom();
                        }
                        return new Vector3(UnityEngine.Random.Range(-5f, 5f), 0f, UnityEngine.Random.Range(-5f, 5f));
                }
            }
        }
        protected override Vector3 respawnTitan
        {
            get
            {
                throw new NotImplementedException();//TODO : respawn titan
            }
        }

        public override string name
        {
            get { return "Custom"; }
        }

        public override bool pvp
        {
            get
            {
                return true;
            }
        }
        public override bool teamTitan { get { return true; } }

        public override RespawnMode respawnMode
        {
            get
            {
                return RespawnMode.DEATHMATCH;
            }
        }

        public override bool horse
        {
            get
            {
                return false;
            }
        }

        public override bool punk
        {
            get
            {
                return false;
            }
        }
    }

}
