﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Levels.LevelType.Custom
{
    class Survive : Custom
    {
        public override GAMEMODE customtype
        {
            get
            {
                return GAMEMODE.SURVIVE_MODE;
            }
        }

        public override void LevelWasLoaded()
        {
            if (PhotonNetwork.isMasterClient)
            {
                int num5 = 90;
                if (FengGameManagerMKII.instance.difficulty == 1)
                {
                    num5 = 70;
                }
                FengGameManagerMKII.instance.randomSpawnTitan("titanRespawn", num5, 1, false);
            }
            base.LevelWasLoaded();
        }
        public override RespawnMode respawnMode
        {
            get
            {
                return RespawnMode.NEWROUND;
            }
        }
    }
}
