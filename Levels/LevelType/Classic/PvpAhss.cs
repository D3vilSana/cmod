﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Levels.LevelType.Classic
{
    abstract class PvpAhss : Classic
    {
        public PvpAhss() : base()
        {
            config.AllowAHSSAirReload = false;
        }
        protected override GAMEMODE basetype
        {
            get
            {
                return GAMEMODE.PVP_AHSS;
            }
        }
        public override void Someoneisdead()
        {
            if (FengGameManagerMKII.instance.isPlayerAllDead())
            {
                FengGameManagerMKII.instance.gameLose();
            }
            if (FengGameManagerMKII.instance.isTeamAllDead(1))
            {
                FengGameManagerMKII.instance.gameWin(2);
            }
            if (FengGameManagerMKII.instance.isTeamAllDead(2))
            {
                FengGameManagerMKII.instance.gameWin(1);
            }
        }
    }
}
