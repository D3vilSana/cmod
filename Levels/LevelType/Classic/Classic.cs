﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Levels.Attributes;
using UnityEngine;

namespace Levels.LevelType.Classic
{
    public abstract class Classic : Level
    {
        



        //public abstract string name { get; }

        //public abstract string mapName { get; }

        public abstract string desc { get; }

        public abstract int enemyNumber { get; }
 

        /*public abstract bool teamTitan
        {
            get;
        }
        */

        private static Nullable<GAMEMODE> current = null;

        public override GAMEMODE type
        {
            get
            {
                return current ?? basetype;
            }
            set
            {//Do something ?
                current = value;
            }
        }

        protected abstract GAMEMODE basetype { get; }

        //public abstract RespawnMode respawnMode { get; }

        public bool noCrawler { get { return false; } }

        public override bool horse { get { return false; } }

        public override bool punk
        {
            get { return true; }
        }

       // public abstract bool pvp { get; }

        public override void LevelWasLoaded()
        {
            base.LevelWasLoaded();
        }

        public override bool isMyLevel(string LevelName)
        {
            return LevelName == name;
        }

        public override void core() { base.core(); }

        protected override Vector3 respawnHuman
        {
            get
            {
                GameObject[] objArray = GameObject.FindGameObjectsWithTag("playerRespawn");
                GameObject pos = objArray[UnityEngine.Random.Range(0, objArray.Length)];
                return pos.transform.position;
            }
        }
        protected override Vector3 respawnTitan
        {
            get
            {
                GameObject[] objArray = GameObject.FindGameObjectsWithTag("titanRespawn");
                return objArray[UnityEngine.Random.Range(0, objArray.Length)].transform.position;   
            }
        }

        public override void ShowLevel(DayLight time,ref bool selected)
        {
            
            GUI.Box(new Rect(0,0,200,50), "");
            ServerList.DrawIcon(this.map.mapname, time.name());

            selected = GUI.Toggle(new Rect(50, 15, 100, 20), selected, this.name);
        }

    }
}