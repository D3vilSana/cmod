﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Levels.LevelType.Classic
{
    abstract class BossFight : Classic
    {
        protected override GAMEMODE basetype
        {
            get
            {
                return GAMEMODE.BOSS_FIGHT_CT;
            }
        }
    }
}
