﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Levels.LevelType.Classic
{
    abstract class Trost : Classic
    {
        protected sealed override GAMEMODE basetype
        {
            get
            {
                return GAMEMODE.TROST;
            }
        }

        public override void LevelWasLoaded()
        {

            PhotonNetwork.Instantiate("TITAN_EREN_trost", new Vector3(-200f, 0f, -194f), Quaternion.Euler(0f, 180f, 0f), 0).GetComponent<TITAN_EREN>().rockLift = true;
            int rate = 90;
            if (FengGameManagerMKII.instance.difficulty == 1)
            {
                rate = 70;
            }
            GameObject[] array2 = GameObject.FindGameObjectsWithTag("titanRespawn");
            GameObject gameObject3 = GameObject.Find("titanRespawnTrost");
            if (gameObject3 != null)
            {
                for (int i = 0; i < array2.Length; i++)
                {
                    GameObject gameObject4 = array2[i];
                    if (gameObject4.transform.parent.gameObject == gameObject3)
                    {
                        FengGameManagerMKII.instance.spawnTitan(rate, gameObject4.transform.position, gameObject4.transform.rotation, false);
                    }
                }
            }
            /*
            GameObject.Find("playerRespawn").SetActive(false);
            UnityEngine.Object.Destroy(GameObject.Find("playerRespawn"));
            GameObject.Find("rock").animation["lift"].speed = 0f;
            GameObject.Find("door_fine").SetActiveRecursively(false);
            GameObject.Find("door_broke").SetActiveRecursively(true);
            UnityEngine.Object.Destroy(GameObject.Find("ppl"));
            if (PhotonNetwork.isMasterClient)
            {
                if (!FengGameManagerMKII.instance.isPlayerAllDead())
                {
                    PhotonNetwork.Instantiate("TITAN_EREN_trost", new Vector3(-200f, 0f, -194f), Quaternion.Euler(0f, 180f, 0f), 0).GetComponent<TITAN_EREN>().rockLift = true;
                    int num3 = 90;
                    if (FengGameManagerMKII.instance.difficulty == 1)
                    {
                        num3 = 70;
                    }
                    GameObject[] objArray3 = GameObject.FindGameObjectsWithTag("titanRespawn");
                    GameObject obj6 = GameObject.Find("titanRespawnTrost");
                    if (obj6 != null)
                    {
                        foreach (GameObject obj7 in objArray3)
                        {
                            if (obj7.transform.parent.gameObject == obj6)
                            {
                                FengGameManagerMKII.instance.spawnTitan(num3, obj7.transform.position, obj7.transform.rotation, false);
                            }
                        }
                    }
                }
            }*/

            base.LevelWasLoaded();
        }
    }
}
