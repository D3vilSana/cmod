﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Levels.LevelType.Classic
{
    [Attributes.Respawn(0)]
    abstract class Racing : Classic
    {
        protected override GAMEMODE basetype
        {
            get
            {
                return GAMEMODE.RACING;
            }
        }
        public override void LevelWasLoaded()
        {
            base.LevelWasLoaded();
        }
        /*
        protected override Vector3 respawnHuman
        {
            get
            {
                if (FengGameManagerMKII.instance.checkpoint!=null)
                {
                    return FengGameManagerMKII.instance.checkpoint.transform.position;
                }
                else
                {
                    return base.respawnHuman;
                }
            }
        }*/
    }
}
