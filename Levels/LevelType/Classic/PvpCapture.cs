﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Levels.LevelType.Classic
{
    [Attributes.Checkpoint]
    abstract class PvpCapture : Classic
    {
        
        public PvpCapture() : base()
        {
            config.FT_Nape_Armor_Multiplier = .8f;
        }
        protected sealed override GAMEMODE basetype
        {
            get
            {
                return GAMEMODE.PVP_CAPTURE;
            }
        }
        public override void LevelWasLoaded()
        {
            base.LevelWasLoaded();
        }
        protected override Vector3 respawnHuman
        {
            get
            {
                return GameObject.Find("PVPchkPtH").transform.position;
            }
        }
        public override void Someoneisdead()
        {
            FengGameManagerMKII.instance.PVPtitanScore += 2;
            FengGameManagerMKII.instance.checkPVPpts();
            FengGameManagerMKII.instance.photonView.RPC("refreshPVPStatus", PhotonTargets.Others, FengGameManagerMKII.instance.PVPhumanScore,FengGameManagerMKII.instance.PVPtitanScore);
        }
    }
}
