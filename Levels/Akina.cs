﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Levels
{
    [Attributes.Racing("door")]
    class Akina : LevelType.Classic.Racing
    {
        public Akina()
        {
            map = new Maps.Akina();
        }
        public new Maps.Akina map { get { return (Maps.Akina)base.map; } private set { base.map = value; } }
        public override string desc
        {
            get
            {
                return "";
            }
        }

        public override int enemyNumber
        {
            get
            {
                return 0;
            }
        }

        public override string name
        {
            get
            {
               return "Racing - Akina";
            }
        }

        public override bool pvp
        {
            get
            {
                return true;
            }
        }

        public override RespawnMode respawnMode
        {
            get
            {
                return RespawnMode.NEVER;
            }
        }

        public override bool teamTitan
        {
            get
            {
                return false;
            }
        }

        public override void LevelWasLoaded()
        {
            base.LevelWasLoaded();
            
        }

    }
}
