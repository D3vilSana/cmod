﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Levels.Attributes
{
    [System.AttributeUsage(AttributeTargets.Class, Inherited = true, AllowMultiple = true)]
    public abstract class LevelAttributes : Attribute
    {
        public virtual void OnLevelLoaded() { }

        public virtual void core() { }

        public virtual void setMyLevel(Level l) { }
    }
}
