﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Levels.Attributes
{
    class NoSupply : LevelAttributes
    {
        public override void core()
        {

        }

        public override void OnLevelLoaded()
        {
            UnityEngine.Object.Destroy(GameObject.Find("aot_supply"));
        }
    }
}
