﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Levels.Attributes
{
    /// <summary>
    /// If you want to make custom checkpoints
    /// (null = default)
    /// </summary>
    class Checkpoint : LevelAttributes
    {
        public UnityEngine.Vector3? respawn;

        public override void OnLevelLoaded()
        {
            respawn = null;
        }
    }
}
