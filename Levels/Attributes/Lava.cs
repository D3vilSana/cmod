﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Levels.Attributes
{
    class Lava : LevelAttributes
    {
        public override void OnLevelLoaded()
        {
            GameObject go =  (GameObject)UnityEngine.Object.Instantiate(Resources.Load("levelBottom"), new Vector3(0f, -29.5f, 0f), Quaternion.Euler(0f, 0f, 0f));
            
            GameObject.Find("aot_supply").transform.position = GameObject.Find("aot_supply_lava_position").transform.position;
            GameObject.Find("aot_supply").transform.rotation = GameObject.Find("aot_supply_lava_position").transform.rotation;
        }
    }
}
