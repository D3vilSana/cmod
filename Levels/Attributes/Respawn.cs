﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Levels.Attributes
{
    class Respawn : LevelAttributes
    {
        public Respawn()
        {

        }
        public Respawn(float time)
        {
            respawntime = time;
        }
        private float _respawn = -1;
        public float respawntime
        {
            get
            {
                if (_respawn >= 0)
                {
                    return _respawn;
                }
                else
                {
                    if (PhotonNetwork.player.isHuman())
                    {
                        return 5;
                    }
                    else
                    {
                        return 10;
                    }
                }

            }
            set
            {
                _respawn = value;
            }
        }
    }
}
