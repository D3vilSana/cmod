﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Levels.Attributes
{
    class Racing : Checkpoint
    {
        public static bool racingStarted = false;
        public static bool racingEnded = false;
        public string[] doornames;

        public Racing(params string[] doornames)
        {
            this.doornames = doornames;
        }

        public override void core()
        {
            if (FengGameManagerMKII.instance.roundTime>20f&&!racingStarted)
            {
                racingStarted = true;
                racingEnded = false;
                foreach (UnityEngine.GameObject d in (from n in UnityEngine.Object.FindObjectsOfType<UnityEngine.GameObject>().Cast<UnityEngine.GameObject>()
                                                      where doornames.Contains(n.name)
                                                      select n))
                {
                    d.SetActive(false);
                }
            }
            base.core();
        }

        public override void OnLevelLoaded()
        {
            racingStarted = false;
            base.OnLevelLoaded();
        }
    }
}
