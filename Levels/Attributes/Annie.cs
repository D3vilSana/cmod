﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Levels.Attributes
{
    sealed class Annie : LevelAttributes
    {
        public Annie()
        {
        }

        public override void core()
        {
        }

        public override void OnLevelLoaded()
        {
            if (PhotonNetwork.isMasterClient)
            {
                PhotonNetwork.Instantiate("FEMALE_TITAN", GameObject.Find("titanRespawn").transform.position, GameObject.Find("titanRespawn").transform.rotation, 0);
            }
        }
        public override void setMyLevel(Level l)
        {
            l.config.On_Female_titan_die = delegate (FEMALE_TITAN ft)
            {
                for (int i = 0; i < 15; i++)
                {
                    FengGameManagerMKII.instance.randomSpawnOneTitan("titanRespawn", 50).GetComponent<TITAN>().beTauntedBy(ft.gameObject, 20f);
                }
            };
            l.config.FT_DieAnimationTime = 20f;
        }
    }
}
