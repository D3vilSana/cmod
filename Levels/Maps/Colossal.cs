﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Levels.Maps
{
    class Colossal : Trost
    {
        public override void Load()
        {
            base.Load();
        }
        public override void LevelLoaded()
        {
            base.LevelLoaded();
            GameObject.Find("playerRespawnTrost").SetActive(false);
            UnityEngine.Object.Destroy(GameObject.Find("playerRespawnTrost"));
            UnityEngine.Object.Destroy(GameObject.Find("rock"));
            if (PhotonNetwork.isMasterClient)
            {
                PhotonNetwork.Instantiate("COLOSSAL_TITAN", (Vector3)(-Vector3.up * 10000f), Quaternion.Euler(0f, 180f, 0f), 0);
            }
        }
    }
}
