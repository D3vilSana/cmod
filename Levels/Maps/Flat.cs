﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Levels.Maps
{
    class Flat : Maps.Forest
    {
        public override void LevelLoaded()
        {
            base.LevelLoaded();
            GameObject[] array2 = (GameObject[])UnityEngine.Object.FindObjectsOfType(typeof(GameObject));
            for (int i = 0; i < array2.Length; i++)
            {
                GameObject gameObject = array2[i];
                if (gameObject.name.Contains("TREE") || gameObject.name.Contains("aot_supply"))
                {
                    UnityEngine.Object.Destroy(gameObject);
                }
                else if (gameObject.name == "Cube_001" && gameObject.transform.parent.gameObject.tag != "player" && gameObject.renderer != null)
                {
                    //this.groundList.Add(gameObject);
                    gameObject.renderer.material.mainTexture = ((Material)FengGameManagerMKII.RCassets.Load("grass")).mainTexture;
                }
            }
        }
    }
}
