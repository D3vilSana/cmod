﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Levels.Maps
{
    public class City : CheckpointMap
    {
        public override string mapname
        {
            get
            {
                return "The City I";
            }
        }

        public override int maxtitannumber
        {
            get
            {
                return 12;
            }
        }
    }
}
