﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Levels.Maps
{
    class Outside : Levels.Maps.CheckpointMap
    {
        public override string mapname
        {
            get
            {
                return "OutSide";
            }
        }

        public override int maxtitannumber
        {
            get
            {
                return 20;
            }
        }

        protected override void setactive(PVPcheckPoint p, bool state)
        {
            base.setactive(p, state);
            if (p.humanPt == p.humanPtMax&&p.state == CheckPointState.Human&&p.photonView.isMine)
            {
                p.supply = PhotonNetwork.Instantiate("aot_supply", p.transform.position - ((Vector3)(Vector3.up * (p.transform.position.y - p.getHeight(p.transform.position)))), p.transform.rotation, 0);
            }
        }
        public override void OnCheckpointStateChanged(PVPcheckPoint p,CheckPointState c)
        {
            base.OnCheckpointStateChanged(p,c);
            if (c == CheckPointState.Human)
            {
                p.supply = PhotonNetwork.Instantiate("aot_supply", p.transform.position - ((Vector3)(Vector3.up * (p.transform.position.y - p.getHeight(p.transform.position)))), p.transform.rotation, 0);
            }
        }
    }
}
