﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Levels.Maps
{
    public abstract class CheckpointMap : Map
    {
        public List<PVPcheckPoint> checkpoints = new List<PVPcheckPoint>();

        private bool _checkpoint = false;
        public abstract int maxtitannumber { get; }
        public bool checkpoint
        {
            get
            {
                return _checkpoint;
            }
            set
            {
                if (UnityEngine.Application.loadedLevelName==mapname && FengGameManagerMKII.level!=null && FengGameManagerMKII.level.map == this)
                {
                    foreach (PVPcheckPoint pvp in checkpoints)
                    {
                        setactive(pvp, value);
                    }
                }
                _checkpoint = value;
            }
        }
        public void registerCheckpoint(PVPcheckPoint p) {
            if (!checkpoints.Contains(p))
            {
                checkpoints.Add(p);
            }
            setactive(p, checkpoint);
        }
        protected virtual void setactive(PVPcheckPoint p,bool state)
        {
            p.gameObject.SetActive(state);
            if (state)
            {
                PVPcheckPoint.chkPts.Add(p);
                PVPcheckPoint.chkPts.Sort(new IComparerPVPchkPtID());

                if (p.humanPt == p.humanPtMax)
                {
                    p.state = CheckPointState.Human;
                }

                if (p.photonView.isMine && !p.hasAnnie)
                {
                    if (UnityEngine.Random.Range(0, 100) < 50)
                    {
                        int num = UnityEngine.Random.Range(1, 2);
                        for (int i = 0; i < num; i++)
                        {
                            p.newTitan();
                        }
                        if (p.isBase)
                        {
                            p.newTitan();
                        }
                    }
                    if (p.titanPt == p.titanPtMax)
                    {
                        p.state = CheckPointState.Titan;
                    }
                    p.hitTestR = 15 * p.size;
                    p.transform.localScale = new UnityEngine.Vector3(p.size, p.size, p.size);
                }
            }
        }
        public virtual void OnCheckpointStateChanged(PVPcheckPoint p,CheckPointState c)
        {
            
        }
    }
}
