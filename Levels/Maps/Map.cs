﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Levels.Maps
{
    public abstract class Map
    {
        public abstract string mapname { get; }
        public virtual void Load()
        {
            PhotonNetwork.LoadLevel(mapname);
        }
        public virtual void LevelLoaded()
        {
            
        }
    }
}
