﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Levels
{
    class ForestIII : LevelType.Classic.Survive
    {
        public ForestIII()
        {
            map = new Maps.Forest();
        }
        public new Maps.Forest map { get { return (Maps.Forest)base.map; } private set { base.map = value; } }
        public override string desc
        {
            get
            {
                return "Survive for 20 waves.player will respawn in every new wave";
            }
        }

        public override int enemyNumber
        {
            get
            {
                return 3;
            }
        }

        public override string name
        {
            get
            {
               return "The Forest III";
            }
        }

        public override bool pvp
        {
            get
            {
                return false;
            }
        }

        public override RespawnMode respawnMode
        {
            get
            {
                return RespawnMode.NEWROUND;
            }
        }

        public override bool teamTitan
        {
            get
            {
                return false;
            }
        }


    }
}
