﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;


class CModConfigs : Configs
{
    public enum Conf {Pseudo}
    private Hashtable ht = new Hashtable();

    private FileManager.DataFile file;

    public override Type enum_config
    {
        get
        {
            return typeof(Conf);
        }
    }

    public override string name
    {
        get
        {
            return "CmodConfig";
        }
    }

    public override float getfloatVal<T>(T key)
    {
        return (float)ht[key];
    }

    public override int getintVal<T>(T key)
    {
        return (int)ht[key];
    }

    public override string getstringVal<T>(T key)
    {
        return (string)ht[key];
    }

    public override bool getBoolVal<T>(T key)
    {
        return (bool)ht[key];
    }

    public override void init(){
        file = config.getfile("CMod.conf.txt");
    }

    public override void load()
    {
        if (file.json["Pseudo"].Value!="")
        {
            ht.Add(Conf.Pseudo, file.json["Pseudo"].Value);
        }
        else
        {
            file.json["Pseudo"] = "CMod's GUEST <3";
            ht.Add(Conf.Pseudo, "CMod's GUEST <3");
        }
        LoginFengKAI.player.name = (string)ht[Conf.Pseudo];
        Menu.LayoutTab t = new Menu.ConfigLayoutTab("Basic");
        t.Init();
        t.AddElem(new Menu.LayoutElems.Text(delegate (string newval, Menu.LayoutElems.Text text)
        {
            Configs.set<Conf>(Conf.Pseudo, newval);
            save(Conf.Pseudo);
        }, "Pseudo", (string)ht[Conf.Pseudo],150));

        IngameMenu.AddTab(t);
        file.flushjson();
    }

    public void save(Conf c)
    {
        switch (c)
        {
            case Conf.Pseudo:
                file.json["Pseudo"] = (string)ht[c];
                break;
            default:
                break;
        }
        file.flushjson();
    }

    public override void setVal<T>(T key, object val)
    {
        set((Conf)(object)key, val);
    }
    private void set(Conf c,object val)
    {
        switch (c)
        {
            case Conf.Pseudo:
                ExitGames.Client.Photon.Hashtable h = new ExitGames.Client.Photon.Hashtable();
                h.Add(PhotonPlayerProperty.name, (string)val);
                PhotonNetwork.player.SetCustomProperties(h);
                LoginFengKAI.player.name = (string)val;
                break;
            default:
                break;
        }
        ht[c] = val;
        save(c);
    }


}
