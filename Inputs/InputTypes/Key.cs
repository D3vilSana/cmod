﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SimpleJSON;

namespace Inputs.InputTypes
{
    class Key : InputType
    {
        private UnityEngine.KeyCode mykeycode;
        public Key(SimpleJSON.JSONNode n)
        {
            mykeycode = (UnityEngine.KeyCode)Enum.Parse(typeof(UnityEngine.KeyCode), n["KeyCode"]);
        }
        public Key(UnityEngine.KeyCode code)
        {
            mykeycode = code;
        }
        protected override bool ispressed
        {
            get
            {
                return UnityEngine.Input.GetKey(mykeycode);
            }
        }

        public override JSONClass serialize()
        {
            JSONClass n = base.serialize();
            n["KeyCode"] = mykeycode.ToString();
            return n;
        }
        public override string displayinfo
        {
            get
            {
                return mykeycode.ToString();
            }
        }
    }
}
