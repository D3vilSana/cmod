﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Inputs.InputTypes
{
    class Mouse : InputType
    {
        private int btn;
        public Mouse(int btnnumber)
        {
            btn = btnnumber;
        }
        public override string displayinfo
        {
            get
            {
                return "Mouse["+btn+"]";
            }
        }

        protected override bool ispressed
        {
            get
            {
                return UnityEngine.Input.GetMouseButton(btn);
            }
        }
    }
}
