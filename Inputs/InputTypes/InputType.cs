﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Inputs.InputTypes
{
    public abstract class InputType
    {
        protected bool laststate = false;
        public bool keypressed { get; private set; }
        public bool keydown
        {
            get
            {
                return keypressed && (!laststate);
            }
        }
        public bool keyup
        {
            get
            {
                return (!keypressed) && laststate;
            }
        }

        protected abstract bool ispressed { get; }

        public virtual void Update()
        {
            laststate = keypressed;
            keypressed = ispressed;
        }

        public static InputType FromKeypressed(UnityEngine.Event e)
        {
            if (e.type == UnityEngine.EventType.mouseDown)
            {
                for (int i = 0; i < 6; i++)
                {
                    if (UnityEngine.Input.GetMouseButton(i))
                    {
                        return new Mouse(i);
                    }
                }
            }
            if (e.type == UnityEngine.EventType.keyDown)
            {
                return new Key(e.keyCode);
            };
            if (e.type == UnityEngine.EventType.scrollWheel)
            {
                float f = UnityEngine.Input.GetAxis("Mouse ScrollWheel");
                if (f>0)
                {
                    return new Scroll("Mouse ScrollWheel");
                }
                if (f<0)
                {
                    return new Scroll("Mouse ScrollWheel", true);
                }
            }
            return null;
        }
        public virtual SimpleJSON.JSONClass serialize()
        {
            SimpleJSON.JSONClass n = new SimpleJSON.JSONClass();
            n["Type"] = this.GetType().ToString();
            return n;
        }
        public abstract string displayinfo
        {
            get;
        }
    }
}
