﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SimpleJSON;

namespace Inputs.InputTypes
{
    class Scroll : InputType
    {
        private string axisname;
        bool inversed;
        public Scroll(string Name,bool inversed = false)
        {
            this.axisname = Name;
            this.inversed = inversed;
        }
        public Scroll(SimpleJSON.JSONNode j)
        {
            this.axisname = j["Axis"];
            this.inversed = j["Inversed"].AsBool;
        }
        protected override bool ispressed
        {
            get
            {
                float v = UnityEngine.Input.GetAxis(axisname);
                if (inversed)
                {
                    v = -v;
                }
                return v>=.95f;
            }
        }

        public override JSONClass serialize()
        {
            JSONClass n = base.serialize();
            n["Axis"] = axisname;
            n["Inversed"].AsBool = inversed;
            return n;
        }
        public float myvalue
        {
            get
            {
                float v = UnityEngine.Input.GetAxis(axisname);
                if (inversed)
                {
                    v = -v;
                }
                return v > 0 ? v : 0;
            }
        }
        public override string displayinfo
        {
            get
            {
                return "Scroll" + (inversed ? " inv" : "");
            }
        }

    }
}
