﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SimpleJSON;

namespace Inputs.InputTypes
{
    class Null : InputType
    {
        public Null() { }
        public Null(SimpleJSON.JSONNode n) { }

        public override string displayinfo
        {
            get
            {
                return null;
            }
        }

        protected override bool ispressed
        {
            get
            {
                return false;
            }
        }

        public override JSONClass serialize()
        {
            return base.serialize();
        }
    }
}
