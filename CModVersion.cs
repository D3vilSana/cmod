﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public class CModVersion
{
    public enum Mod : byte
    {
        Vanilla = 0,
        RC=1,
        NRC=2,
        Cyan=3,
        KnK=4,
        Doge=5,
        Death=6,
        CMod=7,
    }
    private PhotonPlayer p;
    public Version CMod { get; internal set; }
    public Mod name
    {
        get; internal set;
    }
    
    public CModVersion(PhotonPlayer p)
    {
        name = Mod.Vanilla;
        this.p = p;
        updatedProperties(p.customProperties);
    }

    public void receivedUnknownRPC(string rpcname)
    {
        Mod n;
        switch (rpcname)
        {
            case "DogeChat" :
                n = Mod.Doge;
                break;
            case "UserInfo":
                n= Mod.Death;
                break;
            case "NRCRPC":
                n = Mod.NRC;
                break;
            case "LoadObjects":
                n = Mod.Cyan;
                break;
            default:
                return;
        }
        if ((byte)n>(byte)name)
        {
            name = n;
        }
    }
    public void updatedProperties(ExitGames.Client.Photon.Hashtable newprops)
    {
        byte[] cver = newprops[PhotonPlayerProperty.g(CProps.CModVersion)] as byte[] ;
        if (cver!=null)
        {
            CMod = new Version(cver);
            name = Mod.CMod;
            return;
        }
        if (newprops.ContainsKey("RCteam"))
        {
            name = Mod.RC;
        }
        if (newprops.ContainsKey("NRC"))
        {
            name = Mod.NRC;
        }
        if (newprops.ContainsKey("KageNoKishi"))
        {
            name = Mod.KnK;
        }
    }

    public override string ToString()
    {
        string color = "";
        switch (name)
        {
            case Mod.Vanilla:
                return "[V]";
            case Mod.RC:
                color = "<color=#fc9ece>[RC]</color>";
                break;
            case Mod.NRC:
                color = "<color=#53802d>[NRC]</color>";
                break;
            case Mod.Cyan:
                color = "<color=#00F2FF>[Cyan]</color>";
                break;
            case Mod.KnK:
                color = "<color=#965EFF>[KnK]</color>";
                break;
            case Mod.Doge:
                color = "<color=#AFFA500>[Doge]</color>";
                break;
            case Mod.Death:
                color = "<color=#FF007B>[Death]</color>";
                break;
            case Mod.CMod:
                string s = "<color=#04FF00>[";
                switch (CMod.build)
                {
                    case Version.BuildType.Dev:
                        s += "<color=#0033FF>Dev</color>";
                        break;
                    case Version.BuildType.Beta:
                        s += "<color=#FF00AE>Beta</color>";
                        break;
                    case Version.BuildType.Relase:
                        s += "<color=#6E00FF>R-</color>";
                        break;
                    case Version.BuildType.Stable:
                        break;
                    case Version.BuildType.Other:
                        s += "<color=#FFFDA3>?</color>";
                        break;
                }
                s += "<color=" + UnityEngine.Color.Lerp(UnityEngine.Color.magenta, UnityEngine.Color.green, CMod.version / 255).tohex()+">"+CMod.version+"</color>.";
                s += "<color=" + UnityEngine.Color.Lerp(UnityEngine.Color.blue, UnityEngine.Color.green, CMod.subversion / 255).tohex() + ">" + CMod.subversion + "</color>]</color>";
                return s;
        }
        return color;
    }

    public class Version
    {
        public static void SetMyversion(BuildType t,byte ver,byte subver)
        {
            byte[] b = new byte[3] { (byte)t,ver,subver};
            ExitGames.Client.Photon.Hashtable h = new ExitGames.Client.Photon.Hashtable(1);
            h.Add(PhotonPlayerProperty.g(CProps.CModVersion), b);
            PhotonNetwork.player.SetCustomProperties(h);
        }
        public Version(byte[] Property)
        {
            if (Property.Length==3)
            {
                if (Enum.GetValues(typeof(BuildType)).Cast<byte>().Contains(Property[0]))
                {
                    build = (BuildType)Property[0];
                }
                else
                {
                    build = BuildType.Other;
                }
                version = Property[1];
                subversion = Property[2];
            }
        }
        public enum BuildType : byte
        {
            Dev=1,
            Beta=2,
            Relase=3,
            Stable=4,
            Other=255
        }
        public BuildType build {
            get;
            internal set;
        }
        public byte version
        {
            get;
            internal set;
        }
        public byte subversion
        {
            get;
            internal set;
        }
        public static bool operator >(Version a,Version b)
        {
            if (a.version!=b.version)
            {
                return a.version>b.version;
            }
            return a.subversion > b.subversion;
        }
        public static bool operator <(Version a,Version b)
        {
            if (a.version!=b.version)
            {
                return a.version < b.version;
            }
            return a.version < b.version;
        }
        public static bool operator ==(Version a,Version b)
        {
            return a.version == b.version && a.subversion == b.subversion;
        }
        public static bool operator !=(Version a,Version b)
        {
            return a.version != b.version || a.subversion != b.subversion;
        }

        public override bool Equals(object obj)
        {
            return (obj is Version) ? (Version)obj == this : false;
        }
        public override int GetHashCode()
        {
            object[] a = { build, version, subversion };
            return a.GetHashCode();
        }
    }
}
