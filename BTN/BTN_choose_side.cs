﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace BTN
{
    public abstract class BTN_choose_side : MonoBehaviour
    {
        public delegate void SideChoosenHandler();
        public static event SideChoosenHandler OnSideChoosen;
        public static bool needChooseSide = false;
        protected virtual void OnClick()
        {
            NGUITools.SetActive(GameObject.Find("UI_IN_GAME").GetComponent<UIReferArray>().panels[0], true);
            NGUITools.SetActive(GameObject.Find("UI_IN_GAME").GetComponent<UIReferArray>().panels[1], false);
            NGUITools.SetActive(GameObject.Find("UI_IN_GAME").GetComponent<UIReferArray>().panels[2], false);
            NGUITools.SetActive(GameObject.Find("UI_IN_GAME").GetComponent<UIReferArray>().panels[3], false);
            needChooseSide = false;
            FengGameManagerMKII.instance.mainCamera.setHUDposition();
            if (OnSideChoosen!=null)
            {
                OnSideChoosen();
            }
        }
    }
}
