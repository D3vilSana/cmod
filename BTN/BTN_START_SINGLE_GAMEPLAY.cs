using System;
using UnityEngine;
using System.Linq;

public class BTN_START_SINGLE_GAMEPLAY : MonoBehaviour
{
    public void OnStart()
    {
        GameObject.Find("PopupListMap").GetComponent<UIPopupList>().items = Levels.LevelManager.getLevels().Select(l => l.name).ToList();
    }

    private void OnClick()
    {
        string selection = GameObject.Find("PopupListMap").GetComponent<UIPopupList>().selection;
        string str2 = GameObject.Find("PopupListCharacter").GetComponent<UIPopupList>().selection;
        int num = !GameObject.Find("CheckboxHard").GetComponent<UICheckbox>().isChecked ? (!GameObject.Find("CheckboxAbnormal").GetComponent<UICheckbox>().isChecked ? 0 : 2) : 1;
        IN_GAME_MAIN_CAMERA.difficulty = num;
        IN_GAME_MAIN_CAMERA.singleCharacter = str2.ToUpper();
        if (IN_GAME_MAIN_CAMERA.cameraMode == CAMERA_TYPE.TPS)
        {
            Screen.lockCursor = true;
        }
        Screen.showCursor = false;
        if (selection == "trainning_0")
        {
            IN_GAME_MAIN_CAMERA.difficulty = -1;
        }
        string str4 = "";
        if (IN_GAME_MAIN_CAMERA.dayLight == DayLight.Day)
        {
            str4 = "day";
        }
        if (IN_GAME_MAIN_CAMERA.dayLight == DayLight.Dawn)
        {
            str4 = "dawn";
        }
        if (IN_GAME_MAIN_CAMERA.dayLight == DayLight.Night)
        {
            str4 = "night";
        }
        FengGameManagerMKII.level = Levels.LevelManager.getbyname(selection);

        PhotonNetwork.offlineMode = true;

        RoomOptions ro = new RoomOptions()
        {
            isOpen = false,
            isVisible = false,
            maxPlayers = 0,
        };
        TypedLobby tl = new TypedLobby()
        {
            Name = "CMod",
        };
        PhotonNetwork.CreateRoom(string.Join("`", new string[] { "CMod-solo", selection, IN_GAME_MAIN_CAMERA.difficulty.ToString(), "99999999", str4 }));
        //PhotonNetwork.CreateRoom(string.Concat(new object[] { "CMod-solo", "`", selection, "`", IN_GAME_MAIN_CAMERA.difficulty, "`", 999999999, "`", str4, "`", "", "`", UnityEngine.Random.Range(0, 0xc350) }), true, true, 0);

        //Application.LoadLevel(LevelInfo.getInfo(selection).mapName);
    }
}

