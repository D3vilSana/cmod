using System;
using UnityEngine;

public class BTN_PAUSE_MENU_CONTINUE : MonoBehaviour
{
    private void OnClick()
    {
        GameObject obj2 = GameObject.Find("UI_IN_GAME");
        NGUITools.SetActive(obj2.GetComponent<UIReferArray>().panels[0], true);
        NGUITools.SetActive(obj2.GetComponent<UIReferArray>().panels[1], false);
        NGUITools.SetActive(obj2.GetComponent<UIReferArray>().panels[2], false);
        NGUITools.SetActive(obj2.GetComponent<UIReferArray>().panels[3], false);
        if (! FengGameManagerMKII.instance.mainCamera.enabled)
        {
            Screen.showCursor = true;
            Screen.lockCursor = true;
            Managers.InputManager.ignoreKeys = false;
            GameObject.Find("MainCamera").GetComponent<SpectatorMovement>().disable = false;
            GameObject.Find("MainCamera").GetComponent<MouseLook>().disable = false;
        }
        else
        {
            IN_GAME_MAIN_CAMERA.isPausing = false;
            if (IN_GAME_MAIN_CAMERA.cameraMode == CAMERA_TYPE.TPS)
            {
                Screen.showCursor = false;
                Screen.lockCursor = true;
            }
            else
            {
                Screen.showCursor = false;
                Screen.lockCursor = false;
            }
            Managers.InputManager.ignoreKeys = false;
        }
    }
}

