using ExitGames.Client.Photon;
using System;
using UnityEngine;

public class BTN_choose_human : BTN.BTN_choose_side
{
    protected override void OnClick()
    {
        string selection = GameObject.Find("PopupListCharacterHUMAN").GetComponent<UIPopupList>().selection;
        base.OnClick();
        Hashtable h = new Hashtable();
        h.Add(PhotonPlayerProperty.character, selection);
        PhotonNetwork.player.SetCustomProperties(h);

        if (!PhotonNetwork.isMasterClient && FengGameManagerMKII.instance.roundTime > 60f)
        {
            if (!FengGameManagerMKII.instance.isPlayerAllDead())
            {
                FengGameManagerMKII.instance.NOTSpawnPlayer(selection);
            }
            else
            {
                FengGameManagerMKII.instance.NOTSpawnPlayer(selection);
                FengGameManagerMKII.instance.photonView.RPC("restartGameByClient", PhotonTargets.MasterClient);
            }
        }
        else if (((FengGameManagerMKII.level.type == GAMEMODE.BOSS_FIGHT_CT) || (FengGameManagerMKII.level.type == GAMEMODE.TROST)) || (FengGameManagerMKII.level.type == GAMEMODE.PVP_CAPTURE))
        {
            if (FengGameManagerMKII.instance.isPlayerAllDead())
            {
                FengGameManagerMKII.instance.NOTSpawnPlayer(selection);
                FengGameManagerMKII.instance.photonView.RPC("restartGameByClient", PhotonTargets.MasterClient);
            }
            else
            {
                FengGameManagerMKII.level.Spawn(selection, true);
             
            }
        }
        else
        {
            if (FengGameManagerMKII.instance.myhero!=null)
            {
                FengGameManagerMKII.instance.chat.addLINE("WARNING : Trying to respawn an exisiting player");
            }
            else
            {
                FengGameManagerMKII.level.Spawn(selection, true);
            }
        }
        IN_GAME_MAIN_CAMERA.usingTitan = false;
    }
}

