using ExitGames.Client.Photon;
using System;
using UnityEngine;

public class BTN_choose_titan : BTN.BTN_choose_side
{
    protected override void OnClick()
    {
        if (FengGameManagerMKII.level.type == GAMEMODE.PVP_AHSS)
        {
            base.OnClick();
            string id = "AHSS";
            if (!PhotonNetwork.isMasterClient && (FengGameManagerMKII.instance.roundTime > 60f))
            {
                FengGameManagerMKII.instance.NOTSpawnPlayer(id);
                FengGameManagerMKII.instance.photonView.RPC("restartGameByClient", PhotonTargets.MasterClient);
            }
            else
            {
                FengGameManagerMKII.instance.SpawnPlayer(id, "playerRespawn2");//TODO !!!!!
            }
            IN_GAME_MAIN_CAMERA.usingTitan = false;
            Hashtable h = new Hashtable();
            h.Add(PhotonPlayerProperty.character, id);
            PhotonNetwork.player.SetCustomProperties(h);
        }
        else
        {
            string selection = GameObject.Find("PopupListCharacterTITAN").GetComponent<UIPopupList>().selection;
            base.OnClick();
            NGUITools.SetActive(base.transform.parent.gameObject, false);
            if ((!PhotonNetwork.isMasterClient && (FengGameManagerMKII.instance.roundTime > 60f)) || FengGameManagerMKII.instance.justSuicide)
            {
                FengGameManagerMKII.instance.justSuicide = false;
                FengGameManagerMKII.instance.NOTSpawnNonAITitan(selection);
            }
            else
            {
                FengGameManagerMKII.instance.SpawnNonAITitan(selection, "titanRespawn");
            }
            IN_GAME_MAIN_CAMERA.usingTitan = true;
        }
    }

    private void Start()
    {
        if (!FengGameManagerMKII.level.teamTitan)
        {
            base.gameObject.GetComponent<UIButton>().isEnabled = false;
        }
    }
}

