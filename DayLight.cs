using System;

public enum DayLight :int
{
    Day = 0,
    Dawn = 1,
    Night = 2
}

