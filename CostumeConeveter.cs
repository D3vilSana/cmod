using ExitGames.Client.Photon;
using System;
using UnityEngine;

public class CostumeConeveter
{
    private static int DivisionToInt(DIVISION id)
    {
        bool flag = id == DIVISION.TheGarrison;
        int result;
        if (flag)
        {
            result = 0;
        }
        else
        {
            bool flag2 = id == DIVISION.TheMilitaryPolice;
            if (flag2)
            {
                result = 1;
            }
            else
            {
                bool flag3 = id != DIVISION.TheSurveryCorps && id == DIVISION.TraineesSquad;
                if (flag3)
                {
                    result = 3;
                }
                else
                {
                    result = 2;
                }
            }
        }
        return result;
    }

    public static void HeroCostumeToLocalData(HeroCostume costume, string slot)
    {
        slot = slot.ToUpper();
        PlayerPrefs.SetInt(slot + PhotonPlayerProperty.sex, CostumeConeveter.SexToInt(costume.sex));
        PlayerPrefs.SetInt(slot + PhotonPlayerProperty.costumeId, costume.costumeId);
        PlayerPrefs.SetInt(slot + PhotonPlayerProperty.heroCostumeId, costume.id);
        PlayerPrefs.SetInt(slot + PhotonPlayerProperty.cape, (!costume.cape) ? 0 : 1);
        PlayerPrefs.SetInt(slot + PhotonPlayerProperty.hairInfo, costume.hairInfo.id);
        PlayerPrefs.SetInt(slot + PhotonPlayerProperty.eye_texture_id, costume.eye_texture_id);
        PlayerPrefs.SetInt(slot + PhotonPlayerProperty.beard_texture_id, costume.beard_texture_id);
        PlayerPrefs.SetInt(slot + PhotonPlayerProperty.glass_texture_id, costume.glass_texture_id);
        PlayerPrefs.SetInt(slot + PhotonPlayerProperty.skin_color, costume.skin_color);
        PlayerPrefs.SetFloat(slot + PhotonPlayerProperty.hair_color1, costume.hair_color.r);
        PlayerPrefs.SetFloat(slot + PhotonPlayerProperty.hair_color2, costume.hair_color.g);
        PlayerPrefs.SetFloat(slot + PhotonPlayerProperty.hair_color3, costume.hair_color.b);
        PlayerPrefs.SetInt(slot + PhotonPlayerProperty.division, CostumeConeveter.DivisionToInt(costume.division));
        PlayerPrefs.SetInt(slot + PhotonPlayerProperty.statSPD, costume.stat.SPD);
        PlayerPrefs.SetInt(slot + PhotonPlayerProperty.statGAS, costume.stat.GAS);
        PlayerPrefs.SetInt(slot + PhotonPlayerProperty.statBLA, costume.stat.BLA);
        PlayerPrefs.SetInt(slot + PhotonPlayerProperty.statACL, costume.stat.ACL);
        PlayerPrefs.SetString(slot + PhotonPlayerProperty.statSKILL, costume.stat.skillId);
    }

    public static void HeroCostumeToPhotonData(HeroCostume costume, PhotonPlayer player)
    {
        player.SetCustomProperties(new Hashtable
        {
            {
                PhotonPlayerProperty.sex,
                CostumeConeveter.SexToInt(costume.sex)
            }
        });
        player.SetCustomProperties(new Hashtable
        {
            {
                PhotonPlayerProperty.costumeId,
                costume.costumeId
            }
        });
        player.SetCustomProperties(new Hashtable
        {
            {
                PhotonPlayerProperty.heroCostumeId,
                costume.id
            }
        });
        player.SetCustomProperties(new Hashtable
        {
            {
                PhotonPlayerProperty.cape,
                costume.cape
            }
        });
        player.SetCustomProperties(new Hashtable
        {
            {
                PhotonPlayerProperty.hairInfo,
                costume.hairInfo.id
            }
        });
        player.SetCustomProperties(new Hashtable
        {
            {
                PhotonPlayerProperty.eye_texture_id,
                costume.eye_texture_id
            }
        });
        player.SetCustomProperties(new Hashtable
        {
            {
                PhotonPlayerProperty.beard_texture_id,
                costume.beard_texture_id
            }
        });
        player.SetCustomProperties(new Hashtable
        {
            {
                PhotonPlayerProperty.glass_texture_id,
                costume.glass_texture_id
            }
        });
        player.SetCustomProperties(new Hashtable
        {
            {
                PhotonPlayerProperty.skin_color,
                costume.skin_color
            }
        });
        player.SetCustomProperties(new Hashtable
        {
            {
                PhotonPlayerProperty.hair_color1,
                costume.hair_color.r
            }
        });
        player.SetCustomProperties(new Hashtable
        {
            {
                PhotonPlayerProperty.hair_color2,
                costume.hair_color.g
            }
        });
        player.SetCustomProperties(new Hashtable
        {
            {
                PhotonPlayerProperty.hair_color3,
                costume.hair_color.b
            }
        });
        player.SetCustomProperties(new Hashtable
        {
            {
                PhotonPlayerProperty.division,
                CostumeConeveter.DivisionToInt(costume.division)
            }
        });
        player.SetCustomProperties(new Hashtable
        {
            {
                PhotonPlayerProperty.statSPD,
                costume.stat.SPD
            }
        });
        player.SetCustomProperties(new Hashtable
        {
            {
                PhotonPlayerProperty.statGAS,
                costume.stat.GAS
            }
        });
        player.SetCustomProperties(new Hashtable
        {
            {
                PhotonPlayerProperty.statBLA,
                costume.stat.BLA
            }
        });
        player.SetCustomProperties(new Hashtable
        {
            {
                PhotonPlayerProperty.statACL,
                costume.stat.ACL
            }
        });
        player.SetCustomProperties(new Hashtable
        {
            {
                PhotonPlayerProperty.statSKILL,
                costume.stat.skillId
            }
        });
    }

    public static void HeroCostumeToPhotonData2(HeroCostume costume, PhotonPlayer player)
    {
        player.SetCustomProperties(new Hashtable
        {
            {
                PhotonPlayerProperty.sex,
                CostumeConeveter.SexToInt(costume.sex)
            }
        });
        Hashtable hashtable = new Hashtable();
        int num = costume.costumeId;
        bool flag = num == 26;
        if (flag)
        {
            num = 25;
        }
        hashtable.Add(PhotonPlayerProperty.costumeId, num);
        player.SetCustomProperties(hashtable);
        player.SetCustomProperties(new Hashtable
        {
            {
                PhotonPlayerProperty.heroCostumeId,
                costume.id
            }
        });
        player.SetCustomProperties(new Hashtable
        {
            {
                PhotonPlayerProperty.cape,
                costume.cape
            }
        });
        player.SetCustomProperties(new Hashtable
        {
            {
                PhotonPlayerProperty.hairInfo,
                costume.hairInfo.id
            }
        });
        player.SetCustomProperties(new Hashtable
        {
            {
                PhotonPlayerProperty.eye_texture_id,
                costume.eye_texture_id
            }
        });
        player.SetCustomProperties(new Hashtable
        {
            {
                PhotonPlayerProperty.beard_texture_id,
                costume.beard_texture_id
            }
        });
        player.SetCustomProperties(new Hashtable
        {
            {
                PhotonPlayerProperty.glass_texture_id,
                costume.glass_texture_id
            }
        });
        player.SetCustomProperties(new Hashtable
        {
            {
                PhotonPlayerProperty.skin_color,
                costume.skin_color
            }
        });
        player.SetCustomProperties(new Hashtable
        {
            {
                PhotonPlayerProperty.hair_color1,
                costume.hair_color.r
            }
        });
        player.SetCustomProperties(new Hashtable
        {
            {
                PhotonPlayerProperty.hair_color2,
                costume.hair_color.g
            }
        });
        player.SetCustomProperties(new Hashtable
        {
            {
                PhotonPlayerProperty.hair_color3,
                costume.hair_color.b
            }
        });
        player.SetCustomProperties(new Hashtable
        {
            {
                PhotonPlayerProperty.division,
                CostumeConeveter.DivisionToInt(costume.division)
            }
        });
        player.SetCustomProperties(new Hashtable
        {
            {
                PhotonPlayerProperty.statSPD,
                costume.stat.SPD
            }
        });
        player.SetCustomProperties(new Hashtable
        {
            {
                PhotonPlayerProperty.statGAS,
                costume.stat.GAS
            }
        });
        player.SetCustomProperties(new Hashtable
        {
            {
                PhotonPlayerProperty.statBLA,
                costume.stat.BLA
            }
        });
        player.SetCustomProperties(new Hashtable
        {
            {
                PhotonPlayerProperty.statACL,
                costume.stat.ACL
            }
        });
        player.SetCustomProperties(new Hashtable
        {
            {
                PhotonPlayerProperty.statSKILL,
                costume.stat.skillId
            }
        });
    }

    private static DIVISION IntToDivision(int id)
    {
        bool flag = id == 0;
        DIVISION result;
        if (flag)
        {
            result = DIVISION.TheGarrison;
        }
        else
        {
            bool flag2 = id == 1;
            if (flag2)
            {
                result = DIVISION.TheMilitaryPolice;
            }
            else
            {
                bool flag3 = id != 2 && id == 3;
                if (flag3)
                {
                    result = DIVISION.TraineesSquad;
                }
                else
                {
                    result = DIVISION.TheSurveryCorps;
                }
            }
        }
        return result;
    }

    private static SEX IntToSex(int id)
    {
        bool flag = id == 0;
        SEX result;
        if (flag)
        {
            result = SEX.FEMALE;
        }
        else
        {
            bool flag2 = id == 1;
            if (flag2)
            {
                result = SEX.MALE;
            }
            else
            {
                result = SEX.MALE;
            }
        }
        return result;
    }

    private static UNIFORM_TYPE IntToUniformType(int id)
    {
        bool flag = id == 0;
        UNIFORM_TYPE result;
        if (flag)
        {
            result = UNIFORM_TYPE.CasualA;
        }
        else
        {
            bool flag2 = id == 1;
            if (flag2)
            {
                result = UNIFORM_TYPE.CasualB;
            }
            else
            {
                bool flag3 = id != 2;
                if (flag3)
                {
                    bool flag4 = id == 3;
                    if (flag4)
                    {
                        result = UNIFORM_TYPE.UniformB;
                        return result;
                    }
                    bool flag5 = id == 4;
                    if (flag5)
                    {
                        result = UNIFORM_TYPE.CasualAHSS;
                        return result;
                    }
                }
                result = UNIFORM_TYPE.UniformA;
            }
        }
        return result;
    }

    public static HeroCostume LocalDataToHeroCostume(string slot)
    {
        slot = slot.ToUpper();
        bool flag = !PlayerPrefs.HasKey(slot + PhotonPlayerProperty.sex);
        HeroCostume result;
        if (flag)
        {
            result = HeroCostume.costume[0];
        }
        else
        {
            HeroCostume heroCostume = new HeroCostume();
            heroCostume = new HeroCostume
            {
                sex = CostumeConeveter.IntToSex(PlayerPrefs.GetInt(slot + PhotonPlayerProperty.sex)),
                id = PlayerPrefs.GetInt(slot + PhotonPlayerProperty.heroCostumeId),
                costumeId = PlayerPrefs.GetInt(slot + PhotonPlayerProperty.costumeId),
                cape = PlayerPrefs.GetInt(slot + PhotonPlayerProperty.cape) == 1,
                hairInfo = (CostumeConeveter.IntToSex(PlayerPrefs.GetInt(slot + PhotonPlayerProperty.sex)) == SEX.FEMALE) ? CostumeHair.hairsF[PlayerPrefs.GetInt(slot + PhotonPlayerProperty.hairInfo)] : CostumeHair.hairsM[PlayerPrefs.GetInt(slot + PhotonPlayerProperty.hairInfo)],
                eye_texture_id = PlayerPrefs.GetInt(slot + PhotonPlayerProperty.eye_texture_id),
                beard_texture_id = PlayerPrefs.GetInt(slot + PhotonPlayerProperty.beard_texture_id),
                glass_texture_id = PlayerPrefs.GetInt(slot + PhotonPlayerProperty.glass_texture_id),
                skin_color = PlayerPrefs.GetInt(slot + PhotonPlayerProperty.skin_color),
                hair_color = new Color(PlayerPrefs.GetFloat(slot + PhotonPlayerProperty.hair_color1), PlayerPrefs.GetFloat(slot + PhotonPlayerProperty.hair_color2), PlayerPrefs.GetFloat(slot + PhotonPlayerProperty.hair_color3)),
                division = CostumeConeveter.IntToDivision(PlayerPrefs.GetInt(slot + PhotonPlayerProperty.division)),
                stat = new HeroStat()
            };
            heroCostume.stat.SPD = PlayerPrefs.GetInt(slot + PhotonPlayerProperty.statSPD);
            heroCostume.stat.GAS = PlayerPrefs.GetInt(slot + PhotonPlayerProperty.statGAS);
            heroCostume.stat.BLA = PlayerPrefs.GetInt(slot + PhotonPlayerProperty.statBLA);
            heroCostume.stat.ACL = PlayerPrefs.GetInt(slot + PhotonPlayerProperty.statACL);
            heroCostume.stat.skillId = PlayerPrefs.GetString(slot + PhotonPlayerProperty.statSKILL);
            heroCostume.setBodyByCostumeId(-1);
            heroCostume.setMesh2();
            heroCostume.setTexture();
            result = heroCostume;
        }
        return result;
    }

    public static HeroCostume PhotonDataToHeroCostume(PhotonPlayer player)
    {
        HeroCostume heroCostume = new HeroCostume();
        heroCostume = new HeroCostume
        {
            sex = CostumeConeveter.IntToSex((int)player.customProperties[PhotonPlayerProperty.sex]),
            costumeId = (int)player.customProperties[PhotonPlayerProperty.costumeId],
            id = (int)player.customProperties[PhotonPlayerProperty.heroCostumeId],
            cape = (bool)player.customProperties[PhotonPlayerProperty.cape],
            hairInfo = (heroCostume.sex != SEX.MALE) ? CostumeHair.hairsF[(int)player.customProperties[PhotonPlayerProperty.hairInfo]] : CostumeHair.hairsM[(int)player.customProperties[PhotonPlayerProperty.hairInfo]],
            eye_texture_id = (int)player.customProperties[PhotonPlayerProperty.eye_texture_id],
            beard_texture_id = (int)player.customProperties[PhotonPlayerProperty.beard_texture_id],
            glass_texture_id = (int)player.customProperties[PhotonPlayerProperty.glass_texture_id],
            skin_color = (int)player.customProperties[PhotonPlayerProperty.skin_color],
            hair_color = new Color((float)player.customProperties[PhotonPlayerProperty.hair_color1], (float)player.customProperties[PhotonPlayerProperty.hair_color2], (float)player.customProperties[PhotonPlayerProperty.hair_color3]),
            division = CostumeConeveter.IntToDivision((int)player.customProperties[PhotonPlayerProperty.division]),
            stat = new HeroStat()
        };
        heroCostume.stat.SPD = (int)player.customProperties[PhotonPlayerProperty.statSPD];
        heroCostume.stat.GAS = (int)player.customProperties[PhotonPlayerProperty.statGAS];
        heroCostume.stat.BLA = (int)player.customProperties[PhotonPlayerProperty.statBLA];
        heroCostume.stat.ACL = (int)player.customProperties[PhotonPlayerProperty.statACL];
        heroCostume.stat.skillId = (string)player.customProperties[PhotonPlayerProperty.statSKILL];
        heroCostume.setBodyByCostumeId(-1);
        heroCostume.setMesh2();
        heroCostume.setTexture();
        return heroCostume;
    }

    public static HeroCostume PhotonDataToHeroCostume2(PhotonPlayer player)
    {
        HeroCostume heroCostume = new HeroCostume();
        SEX sEX = CostumeConeveter.IntToSex((int)player.customProperties[PhotonPlayerProperty.sex]);
        heroCostume = new HeroCostume
        {
            sex = sEX,
            costumeId = (int)player.customProperties[PhotonPlayerProperty.costumeId],
            id = (int)player.customProperties[PhotonPlayerProperty.heroCostumeId],
            cape = (bool)player.customProperties[PhotonPlayerProperty.cape],
            hairInfo = (sEX != SEX.MALE) ? CostumeHair.hairsF[(int)player.customProperties[PhotonPlayerProperty.hairInfo]] : CostumeHair.hairsM[(int)player.customProperties[PhotonPlayerProperty.hairInfo]],
            eye_texture_id = (int)player.customProperties[PhotonPlayerProperty.eye_texture_id],
            beard_texture_id = (int)player.customProperties[PhotonPlayerProperty.beard_texture_id],
            glass_texture_id = (int)player.customProperties[PhotonPlayerProperty.glass_texture_id],
            skin_color = (int)player.customProperties[PhotonPlayerProperty.skin_color],
            hair_color = new Color((float)player.customProperties[PhotonPlayerProperty.hair_color1], (float)player.customProperties[PhotonPlayerProperty.hair_color2], (float)player.customProperties[PhotonPlayerProperty.hair_color3]),
            division = CostumeConeveter.IntToDivision((int)player.customProperties[PhotonPlayerProperty.division]),
            stat = new HeroStat()
        };
        heroCostume.stat.SPD = (int)player.customProperties[PhotonPlayerProperty.statSPD];
        heroCostume.stat.GAS = (int)player.customProperties[PhotonPlayerProperty.statGAS];
        heroCostume.stat.BLA = (int)player.customProperties[PhotonPlayerProperty.statBLA];
        heroCostume.stat.ACL = (int)player.customProperties[PhotonPlayerProperty.statACL];
        heroCostume.stat.skillId = (string)player.customProperties[PhotonPlayerProperty.statSKILL];
        bool flag = heroCostume.costumeId == 25 && heroCostume.sex == SEX.FEMALE;
        if (flag)
        {
            heroCostume.costumeId = 26;
        }
        heroCostume.setBodyByCostumeId(-1);
        heroCostume.setMesh2();
        heroCostume.setTexture();
        return heroCostume;
    }

    private static int SexToInt(SEX id)
    {
        bool flag = id == SEX.FEMALE;
        int result;
        if (flag)
        {
            result = 0;
        }
        else
        {
            bool flag2 = id == SEX.MALE;
            if (flag2)
            {
                result = 1;
            }
            else
            {
                result = 1;
            }
        }
        return result;
    }

    private static int UniformTypeToInt(UNIFORM_TYPE id)
    {
        bool flag = id == UNIFORM_TYPE.CasualA;
        int result;
        if (flag)
        {
            result = 0;
        }
        else
        {
            bool flag2 = id == UNIFORM_TYPE.CasualB;
            if (flag2)
            {
                result = 1;
            }
            else
            {
                bool flag3 = id > UNIFORM_TYPE.UniformA;
                if (flag3)
                {
                    bool flag4 = id == UNIFORM_TYPE.UniformB;
                    if (flag4)
                    {
                        result = 3;
                        return result;
                    }
                    bool flag5 = id == UNIFORM_TYPE.CasualAHSS;
                    if (flag5)
                    {
                        result = 4;
                        return result;
                    }
                }
                result = 2;
            }
        }
        return result;
    }
}
