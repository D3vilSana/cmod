using System;
using System.Collections;
using UnityEngine;

public class UIMainReferences : MonoBehaviour
{
    private static bool isGAMEFirstLaunch = true;
    public GameObject panelCredits;
    public GameObject PanelDisconnect;
    public GameObject panelMain;
    public GameObject PanelMultiJoinPrivate;
    public GameObject PanelMultiPWD;
    public GameObject panelMultiROOM;
    public GameObject panelMultiSet;
    public GameObject panelMultiStart;
    public GameObject PanelMultiWait;
    public GameObject panelOption;
    public GameObject panelSingleSet;
    public GameObject PanelSnapShot;
    public static string version = "01042015";
    string text = "8/12/2015";

    private void Start()
    {
        NGUITools.SetActive(this.panelMain, true);
        GameObject.Find("VERSION").GetComponent<UILabel>().text = version;
        if (isGAMEFirstLaunch)
        {
            isGAMEFirstLaunch = false;
            GameObject target = (GameObject) UnityEngine.Object.Instantiate(Resources.Load("InputManagerController"));
            target.name = "InputManagerController";
            UnityEngine.Object.DontDestroyOnLoad(target);
            FengGameManagerMKII.s = "verified343,hair,character_eye,glass,character_face,character_head,character_hand,character_body,character_arm,character_leg,character_chest,character_cape,character_brand,character_3dmg,r,character_blade_l,character_3dmg_gas_r,character_blade_r,3dmg_smoke,HORSE,hair,body_001,Cube,Plane_031,mikasa_asset,character_cap_,character_gun".Split(new char[]
{
                ','
});
            base.StartCoroutine(this.request(text, version));
        }
    }
    public IEnumerator request(string versionShow, string versionForm)
    {
        string text = Application.dataPath + "/RCAssets.unity3d";
        bool flag = !Application.isWebPlayer;
        if (flag)
        {
            text = "File://" + text;
        }
        while (true)
        {
            if (Caching.ready)
            {
                break;
            }
            yield return null;
        }
        int num = 1;
            
        WWW wWW = WWW.LoadFromCacheOrDownload(text, num);
        yield return wWW;
        
        bool flag2 = wWW.error != null;
        if (flag2)
        {
            throw new Exception("WWW download had an error:" + wWW.error);
        }
        FengGameManagerMKII.RCassets = wWW.assetBundle;
        //UnityEngine.Object.Instantiate(FengGameManagerMKII.RCassets.Load("backgroundCamera"));
        //FengGameManagerMKII.isAssetLoaded = true;
        //FengGameManagerMKII.instance.setBackground();
        yield break;
    }
}

