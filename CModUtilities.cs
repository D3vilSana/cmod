﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using UnityEngine;

static class CModUtilities
{

    public static IEnumerable<Type> GetTypesof<T>()
    {
        return from a in AppDomain.CurrentDomain.GetAssemblies()
               from t in a.GetTypes()
               where t.IsSubclassOf(typeof(T))
               where !t.IsAbstract
               where t.GetConstructor(Type.EmptyTypes) != null
               select t;
    }
    public static IEnumerable<Type> GetTypesofInterface<T>()
    {
        return from a in AppDomain.CurrentDomain.GetAssemblies()
               from t in a.GetTypes()
               where !t.IsAbstract
               where t.GetInterfaces().Contains(typeof(T))
               where t.GetConstructor(Type.EmptyTypes) != null
               select t;
    }

    private static Regex colorparse = new Regex(@"^#?(?<r>[0-9a-f]{2})(?<g>[0-9a-f]{2})(?<b>[0-9a-f]{2})", RegexOptions.IgnoreCase);

    public static UnityEngine.Color colorfromhex(string s)
    {
        MatchCollection mc = colorparse.Matches(s);
        if (mc.Count < 0)
        {
            return default(UnityEngine.Color);
        }
        else
        {
            int r = int.Parse(mc[0].Groups["r"].Value, System.Globalization.NumberStyles.HexNumber);
            int g = int.Parse(mc[0].Groups["g"].Value, System.Globalization.NumberStyles.HexNumber);
            int b = int.Parse(mc[0].Groups["b"].Value, System.Globalization.NumberStyles.HexNumber);
            return new UnityEngine.Color(r/255, g/255, b/255);
        }
    }
    public static bool validcolor(string s)
    {
        return colorparse.IsMatch(s);
    }

    public static string tohex(this UnityEngine.Color c)
    {
        return "#" + ((int)(c.r*255)).ToString("X2") + ((int)(c.g*255)).ToString("X2") + ((int)(c.b*255)).ToString("X2");
    }

    public static UnityEngine.Rect fromCoord(this UnityEngine.Vector2 v,UnityEngine.Vector2 start)
    {
        return new UnityEngine.Rect(start.x, start.y, start.x + v.x, start.y + v.y);
    }

    public static bool isHuman(this PhotonPlayer p)
    {
        return ((int)PhotonNetwork.player.customProperties[PhotonPlayerProperty.isTitan]) == 1;
    }
    public static string name(this DayLight d)
    {
        switch (d)
        {
            case DayLight.Day:
                return "day";
            case DayLight.Dawn:
                return "dawn";
            case DayLight.Night:
                return "night";
            default:
                return "";
        }
    }
    public static T getRandom<T>(this IEnumerable<T> l)
    {
        return l.ElementAt(UnityEngine.Random.Range(0, l.Count()));
    }
    
   
}
