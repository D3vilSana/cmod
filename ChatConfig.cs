﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

class ChatConfig : ClassicConfig
{
    public enum P : int { Showlinks,ShowMsgIds}
    public override Type enum_config
    {
        get
        {
            return typeof(P);
        }
    }

    public override string name
    {
        get
        {
            return "chat";
        }
    }

    public override object defaultval(int val)
    {
        switch ((P)val)
        {
            case P.Showlinks:
                return true;
            case P.ShowMsgIds:
                return false;
            default:
                return null;
        }
    }

    public override void init()
    {
       
    }

    public override void Onvalchanged(int val)
    {
        switch ((P)val)
        {
            case P.Showlinks:
                ChatHandler.CText.dolabellink = bools[val];
                break;
            case P.ShowMsgIds:
                ChatHandler.showId = bools[val];
                break;
            default:
                break;
        }
    }

    protected override Valtypes typeofkey(int key)
    {
        switch ((P)key)
        {
            case P.Showlinks:
                return Valtypes.Bool;
            case P.ShowMsgIds:
                return Valtypes.Bool;
            default:
                return default(Valtypes);
        }
    }
}

