﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Parameters
{
    public class ChatMessageIDs : Parameter<List<ChatHandler.Chatcontent>>
    {
        private class ChatMessageIDList : Parameter
        {
            public override bool canbeme(string parameter)
            {
                string[] s = parameter.Split(',');
                IEnumerable<string> ids = ChatHandler.messages.Select(c => c.Id.ToString());
                for (int i = 0; i < s.Length - 1; i++)
                {
                    if (!ids.Contains(s[i]))
                    {
                        return false;
                    }
                }
                return ids.Any(t => t.StartsWith(s[s.Length - 1]));
            }

            public override List<string> getpossibleparameters(string start)
            {
                string[] s = start.Split(',');
                List<string> ids = ChatHandler.messages.Select(c => c.Id.ToString()).ToList();
                for (int i = 0; i < s.Length - 1; i++)
                {
                    ids.Remove(s[i]);
                }

                if (!start.EndsWith(","))
                {
                    List<string> n = ids.Where(u => u.StartsWith(s[s.Length - 1])).ToList();
                    if (ids.Contains(s[s.Length - 1]))
                    {

                        if (ids.Count != 0)
                        {
                            List<string> ls = ids.Select(g => "," + g).ToList();
                            n.AddRange(ls);
                        }
                    }
                    return n;
                }
                else
                {
                    ids.Remove(s[s.Length - 1]);
                }
                return ids;
            }

            public override bool isme(string param)
            {
                if (param.EndsWith(","))
                {
                    return false;
                }
                string[] s = param.Split(',');
                List<string> ids = ChatHandler.messages.Select(c => c.Id.ToString()).ToList();
                return s.All(e => ids.Contains(e));
            }

            public override string sringshow()
            {
                return "[Message Id List]";
            }
        }
        private class ChatMessageIDRange : Parameter
        {
            public override bool canbeme(string parameter)
            {
                if (parameter == "")
                {
                    return true;
                }
                else if (IsDigitsOnly(parameter))
                {
                    return ChatHandler.messages.Any(t => t.Id.ToString().StartsWith(parameter));
                }
                else if (parameter.EndsWith("-"))
                {
                    parameter = parameter.Remove(parameter.Length - 1);
                    return ChatHandler.messages.Any(t => t.Id.ToString().StartsWith(parameter));
                }
                else if (parameter.EndsWith("->"))
                {
                    parameter = parameter.Remove(parameter.Length - 2);
                    return ChatHandler.messages.Any(t => t.Id.ToString().StartsWith(parameter));
                }
                else if (parameter.Contains("->"))
                {
                    string[] s = parameter.Split(new string[] { "->" }, StringSplitOptions.None);
                    if (!ChatHandler.messages.Any(t => t.Id.ToString() == s[0]))
                    {
                        return false;
                    }
                    return ChatHandler.messages.Any(t => t.Id.ToString().StartsWith(s[1]));
                }
                else
                {
                    return false;
                }
            }

            public override List<string> getpossibleparameters(string start)
            {
                if (start == "")
                {
                    return ChatHandler.messages.Select(t => t.Id.ToString()).ToList();
                }
                else if (!start.Contains("->") && IsDigitsOnly(start))
                {
                    List<string> l = ChatHandler.messages.Select(t => t.Id.ToString()).Where(t => t.StartsWith(start)).ToList();
                    if (l.Contains(start))
                    {
                        l.Add("->");
                    }
                    return l;
                }
                else if (start.EndsWith("-"))
                {
                    List<string> l = new List<string>();
                    l.Add(">");
                    return l;
                }
                else if (start.EndsWith("->"))
                {
                    int i = int.Parse(start.Substring(0, start.Length - 2));
                    return ChatHandler.messages.Where(t => t.Id > i).Select(t => t.Id.ToString()).ToList();
                }
                else if (start.Contains("->"))
                {
                    string s = start.Split(new string[] { "->" }, StringSplitOptions.None)[1];
                    int i = int.Parse(s);
                    return ChatHandler.messages.Where(t => t.ToString().StartsWith(s) && t.Id > i).Select(t => t.Id.ToString()).ToList();
                }
                else
                {
                    return new List<string>();
                }
            }

            public override bool isme(string param)
            {
                if (param.Contains("->"))
                {
                    string[] s = param.Split(new string[] { "->" }, StringSplitOptions.None);
                    return (ChatHandler.messages.Any(f => f.Id.ToString() == s[0]) || ChatHandler.messages.Any(f => f.Id.ToString() == s[1]));
                }
                else
                {
                    return false;
                }
            }

            public override string sringshow()
            {
                return "[Message Id range]";
            }
            private bool IsDigitsOnly(string str)
            {
                foreach (char c in str)
                {
                    if (c < '0' || c > '9')
                        return false;
                }

                return true;
            }
        }

        private ChatMessageIDList listparam;
        private ChatMessageIDRange rangeparam;

        public override List<ChatHandler.Chatcontent> Parse(string val)
        {
            if (val.Contains("->"))
            {
                string[] s = val.Split(new string[] { "->" }, StringSplitOptions.None);
                int i = int.Parse(s[0]);
                int j = int.Parse(s[1]);
                return ChatHandler.messages.Where(r => r.Id > i && r.Id < j).ToList();
            }
            else
            {
                string[] s = val.Split(',');
                return ChatHandler.messages.Where(r => s.Contains(r.Id.ToString())).ToList();

            }
        }

        public ChatMessageIDs()
        {
            listparam = new ChatMessageIDList();
            rangeparam = new ChatMessageIDRange();
        }
        public override bool canbeme(string parameter)
        {
            return listparam.canbeme(parameter) || rangeparam.canbeme(parameter);
        }

        public override List<string> getpossibleparameters(string start)
        {
            return listparam.getpossibleparameters(start).Union(rangeparam.getpossibleparameters(start)).ToList();
        }

        public override bool isme(string param)
        {
            return listparam.isme(param) || rangeparam.isme(param);
        }

        public override string sringshow()
        {
            return "[ChatMessages IDs]";
        }
    }
}
