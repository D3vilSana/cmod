﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Parameters
{
    [DefaultParamForType(typeof(ChatHandler.Chatcontent))]
    public class ChatMessageID : Parameter<ChatHandler.Chatcontent>
    {
        public override ChatHandler.Chatcontent Parse(string s)
        {
            return ChatHandler.messages.Find(p => p.Id.ToString() == s);
        }
        public override bool canbeme(string parameter)
        {
            return ChatHandler.messages.Any(c => c.Id.ToString().StartsWith(parameter));
        }

        public override List<string> getpossibleparameters(string start)
        {
            return ChatHandler.messages.Select(m => m.Id.ToString()).Where(i => i.StartsWith(start)).ToList();
        }

        public override bool isme(string param)
        {
            return ChatHandler.messages.Any(c => c.Id.ToString() == param);
        }
        

        public override string sringshow()
        {
            return "[MessageId]";
        }
    }
}
