﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Parameters
{
    class ListParameter<T> : Parameter<IEnumerable<T>>
    {
        private Parameter<T> p;
        private Enumparam<T> e;

        public ListParameter(Parameter<T> p)
        {
            this.p = p;
            e = new Enumparam<T>(p);
        }

        public override bool canbeme(string parameter)
        {
            return e.canbeme(parameter);
        }

        public override List<string> getpossibleparameters(string start)
        {
            return e.getpossibleparameters(start);
        }

        public override bool isme(string param)
        {
            return e.isme(param);
        }

        public override IEnumerable<T> Parse(string param)
        {
            return e.Parse(param);
        }

        public override string sringshow()
        {
            return e.sringshow();
        }

        private class Enumparam<U> : Parameter<IEnumerable<U>>
        {
            private Parameter<U> p;
            
            public Enumparam(Parameter<U> b)
            {
                p = b;
            }

            public override bool canbeme(string parameter)
            {
                if (parameter == "")
                {
                    return true;
                }
                string[] s = parameter.Split(',');
                for (int i = 0; i < s.Length-1; i++)
                {
                    if (!p.isme(s[i]))
                    {
                        return false;
                    }
                }
                return p.canbeme(s[s.Length-2]);
            }

            public override List<string> getpossibleparameters(string start)
            {
                bool wn = true;
                if (start.EndsWith(","))
                {
                    wn = false;
                    start.Remove(start.Length - 1);
                }
                string[] a = start.Split(',');
                List<string> m = p.getpossibleparameters(a.Last());
                IEnumerable<string> r = m.Where(l => !a.Contains(l));
                if (wn)
                {
                    r = r.Select(l => "," + l);
                }
                return r.ToList();
            }

            public override bool isme(string param)
            {
                string[] s = param.Split(',');
                return s.All(u => p.isme(u));
                
            }

            public override IEnumerable<U> Parse(string param)
            {
                string[] s = param.Split(',');
                return s.Select(d => p.Parse(d));
            }

            public override string sringshow()
            {
                return p.sringshow() + ",...";
            }
        }
    }/*
    private class List_Range<T> : Parameter<IEnumerable<T>> where T : IComparable
    {
        public List_Range(Parameter<T> p)
        {
            this.p = p;
        }
        private Parameter<T> p;

        public override bool canbeme(string parameter)
        {
            if (parameter == "")
            {
                return true;
            }
            if (parameter.Contains("->"))
            {
                string[] s = parameter.Split(',');
                return p.isme(s[0]) && p.canbeme(s[1]);
            }
            else
            {
                if (parameter.EndsWith("-"))
                {
                    parameter = parameter.Remove(parameter.Length - 1);
                }
                return p.canbeme(parameter);
            }
        }

        public override List<string> getpossibleparameters(string start)
        {
            if (start == "")
            {
                return p.getpossibleparameters("");
            }
            if (start.Contains("->"))
            {
                string[] s = start.Split(',');

                T fv = p.Parse(s[0]);
                return  p.getpossibleparameters(s[1]).Where(u=>p.Parse(u).CompareTo(fv)>=1).ToList();
            }
            else
            {
                bool nc = false;
                if (start.EndsWith("-"))
                {
                    nc = true;
                    start.Remove(start.Length - 1);
                }
                List<string> v = p.getpossibleparameters(start);
                if (nc)
                {
                    return v.Select(d => ">" + d).ToList();
                }
                else
                {
                    if (p.isme(start))
                    {
                        v.Add("->");
                    }
                    return v;
                }
            }
        }

        public override bool isme(string param)
        {
            if (param.Contains("->"))
            {
                string[] s = param.Split(new string[] { "->" }, StringSplitOptions.None);
                return p.isme(s[0]) && p.isme(s[1]) && p.Parse(s[0]).CompareTo(p.Parse(s[1])) <= 0;
            }
            else
            {
                return false;
            }
        }

        public override IEnumerable<T> Parse(string param)
        {
            string[] s = param.Split(new string[] { "->" }, StringSplitOptions.None);
            T i = p.Parse(s[0]);
            T j = p.Parse(s[1]);
            return p.
        }

        public override string sringshow()
        {
            return "[Message Id range]";
        }
        private bool IsDigitsOnly(string str)
        {
            foreach (char c in str)
            {
                if (c < '0' || c > '9')
                    return false;
            }

            return true;
        }
    }*/

}
