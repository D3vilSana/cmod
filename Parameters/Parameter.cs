﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Parameters
{
    public abstract class Parameter<T> : Parameter
    {
        public abstract T Parse(string param);
    }

    public abstract class Parameter
    {
        public abstract string sringshow();
        public abstract List<string> getpossibleparameters(string start);
        public abstract bool canbeme(string parameter);
        public abstract bool isme(string param);
    }
}
