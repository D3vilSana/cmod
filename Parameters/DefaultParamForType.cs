﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Parameters
{
    /// <summary>
    /// Set this parameter as the default parameter for this type
    /// Note : You must implement an empty constructor
    /// </summary>
    [System.AttributeUsage(AttributeTargets.Class, Inherited = false, AllowMultiple = true)]
    sealed class DefaultParamForType : Attribute
    {
        public DefaultParamForType(Type t)
        {
            type = t;
        }
        public Type type
        {
            get;
            private set;
        }
    }
}
