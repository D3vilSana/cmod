﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Parameters
{
    [DefaultParamForType(typeof(Boolean))]
    public class Boolean : Parameter<bool>
    {
        public override bool Parse(string b)
        {
            return b == t;
        }
        private const string t = "true";
        private const string f = "false";
        public override bool canbeme(string parameter)
        {
            return t.StartsWith(parameter) || f.StartsWith(parameter);
        }

        public override List<string> getpossibleparameters(string start)
        {
            List<string> l = new List<string>();
            if (t.StartsWith(start))
            {
                l.Add(t);
            }
            if (f.StartsWith(start))
            {
                l.Add(f);
            }
            return l;
        }

        public override bool isme(string param)
        {
            return param == t || param == f;
        }

        public override string sringshow()
        {
            return "Boolean";
        }
    }
}
