﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Parameters
{
    [DefaultParamForType(typeof(PhotonPlayer))]
    public class PlayerID : Parameter<PhotonPlayer>
    {
        public override PhotonPlayer Parse(string param)
        {
            int i;
            if (int.TryParse(param, out i))
            {
                return PhotonPlayer.Find(i);
            }
            else
            {
                return null;
            }
        }
        public override bool canbeme(string parameter)
        {
            return PhotonNetwork.playerList.Any(p => p.ID.ToString().StartsWith(parameter));
        }

        public override List<string> getpossibleparameters(string start)
        {
            return PhotonNetwork.playerList.Select(p => p.ID.ToString()).Where(s => s.StartsWith(start)).ToList();
        }

        public override string sringshow()
        {
            return "[ID]";
        }
        public override bool isme(string param)
        {
            return PhotonNetwork.playerList.Any(p => p.ID.ToString() == param);
        }
    }
}
