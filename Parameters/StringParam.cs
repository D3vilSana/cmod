﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Parameters
{
    public class StringParam : Parameter<string>
    {
        public StringParam(string val) { this.val = val; }
        protected string val;
        public override string sringshow()
        {
            return val;
        }
        public override List<string> getpossibleparameters(string start)
        {
            if (val.StartsWith(start))
            {
                return new List<string>() { val };
            }
            else
            {
                return new List<string>();
            }
        }
        public override bool canbeme(string parameter)
        {
            return val.StartsWith(parameter);
        }
        public override bool isme(string param)
        {
            return val == param;
        }

        public override string Parse(string param)
        {
            return val;
        }
    }
}
