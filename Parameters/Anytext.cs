﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Parameters
{
    [DefaultParamForType(typeof(string))]
    public class Anytext : Parameter<string>
    {
        protected string val;
        public Anytext(string name = "text") { val = name; }
        public override bool canbeme(string parameter)
        {
            return true;
        }

        public override List<string> getpossibleparameters(string start)
        {
            return new List<string>();
        }

        public override string sringshow()
        {
            return "[" + val + "]";
        }
        public override bool isme(string param)
        {
            return true;
        }

        public override string Parse(string param)
        {
            return param;
        }
    }
}
