﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Parameters
{
    public class ChatLinkID : Parameter<int>
    {
        public ChatLinkID() { }

        public override bool canbeme(string parameter)
        {
            return ChatHandler.CText.ids.Any(i => i.ToString().StartsWith(parameter));
        }

        public override List<string> getpossibleparameters(string start)
        {
            return ChatHandler.CText.ids.Select(i => i.ToString()).Where(s => s.StartsWith(start)).ToList();
        }

        public override bool isme(string param)
        {
            int i;
            return int.TryParse(param, out i) && ChatHandler.CText.ids.Contains(i);
        }

        public override string sringshow()
        {
            return "LinkID";
        }
        public static void exec(string val)
        {
            int i;
            if (int.TryParse(val, out i))
            {
                ChatHandler.CText.Openlink(i);
            }
            else
            {
                FengGameManagerMKII.instance.chat.addLINE("ERROR : Link not found");
            }
        }

        public override int Parse(string param)
        {
            return int.Parse(param);
        }
    }
}
