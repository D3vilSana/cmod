﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Parameters
{
    [DefaultParamForType(typeof(HERO))]
    public class HEROID : Parameter<HERO>
    {
        private static IEnumerable<int> getheroes()
        {
            return FengGameManagerMKII.Heroes.Keys.Cast<int>();
        }
        public override bool canbeme(string parameter)
        {
            return getheroes().Any(t => t.ToString().StartsWith(parameter));
        }

        public override List<string> getpossibleparameters(string start)
        {
            return getheroes().Select(i => i.ToString()).Where(i => i.StartsWith(start)).ToList();
        }

        public override bool isme(string param)
        {
            return getheroes().Any(i => i.ToString() == param);
        }

        public override string sringshow()
        {
            return "HERO Id";
        }

        public override HERO Parse(string param)
        {
            int i = getheroes().First(j => j.ToString() == param);
            return FengGameManagerMKII.instance.getHero(i);
        }
    }
}
