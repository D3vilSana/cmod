﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Parameters
{
    /// <summary>
    /// Give a choice in a enum
    /// </summary>
    /// <typeparam name="T">must be an Enum</typeparam>
    class PEnum<T> : Parameter<T> 
    {
        private string[] names;
        public PEnum()
        {
            if (!typeof(T).IsEnum)
            {
                UnityEngine.Debug.LogError("Creating a TEnum param : " + typeof(T) + " is not an enum !!!");
            }
            names = Enum.GetNames(typeof(T));
        }

        public override bool canbeme(string parameter)
        {
            return names.Any(s => s.StartsWith(parameter));
            throw new NotImplementedException();
        }

        public override List<string> getpossibleparameters(string start)
        {
            return names.Where(s => s.StartsWith(start)).ToList();
        }

        public override bool isme(string param)
        {
            return names.Any(s => s == param);
        }

        public override T Parse(string param)
        {
            return (T)Enum.Parse(typeof(T), param);
        }

        public override string sringshow()
        {
            return typeof(T).ToString();
        }
    }
}
