﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Parameters
{
    [DefaultParamForType(typeof(UnityEngine.Vector3))]
    class Vector3 : Parameter<UnityEngine.Vector3>
    {

        public override bool canbeme(string parameter)
        {
            string[] s = parameter.Split(new char[] { ':' }, 2);
            if (s.Length == 1 || s[1] == "")
            {
                string[] poss = { "h", "c" };
                return poss.Any(p => p.StartsWith(parameter));
            }
            else
            {
                if (s[0] == "h")
                {
                    return FengGameManagerMKII.Heroes.Keys.Cast<int>().Any(i => i.ToString().StartsWith(s[1]));
                }
                else if (s[0] == "c")
                {
                    return true; //TODO : better check ?
                }
            }
            return false;
        }

        public override List<string> getpossibleparameters(string start)
        {
            string[] s = start.Split(new char[] { ':' },2);
            if (s.Length==1||s[1] == "")
            {
                List<string> l = new List<string>();
                if ("h:".StartsWith(start))
                {
                    l.AddRange(FengGameManagerMKII.Heroes.Keys.Cast<int>().Select(u => "hero:"+u.ToString()));
                }
                if ("c:".StartsWith(start))
                {
                    l.Add("c:");
                }
                return l;
            }
            else
            {
                if (s[0]=="h")
                {
                    return FengGameManagerMKII.Heroes.Keys.Cast<int>().Select(u => u.ToString()).Where(u => u.StartsWith(s[1])).ToList();
                }
                else if (s[0]=="c")
                {
                    List<string> l = new List<string>();
                    l.Add("x/y/z");
                    return l;
                }
            }
            return new List<string>();
        }

        public override bool isme(string param)
        {
            if (param.StartsWith("h"))
            {
                param = param.Substring(5);
                int i;
                if (int.TryParse(param,out i))
                {
                    return FengGameManagerMKII.Heroes.ContainsKey(i);

                }
            }
            else if (param.StartsWith("c:"))
            {
                param = param.Substring(6);
                string[] s = param.Split('/');
                if (s.Length==3)
                {
                    float f;
                    return s.All(y => float.TryParse(y, out f));
                }
            }
            return false;
        }

        public override UnityEngine.Vector3 Parse(string param)
        {
            if (param.StartsWith("h:"))
            {
                param = param.Substring(5);
                int i;
                if (int.TryParse(param, out i))
                {
                    if (FengGameManagerMKII.Heroes.ContainsKey(i))
                    {
                        return ((HERO)FengGameManagerMKII.Heroes[i]).transform.position;
                    }

                }
            }
            else if (param.StartsWith("c:"))
            {
                param = param.Substring(6);
                string[] s = param.Split('/');
                if (s.Length == 3)
                {
                    return new UnityEngine.Vector3(float.Parse(s[0])
                        , float.Parse(s[1])
                        , float.Parse(s[2]));
                }

            }

            return default(UnityEngine.Vector3);
        }
        public override string sringshow()
        {
            return "Vector3";
        }
    }
}
