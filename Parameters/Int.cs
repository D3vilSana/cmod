﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Parameters
{
    [DefaultParamForType(typeof(int))]
    public class Int : Parameter<int>
    {
        protected string name;
        public Int(string name = "Int") { this.name = name; }
        public override bool canbeme(string parameter)
        {
            if (parameter == "")
            {
                return true;
            }
            int i;
            return int.TryParse(parameter, out i);
        }

        public override List<string> getpossibleparameters(string start)
        {
            return new List<string>();
        }

        public override bool isme(string param)
        {
            int i;
            return int.TryParse(param, out i);
        }

        public override int Parse(string param)
        {
            return int.Parse(param);
        }

        public override string sringshow()
        {
            return "[" + name + "]";
        }
    }
}
