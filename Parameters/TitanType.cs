﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Parameters
{
    public class TitanType : Parameter
    {
        public static string[] types = { "normal", "aberant", "jumper", "crawler", "punk" };
        public override bool canbeme(string parameter)
        {
            return types.Any(s => s.StartsWith(parameter));
        }

        public override List<string> getpossibleparameters(string start)
        {
            return types.Where(s => s.StartsWith(start)).ToList();
        }

        public override bool isme(string param)
        {
            return types.Contains(param);
        }

        public override string sringshow()
        {
            return "[TitanType]";
        }
    }
}
