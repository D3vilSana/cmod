﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HumanSkill
{
    class Sacha : Skill
    {
        public override float CoolDown
        {
            get
            {
                return 20f;
            }
        }

        public override string skillId
        {
            get
            {
                return "sasha";
            }
        }

        protected override bool exec()
        {
            if (myhero.IsGrounded())
            {
                myhero.attackAnimation = "special_sasha";
                myhero.playAnimation("special_sasha");
                myhero.currentBuff = BUFF.SpeedUp;
                myhero.buffTime = 10f;
            }
            else
            {
                resetCoolDown();
            }
            return true ;
        }
    }
}
