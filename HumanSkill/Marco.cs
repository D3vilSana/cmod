﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HumanSkill
{
    class Marco : Skill
    {
        public override float CoolDown
        {
            get
            {
                return 10f;
            }
        }

        public override string skillId
        {
            get
            {
                return "marco";
            }
        }

        protected override bool exec()
        {
            if (myhero.IsGrounded())
            {
                myhero.attackAnimation = (UnityEngine.Random.Range(0, 2) != 0) ? "special_marco_1" : "special_marco_0";
                myhero.playAnimation(myhero.attackAnimation);
            }
            else
            {
                resetCoolDown();
            }
            return true;
        }
    }
}
