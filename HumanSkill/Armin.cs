﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HumanSkill
{
    class Armin : Skill
    {
        public override float CoolDown
        {
            get
            {
                return 5f;
            }
        }

        public override string skillId
        {
            get
            {
                return "armin";
            }
        }

        protected override bool exec()
        {
            if (myhero.IsGrounded())
            {
                myhero.attackAnimation = "special_armin";
                myhero.playAnimation("special_armin");
            }
            else
            {
                resetCoolDown();
            }
            return true;
        }
    }
}
