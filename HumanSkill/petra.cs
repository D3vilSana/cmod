﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace HumanSkill
{
    class petra : Skill
    {
        public override float CoolDown
        {
            get
            {
                return 3.5f;
            }
        }

        public override string skillId
        {
            get
            {
                return "petra";
            }
        }

        protected override bool exec()
        {
            RaycastHit hit2;
            myhero.attackAnimation = "special_petra";
            myhero.playAnimation("special_petra");
            Rigidbody rigidbody2 = myhero.rigidbody;
            rigidbody2.velocity += (Vector3)(Vector3.up * 5f);
            Ray ray2 = Camera.main.ScreenPointToRay(Input.mousePosition);
            LayerMask mask4 = ((int)1) << LayerMask.NameToLayer("Ground");
            LayerMask mask5 = ((int)1) << LayerMask.NameToLayer("EnemyBox");
            LayerMask mask6 = mask5 | mask4;
            if (Physics.Raycast(ray2, out hit2, 1E+07f, mask6.value))
            {
                if (myhero.bulletRight != null)
                {
                    myhero.bulletRight.GetComponent<Bullet>().disable();
                    myhero.releaseIfIHookSb();
                }
                if (myhero.bulletLeft != null)
                {
                    myhero.bulletLeft.GetComponent<Bullet>().disable();
                    myhero.releaseIfIHookSb();
                }
                myhero.dashDirection = hit2.point - myhero.transform.position;
                myhero.launchLeftRope(hit2, true, 0);
                myhero.launchRightRope(hit2, true, 0);
                myhero.rope.Play();
            }
            myhero.facingDirection = Mathf.Atan2(myhero.dashDirection.x, myhero.dashDirection.z) * 57.29578f;
            myhero.targetRotation = Quaternion.Euler(0f, myhero.facingDirection, 0f);
            myhero.attackLoop = 3;
            return true;
        }
    }
}
