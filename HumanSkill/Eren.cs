﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HumanSkill
{
    class Eren : Skill
    {
        public override float CoolDown
        {
            get
            {
                return 120f;
            }
        }

        public override string skillId
        {
            get
            {
                return "eren";
            }
        }

        protected override bool exec()
        {
           myhero.erenTransform();
            return false;
        }
    }
}
