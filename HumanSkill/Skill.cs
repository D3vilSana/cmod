﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HumanSkill
{
    public abstract class Skill
    {
        private static List<Skill> skills = CModUtilities.GetTypesof<Skill>().Select(t => (Skill)Activator.CreateInstance(t)).ToList();

        public static Skill find(string Id)
        {
            if (CustomGameMode.getbool("Bomb"))
            {
                return skills.Find(s => s.skillId == "bomb");
            }
            else
            {

                return skills.FirstOrDefault(s => s.skillId == Id);
            }
            
        }
        public static Skill find(string Id,HERO myhero)
        {
            Skill s = find(Id);
            s.myhero = myhero;
            return s;
        }

        public UnityEngine.GameObject skillIcon
        {
            get { return UnityEngine.GameObject.Find("skill_cd_" + skillId); }
        }

        protected HERO myhero { get; private set; }

        public abstract string skillId { get; }

        public abstract float CoolDown { get; }

        public static float nextskill;

        public bool cancast { get { return UnityEngine.Time.time > nextskill; } set
            {
                if (value)
                {
                    nextskill = UnityEngine.Time.time;
                }
                else
                {
                    nextskill = UnityEngine.Time.time + CoolDown;
                }
            } }

        public float percentageWaited
        {
            get
            {
                return (CoolDown -(nextskill - UnityEngine.Time.time)) / CoolDown;
            }
        }


        protected void resetCoolDown()
        {
            nextskill = UnityEngine.Time.time;
        }

        public bool execute()
        {
            float t = UnityEngine.Time.time;
            if (t>nextskill)
            {
                nextskill = t+CoolDown;
                return exec();
            }
            return true;
        }
        
        protected abstract bool exec();
    }
}
