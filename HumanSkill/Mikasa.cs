﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HumanSkill
{
    class Mikasa : Skill
    {
        public override float CoolDown
        {
            get
            {
                return 1.5f;
            }
        }

        public override string skillId
        {
            get
            {
                return "mikasa";
            }
        }

        protected override bool exec()
        {
            myhero.attackAnimation = "attack3_1";
            myhero.playAnimation("attack3_1");
            myhero.rigidbody.velocity = (UnityEngine.Vector3.up * 10f);
            return true;
        }
    }
}
