﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HumanSkill
{
    class Bomb : Skill
    {
        public static int Pcooldown = 10;
        public override float CoolDown
        {
            get
            {
                return (float)Pcooldown * -0.4f + 5f;
            }
        }

        public override string skillId
        {
            get
            {
                return "bomb";
            }
        }

        protected override bool exec()
        {

            throw new NotImplementedException();
        }
        public new UnityEngine.GameObject skillIcon
        {
            get { return UnityEngine.GameObject.Find("skill_cd_" + "armin"); }
        }
    }
}
