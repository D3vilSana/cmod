﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace HumanSkill
{
    class Levi : Skill
    {
        public override float CoolDown
        {
            get
            {
                return 3.5f;
            }
        }

        public override string skillId
        {
            get
            {
                return "levi";
            }
        }

        protected override bool exec()
        {
            RaycastHit hit;
            myhero.attackAnimation = "attack5";
            myhero.playAnimation("attack5");
            Rigidbody rigidbody = myhero.rigidbody;
            rigidbody.velocity += (Vector3)(Vector3.up * 5f);
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            LayerMask mask = ((int)1) << LayerMask.NameToLayer("Ground");
            LayerMask mask2 = ((int)1) << LayerMask.NameToLayer("EnemyBox");
            LayerMask mask3 = mask2 | mask;
            if (Physics.Raycast(ray, out hit, 1E+07f, mask3.value))
            {
                if (myhero.bulletRight != null)
                {
                    myhero.bulletRight.GetComponent<Bullet>().disable();
                    myhero.releaseIfIHookSb();
                }
                myhero.dashDirection = hit.point - myhero.transform.position;
                myhero.launchRightRope(hit, true, 1);
                myhero.rope.Play();
            }
            myhero.facingDirection = Mathf.Atan2(myhero.dashDirection.x, myhero.dashDirection.z) * 57.29578f;
            myhero.targetRotation = Quaternion.Euler(0f, myhero.facingDirection, 0f);
            myhero.attackLoop = 3;
            return true;
        }
    }
}
